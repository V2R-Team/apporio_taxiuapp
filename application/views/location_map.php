<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Apporio Taxi</title>
<!-- <meta name="viewport" content="initial-scale=1.0, user-scalable=no"> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://apporio.org/Alakowe/webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="http://apporio.org/Alakowe/css/custom1.css" type="text/css">
<link rel="stylesheet" href="http://apporio.org/Alakowe/css/bootstrap1.css" type="text/css">
<style type="text/css">
#map_canvas {
  height: 100%;
}
@media print {
  html, body {
    height: auto;
  }
 #map_canvas {
    height: 900px;
  }
}
.main_bg{background-image:url(https://images7.alphacoders.com/421/421296.jpg);}
.lft_side{ width: 36%; background-color:#efefef; height:100% !important; float:left; padding:1px 1px 1px 10px; position:absolute; margin:0px !important;     overflow-y: hidden; overflow:scroll;}
.rgt_side{ }
.clear{clear:both !important;}
@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script>
      var geocoder;
      var map;
      var marker;
      var infowindow = new google.maps.InfoWindow({size: new google.maps.Size(0,0)});
      function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
          zoom: 16,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        google.maps.event.addListener(map, 'click', function() {
          infowindow.close();
        });
      }

  function clone(obj){
      if(obj == null || typeof(obj) != 'object') return obj;
      var temp = new obj.constructor(); 
      for(var key in obj) temp[key] = clone(obj[key]);
      return temp;
  }


function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      marker.formatted_address = responses[0].formatted_address;
	   
    } else {
      marker.formatted_address = 'Cannot determine address at this location.';
    }
   // infowindow.setContent(marker.formatted_address+"<br>coordinates: "+marker.getPosition().toUrlValue(6));
    //infowindow.open(map);
	 
	document.getElementById('latlon').value = marker.getPosition();
    document.getElementById('addrress1').value = marker.formatted_address;
  });
}

      function codeAddress() {
        var address = document.getElementById('address').value;
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
	    if (marker) {
               marker.setMap(null);
               if (infowindow) infowindow.close();
            }
            marker = new google.maps.Marker({
                map: map,
		draggable: true,
                position: results[0].geometry.location
            });
            google.maps.event.addListener(marker, 'dragend', function() {
              // updateMarkerStatus('Drag ended');
              geocodePosition(marker.getPosition());
            });
            google.maps.event.addListener(marker, 'click', function() {
              if (marker.formatted_address) {
               // infowindow.setContent(marker.formatted_address+"<br>coordinates: "+marker.getPosition().toUrlValue(6));
              } else  {
             //   infowindow.setContent(address+"<br>coordinates: "+marker.getPosition().toUrlValue(6));
              }
              infowindow.open(map);
            });
            google.maps.event.trigger(marker, 'click');
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    </script>
  </head>
  <body onload="initialize(); codeAddress();">
  
  <style>
	.hello1{
		height: 44px;
		width: 95%;
		font-weight: 500;
		background-color: #fff;
		text-align:left;
		position:relative;
		top:80px;
		border-radius:5px;
		z-index:99;
		border:none;
		padding-left:15px;
		box-shadow: 0px 0px 11px rgba(0, 0, 0, 0.3);
		}
  </style>

<div class="main_dv">
 <div class="lft_side">
  <div class="tp_header" style="margin-bottom:0px">
  <div class="col-md-2 col-sm-2 col-xs-3 tp_menu" style="width:auto;"><span style="font-size:30px;cursor:pointer"><a href ="">&#8592;</a></span></div>
    <div class="col-md-8 col-sm-8 col-xs-6 tp_logo" style="margin: 17px 0px 0px 40px;">Move map to adjust location </div>
</div>
     <div style="width: 100%; text-align: center;">
	 <form action="submit.php" method="post">
          <input type="hidden" name="address" id="address" type="textbox" value="Gurugram, IN">
	  <input type="text" id="latlon" name="latlon" hidden>
	  <input type="text" class="hello1" id="addrress1" name="addrress1" value="Gurugram, IN" readonly>
     </div>
    <div id="map_canvas" style="height:70%;top:10px"></div>
	<br><br><br> 	<br><br><br> 
<button type="submit" style="height: 44px;width: 100%;font-weight: 500;background-color: #9301DD;" class="tab tab-active"><font size="4" color="#FFF">Confirm Booking</font></button>
	 </form>
<script src="https://www.google-analytics.com/urchin.js" type="text/javascript"> 
</script> 
 </div>
</div>  
  </body>
</html>
