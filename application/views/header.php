<!doctype html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta name="description" content="izycab">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $header_data['web_title'];?></title>
<link rel="stylesheet" href="<?php echo base_url();?>/css/custom.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/responsive.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>/css/font-awesome.min.css" type="text/css">
<!--<link rel="stylesheet" href="css/icomoon.css" type="text/css">-->
</head>
<body>
<div id="wrapper">
  <header class="header">
    <div id="slide-menu" class="side-navigation">
      <ul class="navbar-nav">
        <li id="close"><a href="#"><i class="fa fa-close"></i></a></li>
         <li><a href="<?php echo base_url();?>">Home</a></li>
        <!-- <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/signin">Sign in</a></li>
         <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/register">Register</a></li> -->
        <li><a href="about-us.php">About us</a></li>
        <li><a href="terms.php">Terms & Conditions</a></li>
        <li><a href="contact-us.php">Contact us</a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Language <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#"><i><img src="images/english.png"> &nbsp; </i> English</a></li>
            <li><a href="#"><i><img src="images/espanol.png"> &nbsp; </i> Español</a></li>
            <li><a href="#"><i><img src="images/Deutsch.png"> &nbsp; </i> Deutsch</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="navigation-row">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-4"><strong class="logo"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>/images/logo.png" alt=""></a> </strong></div>
          <div class="col-md-9 col-sm-6 col-xs-8">
            <div class="topbar">
              <ul class="top-listed">
                 <?php if($this->session->userdata('user') == ""){?>
			<!--  <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/contact_us">Contact us</a></li>   
              <li class="hidden-xs"> <span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_login"><button>Sign in</button></a></span></li>   <!--  <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/register">Register</a></li>-->
        
                 <?php  } else if($this->session->userdata('user') == "driver"){ ?>
                  <li class="hidden-xs">
                <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
                </li>
                <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('Home');?></a></li>
                 <li><a href="<?php echo base_url();?>index.php/Welcome/driver_profile"><?php echo $this->lang->line('dashboard');?></a></li> 
              <!--    <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/contact_us">Contact us</a></li>     -->
                  <?php } else { ?>
                  
                  <li class="hidden-xs">
                <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
                </li>
				 <li class=""><a href="<?php echo base_url();?>index.php/Booking_controller/user_profile"><?php echo $this->lang->line('dashboard');?></a></li> 
				 <li class=""><a href="<?php echo base_url();?>index.php/Booking_controller/user_allrides"><?php echo $this->lang->line('myrides');?></a></li> 
				 <li class=""> <span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city"><button><?php echo $this->lang->line('book_now');?></button></a></span></li> 
                 <!--   <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/contact_us">Contact us</a></li>  --> 
                   
                  
                   <?php } ?>
          
                 <?php if($this->session->userdata('user') == ""){?>
				  <li class="hidden-xs">
                <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
                </li>
				  <li class=""> <span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_login"><?php echo $this->lang->line('sign');?></a></span> 
				    <li><span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Welcome/driver_signup" > 
                <?php echo $this->lang->line('become_driver');?> </a> </span> 
				 <li class=""> <span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city"><button><?php echo $this->lang->line('book_now');?></button></a></span>
                  <?php } else { ?>
                <li class=""><span class="top_bcmdri_btn"><a href="<?php echo base_url();?>index.php/Welcome/logout"><button><?php echo $this->lang->line('logout');?></button></a> </span></li>
                <?php } ?>
                 </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>