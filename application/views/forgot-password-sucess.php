<?php include('header.php'); ?>
  <div class="clear"></div>
  <div class="bg-pattern"></div>
  <div class="clear"></div>
  <div id="cp-main-content">
    <section class="pb-50">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <p class="section-title1 text-center mt-40 mb-30">You have sucessfully reset your password! Use this password to login to the app or continue to your web dashboard.</p>
           
           <a class="signin_driver" href="driver-signin.php">
          <button>Driver Sign in<i class="fa fa-angle-right" aria-hidden="true"></i></button>
          </a>
          
          <a class="signin_rider" href="rider-signin.php">
          <button>Rider Sign in<i class="fa fa-angle-right" aria-hidden="true"></i></button>
          </a> 
           
        </div>
        <div class="col-md-4"></div>
      </div>
    </section>
  </div>
  <div class="clear"></div>
  <?php include('footer.php'); ?>