<!DOCTYPE html>
<html>
<title><?php echo $header_data['web_title'];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script>
    function yesnoCheck(that) {
        if (that.value == "other") {
           
            document.getElementById("ifYes").style.display = "block";
        } else {
            document.getElementById("ifYes").style.display = "none";
        }
    }
</script>

<script> 
function yesnoChe(that) {
        if (that.value == "other") {
           document.getElementById("ifYes11").style.display = "none";
        } else{       
            document.getElementById("ifYes11").style.display = "block";
        }
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>
<script>
    function initialize() {

        var input = document.getElementById('origin-input');

        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            document.getElementById('lat').value = place.geometry.location.lat();
            document.getElementById('lon').value = place.geometry.location.lng();
            //alert("This function is working!");
            //alert(place.name);
            // alert(place.address_components[0].long_name);

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    function initialize1() {

        var input = document.getElementById('destination-input');

        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            document.getElementById('lat1').value = place.geometry.location.lat();
            document.getElementById('lon1').value = place.geometry.location.lng();
            //alert("This function is working!");
            //alert(place.name);
            // alert(place.address_components[0].long_name);

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>



<style>

body {
    position: relative;
    overflow-x: hidden;
}
body,
html { height: 100%;}
.nav .open > a,
.nav .open > a:hover,
.nav .open > a:focus {background-color: transparent;}

/*-------------------------------*/
/*           Wrappers            */
/*-------------------------------*/


.clear{clear:both !important;}


</style>


<script>
function update(val){
       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Rental_controller/update_mode",
        data: "mode_id="+val,
        success: 
        function(data){
         
        }
    });
}
</script>



<script>

function update_cars(val){

       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Rental_controller/getrental_cars",
        data: "mode_id="+val,
        success: 
        function(data){
         $('#rental').html(data);
        }
    });
}


</script>


 
<script>
function update_date(val){
       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Rental_controller/update_date",
        data: "mode_id="+val,
        success: 
        function(data){
         
        }
    });
}
 </script>
 
<script>
function  update_time(val){
       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Rental_controller/update_time",
        data: "mode_id="+val,
        success: 
        function(data){
        }
    });
}
 </script>
 
 
 

<body class="main_bg">

<!-- Sidebar -->
 

<!-- Page Content -->
<div class="main_dv">
 <div class="lft_side">


     <div id="wrapper1">
         <div class="overlay1"></div>

         <!-- Sidebar -->
         <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper1" role="navigation">
             <ul class="nav sidebar-nav">

                 <li class="sidebar-brand">
                     <a href="<?php echo base_url();?>">
                         <img src="<?php echo base_url();?>images/logo.png" alt="">
                     </a>
                 </li>

                 <li>
                     <a href="<?php echo base_url();?>"><?php echo $this->lang->line('navbarhome');?></a>
                 </li>
                 <li>
                     <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city"><?php echo $this->lang->line('navbarbookride');?></a>
                 </li>
                  <?php if($this->session->userdata('user') != ""){?>
                  <li>
                   <a href="<?php echo base_url();?>index.php/Booking_controller/user_allrides"><?php echo $this->lang->line('navbaryourride');?></a>
    		  </li>
   		 <?php } ?>
                 <li>
                     <a href="<?php echo base_url();?>index.php/Booking_controller/view_ratecard"><?php echo $this->lang->line('navbarratecard');?></a>
                 </li>
                 <li>
                     <a href="<?php echo base_url();?>index.php/Welcome/contact_us" target="_blank"><?php echo $this->lang->line('navbarsupport');?></a>
                 </li>
             </ul>
             <button class="dwld_btn"><i class="fa fa-download" aria-hidden="true"></i><?php echo $this->lang->line('navbarappdown');?></button>
         </nav>
         <!-- /#sidebar-wrapper -->

         <!-- Page Content -->
         <div id="">
             <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                 <span class="hamb-top"></span>
                 <span class="hamb-middle"></span>
                 <span class="hamb-bottom"></span>
             </button>

         </div>
         <!-- /#page-content-wrapper -->

     </div>
     <!-- /#wrapper -->



<div class="tp_header">
  <div class="col-md-2 col-sm-2 col-xs-3 tp_menu"></div>
  <div class="col-md-8 col-sm-8 col-xs-6 tp_logo"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>/images/logo.png" alt=""/></a> </div>

  <?php if($this->session->userdata('user') == ""){?>
    <div class="col-md-2 col-sm-2 col-xs-3 tp_login"><a style="padding-top:18px; display: block;" href="<?php echo base_url();?>index.php/Booking_controller/search_cab_login"><?php echo $this->lang->line('rentallogin');?></a> </div>
  <?php }else{?>
    <div class="col-md-2 col-sm-2 col-xs-3 tp_login"><a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/user_profile"><img src="https://maxcdn.icons8.com/Share/icon/Users//user1600.png" height="30px" width="30px" alt=""/></a> </div>

<?php }?>
<div class="clear"></div>
</div>

              <div class="ola-booking">
                                <div class="booking-tab">
                                     <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city" class="tab"><?php echo $this->lang->line('cityheader');?></a>
    				     <a href="" class="tab tab-active"><?php echo $this->lang->line('rentalheader');?></a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
        
         <!-- <h3 align="center"> Your everyday travel partner<br><br>AC Cabs for point to point travel</br></br></h3> -->
         
      <form name="myForm" id="myForm" action="<?php echo base_url();?>index.php/Rental_controller/search_cab_city" method="post">
       <a class="bnr_a" href="<?php echo base_url();?>index.php/Rental_controller/location">
          
		  
		  <div class="bnr_input_group1">
            <span class="bnr_span1"><?php echo $this->lang->line('rentalfrom');?></span>
            <span class="bnr_input1">
				<?php  if($this->session->userdata('origin_input_rental') != "" )
                        {
                            echo $this->session->userdata('origin_input_rental');
                        }
                        else
                        {
                            echo "";
                        }
                    ?>
			</span>
        </div>
    </a>
	
	
	<div class="bnr_input_group1">
        <span class="bnr_span1"><?php echo $this->lang->line('rentalpkg');?></span>
        <span class="bnr_input1">
		<select class="bnr_select1" id="sel1" name="sel1"  onchange="yesnoChe(this);update_cars(this.value);">
            <?php foreach($get_package_rental as $package) { ?>
				<option value="<?= $package['rental_category_id'] ?>"><?= $package['rental_category']?></option>
			<?php } ?>	
        </select>
		</span>
		
    </div>
	
	
	
	
 

	
    <div id="ifYes11" style="display: none;">
    
     <div class="bnr_input_group1" id="ifYes11">
        <span  class="bnr_span1"><?php echo $this->lang->line('rentalwhen');?></span>
        <span class="bnr_input1">
			<select class="bnr_select1" id="sel1" name="sel1" onchange="yesnoCheck(this);update(this.value);">
				<option value="now" <?php if ( $this->session->userdata('when') == 'now'){ echo "selected='selected'";}?> ><?php echo $this->lang->line('now');?></option>
				<option value="other" <?php if ( $this->session->userdata('when') == 'other'){ echo "selected='selected'";}?> select ><?php echo $this->lang->line('schlater');?></option>
			</select>
		</span>
    </div>
    
  

 <?php /*if ( $this->session->userdata('rentalwhen') == 'now' ) { ?>
  <div id="ifYes" style="display: none;" >  
 <?php } ?>
  <?php if ( $this->session->userdata('rentalwhen') == 'other' ) { ?>
 <div id="ifYes" style="display: block;" >  
  <?php } ?>
  <?php if ( $this->session->userdata('rentalwhen') == '' ) { ?>
 <div id="ifYes" style="display: none;" >  
  <?php } */?> 
  
  
  
  
 <div id="ifYes" style="display: none;">
    <div class="bnr_input_group1">   
   
  
  
      <!--<span class="bnr_span1 input-group-addon">&nbsp;&nbsp;<?php echo $this->lang->line('depart');?>&nbsp;</span>-->
     <?php 
   
   $days   = [];
$period = new DatePeriod(
    new DateTime(), // Start date of the period
    new DateInterval('P1D'), // Define the intervals as Periods of 1 Day
    15   // Apply the interval 6 times on top of the starting date
);
 
foreach ($period as $day)
{
    $days[] = $day->format('D , d M');
}
echo "<select name='date' class='bnr_select2_1' name='date' id='date' onchange='update_date(this.value);'>";
for ($i=0;$i < sizeof($days);$i++){
  
  $date = $this->session->userdata('date');  
  $selected = $days[$i] == $date  ? 'selected="selected"' : "";
        
     printf('<option value="%s"%s>%s</option>',$days[$i], $selected , $days[$i]);
}
echo "</select>";
?>

    
    <?php 
    $start=strtotime('00:00');
    $end=strtotime('23:45');
echo '<select  class="bnr_select2_2" name="time" id="time" onchange="update_time(this.value);">';
for ($halfhour=$start;$halfhour<=$end;$halfhour=$halfhour+15*60) {
  $time = $this->session->userdata('time');  
  $selected = date('g:i a',$halfhour) == $time  ? 'selected="selected"' : "";
    printf('<option value="%s"%s>%s</option>',date('g:i a',$halfhour), $selected , date('g:i a',$halfhour));
}
  echo "</select>";
?>

<div class="clear"></div>
</div>  
</div>   



<?php if(!empty($get_package_rental)) { ?>
 <div class="list-group" id="rental">
</div>
<?php } else {
if($this->session->userdata('origin_input_rental') == "" ) { ?>
 <div class="clear" ></div>
 	<hr>
 <div class="list-group"  style="border:0px solid; !important;width:100%;">
   <br><br>  
  <center> <img src="https://image.flaticon.com/icons/svg/67/67347.svg" style="padding:0px 0px 0px 0px" width="150px" height="150px" alt=""></center>
   <!-- <strong><center>Sorry!</center><br></strong>-->
   
	<br><center><strong><font size="4">Please enter location <br>for getting fare estimation.</font></strong></center>
	 <br><br>
</div>
</hr>
<?php } else { ?>
<div class="clear" ></div>
 	<hr>
 <div class="list-group"  style="border:0px solid; !important;width:100%;">
   <br><br>  
  <center> <img src="<?php echo base_url();?>/webstatic/sorry.png" style="padding:0px 0px 0px 0px" width="150px" height="150px" alt=""></center>
	<br><center><strong><font size="4">No driver available now.</font></strong></center>
	 <br><br>
	 
</div>
</hr>
<?php }}?>
                 
    </div>    
   <div id="userTable" class="list-group">
   </div>
   </div>  
    </div>
  </div>
</div>
<script>

    $(document).ready(function () {
        var trigger = $('.hamburger'),
            overlay1 = $('.overlay1'),
            isClosed = false;

        trigger.click(function () {
            hamburger_cross();
        });

        function hamburger_cross() {

            if (isClosed == true) {
                overlay1.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
            } else {
                overlay1.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
            }
        }

        $('[data-toggle="offcanvas"]').click(function () {
            $('#wrapper1').toggleClass('toggled');
        });
    });
</script>
 <div class="web_lang hidden-xs">
                  <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
</div>   
</body>
</html> 
