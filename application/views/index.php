<?php
if($this->session->flashdata('pass_success')):?>
 <script>alert("Please Check Your Registered Email Address!!");</script>
<?php endif; ?>
<?php if($this->session->flashdata('pass_fail')):?>
 <script>alert("Email Id is Not registered with Us..");</script>
<?php endif; ?>
<?php $site_lang =$this->session->userdata('site_lang'); ?>
<!doctype html>
<html>
<head>
<meta name="description" content="Scootaride">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $Get_homedata['web_title'];?></title>
<link rel="stylesheet" href="<?php echo base_url();?>css/custom.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/responsive.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/slick.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style.css" type="text/css">
<!-- Owl Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/owlcarousel/owl.theme.default.min.css">
<!-- javascript -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/jquery-1.11.3.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<?php echo base_url();?>webstatic/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url();?>webstatic/slick.js"></script>
<script src="<?php echo base_url();?>webstatic/landing-main.js"></script>
<script src="<?php echo base_url();?>webstatic/send-events.js"></script>
<script src="<?php echo base_url();?>assets/owlcarousel/owl.carousel.min.js"></script>
<style>
.owl-nav {
  display: none;
}

.product_footer_taxi {
    width: 100%;
    background-image: url(https://www.apporio.com/wp-content/uploads/2016/10/taxi_bg.jpg) !important;
    padding: 20px 0px 50px 0px;
    background-repeat: no-repeat;
    background-size: cover;
}

.get_app {
    color: #000;
    padding: 0px 0px 25px 0px;
    text-align: center;
    line-height: 1.7;
}

.apporioh2 {
    font-size: 22px !important;
    padding: 10px 0px 5px 0px !important;
    color: #000 !important;
    line-height: 1.5;
    /* text-align: left; */
}

.apporioh3 {
    font-size: 17px !important;
    padding: 10px 0px 5px 0px !important;
    text-align: left;
}

.product_app ul {
    padding: 0px !important;
    margin-top: 15px;
}

.product_app ul li {
    list-style: none;
    font-size: 16px;
    line-height: 1.8;
    padding-bottom: 10px;
}

.btn-app-download{ padding: 12px 25px 8px 65px; position: relative; border: 2px solid #000; border-radius:30px; display: inline-block; color: #000; margin: 5px;
    text-align: left; transition: 1s; background-color:inherit;}
.btn-app-download:hover{ border: 2px solid #0066a1; color: #fff; transition: 1s; background-color:#0066a1;}
.btn-app-download:hover i, .btn-app-download:hover span, .btn-app-download:hover strong{ color:#fff; transition: 1s;}    
.btn-app-download i { font-size: 35px; left: 0; line-height: 1; margin: 15px 0 0 20px; position: absolute; top: 0; color: #000;}
.btn-app-download span { font-size: 14px; color: #000; font-weight: 700; letter-spacing: 0.1px; margin-top: -3px;}
.btn-app-download strong { display: block; font-weight: 700; margin-bottom: 0px; font-size: 17px; color: #000;}

.product_admin{}
.product_admin ul{ padding:0px !important; margin-top:15px;}
.product_admin ul li{ list-style:none; font-size:16px; line-height:1.8; padding-bottom:10px;}
.product_admin h2{ font-size:22px; color:#333; padding:0px 0px 20px 0px; text-align: left; line-height:1.7;}
.app_demo{ text-align:center; color:#000; border:2px solid #000; padding:10px 20px 10px 20px; border-radius:20px; background-color:inherit; -webkit-transition:1s; font-size:16px; font-weight:600; margin-top:33px;}
.app_demo:hover{background-color:#0066a1; border:2px solid #0066a1; color:#fff; -webkit-transition:1s;}





.app_btn{ text-align:center; margin:20px auto;}
.app_btn img{ text-align:center; margin:0px auto;}
.uname_pass{ display:block; margin:0px auto; text-align:center; color:#fff; font:weight:600; font-size:16px; padding:20px 0px;}


</style>
 
<script>
    function yesnoCheck(that) {
        if (that.value == "other") {
           
            document.getElementById("ifYes").style.display = "block";
        } else {
            document.getElementById("ifYes").style.display = "none";
        }
    }
</script>

<script>
function yesnoCheckk1(that) {
        if (that.value == "other1") {
           
            document.getElementById("ifYes1").style.display = "block";
        } else {
            document.getElementById("ifYes1").style.display = "none";
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1qYGiK9hiBs6q6mkYdydTKDovdU2C-wE&libraries=places"></script>
<script>
    function initialize() {

        var input = document.getElementById('origin-input');
       var options = {
                   componentRestrictions: {country: "NL"}
                     };
        var autocomplete = new google.maps.places.Autocomplete(input,options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            document.getElementById('lat').value = place.geometry.location.lat();
            document.getElementById('lon').value = place.geometry.location.lng();
            //alert("This function is working!");
            //alert(place.name);
            // alert(place.address_components[0].long_name);

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    function initialize1() {

        var input = document.getElementById('destination-input');
       var options = {
            componentRestrictions: {country: "NL"}
        };
        var autocomplete = new google.maps.places.Autocomplete(input,options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            document.getElementById('lat1').value = place.geometry.location.lat();
            document.getElementById('lon1').value = place.geometry.location.lng();
            //alert("This function is working!");
            //alert(place.name);
            // alert(place.address_components[0].long_name);

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
function check() {
if(document.getElementById('origin-input').value != "")
    document.getElementById('destination-input').disabled = false;
else
    document.getElementById('destination-input').disabled = true;
}
</script>
<script>
$(function () {
  $('#myForm').on('keyup keypress', "input", function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });
});
</script>
<script>
    function validatelocation() {
        var full_name = document.getElementById('full_name').value;
        if(full_name == ""){ alert("Please enter full name"); return false; }
    }
</script>

</head>
<body>
<div id="wrapper">
  <header class="header">
    <div id="slide-menu" class="side-navigation">
      <ul class="navbar-nav">
        <li id="close"><a href="#"><i class="fa fa-close"></i></a></li>
         <li><a href="<?php echo base_url();?>">Home</a></li>
        <!-- <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/signin">Sign in</a></li>
         <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/register">Register</a></li> -->
        <li><a href="about-us.php">About us</a></li>
        <li><a href="terms.php">Terms & Conditions</a></li>
        <li><a href="contact-us.php">Contact us</a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Language <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#"><i><img src="images/english.png"> &nbsp; </i> English</a></li>
            <li><a href="#"><i><img src="images/espanol.png"> &nbsp; </i> Español</a></li>
            <li><a href="#"><i><img src="images/Deutsch.png"> &nbsp; </i> Deutsch</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="navigation-row">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-3"><strong class="logo"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>/images/logo.png" alt=""></a> </strong></div>
          <div class="col-md-9 col-sm-9 col-xs-9">
            <div class="topbar">
              <ul class="top-listed">
                 <?php if($this->session->userdata('user') == ""){?>
			<!--  <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/contact_us">Contact us</a></li>   
              <li class="hidden-xs"> <span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_login"><button>Sign in</button></a></span></li>   <!--  <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/register">Register</a></li>-->
        
                 <?php  } else if($this->session->userdata('user') == "driver"){ ?>
                  <li class="hidden-xs">
                <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
                </li>
                <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('Home');?></a></li>
                 <li><a href="<?php echo base_url();?>index.php/Welcome/driver_profile"><?php echo $this->lang->line('dashboard');?></a></li> 
              <!--    <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/contact_us">Contact us</a></li>     -->
                  <?php } else { ?>
                  
                  <li class="hidden-xs">
                <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
                </li>
				 <li class=""><a href="<?php echo base_url();?>index.php/Booking_controller/user_profile"><?php echo $this->lang->line('dashboard');?></a></li> 
				 <li class=""><a href="<?php echo base_url();?>index.php/Booking_controller/user_allrides"><?php echo $this->lang->line('myrides');?></a></li> 
				 <li class=""> <span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city"><button><?php echo $this->lang->line('book_now');?></button></a></span></li> 
                 <!--   <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/contact_us">Contact us</a></li>  --> 
                   
                  
                   <?php } ?>
          
                 <?php if($this->session->userdata('user') == ""){?>
				  <li class="hidden-xs">
                <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
                </li>
				  <li class=""> <span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_login"><?php echo $this->lang->line('sign');?></a></span> 
				    <li><span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Welcome/driver_signup" > 
                <?php echo $this->lang->line('become_driver');?> </a> </span> 
				 <li class=""> <span class="top_bcmdri_btn"> <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city"><button><?php echo $this->lang->line('book_now');?></button></a></span>
                  <?php } else { ?>
                <li class=""><span class="top_bcmdri_btn"><a href="<?php echo base_url();?>index.php/Welcome/logout"><button><?php echo $this->lang->line('logout');?></button></a> </span></li>
                <?php } ?>
                 </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  
   <section>
            <div class="top-banner">
                <div class="">
                  <div class="banner"  style="background-image:url(<?php echo base_url($Get_homedata['banner_img']);?>)">


                   <!--   <center class="bnr_top">
                          <h1 class="bnr_title"><?php //echo $this->lang->line('bannertxt1');?></h1>
                          <p class="bnr_desc"><?php //echo $this->lang->line('bannertxt2');?></p>
                      </center>-->
					  <center class="bnr_top">
                          <h1 class="bnr_title"><?php echo $Get_heading['heading_1']?></h1>
                          <p class="bnr_desc"><?php echo $Get_heading['heading1']?></p>
                      </center>


                        <div class="overlay"></div>
                        <div class="overlay-left"></div>
                        
                        <div class="container bnr_outer">
                            <div class="bnr_form_inner">
                                <div class="booking-tab">
                                    <div class="tab-btn-wrapper text-center">
                                      <a id="stars" class="tab tab-active"  href="#tab1" data-toggle="tab" ><font color="#FFF"><?php echo $this->lang->line('cityheader');?></font></a>
                                      <a id="favorites" class="tab" href="<?php echo base_url();?>index.php/Welcome/index1"  ><?php echo $this->lang->line('rentalheader');?></a> 
                                    </div>
                                   
                                    <div class="clearfix"></div>
                         </div>
        <script>
    function showhide()
     {
           var div = document.getElementById("newpost");
    if (div.style.display !== "none") {
        div.style.display = "none";
    var div1 = document.getElementById("from");
    div1.style.display = "block";
    }
    else {
        div.style.display = "block";
    var div1 = document.getElementById("from");
    div1.style.display = "none";
    }
     }
  </script> 
   
  <script>
    function validateForm() {
         var origin = document.getElementById('origin-input').value;
	 var destination = document.getElementById('destination-input').value;
        
        if(origin == ""){ alert("Please enter pick up location"); return false; }
	if(destination == ""){ alert("Please enter drop location"); return false; }
       
        }
</script>

  <script>
    function checktype() {
       alert('Please login as User to book/search a Ride.');
        }
</script>
  
  

    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  
  
  

      <div id="home"class="well">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
        
            <center class="bnr_msg">
                <p class="bnr_msg_p1"> <?php echo $this->lang->line('bnrsearchbox1');?></p>
                <p class="bnr_msg_p2"> <?php echo $this->lang->line('bnrsearchbox2');?></p>
            </center>
            <form name="myForm" id="myForm" onsubmit="return validateForm()" method="post" target="_blank" action="<?php echo base_url();?>index.php/Booking_controller/search_cab_city">
                
				<div class="input-group bnr_input_group1">
					<span class="bnr_span1 input-group-addon"><?php echo $this->lang->line('pickup');?></span>
					
					<span class="bnr_input1">
						<input  type="text" class="home_input1" onkeypress="initialize()" id="origin-input" name="origin-input"
                        onkeyup="check()"  placeholder="<?php echo $this->lang->line('enterpick');?>">	
					<input type="text" id="lat" name="latitude" hidden><input type="text" id="lon" name="longitude"  id="lon" hidden>
                    <input type="text" id="index" name="index" value="index" hidden>						
					</span>		
				</div>
				
				
				<div class="input-group bnr_input_group1">
					<span class="bnr_span1 input-group-addon"><?php echo $this->lang->line('drop');?></span>
					<span class="bnr_input1">
						<input  type="text" class="home_input1" id="destination-input" onkeypress="initialize1()"
                            name="destination-input"   placeholder="<?php echo $this->lang->line('enterdrop');?>">
						<input type="text" id="lat1" name="destlatitude" hidden><input type="text" id="lon1" name="destlongitude" hidden>						
					</span>		
				</div>
				
				<div class="input-group bnr_input_group1">
					<span class="bnr_span1 input-group-addon"><?php echo $this->lang->line('when');?></span>
					
					<span class="bnr_input1">
						<select class="home_input1" name="sel1" id="sel1" onchange="yesnoCheck(this);">
                            <option value="now"><?php echo $this->lang->line('now');?>&nbsp;</option>
                            <option value="other"><?php echo $this->lang->line('schlater');?></option>
                        </select>
					</span>		
				</div>
				
				

                
    
 <div id="ifYes" style="display: none;">
    <div class="bnr_input_group1"> 
      <!--<span class="bnr_span input-group-addon"><?php echo $this->lang->line('depart');?></span>-->
   <?php 
   $days   = [];
$period = new DatePeriod(
    new DateTime(), // Start date of the period
    new DateInterval('P1D'), // Define the intervals as Periods of 1 Day
    15   // Apply the interval 6 times on top of the starting date
);
 
foreach ($period as $day)
{
    $days[] = $day->format('D , d M');
}
echo "<select name='date' class='bnr_select2_1' name='date' id='date'>";
for ($i=0;$i < sizeof($days);$i++){
   echo "<option>".$days[$i]."</option>";        
}
echo "</select>";
?>
  
    
    
    
    
      <?php 
    $start=strtotime('00:00');
      $end=strtotime('23:45');
echo '<select  class="bnr_select2_2" name="time" id="time">';
for ($halfhour=$start;$halfhour<=$end;$halfhour=$halfhour+15*60) {
    printf('<option value="%s">%s</option>',date('g:i a',$halfhour),date('g:i a',$halfhour));
}
  echo "</select>";
?>
   <div class="clear"></div>
  </div>  
</div>  
 <div class="form-group">
<?php if($this->session->userdata('user') == "driver") { ?>
<button type="button" onclick="checktype();" style="height: 44px;width: 100%;font-weight: 500;background-color: #9301DD;"  class="tab tab-active"><font size="4" color="#FFF"><?php echo $this->lang->line('searchbtn');?></font></button>
<?php } else { ?>
<button type="submit" class="bnr_search" class="tab tab-active"><font size="4" color="#FFF"><?php echo $this->lang->line('searchbtn');?></font></button>
<?php } ?>

  </div> 
  
    </div>    
   </form>     
        
 
      </div>
    </div>
  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  
    <div class="clear"></div>
  
   <section>
            <div class="category-container">
                <div class="section-container ola-fleet max-width">
                  <h2 class="section-heading"><?php echo $Get_heading['heading2']?></h2>
                    <label class="section-sub-heading">
                      <?php echo $Get_heading['heading3']?>
                    </label>
                    <div class="ola-fleet-options" id="awesome-fleet">
                        <div class="slider-nav">
       <?php foreach($Get_car_type as $get_model) { ?>
                           <div class="indiv-fleet" data-tab="<?php echo $get_model['car_type_id'];?>">
                                <div class="img-holder">
                                    <img src="<?php echo base_url($get_model['car_type_image']);?>" class="ola-fleet-icon" />
                                    <img src="<?php echo base_url($get_model['car_type_image']);?>" class="ola-fleet-icon-active" />
                                </div>
                                <label>
								<?php
									if($site_lang == 'french')
															{
													 echo $get_model['car_name_arabic'];			 
														    }
									else
										{ 
											 echo $get_model['car_type_name'];
										}
		  ?></label>
                                <div class="triangle-up">
                                    <img src="<?php echo base_url();?>webstatic/img/triangle-up-active.svg" />
                                </div>
                            </div>
                            
        <?php } ?>
                        </div>
                        <button type="button" class="prev-btn-nav">Prev</button>
                        <button type="button" class="next-btn-nav">Next</button>
                    </div>
                </div>
                <!-- slick -->
                <div class="fleet-tab-content">
                    <div class="max-width fleet-tab-wrapper">
                        <div class="fleet-indiv-list slider-for">
      <?php foreach($Get_car_type as $get_model) { ?>
                            <div class="single-content" id="<?php echo $get_model['car_type_id'];?>">
                                <div class="ft-comm-content">
                                    <div class="wd-60 left-content">
                                        <img src="<?php echo base_url($get_model['cartype_big_image']);?>" class="position-1" />
                                    </div>
                                    <div class="wd-40">
                                        <div class="right-content">
                                            <h4 class="heading"><?php 	if($site_lang == 'french')
															{
													 echo $get_model['car_name_arabic'];			 
														    }
									else
										{ 
											 echo $get_model['car_type_name'];
										}?></h4>
                                            <div class="sub-title"> <?php
												                if($site_lang == 'french'){
																	echo $get_model['car_description_arabic'];
																     }
																   else{
																	echo $get_model['car_description'];
																    }
																	?> </div>  
                                            <div class="info">
											<?php
                                                               if($site_lang == 'french'){
																	echo $get_model['car_longdescription_arabic'];
																     }
																   else{
																	echo $get_model['car_longdescription'];
																    }
																	?>
                                            </div>
                                            <div class="feature-img">
                                                <div class="img-holder" data-toggle="tooltip" data-placement="top" title="Eco-Friendly Ride">
                                                    <img class="option-img" src="<?php echo base_url();?>webstatic/img/car-features/eco-friendly.svg">
                                                </div>
                                                <div class="img-holder" data-toggle="tooltip" data-placement="top" title="Value for Money">
                                                    <img class="option-img" src="<?php echo base_url();?>webstatic/img/car-features/value-money.svg">
                                                </div>
                                                <div class="img-holder" data-toggle="tooltip" data-placement="top" title="Cashless Ride">
                                                    <img class="option-img" src="<?php echo base_url();?>webstatic/img/car-features/cashless.svg">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>   
              <?php } ?>
                        </div>
                        <button type="button" class="prev-btn">Prev</button>
                        <button type="button" class="next-btn">Next</button>
                    </div>
                </div>
                <!-- End slick -->
            </div>
        </section>
    
 
  
    
  
       <section>
            <div class="section-container why-ola max-width">
               <h2 class="section-heading"><?php echo $Get_heading['heading4']?></h2>
                <label class="section-sub-heading">
                  <?php echo $Get_heading['heading5']?>
                </label>
                <div class="item-list-wrapper">
          <?php foreach($Get_pagedata as $data)
          { ?>
                    <a href="<?php echo base_url();?>index.php/Welcome/blog_page?page=<?php echo $data['page_id'];?>" target="_blank" class="indiv-item-list left col-md-6">
                        <div class="wd-30">
                            <div class="img-holder">
                                <img src="<?php echo base_url($data['image']);?>" />
                            </div>
                        </div>
                        <div class="wd-70">
                            <h4 class="heading">
                               <?php                              if($site_lang == 'french'){
																	echo $data['heading_arabic'];
																     }
																   else{
																	echo $data['heading'];
																    }
																	?>
                            </h4>	
                            <div class="info homep">
                                  <?php                     if($site_lang == 'french'){
																	echo $data['content_arabic'];
																     }
																   else{
																	echo $data['content'];
																    }?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </a>
           <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
          
        

<div class="product_footer_taxi">
<div class="container">
<div class="row get_app">
<!--<h2 class="apporioh2" style="text-align: center;"><a style="color: #0066a1;" href="https://www.apporio.com/contact-us/">GET DEMO</a> OF ALAKOWE TAXI BOOKING SCRIPT / DELIVERY APP</h2>-->
</div>
<div class="col-md-4 product_app">
<h3 class="apporioh3"><?php echo $Get_homedata['app_name'];?> App</h3>
<ul>
<li><a class="btn btn-app-download" href="<?php echo $Get_homedata['itunes_url'];?>"  rel="noopener noreferrer"><i class="fa fa-apple"></i><?php echo $this->lang->line('iphone');?><strong><?php echo $this->lang->line('iphone1');?></strong></a></li>
<li><a class="btn btn-app-download" href="<?php echo $Get_homedata['google_play_url'];?>" target="_blank" rel="noopener noreferrer"><i class="fa fa-play"></i><?php echo $this->lang->line('android');?><strong><?php echo $this->lang->line('android1');?></strong></a></li>
</ul>
</div>
<div class="col-md-4 product_admin">
<h3 class="apporioh3"><!-- Demo Admin Panel Credentials --></h3>
<ul>
<li><strong><!-- User Name:</strong> infolabs --></li>
<li><strong><!-- Password:</strong> apporio7788 --></li>
<li><!--<a style="text-decoration: none;" href="http://www.apporiotaxi.com/admin/" target="_blank" rel="noopener noreferrer"><button class="app_demo" type="button">Admin Demo </button><br>
</a>--></li>
</ul>
</div>
<div class="col-md-4"></div>
</div>
</div>
  </div>
    
  <footer class="footer">
    <section class="bottom-section">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-sm-8 col-xs-7">
            <nav class="footer-nav">
              <ul>
                <li><a href="<?php echo base_url();?>index.php/Welcome/about"><?php echo $this->lang->line('about_us');?></a></li>
                <li><a href="<?php echo base_url();?>index.php/Welcome/contact_us"><?php echo $this->lang->line('contact_us');?></a></li>
              </ul>
            </nav>
          </div>
          <div class="col-md-2 col-sm-4 col-xs-5 copyright">© <?php echo $Get_homedata['web_footer']?></div>
        </div>
      </div>
    </section>
  </footer>
</div>
<script type="text/javascript" src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
</body>
</html>