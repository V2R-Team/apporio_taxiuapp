 <!-- <section><img src="<?php echo base_url();?>images/footer.png"/></section>-->
  </div>
  <footer class="footer">
    <section class="bottom-section">
      <div class="container">
        <div class="row">
          <div class="col-md-10">
            <nav class="footer-nav">
              <ul>
               <li><a href="<?php echo base_url();?>index.php/Welcome/about"><?php echo $this->lang->line('about_us');?></a></li>
                <li><a href="<?php echo base_url();?>index.php/Welcome/contact_us"><?php echo $this->lang->line('contact_us');?></a></li>
              </ul>
            </nav>
          </div>
          <div class="col-md-2">
            <p class="copyright"> © <?php echo $header_data['web_footer'];?></p>
          </div>
        </div>
      </div>
    </section>
  </footer>
</div>
<script type="text/javascript" src="<?php echo base_url();?>/js/jquery-1.11.3.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/js/custom-script.js"></script>
</body>

</html>