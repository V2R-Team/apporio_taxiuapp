<?php $site_lang =$this->session->userdata('site_lang');
 ?>
<!DOCTYPE html>
<html>
<title><?php echo $header_data['web_title'];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap1.css" type="text/css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>


<style>


.clear{clear:both !important;}


.tab-content a{ color:#222; font-style:normal;}
.tab-content a:hover{ text-decoration:none; color:#222;}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
.modal_lft{
    position:absolute !important;
    top:35%;
    margin-left: 5%;
    margin-right: 5%;
}

</style>
<body class="main_bg">

<!-- Sidebar -->
 
<!-- Page Content -->
<div class="main_dv">
 <div class="lft_side">
 
 
 <div class="tp_header1">
  <div class="col-md-2 col-sm-2 col-xs-3 tp_menu">
      <a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city">
          <img src="http://apporio.org/Alakowe/images/back1.png" width="19">
      </a>
  </div>

 

      <div class="col-md-8 col-sm-8 col-xs-6 tp_logo_txt"><?php echo $this->lang->line('useryourprofile');?> </div>
       <div class="col-md-2 col-sm-2 col-xs-2"></div>

<div class="clear"></div>
</div>
 
 
 
          <div class="modal fade modal_lft" id="myModalcancel" role="dialog">
               <div class="modal-dialog" style="width: auto; !important">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <center>  <h4 class="modal-title"><?php echo $this->lang->line('userseeshowconfirmprofile');?></h4>  </center>
                    </div>
                    <div class="modal-footer">
                    <div class="col-md-6" style="text-align:center;border-right:1px solid grey;">
		      <span type="" class="" data-dismiss="modal" style="width: 40%; !important">Cancel</span> 
		      </div>
		     <div class="col-md-6" style="text-align:center;">  
                   <a onclick="javascript:window.location='http://apporio.org/Alakowe/index.php/Welcome/logout'"  data-dismiss="modal"  class=""   style="width: 40%; !important"> <b><?php echo $this->lang->line('userseelogoutprofile');?></b></a>	 </div>
                 
                
                    </div>
					 
                </div>
                   </div>
                </div>	
 
 
 
 
 
 
 
 
 

 <div class="clear"></div>
         
         
         <div class="usr_profile_main">
        
         <?php if($user_data['user_image'] == "")  { ?>
         		<img class="usr_profile_img" src="http://soul-fi.ipn.pt/wp-content/uploads/2014/09/user-icon-silhouette-ae9ddcaf4a156a47931d5719ecee17b9.png" style="padding:0px 0px 0px 0px" width="100px" height="100px" alt="">
         		<?php } else {  ?>
         			<img class="usr_profile_img" src="<?php echo $user_data['user_image'];?>" style="padding:0px 0px 0px 0px" width="100px" height="100px" alt="">
         		<?php } ?>         
         
        		<div class="usr_details">
        		
        			<p class="usr_name">
        			
        				<?php
                    			$user_name=$user_data['user_name'];
					if($user_name=="")
					{
						echo "------";
					}
					else
					{
					echo $user_name;
					}
                      			?>
        			
        			</p>
         
         			<p class="usr_email">
         				<?php
                    			$user_email=$user_data['user_email'];
					if($user_email=="")
					{
						echo "------";
					}
					else
					{
					echo $user_email;
					}
                      			?>
         			</p> 
         
         			<p class="usr_phone">
         				<?php
                    			$user_phone=$user_data['user_phone'];
					if($user_phone=="")
					{
						echo "------";
					}
					else
					{
					echo $user_phone;
					}
                      			?>
         			</p>
        		</div> 
         <div class="clear"></div>
            
         </div>
         
         
         <div class="clear"></div>
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         	                      
      <div class="car_type_full_dlts">
      
    <div class="car_type_dtls">
       <?php if(!empty($ride))
      {
	  
	  
	  if($type['modeid'] == 1) { ?>
	  
      <a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/view_ride?ride_id=<?php echo urlencode(base64_encode($ride['ride_id']));?>&mode=<?php echo urlencode(base64_encode(1));?>">
      
      <div class="car_type_img">
     		<img src="<?php echo base_url($ride['car_type_image']);?>" width="40px" height="40px" alt="">
     	</div>
     	
     	<div class="car_nm_desc">
     		<h3 class="ride_date_time"><?php echo $ride['ride_date'];?> ,<?php echo $ride['last_time_stamp'];?>  </h3>
     		<p class="crn_number"><?php  if($site_lang == 'french'){ 
						  echo $ride['car_name_arabic'];
						  }
						else{
							 echo $ride['car_type_name'];
						     }?> CRN <?php echo $ride['ride_id'];?> </p>
     	</div>
     	
     	<div class="car_nm_desc1">
     			<h3 class="ride_date_time"><?php echo $ride['booking_date'];?> ,<?php echo $ride['last_time_stamp'];?> </h3>
     			<p class="crn_number"><?php if($site_lang == 'french'){ 
						  echo $ride['car_name_arabic'];
						  }
						else{
							 echo $ride['car_type_name'];
						     }?> CRN <?php echo $ride['rental_booking_id'];?></p>
     			<p class="pick_drop_location">
     				<img src="http://www.thepointless.com/images/greendot.jpg" style="margin:0px 4px 0px 0px" width="10px" height="10px" alt="">
     				<?php
                    			$pickup_location=$ride['pickup_location'];
					if($pickup_location=="")
					{
						echo " ------";
					}
					else
					{
					echo $pickup_location;
					}
                      		?>
     			</p>
     			<p class="pick_drop_location">
     				<img src="http://www.thepointless.com/images/reddot.jpg" style="margn:0px 4px 0px 0px" width="10px" height="10px" alt="">
     				<?php
                    			$drop_location=$ride['drop_location'];
					if($drop_location=="")
					{
						echo " ------";
					}
					else
					{
					echo $drop_location;
					}
                      		?>
     			</p>
     		</div>
      

    
     <div class="car_type_rgt1"> <?php $ride_status=$ride['ride_status'];
  
         					 switch ($ride_status){
                                                        case "1":
                                                            echo nl2br("<font color='green'> ". $this->lang->line('New_Booking')." </font> \n ".$timestap);
                                                            break;
                                                        case "2":
                                                            echo nl2br("<font color='red'>". $this->lang->line('Cancelled_By_User')." </font> \n ".$timestap);
                                                            break;
                                                        case "3":
                                                            echo nl2br("<font color='green'>". $this->lang->line('Accepted_by_Driver')." </font>  \n ".$timestap);
                                                            break;
                                                        case "4":
							   echo nl2br("<font color='red'>". $this->lang->line('Cancelled_by_driver')." </font> \n ".$timestap);
                                                            break;
                                                        case "5":
                                                            echo nl2br("<font color='green'>". $this->lang->line('Driver_Arrived')."</font> \n ".$timestap);
                                                            break;
                                                        case "6":
                                                            echo nl2br("<font color='green'>". $this->lang->line('Trip_Started')." </font> \n ".$timestap);
                                                            break;
                                                        case "7":
                                                            echo "€"."".$ride['total_amount'];
                                                            break;
                                                        case "8":
                                                            echo nl2br("<font color='green'> ". $this->lang->line('Trip_Book_By_Admin')." <font>  \n ".$timestap);
                                                            break;
							case "9":
							 echo nl2br("<font color='red'>". $this->lang->line('Cancelled_by_driver')." </font> \n ".$timestap);
                                                            break;
                                                        case "17":
                                                            echo nl2br("<font color='red'>". $this->lang->line('Trip_Cancel_By_Admin')." </font> \n ".$timestap);
                                                            break;
                                                        default:
                                                            echo "----";
                                                    } ?> 
                                                </div>
     
     
    
     <div class="clear"></div>
     </a>
	 
	 
	 <?php } else { ?>
	 
	 
	 
	       <a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/view_ride?ride_id=<?php echo urlencode(base64_encode($ride['rental_booking_id']));?>&mode=<?php echo urlencode(base64_encode(2));?>">
	       
	       <div class="car_type_img">
     			<img src="<?php echo base_url($ride['car_type_image']);?>" width="40px" height="40px" alt="">
     		</div>
     		
     		<div class="car_nm_desc1">
     			<h3 class="ride_date_time"><?php echo $ride['booking_date'];?> ,<?php echo $ride['last_time_stamp'];?> </h3>
     			<p class="crn_number"><?php echo $ride['car_type_name'];?> CRN <?php echo $ride['rental_booking_id'];?></p>
     			<p class="pick_drop_location">
     				<img src="http://www.thepointless.com/images/greendot.jpg" style="margin:0px 4px 0px 0px" width="10px" height="10px" alt="">
     				<?php
                    			$pickup_location=$ride['pickup_location'];
					if($pickup_location=="")
					{
						echo " ------";
					}
					else
					{
					echo $pickup_location;
					}
                      		?>
     			</p>
     			<p class="pick_drop_location">
     				<img src="http://www.thepointless.com/images/reddot.jpg" style="margn:0px 4px 0px 0px" width="10px" height="10px" alt="">
     				<?php
                    			$end_location=$ride['end_location'];
					if($end_location=="")
					{
						echo " ------";
					}
					else
					{
					echo $end_location;
					}
                      		?>
     			</p>
     		</div>
	       
    
     <div class="car_type_rgt1">
      <?php $ride_status=$ride['booking_status'];

         					    switch ($ride_status){
                                                         case "10":
                                                            if( $rental_ride['booking_type'] == '2') {
                                                              echo  "<font color='green'> ". $this->lang->line('Scheduled')." - ".$rental_ride['later_date']."</font>";
                                                              }
                                                              else
                                                              {
                                                              echo ("<font color='green'>". $this->lang->line('New_Booking')." </font>\n ".$timestap);
                                                              }
                                                         break;
                                                         case "11":
                                                           echo ("<font color='green'>". $this->lang->line('Accepted_by_Driver')." </font>   ".$timestap);
                                                            break;
                                                        case "12":
                                                           echo ("<font color='green'>". $this->lang->line('Driver_Arrived')." </font>    ".$timestap);
                                                            break;
                                                        case "13":
                                                             echo ("<font color='green'>". $this->lang->line('Trip_Started')." </font>    ".$timestap);
                                                            break;
                                                       case "14":
                                                           echo nl2br("<font color='red'>". $this->lang->line('Trip_Reject_By_Driver')."  </font>    ".$timestap);
                                                            break;
                                                            case "15":
                                                            echo ("<font color='red'>". $this->lang->line('Trip_Reject_By_User')." </font>    ".$timestap);
                                                            break;
                                                       
                                                           case "16":
                                                            //echo ("<font color='green'>Trip Completed</font>   ".$timestap);
                                                             echo "<font color='green'> €"."".$ride['final_bill_amount'] ."</font>";
                                                            break;
                                                                case "17":
                                                            echo ("<font color='green'>". $this->lang->line('Trip_Cancel_By_Admin')." </font>   ".$timestap);
                                                            break;
                                                                case "18":
                                                            echo ("<font color='green'>". $this->lang->line('Trip_Cancel_By_Admin')." </font>   ".$timestap);
                                                            break;
                                                                /* case "21":
                                                            echo ("<font color='green'>Trip Completed</font>   ".$timestap);
                                                            break; */
                                                        default:
                                                            echo "----";
                                                    }
                                                    ?>
                                                    
                                                    <div class="clear"></div>
                      <?php if($ride['driver_image'] == "" )  {?>                             
                    <img class="drvr_img" src="http://soul-fi.ipn.pt/wp-content/uploads/2014/09/user-icon-silhouette-ae9ddcaf4a156a47931d5719ecee17b9.png" width="40" height="40"/>
                   <?php } else {?>
                    <img class="drvr_img" src="<?php echo base_url($ride['driver_image']);?>" width="40" height="40"/>
                   <?php }?>                                 
                                                    
                                                    </div>    
     
     
     
      
      <?php if ($ride['end_location'] != "") {?>
      <span style="padding:0px 0px 0px 60px;">DROP</span>
     <span style="padding:0px 0px 0px 35px;"><img src="http://www.thepointless.com/images/reddot.jpg" style="padding:0px 0px 0px 0px" width="10px" height="10px" alt="">&nbsp;<?php echo $ride['end_location'];?></span> 
     <?php } ?>
     <div class="clear"></div>
     </a>
	 
	 
	 <?php }?>
	 
	 
     <hr>
       <center><span style="padding:0px 0px 0px 0px;"><font color="#6495ed" size="3"><a href="<?php echo base_url();?>index.php/Booking_controller/user_allrides"><?php echo $this->lang->line('userseeallridesprofile');?></a></font></span> </center>
       <?php } else {?>
         <hr>
       <center><span style="padding:0px 0px 0px 0px;"><font color="#6495ed" size="3"><a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city?index_ch=book_ride">Y<?php echo $this->lang->line('userseenoridesprofile');?></a></font></span> </center>
        <hr>
       <?php } ?>
     </div>
    <!--  <br>
      <div class="list-group-item">
     <img  style="float:left;" src="https://www.nrel.gov/_resources/images/icon-phone.svg" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
      <i style="padding:0px 0px 0px 25px;"><font color="" size="3">Energency Contact </font></i> 
           <img  style="float:right;" src="http://icons.iconarchive.com/icons/iconsmind/outline/512/Arrow-Forward-2-icon.png" style="padding:0px 0px 0px 0px" width="12px" height="12px" alt=""> 
		   -->
      <br>
     </div>  
         <br>   
          <div class="list-group-item">
      <center><a href="" data-toggle="modal" data-target="#myModalcancel"><span style="padding:0px 0px 0px 25px;"><font color="red" size="3"><?php echo $this->lang->line('userseelogoutprofile');?></font></span> </a> <center>
     </div>

       
 
  
   
</div>  
    </div>
  </div>
  <div class="web_lang hidden-xs">
                  <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
</div>

  
</body>
</html> 
