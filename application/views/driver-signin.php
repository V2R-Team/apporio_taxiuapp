<?php if($this->session->flashdata('Wrong')):?>
 <script>alert("Wrong Email or Password!!");</script>
<?php endif; ?>
<?php include('header.php'); ?>
 
<script>
function validateForm() {
    var x = document.forms["myForm"]["email"].value;
    var y = document.forms["myForm"]["pass"].value;
    var z = document.forms["myForm"]["email1"].value;

	 
    if (x == "" && z == "" ) {
        alert("Please Enter email OR phone");
        return false;
    }
	 else if (y == "") {
        alert("Enter your Password");
        return false;
    }
}
</script>
<script> 
function setId(val)
{
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>index.php/Booking_controller/get_codecountry",
            data: "id="+val,
            success:
                function(data){
                    $('#phonecode').val(data);
                }
        });
}
	
	
</script>

<section class="mt-80 mb-30">
<div class="container">
<div class="mt-80 pb-20">
    <div class="col-md-5 pt-60">
        <h2 class="signin_heading mb-30"><?php echo $this->lang->line('driversignin');?></h2>
         <form name="myForm" action="<?php echo base_url();?>index.php/Useraccounts/driver_signin" onsubmit="return validateForm()" method="post">
         <div class="col-sm-12 form-group left_icon_input">
            <i class="" aria-hidden="true"></i>
             <select class="form-control newsignup" id="country" name="country" onchange="setId(this.value);">
             
            <?php foreach($country as $country_list){ ?>
                                    <option  value="<?php echo $country_list['id'];?>" <?php if($country_list['id'] == 99){ ?> selected <?php } ?>><?php echo $country_list['name']; ?></option>
        <?php } ?>
          </select>
        </div>
        
        
                   
          <div class="col-md-3 form-group">
              <input type="text" name="phonecode"  id="phonecode" value="+91" class="form-control newsignup" style="background-color: #fff; !important;" readonly="readonly">
        </div>
        <div class="col-md-9 form-group">
           <input type="text" width="48" height="48" class="form-control newsignup" id="email" name="email"  placeholder="<?php echo $this->lang->line('entermob');?>" >
        </div>
         <div class="col-md-12 form-group">
       <center> OR </center>
       </div>
         <div class="col-md-12 form-group">
           <input type="text" width="48" height="48" class="form-control newsignup" id="email1" name="email1"  placeholder="<?php echo $this->lang->line('enteremail');?>" >
        </div>
        
        
            <div class="col-md-12 form-group">
            <input type="password" class="form-control newsignup" id="pass" name="pass" placeholder="<?php echo $this->lang->line('password');?>" >
        </div>
        <div class="col-md-12 form-group">
            <input type="submit" class="submit_btn" width="100%" value="<?php echo $this->lang->line('next');?>">
        </div>
        <span class="col-md-12 tc_pp"><?php echo $this->lang->line('dontsignup');?> <a href="<?php echo base_url();?>index.php/Welcome/driver_signup"><?php echo $this->lang->line('signup');?></a></span>
        </form>
    </div>
    <div class="col-md-1"></div>
    
    <div class="col-md-6 driver_signin"><img src="<?php echo base_url();?>images/driver-signin.jpg"/></div>
    <div class="clear"></div>
</div> 
</div>
</section>
 

<?php include('footer.php'); ?>