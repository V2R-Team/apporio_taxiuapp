<?php $site_lang =$this->session->userdata('site_lang'); ?>
<!DOCTYPE html>
<html>
<title><?php echo $header_data['web_title'];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script>
    function yesnoCheck(that) {
        if (that.value == "other") {
            document.getElementById("ifYes").style.display = "block";
               } 
           else if(that.value == "now")  {
            document.getElementById("ifYes").style.display = "none";
            }else {
            document.getElementById("ifYes").style.display = "none";
            }
             }
</script>

<script>
$(function () {
  $('#myForm').on('keyup keypress', "input", function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });
});
</script>


 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1qYGiK9hiBs6q6mkYdydTKDovdU2C-wE&libraries=places"></script>
<script>
    function initialize() {
        var input = document.getElementById('origin-input');

        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            document.getElementById('lat').value = place.geometry.location.lat();
            document.getElementById('lon').value = place.geometry.location.lng();
            //alert("This function is working!");
            //alert(place.name);
            // alert(place.address_components[0].long_name);

        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    function initialize1() {
        var input = document.getElementById('destination-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            document.getElementById('lat1').value = place.geometry.location.lat();
            document.getElementById('lon1').value = place.geometry.location.lng();
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
function update(val){
       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Booking_controller/update_mode",
        data: "mode_id="+val,
        success: 
        function(data){
         
        }
    });
}
</script>
 
<script>
function update_date(val){
       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Booking_controller/update_date",
        data: "mode_id="+val,
        success: 
        function(data){
         
        }
    });
}
 </script>
 
<script>
function  update_time(val){
       $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Booking_controller/update_time",
        data: "mode_id="+val,
        success: 
        function(data){
        }
    });
}
 </script>
 
 
 


<style>


.rgt_side{ }



body {
    position: relative;
    overflow-x: hidden;
}
body,
html { height: 100%;}
.nav .open > a,
.nav .open > a:hover,
.nav .open > a:focus {background-color: transparent;}

/*-------------------------------*/
/*           Wrappers            */
/*-------------------------------*/


.clear{clear:both !important;}


</style>
<body class="main_bg">
<!-- Sidebar -->
<!-- Page Content -->
<div class="main_dv">
 <div class="lft_side">

     <div id="wrapper1">
         <div class="overlay1"></div>

         <!-- Sidebar -->
         <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper1" role="navigation">
             <ul class="nav sidebar-nav">

                 <li class="sidebar-brand">
                     <a href="<?php echo base_url();?>">
                         <img src="<?php echo base_url();?>images/logo.png" alt="">
                     </a>
                 </li>

                 <li>
                     <a href="<?php echo base_url();?>"><?php echo $this->lang->line('navbarhome');?></a>
                 </li>
                 <li>
                     <a href="<?php echo base_url();?>index.php/Booking_controller/search_cab_city"><?php echo $this->lang->line('navbarbookride');?></a>
                 </li>
                  <?php if($this->session->userdata('user') != ""){?>
                  <li>
                   <a href="<?php echo base_url();?>index.php/Booking_controller/user_allrides"><?php echo $this->lang->line('navbaryourride');?></a>
    		  </li>
   		 <?php } ?>
                 <li>
                     <a href="<?php echo base_url();?>index.php/Booking_controller/view_ratecard"><?php echo $this->lang->line('navbarratecard');?></a>
                 </li>
                 <li>
                     <a href="<?php echo base_url();?>index.php/Welcome/contact_us" target="_blank"><?php echo $this->lang->line('navbarsupport');?></a>
                 </li>
             </ul>
             <button class="dwld_btn"><i class="fa fa-download" aria-hidden="true"></i><?php echo $this->lang->line('navbarappdown');?></button>
         </nav>
         <!-- /#sidebar-wrapper -->

         <!-- Page Content -->
         <div id="">
             <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                 <span class="hamb-top"></span>
                 <span class="hamb-middle"></span>
                 <span class="hamb-bottom"></span>
             </button>

         </div>
         <!-- /#page-content-wrapper -->

     </div>
     <!-- /#wrapper -->



<div class="tp_header">
  <div class="col-md-2 col-sm-2 col-xs-3 tp_menu"></div>
  <div class="col-md-8 col-sm-8 col-xs-6 tp_logo"><a class="bnr_a" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>/images/logo.png" alt=""/></a> </div>

  <?php if($this->session->userdata('user') == ""){?>
    <div class="col-md-2 col-sm-2 col-xs-3 tp_login"><a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/search_cab_login"><?php echo $this->lang->line('normallogin');?></a> </div>
  <?php }else{?>
    <div class="col-md-2 col-sm-2 col-xs-3 tp_login"><a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/user_profile"><img src="https://maxcdn.icons8.com/Share/icon/Users//user1600.png" height="30px" width="30px" alt=""/></a> </div>

<?php }?>
<div class="clear"></div>
</div>
 <div class="ola-booking">
                                <div class="booking-tab">
                                       <a href="" class="tab tab-active"><?php echo $this->lang->line('cityheader');?></a>
                                       <a href="<?php echo base_url();?>index.php/Rental_controller/search_cab_rental" class="tab"><?php echo $this->lang->line('rentalheader');?></a>
                               </div>
                               <div class="clearfix"></div>
                                </div>
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
        
         <!-- <h3 align="center"> Your everyday travel partner<br><br>AC Cabs for point to point travel</br></br></h3> -->
      <form name="myForm" id="myForm" action="<?php echo base_url();?>index.php/Booking_controller/search_cab_city" method="post">



    
       <a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/location?lo=f">
        <div class="bnr_input_group1">
            <span class="bnr_span1"><?php echo $this->lang->line('normalfrom');?></span>
            <span class="bnr_input1">
                <?php  if($this->session->userdata('origin_input_city') != "" )
                     {
                         echo $this->session->userdata('origin_input_city');
                     }
                     else
                     {
                         echo "";
                     }
                ?>
            </span>
        </div>
    </a>
    
    

    <a class="bnr_a" href="<?php echo base_url();?>index.php/Booking_controller/location?lo=d">
        <div class="bnr_input_group1">
            <span class="bnr_span1"><?php echo $this->lang->line('normalto');?></span>
            <span class="bnr_input1">
                <?php  if($this->session->userdata('destination_input_city') != "" )
                     {
                         echo $this->session->userdata('destination_input_city');
                     }
                     else
                     {
                         echo "";
                     }
                ?>
            </span>
        </div>
    </a>

    <div class="bnr_input_group1" id="ifYes11">
        <span  class="bnr_span1"><?php echo $this->lang->line('normalwhen');?></span>
        <span class="bnr_input1">
		<select class="bnr_select1" id="sel1" name="sel1" onchange="yesnoCheck(this);update(this.value);">
            <option value="now" <?php if ( $this->session->userdata('when') == 'now'){ echo "selected='selected'";}?> ><?php echo $this->lang->line('now');?></option>
            <option value="other" <?php if ( $this->session->userdata('when') == 'other'){ echo "selected='selected'";}?> select ><?php echo $this->lang->line('schlater');?></option>
        </select>
		</span>
		
    </div>

 <div class="form-group">
</div>  
 <?php if ( $this->session->userdata('when') == 'now' ) { ?>
  <div id="ifYes" style="display: none;" >  
 <?php } ?>
  <?php if ( $this->session->userdata('when') == 'other' ) { ?>
 <div id="ifYes" style="display: block;" >  
  <?php } ?>
    <?php if ( $this->session->userdata('when') == '' ) { ?>
 <div id="ifYes" style="display: none;" >  
  <?php } ?>  
  
     <div class="bnr_input_group1">   
 
  
  
      <!--<span class="bnr_span1 input-group-addon"><?php echo $this->lang->line('depart');?></span>-->
     <?php 
   
   $days   = [];
$period = new DatePeriod(
    new DateTime(), // Start date of the period
    new DateInterval('P1D'), // Define the intervals as Periods of 1 Day
    15   // Apply the interval 6 times on top of the starting date
);
 
foreach ($period as $day)
{
    $days[] = $day->format('D , d M');
}
echo "<select name='date' class='bnr_select2_1' name='date' id='date' onchange='update_date(this.value);'>";
for ($i=0;$i < sizeof($days);$i++){
  
  $date = $this->session->userdata('date');  
  $selected = $days[$i] == $date  ? 'selected="selected"' : "";
        
     printf('<option value="%s"%s>%s</option>',$days[$i], $selected , $days[$i]);
}
echo "</select>";
?>
 
    

    <?php 
    $start=strtotime('00:00');
    $end=strtotime('23:45');
echo '<select  class="bnr_select2_2" name="time" id="time" onchange="update_time(this.value);">';
for ($halfhour=$start;$halfhour<=$end;$halfhour=$halfhour+15*60) {
  $time = $this->session->userdata('time');  
  $selected = date('g:i a',$halfhour) == $time  ? 'selected="selected"' : "";
    printf('<option value="%s"%s>%s</option>',date('g:i a',$halfhour), $selected , date('g:i a',$halfhour));
}
  echo "</select>";
?>
    

  </div>  </div>                           
 <!--  <div class="form-group">
<button type="submit" style="padding:20px 0px; font-size:18px; width: 100%;font-weight: 500;background-color: #0066a1; border:0px; color:#fff; " class="tab tab-active">Search Cabs</button>
  </div>  -->
    </div>  
    
    
    <div class="clear"></div>
    
	<?php if(!empty($get_city_cars)){ ?>
 <div class="">
  <p class="available_rides"><?php echo $this->lang->line('normalavaliable');?> (<?php echo count($get_city_cars);?>) </p>
  <div class="car_type_full_dlts">
  <?php foreach($get_city_cars as $value)
       { ?>
     
     <div class="car_type_dtls">
     <a class="available_ride_a" href="<?php echo base_url();?>index.php/Booking_controller/booking_ride?car_type=<?php echo $value['car_type_id'];?>&amo=<?php echo $value['total_amount'];?>" onClick="checkInputs()">
     
     	<div class="car_type_img">
     		<img src="<?php echo base_url($value['car_type_image']);?>" width="40px" height="40px" alt="">
     	</div>
     	
     	<div class="car_nm_desc">
			<?php if($site_lang == 'french'){ ?>
			<h3 class="car_type_nm"><?php echo $value['car_name_arabic'];?>  </h3>
     		<p class="car_type_desc"><?php echo $value['car_description_arabic'];?> </p>	 
														  <?php  }
									else
										{ ?>
			<h3 class="car_type_nm"><?php echo $value['car_type_name'];?>  </h3>
     		<p class="car_type_desc"><?php echo $value['car_description'];?> </p>
									 <?php	} ?>
     
     	</div>
     	<div class="car_type_rgt">
     		<strong class="car_type_price">€ <?php if($value['total_amount'] != 0){ echo $value['total_amount'];}?> </strong>
     		<i class="car_type_next_btn fa fa-angle-right" aria-hidden="true"></i>
     	</div>
     	
     	
     	<div class="clear"></div>
      	
     
     <!---->
     </a>
     
     </div>
     
     
     
     <?php } ?>
  </div>
</div>

	<?php } else {
if($this->session->userdata('origin_input_city') == "" && $this->session->userdata('destination_input_city') == "" ) { ?>
 <div class="clear" ></div>
 	<hr>
 <div class="list-group"  style="border:0px solid; !important;width:100%;">
   <br><br>  
  <center> <img src="https://image.flaticon.com/icons/svg/67/67347.svg" style="padding:0px 0px 0px 0px" width="150px" height="150px" alt=""></center>
   <!-- <strong><center>Sorry!</center><br></strong>-->
   
	<br><center><strong><font size="4">Please enter location <br>for getting fare estimation.</font></strong></center>
	 <br><br>
</div>
</hr>
<?php } else { ?>
<div class="clear" ></div>
 	<hr>
 <div class="list-group"  style="border:0px solid; !important;width:100%;">
   <br><br>  
  <center> <img src="<?php echo base_url();?>/webstatic/sorry.png" style="padding:0px 0px 0px 0px" width="150px" height="150px" alt=""></center>
   <!-- <strong><center>Sorry!</center><br></strong>-->
   
	<br><center><strong><font size="4">No driver available now.</font></strong></center>
	 <br><br>
	 
</div>
</hr>
<?php }}?>

 <div class="clear"></div>

       
  </div>
  
   
</div>  
    </div>
  </div>
</div>
 <div id="map"></div>
<script>

    $(document).ready(function () {
        var trigger = $('.hamburger'),
            overlay1 = $('.overlay1'),
            isClosed = false;

        trigger.click(function () {
            hamburger_cross();
        });

        function hamburger_cross() {

            if (isClosed == true) {
                overlay1.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
            } else {
                overlay1.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
            }
        }

        $('[data-toggle="offcanvas"]').click(function () {
            $('#wrapper1').toggleClass('toggled');
        });
    });
</script>

  <div class="web_lang hidden-xs">
                  <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
</div>
      
     
</body>
</html> 
