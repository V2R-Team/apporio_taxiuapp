<?php $site_lang =$this->session->userdata('site_lang'); ?>
<!DOCTYPE html>
<html>
<title><?php echo $header_data['web_title'];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap1.css" type="text/css">
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.main_bg{background-image:url(https://images7.alphacoders.com/421/421296.jpg);}
.lft_side1{ width:55%; background-color:#FFF; height:35% !important; float:left; padding:15px 25px 10px 20px; position:absolute; margin:0px !important;}
.lft_side{ width:36%; background-color:#efefef; height:100% !important; float:left; padding:10px 20px; position:absolute; margin:0px !important;     overflow-y: hidden; overflow:scroll;}
.rgt_side{ }

.clear{clear:both !important;}

.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: ghostwhite;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
  
    padding: 20px 8px 8px 23px;
    text-decoration: none;
    font-size: 25px;
    color: #000;
    display: block;
    transition: 0.3s;
}

.sidenav a:hover {
    color: #f1f1f1;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

.modal_lft{
    position:absolute !important;
    top:35%;
}
</style>
<style>
    .divmap{ height: 300px}
</style>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCDl845ZGSDvGV5BzufLsjRQC04JARqErg&sensor=false"></script>
<?php
$lat = $normal_ride['pickup_lat'];
$pickup_long = $normal_ride['pickup_long'];
$pickup_location = $normal_ride['pickup_location'];
$drop_lat = $normal_ride['drop_lat'];
$drop_long = $normal_ride['drop_long'];
$drop_location = $normal_ride['drop_location'];
?>
<script type="text/javascript">
    var markers = [
        {
            "title": 'Pick Up Location',
            "lat": '<?php echo $lat;?>',
            "lng": '<?php echo $pickup_long;?>',
            "description": '<?php echo $pickup_location;?>'
        }
        ,
        {
            "title": 'Drop Up Location',
            "lat": '<?php echo $drop_lat;?>',
            "lng": '<?php echo $drop_long;?>',
            "description": '<?php echo $drop_location;?>'
        }
    ];
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
        var path = new google.maps.MVCArray();
        var service = new google.maps.DirectionsService();


        var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
        for (var i = 0; i < lat_lng.length; i++) {
            if ((i + 1) < lat_lng.length) {
                var src = lat_lng[i];
                var des = lat_lng[i + 1];
                path.push(src);
                poly.setPath(path);
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }
                    }
                });
            }
        }
    }
</script>

<script>
 function getInfo(){

    $.ajax({
        type: "GET",
        url: "<?php echo base_url();?>index.php/Mail_invoice?id=<?php echo urlencode(base64_encode($normal_ride['ride_id']));?>&em=<?php echo urlencode(base64_encode($normal_ride['user_email']));?>&type=<?php echo urlencode(base64_encode(1));?>",
        
       data:{
        },
        success: 
        function(data){
       
     $('#myModal').modal({ show: 'true'})
			 
        }
    });
       
  };
  </script>
  <script>
 function getInfo1(){
 var email_id=document.getElementById("email_id").value;
 if(email_id == "")
 {
alert('Plz enter Email id First');
return false;
 }
 var ride_id=document.getElementById("ride_id").value;
 var type=document.getElementById("type").value;
  $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>index.php/Mail_invoice/new_mail",
       data: {
				email_id : email_id,
				ride_id : ride_id,
				type : type,
			 } 
			 ,
        success: 
        function(data){
           $('#myModal').modal({ show: 'true'})
        }
    });
}

</script> 
  

<body class="main_bg">

<!-- Sidebar -->
 
<!-- Page Content -->
<div class="main_dv">
 <div class="lft_side">
 
 
 
 
      <div class="modal fade modal_lft" id="myModal" role="dialog">
            <div class="modal-dialog" style="width: auto; !important">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <center>  <h4 class="modal-title"><?php echo $this->lang->line('mailsuccess');?></h4>  </center>
                    </div>
                    <div class="modal-footer">
                      <center> <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('mailclose');?></button></center>
                    </div>
                </div>
                   </div>
                </div>

	 <div class="modal fade modal_lft" id="myModal1" role="dialog">
            <div class="modal-dialog" style="width: auto; !important">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <center>  <h4 class="modal-title"><?php echo $this->lang->line('mailinvoice');?></h4>  </center>
                       <div class="input-group" style="width: 100%; !important">
                        <input  type="text" name="email_id" id="email_id" class="form-control newsignup" style="background-color:#FFF; width: 100%; !important;" placeholder="<?php echo $this->lang->line('mailinvoice1');?>" required>
						<input  type="hidden" name="ride_id" id="ride_id" value="<?php echo $normal_ride['ride_id'];?>">
						<input  type="hidden" name="type" id="type" value="1">
					 </div>
                    </div>
                    <div class="modal-footer">
                     <center> <button onclick='getInfo1();' type="submit" class="btn btn-default" data-dismiss="modal" style="width: 100%; !important"><?php echo $this->lang->line('sendinvoice');?></button></center>
                    </div>
                </div>
                   </div>
                </div>		


  <div class="modal fade modal_lft" id="myModalcancel" role="dialog">
            <div class="modal-dialog" style="width: auto; !important">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <center>  <h4 class="modal-title"><?php echo $this->lang->line('selectcancel');?></h4>  </center>
						<form name="myForm" action="<?php echo base_url();?>index.php/Booking_controller/cancel_booking1" method="post">
                       <div class="input-group" style="width: 100%; !important">
						 <select class="form-control newsignup" name="reason_id" id="reason_id"  style="background-color:#FFF; width: 100%; !important;" required >
						   <?php foreach($cancel_reason  as $reason ):?>
                         <option value="<?= $reason['reason_id'] ?>"><?= $reason['reason_name'] ?></option>
                        <?php endforeach; ?>
					</select> 
						<input  type="hidden" name="ride_id" id="ride_id" value="<?php echo $normal_ride['ride_id'];?>">
					 </div>
                    </div>
                    <div class="modal-footer">
			<center>     <button  type="" class="btn btn-default" data-dismiss="modal" style="width: 40%; !important"><?php echo $this->lang->line('mailclose');?></button> 
                 <button  type="submit" class="btn btn-default"  style="width: 40%; !important"><?php echo $this->lang->line('cancelride');?></button>	</center> 
                    </div>
					</form>
                </div>
                   </div>
                </div>	
 
 
   
    <div class="modal fade" id="myModal21" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('callbelow');?></h4>
        </div>
        <div class="modal-body">
          <p><?php echo $normal_ride['driver_phone'];?></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('mailclose');?></button>
        </div>
      </div>
      
    </div>
  </div>
  
  
   
<div class="col-md-3 col-sm-6 col-xs-4"><span style="font-size:20px;cursor:pointer"><a href ="<?php echo base_url();?>index.php/Booking_controller/user_profile">&#8592;</a></span></div>
<div class="col-md-6 col-sm-6 col-xs-8" style="padding: 0px 0px 0px 85px; !important;"><span> 
           CRN <?php echo $normal_ride['ride_id'];?>  
        </span></div>
         <div class="ola-booking" style="margin-top:0; !important;">
          <br></br>                       
      <div class="tab-content">
	 <div class="list-group-item" style="text-align:center">
     
     <span> <?php $ride_status=$normal_ride['ride_status'];
                                                         switch ($ride_status){
                                                         case "1":
                                                            if( $normal_ride['ride_type'] == '2') {
                                                              echo  "<font color='green'>". $this->lang->line('Scheduled')." - ".$normal_ride['later_date']."</font>";
                                                              }
                                                              else
                                                              {
                                                              echo ("<font color='green'>". $this->lang->line('New_Booking')." </font>\n ".$timestap);
                                                              }
                                                            break;
                                                        case "2":
                                                            echo ("<font color='red'>". $this->lang->line('Cancelled_By_User')." </font>   ".$timestap);
                                                            break;
                                                        case "3":
                                                            echo ("<font color='green'>". $this->lang->line('Accepted_by_Driver')." </font>   ".$timestap);
                                                            break;
                                                        case "4":
                                                            echo nl2br("<font color='red'>". $this->lang->line('Cancelled_by_driver')." </font>    ".$timestap);
                                                            break;
                                                        case "5":
                                                            echo ("<font color='green'>". $this->lang->line('Driver_Arrived')." </font>    ".$timestap);
                                                            break;
                                                        case "6":
                                                            echo ("<font color='green'>". $this->lang->line('Trip_Started')." </font>    ".$timestap);
                                                            break;
                                                        case "7":
                                                            echo ("<font color='green'>". $this->lang->line('Trip_Completed')."  </font>   ".$timestap);
                                                            break;
                                                        case "8":
                                                            echo ("<font color='green'>". $this->lang->line('Trip_Book_By_Admin')." </font>    ".$timestap);
                                                            break;
														case "9":
                                                            echo nl2br("<font color='red'>". $this->lang->line('Cancelled_by_driver')." </font>    ".$timestap);
                                                            break;
                                                        case "17":
                                                            echo ("<font color='red'>". $this->lang->line('Trip_Cancel_By_Admin')." </font>    ".$timestap);
                                                            break;
                                                        default:
                                                            echo "----";
                                                    }
                                                    ?> - <?php echo $normal_ride['last_time_stamp']; ?> </span>
  
     </div>
         <br>     
 <div class="list-group">
  <div class="panel panel-default">
	<div id="dvMap" class="divmap col-md-12"></div>
	<div class="clear"></div>
  </div>
</div>
 <div class="list-group" style="margin-top:30px;">
  <div class="panel panel-default">
  <div class="clear"></div>
  
  
     <div class="list-group-item">
     <div class="lst_lft" style="width:13%; float:left;">
     <img src="<?php echo base_url($normal_ride['car_type_image']);?>" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
	 </div>
	 <div class="lst_lft" style="width:87%; float:left;">
     <?php if($normal_ride['ride_type'] == '1') { ?> 
      <strong style="padding:0px 0px 0px 0px"><?php  if($site_lang == 'french'){ 
		                                                echo $normal_ride['car_name_arabic'];
															}
															else{
												echo $normal_ride['car_type_name'];
											}?>  </strong>
     <?php } elseif($normal_ride['car_model_name'] == "") { ?>
      <strong style="padding:0px 0px 0px 0px"><?php if($site_lang == 'french'){ 
		                                                echo $normal_ride['car_name_arabic'];
															}
															else{
												echo $normal_ride['car_type_name'];
											}?>  </strong>
     <?php } else {?>
          <strong style="padding:0px 0px 0px 0px"><?php echo $normal_ride['car_model_name'];?>  </strong>
        <?php } ?>
	  <div class="clear"></div>
        <br>
   
      <?php if($normal_ride['ride_type'] == '1') { ?>
 
        <strong style="padding:0px 0px 0px 0px"><?php //echo $normal_ride['car_type_name'];?> </strong>
    
      <?php } else { ?>
	    
	    <?php if($ride_status ==1 ) { ?>
         <strong style="padding:0px 0px 0px 0px"><?php echo $this->lang->line('later_ride');?> </strong>
		<?php }else { ?>
		  <strong style="padding:0px 0px 0px 0px"><?php //echo $normal_ride['car_type_name'];?> </strong>
		
	  <?php } } ?>
     </div>
	    <div class="clear"></div>
     </div>
	 
	 
     <?php if($normal_ride['driver_name'] != "") {?> 
      <?php if($normal_ride['ride_type'] != '1' || $normal_ride['ride_status'] == '3' || $normal_ride['ride_status'] == '5' || $normal_ride['ride_status'] == '6' || $normal_ride['ride_status'] == '8' || $normal_ride['ride_status'] == '7'  ) { ?> 
     <div class="list-group-item">
     <?php if( $normal_ride['driver_image'] != "") { ?>
	  <div class="lst_lft" style="width:13%; float:left;">
     <img src="<?php echo base_url($normal_ride['driver_image']);?>" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
	 </div>
     <?php } else { ?>
	  <div class="lst_lft" style="width:13%; float:left;">
      <img src="http://soul-fi.ipn.pt/wp-content/uploads/2014/09/user-icon-silhouette-ae9ddcaf4a156a47931d5719ecee17b9.png" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
	  </div>
     <?php }?>
	 	  <div class="lst_lft" style="width:87%; float:left;">
     <strong style="padding:0px 0px 0px 0px"><?php echo $normal_ride['driver_name'];?>  </strong>
    </div>
	   <div class="clear"></div>
     </div>
      <?php } } ?>
	  
	  
     <?php if($normal_ride['total_amount'] != 0 ) { ?>
   <div class="list-group-item">
   	  <div class="lst_lft" style="width:13%; float:left;">
      <img src="<?php echo base_url();?>webstatic/speed-meter.png" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
      </div>
	  <div class="lst_lft" style="width:87%; float:left;">
     <?php echo "€"." ".$normal_ride['total_amount'];?>
	  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
     <?php echo $normal_ride['distance'];?>
	   &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
	 <?php 
		$checkTime = strtotime($normal_ride['arrived_time']); 
		//echo date('H:i:s', $checkTime);
		$loginTime = strtotime($normal_ride['end_time']);
		$diff = $checkTime - $loginTime;
		
	//echo round(abs($diff)/60)." min";
	echo round(abs($time)/60)." min"; ?>
	<strong style="float:right;"></strong>
       </div>
	   <div class="clear"></div>
    </div>
     <?php } ?>
     
	 
	 
      <div class="list-group-item">
	  	  <div class="lst_lft" style="width:13%; float:left;">
     <span style="float:left;"><?php echo $this->lang->line('pickup');?></span>
	    </div>
      <div class="lst_lft" style="width:87%; float:left;">
     <span style="padding:0px 0px 0px 0px;"><img src="http://www.thepointless.com/images/greendot.jpg" style="padding:0px 0px 0px 0px" width="10px" height="10px" alt=""><?php echo $normal_ride['pickup_location'];?> </span>
     </div>
	 	<div class="clear"></div>
	 <br>
     	  <div class="lst_lft" style="width:13%; float:left;">     
      <span style="float:left;"><?php echo $this->lang->line('drop');?></span>
	     </div>
	    <div class="lst_lft" style="width:87%; float:left;">
     <span style="padding:0px 0px 0px 0px;"><img src="http://www.thepointless.com/images/reddot.jpg" style="padding:0px 0px 0px 0px" width="10px" height="10px" alt=""><?php  echo $normal_ride['drop_location'];?> </span>
      	  </div>
	<div class="clear"></div>
     </div>
	 
	 
	 
	 
	 
    <?php if($normal_ride['driver_name'] != "") {?>
    <?php  if($normal_ride['ride_status'] == '3') { ?>   
     <a data-target="#myModal21" data-toggle="modal" id="myModal21"  class="list-group-item">
	 <div class="lst_lft" style="width:13%; float:left;">
     <img src="https://cdn4.iconfinder.com/data/icons/rcons-phone/16/handset_round-2-256.png" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
	 </div>
	 <div class="lst_lft" style="width:87%; float:left;">
     <strong style="padding:0px 0px 0px 0px">  <?php echo $this->lang->line('calldriver');?>  </strong>
     </div>
	 <div class="clear"></div> 
     </a>
     <?php } } ?>
	 
	 
	 
	 
	   <?php if($normal_ride['ride_status'] == 7 ) { 
	   if($normal_ride['user_email'] != "")
	   {                ?> 
	 <a href="#" onclick='getInfo();' class="list-group-item">
	  <div class="lst_lft" style="width:13%; float:left;">
     <img src="http://www.iconsfind.com/wp-content/uploads/2017/01/20170117_587d98a7d9db8.png" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
	 </div>
	 <div class="lst_lft" style="width:87%; float:left;">
     <strong style="padding:0px 0px 0px 0px"> <font color=""> <?php echo $this->lang->line('mailinvoice');?>  </font></strong>
	   	  </div>
	<div class="clear"></div>
     </a>
	   <?php } else {  ?>
         
		   <a href="#"  data-toggle="modal" data-target="#myModal1" class="list-group-item">
		     <div class="lst_lft" style="width:13%; float:left;">
         <img src="http://www.iconsfind.com/wp-content/uploads/2017/01/20170117_587d98a7d9db8.png" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
		  </div>
	 <div class="lst_lft" style="width:87%; float:left;">
        <strong style="padding:0px 0px 0px 0px"> <font color=""> <?php echo $this->lang->line('mailinvoice');?> </font></strong>
		 	  </div>
	<div class="clear"></div>
         </a>
	   <?php } } ?>
	   
	   
	   
	 
     <a href="<?php echo base_url();?>index.php/Welcome/contact_us" target="_blank" class="list-group-item">
	   <div class="lst_lft" style="width:13%; float:left;">
     <img src="https://maxcdn.icons8.com/Share/icon/dotty/Business//online_support1600.png" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
	 </div>
	 	 <div class="lst_lft" style="width:87%; float:left;">
     <strong style="padding:0px 0px 0px 0px">  <?php echo $this->lang->line('suppourt_ride');?>  </strong>
	   </div>
	<div class="clear"></div>
     </a>
	 
	 
    <?php if($normal_ride['ride_status'] == '1' || $normal_ride['ride_status'] == '3' ) { ?> 
 
   <a href="" onClick="checkInputs()" data-toggle="modal" data-target="#myModalcancel" class="list-group-item"  class="list-group-item">
   	   <div class="lst_lft" style="width:13%; float:left;">
    <img src="https://maxcdn.icons8.com/Share/icon/Very_Basic/cancel1600.png" style="padding:0px 0px 0px 0px" width="35px" height="35px" alt="">
	</div>
  <div class="lst_lft" style="width:87%; float:left;">
    <strong style="padding:0px 0px 0px 0px"> <font color="red"> <?php echo $this->lang->line('cancelride');?> </font></strong>
	</div>
	<div class="clear"></div>
    </a>
     <?php } ?>
     
      
  </div>
</div>

</div>
       
  </div>
  
   </form> 
</div>  
    </div>
  </div>
</div>
  <div class="web_lang hidden-xs">
                  <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
</div>

</body>
</html> 
