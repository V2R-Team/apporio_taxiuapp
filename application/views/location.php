<!DOCTYPE html>
<html>
<title><?php echo $header_data['web_title'];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>
<script>
   
       function initialize() {
        var input = document.getElementById('origin-input');
          var options = {
            componentRestrictions: {country: "NL"}
        };
        var autocomplete = new google.maps.places.Autocomplete(input,options );
		 autocomplete.addListener('place_changed', function () {
		$('#myForm').submit();		 
        });
       }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
   
       function initialize1() {
        var input = document.getElementById('destination-input');
         var options = {
            componentRestrictions: {country: "NL"}
        };
        var autocomplete = new google.maps.places.Autocomplete(input,options );
		 autocomplete.addListener('place_changed', function () {
		$('#myForm1').submit();		 
        });
       }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<style>



.clear{clear:both !important;}

.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #FFFFFF;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
  
    padding: 20px 8px 8px 23px;
    text-decoration: none;
    font-size: 25px;
    color: #000;
    display: block;
    transition: 0.3s;
}

.sidenav a:hover {
    color: #f1f1f1;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

</style>

<body class="main_bg">
<!-- Sidebar -->
<!-- Page Content -->
<div class="main_dv">
 <div class="lft_side">
 
 <div class="tp_header1">
  <div class="col-md-1 col-sm-1 col-xs-1 tp_menu">
      <a class="bnr_a" href ="<?php echo base_url();?>index.php/Booking_controller/search_cab_city?index_ch=book_ride">
          <img src="http://apporio.org/Alakowe/images/back1.png" width="19"/>
      </a>
  </div>

 

  <?php if($this->input->get('lo') == 'f'){ ?>
    <div class="col-md-10 col-sm-10 col-xs-10 tp_logo_txt"><?php echo $this->lang->line('normallocfrom');?> </div>
  <?php }else{?>
    <div class="col-md-10 col-sm-10 col-xs-10 tp_logo_txt"><?php echo $this->lang->line('normallocto');?></div>

<?php }?>
     <div class="col-md-1 col-sm-1 col-xs-1"></div>

<div class="clear"></div>
</div>

 
  <div class="ola-booking" style="margin-top:0; !important;"><br></br>
    <div class="tab-content">
    <?php if($this->input->get('lo') == 'f'){ ?>
      	<div class="tab-pane fade in active" id="tab1">
        <form name="myForm" id="myForm" action="<?php echo base_url();?>index.php/Booking_controller/getpicklocation" method="post">
          <div class="form-group">
          
    <i class="fa fa-search bnr_input_fa" aria-hidden="true"></i>
  <input  type="text" class="bnr_input1 bnr_fa_input" required onkeypress="initialize()"  id="origin-input" name="origin-input"  placeholder="<?php echo $this->lang->line('normallocfrom');?>"  autocomplete="" autofocus> 
    </div> 
  </div>  
  <?php } else { ?>
   <div class="tab-pane fade in active" id="tab1">
    <form id="myForm1" name="myForm1" action="<?php echo base_url();?>index.php/Booking_controller/getdroplocation" method="post">
        <div class="form-group">
			<i class="fa fa-search bnr_input_fa" aria-hidden="true"></i>
			<input  type="text" class="bnr_input1 bnr_fa_input" required onkeypress="initialize1()"  id="destination-input" name="destination-input"  placeholder="<?php echo $this->lang->line('normallocto');?>"  autocomplete="off" autofocus > 
		</div> 
  </div>  
  <?php } ?>
  </div>                             
   </div>   
 <div class="clear"></div>
 </div>
</div>  
<div class="web_lang">
                  <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
</div>
</body>
</html> 
