<?php   //echo "<pre>";print_r($rides);die();
foreach($profile as $profile){ 
                              $driver_id=$profile['driver_id'];
                              $driver_name= $profile['driver_name'];
                              $data=(explode(" ",$driver_name));
                              $first_name=$data['0'];
                              $last_name=$data['1'];
                               $driver_image= $profile['driver_image'];
                              $driver_email= $profile['driver_email'];
                              $driver_mobile= $profile['driver_phone'];
                              $car_model_name= $profile['car_model_name'];
                              $car_number= $profile['car_number'];
                              $status= $profile['status']; 
							  $login_logout= $profile['login_logout'];
                               $driver_image= $profile['driver_image'];
                              } 
                              ?>
<!doctype html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">

<meta name="description" content="Scootaride">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>TaxiU || Website</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/responsive.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css">
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>css/material.min.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-material-datetimepicker.css"/>
<script type="text/javascript" src="<?php echo base_url();?>js/material.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap-material-datetimepicker.js"></script>


</head>
<body>
<div id="wrapper">
  <header class="header">
  
   <div id="slide-menu" class="side-navigation">
      <ul class="navbar-nav">
        <li id="close"><a href="#"><i class="fa fa-close"></i></a></li>
        <li><a href="<?php echo base_url();?>">Home</a></li>        
          <li><a href="signin.php">Sign in</a></li>
          <li><a href="register.php">Register</a></li>
          <li><a href="about-us.php">About us</a></li>
          <li><a href="terms.php">Terms & Conditions</a></li>
          <li><a href="contact-us.php">Contact us</a></li>
          
       <!--   <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Language <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu" role="menu">
              <li><a href="#"><i><img src="images/english.png"> &nbsp; </i> English</a></li>
              <li><a href="#"><i><img src="images/espanol.png"> &nbsp; </i> Español</a></li>
            <li><a href="#"><i><img src="images/Deutsch.png"> &nbsp; </i> Deutsch</a></li>
          </ul>
        </li>-->
          
       
      </ul>
    </div>
	
    <div class="navigation-row">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-4"><strong class="logo"> <a href="<?php echo base_url();?>"> <img src="<?php echo base_url();?>/images/logo.png" alt=""> </a> </strong></div>
          <div class="col-md-9 col-sm-6 col-xs-8 login_header_profile">
            <div class="topbar">
              <ul class="top-listed">
                <li class="hidden-xs">
                      <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
                </li>
                <li>
                  <div class="dropdown" style="float:right;"> 
                       <?php if ($driver_image == "") { ?>
                      <img alt="" src="<?php echo base_url();?>/images/profile.png" class="img-circle header_profile_img">
                       <?php } else{?>
                       <img src="<?php echo base_url($driver_image); ?>" class="img-circle header_profile_img">
                        <?php }?>
                      <span class="profile_name"><?php echo $driver_name?> </span> <span class="caret"></span>
                         <div class="dropdown-content">
                          <a href="<?php echo base_url();?>index.php/Welcome/driver_profile"><?php echo $this->lang->line('drprofile');?><i class="fa fa-user"></i></a>
                          <a href="<?php echo base_url();?>index.php/Welcome/driver_rides"><?php echo $this->lang->line('drmyrides');?><i class="fa fa-car"></i></a>
                          <!--<a href="driver-earning.php">Earnings<i class="fa fa-usd"></i></a> -->
                          <a href="<?php echo base_url();?>index.php/Welcome/driver_documents"><?php echo $this->lang->line('drdocument');?><i class="fa fa-file-text"></i></a>
                          <a href="<?php echo base_url();?>index.php/Welcome/driver_password"><?php echo $this->lang->line('drchpass');?><i class="fa fa-lock"></i></a>
                          <a href="<?php echo base_url();?>index.php/Welcome/driver_suppourt"><?php echo $this->lang->line('drsupport');?><i class="fa fa-life-ring"></i></a>
                           <a href="<?php echo base_url();?>index.php/Welcome/logout"><?php echo $this->lang->line('drlogout');?><i class="fa fa-sign-out"></i></a>
                      </div>
					 
                  </div>
                </li>
				 
				<li id="push" class="sidemenu visible-xs"><a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>