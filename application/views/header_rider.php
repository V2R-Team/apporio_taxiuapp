<!doctype html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
<meta name="description" content="Scootaride">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Apporio Taxi</title>
<link rel="stylesheet" href="<?php echo base_url();?>/css/custom.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/responsive.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css">
</head>
<body>
<div id="wrapper">
  <header class="header">
  <div id="slide-menu" class="side-navigation">
      <ul class="navbar-nav">
        <li id="close"><a href="#"><i class="fa fa-close"></i></a></li>
        <li><a href="<?php echo base_url();?>">Home</a></li>        
          <li><a href="signin.php">Sign in</a></li>
          <li><a href="register.php">Register</a></li>
          <li><a href="about-us.php">About us</a></li>
          <li><a href="terms.php">Terms & Conditions</a></li>
          <li><a href="contact-us.php">Contact us</a></li>
         <!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Language <i class="fa fa-angle-down"></i></a>
          <ul class="dropdown-menu" role="menu">
              <li><a href="#"><i><img src="images/english.png"> &nbsp; </i> English</a></li>
              <li><a href="#"><i><img src="images/espanol.png"> &nbsp; </i> Español</a></li>
            <li><a href="#"><i><img src="images/Deutsch.png"> &nbsp; </i> Deutsch</a></li>
          </ul>
        </li>-->
          
       
      </ul>
    </div>
	
    <div class="navigation-row">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-4"><strong class="logo"> <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/logo.png" alt=""></a> </strong></div>
          <div class="col-md-9 col-sm-8 col-xs-8 login_header_profile">
            <div class="topbar">
              <ul class="top-listed">
               
                <li>
                  <div class="dropdown" style="float:right;"> 
                       <?php if ($user_image == "") { ?>
                      <img alt="" src="<?php echo base_url();?>/images/profile.png" class="img-circle header_profile_img">
                       <?php } else{?>
                       <img src="<?php echo base_url($user_image); ?>" class="img-circle header_profile_img">
                        <?php }?>
                      <span class="profile_name"><?php echo $user_name; ?> </span> <span class="caret"></span>
                        <div class="dropdown-content">
                         
              
                            <a href="<?php echo base_url();?>index.php/Welcome/rider_dashboard">My Dashboard<i class="fa fa-tachometer"></i></a>
                           <a href="<?php echo base_url();?>index.php/Welcome/rider_trips">My Trips<i class="fa fa-car"></i></a>
                            <a href="<?php echo base_url();?>index.php/Welcome/rider_later">Book Later<i class="fa fa-car"></i></a> 
                            <a href="<?php echo base_url();?>index.php/Welcome/rider_profile">Profile<i class="fa fa-user"></i></a>
                            <a href="<?php echo base_url();?>index.php/Welcome/rider_change_password">Change Password<i class="fa fa-lock"></i></a>
                           <!-- <a href="rider-payment.php">Payment<i class="fa fa-money"></i></a>
                            <a href="rider-coupons.php">Coupons Applied<i class="fa fa-tags"></i></a>-->
                            <a href="<?php echo base_url();?>index.php/Welcome/logout">log out<i class="fa fa-sign-out"></i></a>
                      </div>
                  </div>
                </li>
				<li id="push" class="sidemenu visible-xs"><a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
                  
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>