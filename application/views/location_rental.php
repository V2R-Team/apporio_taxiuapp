<!DOCTYPE html>
<html>
<title><?php echo $header_data['web_title'];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap1.css" type="text/css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>
<script>
   
       function initialize() {
        var input = document.getElementById('origin-input');
         var options = {
            componentRestrictions: {country: "NL"}
        };
        var autocomplete = new google.maps.places.Autocomplete(input,options);
		 autocomplete.addListener('place_changed', function () {
		$('#myForm').submit();		 
        });
       }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
   
       function initialize1() {
        var input = document.getElementById('destination-input');
         var options = {
            componentRestrictions: {country: "NL"}
        };
        var autocomplete = new google.maps.places.Autocomplete(input,options );
		 autocomplete.addListener('place_changed', function () {
		$('#myForm1').submit();		 
        });
       }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<style>



.clear{clear:both !important;}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

</style>

<body class="main_bg">
<!-- Sidebar -->
<!-- Page Content -->
<div class="main_dv">
 <div class="lft_side">
 
 <div class="tp_header1">
  <div class="col-md-2 col-sm-2 col-xs-3 tp_menu">
      <a class="bnr_a" href ="<?php echo base_url();?>index.php/Rental_controller/search_cab_rental?index_ch=book_ride">
          <img src="http://apporio.org/Alakowe/images/back1.png" width="19"/>
      </a>
  </div>
 
    <div class="col-md-8 col-sm-8 col-xs-6 tp_logo_txt"><?php echo $this->lang->line('rentallocfrom');?></div>
     <div class="col-md-2 col-sm-2 col-xs-2"></div>

<div class="clear"></div>
</div>

 
  <div class="ola-booking" style="margin-top:0; !important;"><br></br>
    <div class="tab-content">
    
      	<div class="tab-pane fade in active" id="tab1">
        <form name="myForm" id="myForm" action="<?php echo base_url();?>index.php/Rental_controller/getpicklocation" method="post">
          <div class="input-group form-group">
          
    <span style="background-color:#FFFFFF;"  class="input-group-addon"><img src="https://cdn1.iconfinder.com/data/icons/hawcons/32/698956-icon-111-search-128.png" height="34px" width="34px" alt="" /></span>
  <input  type="text" class="bnr_input1" style="background-color:#FFFFFF; width: 102%; !important;" required onkeypress="initialize()"  id="origin-input" name="origin-input"  placeholder="<?php echo $this->lang->line('rentallocfrom');?>"  autocomplete="" autofocus> 
    </div> 
  </div>  
 
  </div>                             
   </div>   
 <div class="clear"></div>
 </div>
</div> 
<div class="web_lang">
                  <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
</div> 
</body>
</html> 
