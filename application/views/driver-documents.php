<?php 
 //echo "<pre>";print_r($list);die();
if($this->session->flashdata('docsuccess')):?>
     <script>alert("your document id in under process, You will be notified very soon!!");</script>
<?php unset($_SESSION['docsuccess']);
              endif; ?>
<?php include('header_driver.php'); ?>
<script>
	function validateForm() {
		<?php foreach($list as $data) { ?>
			var na=document.getElementById("expire<?php echo $data['document_id'] ?>").value;
			  
			if(na=="")
			{
				alert('Please select expiry dates first');
				return false;
			}
		<?php } ?>
	}
</script>
<script>
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(300)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
		
</script>
  <section class="mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
              <?php if ($driver_image == "") { ?>
              <li class="driver_profile"> <img class="col-md-4 col-sm-4 col-xs-4"  src="<?php echo base_url();?>/images/profile.png"/>
                <?php } else{?>
                 <li class="driver_profile"> <img    class="col-md-4 col-sm-8 col-xs-8" src="<?php echo base_url($driver_image); ?>">
                <?php } ?>
                <div class="col-md-8 col-sm-8 col-xs-8 driver_info">
                  <div class="driver_name"><?php echo $driver_name; ?></div>
                  <div class="driver_car_name"><?php echo $car_model_name; ?></div>
                  <div class="driver_car_number"><?php echo $car_number; ?></div>
                  <div class="driver_rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> </div>
                </div>
                <div class="clear"></div>
               	<?php if($login_logout == 1) { ?>
                <button class="btn btn-success btn-xs online_offline"><?php echo $this->lang->line('online');?></button>
				<?php } else { ?>
				  <button   class="btn btn-danger btn-xs online_offline"><?php echo $this->lang->line('offline');?></button>
				<?php } ?>
                <div class="clear"></div>
              </li>
               <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/driver_profile"><?php echo $this->lang->line('drprofile');?><i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/driver_rides"><?php echo $this->lang->line('drmyrides');?><i class="fa fa-car" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="driver-earnings.php">Earnings <i class="fa fa-usd" aria-hidden="true"></i></a> </li>-->
              <li class="active"> <a href="<?php echo base_url();?>index.php/Welcome/driver_documents"><?php echo $this->lang->line('drdocument');?><i class="fa fa-file-text" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/driver_password"><?php echo $this->lang->line('drchpass');?><i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/driver_suppourt"><?php echo $this->lang->line('drsupport');?><i class="fa fa-life-ring" aria-hidden="true"></i></a> </li>
             <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/about"><?php echo $this->lang->line('drabout');?> <i class="fa fa-info" aria-hidden="true"></i></a> </li>
             <!--  <li class=""> <a href="terms.php">Terms & Conditions<i class="fa fa-cogs" aria-hidden="true"></i></a> </li>-->
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/logout"><?php echo $this->lang->line('drlogout');?><i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
            <div class="" id="">
              <h4 class="pb-30"><?php echo $this->lang->line('dryourdocs');?> </h4>
              
                <?php echo form_open_multipart('Useraccounts/Upload_driver_docs',['id'=>"myForm" ,"required"=>"required" ,'onsubmit'=>"return validateForm()"] );?>
			   <?php if(!empty($list)){ foreach($list as $data) { ?>
                  <div class="form-group edit_profile_label col-md-6" style="margin-bottom:30px;">   
				     <label><b><?php echo $data['document_name'];?><b></label>
					<div>
					<?php if($data['uploaded']['document_path'] != "") { ?>
		     <img style="margin-bottom: 2%;!important;" id="blah" src="<?php echo base_url($data['uploaded']['document_path']);?>"  height="250px" width="100%"/> 
					<?php } else { ?>
				<img style="margin-bottom: 2%;!important;" id="blah" src="http://waterfallsoftasmania.com.au/uploads/waterfalls/featured/medium/medium_no_image_available.jpg"  height="250px" width="100%"/> 	
					<?php }?>
			  	<?php if($button_hide[0] != $button_hide[1]) {?>
			  <input style="width:49%; float:left;" type="file" class="form-control edit_profile_field"  placeholder="<?php echo $data['document_name'] ?>" name="document[]" id="document" required>
			  <input style="width:2%; float:left; visibility:hidden;"></input>
			  <input style="width:49%; float:left;" type="text" class="form-control edit_profile_field" value="<?php echo $data['uploaded']['document_expiry_date'];?>"  placeholder="mm/dd/yyyy" name="expire[]" id="expire<?php echo $data['document_id'] ?>">
			  <?php } ?>
                  </div>
				    </div>
			   <?php }} else { ?>
			   No Need To upload Documents
			   <?php }?>
				<?php if($button_hide[0] != $button_hide[1]) {?>
                  <div class="form-group">
                    <input type="submit" class="submit_btn" style="width:100%;" value="Update Document">
                  </div>
				<?php } else { ?>
				
				<?php } ?>
                </form>
              
          </div>
<script type="text/javascript">
		$(document).ready(function()
		{
			$('[id^=expire]').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY', weekStart : 0, time: false ,minDate : new Date() });
			
		});
		</script>
        </div>
      </div>
    </div>
  </section>
</div>
 <!--  <section><img src="<?php echo base_url();?>images/footer.png"/></section> -->
  </div>
  <footer class="footer">
    <section class="bottom-section">
      <div class="container">
        <div class="row">
          <div class="col-md-10">
            <nav class="footer-nav">
              <ul>
                <li><a href="<?php echo base_url();?>">Home</a></li>
                 <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/about">About Us</a></li>
                  <li><a href="#">Blog</a></li>
                  <li class="hidden-xs"><a href="<?php echo base_url();?>index.php/Welcome/contact_us">Contact Us</a></li>
              </ul>
            </nav>
          </div>
          <div class="col-md-2">
            <p class="copyright"> © 2017 Apporio Taxi.</p>
          </div>
        </div>
      </div>
    </section>
  </footer>
</div>
</body>

</html>