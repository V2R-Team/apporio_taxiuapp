 <script>
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>			
<?php if($this->session->flashdata('prosuccess')):?>
     <script>alert("Profile Succesfully Updated!!");</script>
<?php  unset($_SESSION['prosuccess']);  endif; ?>

<?php include('header_driver.php'); ?>
  <section class="mt-30">
    <div class="container">
      <div class="row mt-60 mb-100 pt-20 pb-20 profile_content">
        <div class="tabs-vertical-env">
          <div class="col-md-3 hidden-xs">
            <ul class="nav tabs-vertical left_tab">
              <?php if ($driver_image == "") { ?>
              <li class="driver_profile"> <img class="col-md-4 col-sm-4 col-xs-4"  src="<?php echo base_url();?>/images/profile.png"/>
                <?php } else{?>
                 <li class="driver_profile"> <img    class="col-md-4 col-sm-8 col-xs-8" src="<?php echo base_url($driver_image); ?>">
                <?php } ?>
                <div class="col-md-8 col-sm-8 col-xs-8 driver_info">
                  <div class="driver_name"><?php echo $driver_name; ?></div>
                  <div class="driver_car_name"><?php echo $car_model_name; ?></div>
                  <div class="driver_car_number"><?php echo $car_number; ?></div>
                  <div class="driver_rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> </div>
                </div>
                <div class="clear"></div>
				<?php if($login_logout == 1) { ?>
                <button class="btn btn-success btn-xs online_offline"><?php echo $this->lang->line('online');?></button>
				<?php } else { ?>
				  <button   class="btn btn-danger btn-xs online_offline"><?php echo $this->lang->line('offline');?></button>
				<?php } ?>
                <div class="clear"></div>
              </li>
              <li class="active"> <a href="<?php echo base_url();?>index.php/Welcome/driver_profile"><?php echo $this->lang->line('drprofile');?><i class="fa fa-user" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/driver_rides"><?php echo $this->lang->line('drmyrides');?><i class="fa fa-car" aria-hidden="true"></i></a> </li>
              <!--<li class=""> <a href="driver-earnings.php">Earnings <i class="fa fa-usd" aria-hidden="true"></i></a> </li>-->
              <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/driver_documents"><?php echo $this->lang->line('drdocument');?><i class="fa fa-file-text" aria-hidden="true"></i></a> </li>
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/driver_password"><?php echo $this->lang->line('drchpass');?><i class="fa fa-lock" aria-hidden="true"></i></a> </li>
              <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/driver_suppourt"><?php echo $this->lang->line('drsupport');?><i class="fa fa-life-ring" aria-hidden="true"></i></a> </li>
             <li class=""> <a href="<?php echo base_url();?>index.php/Welcome/about"><?php echo $this->lang->line('drabout');?> <i class="fa fa-info" aria-hidden="true"></i></a> </li>
             <!--  <li class=""> <a href="terms.php">Terms & Conditions<i class="fa fa-cogs" aria-hidden="true"></i></a> </li>-->
              <li class="">  <a href="<?php echo base_url();?>index.php/Welcome/logout"><?php echo $this->lang->line('drlogout');?><i class="fa fa-sign-out" aria-hidden="true"></i></a> </li>
            </ul>
          </div>
          <div class="tab-content col-md-9 tab_content">
            <div class="" id="">
              <h4 class="pb-30"><?php echo $this->lang->line('drprofile');?></h4>
              <div class="row">
                <div class="col-md-6">
                  <?php echo form_open_multipart('Useraccounts/update_driver_profile',['name'=>'update_pro']) ?>
                    <div class="form-group edit_profile_label">
                      <label><?php echo $this->lang->line('drName');?></label>
                      <input type="text" name="drivername" class="form-control edit_profile_field" id="drivername" name="drivername" placeholder="<?php echo $this->lang->line('drName1');?>" value="<?php echo $driver_name; ?>" required>
                    </div>
                    <div class="form-group edit_profile_label">
                      <label><?php echo $this->lang->line('dremail');?></label>
                      <input type="email" name="driveremail" class="form-control edit_profile_field" id="driveremail" name="driveremail" placeholder="<?php echo $this->lang->line('dremail1');?>" value="<?php echo $driver_email; ?>" required>
                    </div>
                    <div class="form-group edit_profile_label">
                      <label><?php echo $this->lang->line('drphone');?></label>
                      <input type="text"  class="form-control edit_profile_field" id="driverphone" name="driverphone" placeholder="<?php echo $this->lang->line('drphone1');?>" value="<?php echo $driver_mobile; ?>" required>
                    </div>
					
					<div class="form-group edit_profile_label">
                      <label> <?php echo $this->lang->line('about_us');?> </label>
					  <br>
					  <?php if($driver_image != "") { ?>
                     <img class="col-md-4 col-sm-8 col-xs-8 img-responsive" id="blah" src="<?php echo base_url($driver_image);?>" style="margin-bottom: 10px;width: 200px;
height: 200px; !important;" />
					  <?php } else { ?>
					   <img class="col-md-4 col-sm-8 col-xs-8 img-responsive" id="blah" src="<?php echo base_url();?>/images/profile.png" style="margin-bottom: 10px;width: 200px;
height: 200px; !important;" />

							<?php } ?>
<br>
 <input type="file" class="form-control edit_profile_field"  name ="driver_image" id="blah"  id="main_image1" onchange="readURL(this);" >
                    </div>
					
					
                    <div class="form-group">
                      <input type="submit" class="submit_btn" style="width:100%;" value="<?php echo $this->lang->line('updatepro');?>">
                    </div>
                  </form>
                </div>
              </div>
            </div>
            
            
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include('footer.php'); ?>
