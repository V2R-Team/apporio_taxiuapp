<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Alakowe Taxi</title>
<!-- <meta name="viewport" content="initial-scale=1.0, user-scalable=no"> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url();?>webstatic/css/style1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/custom1.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap1.css" type="text/css">
<style type="text/css">
#map_canvas {
  min-height: 480px;
  margin:0px -20px !important;
}
@media print {
  html, body {
    height: auto;
  }
 #map_canvas {
  }
}

.clear{clear:both !important;}
@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&libraries=places&callback=initMap" async defer></script>
<script>
      var geocoder;
      var map;
      var marker; 
      var infowindow = new google.maps.InfoWindow({size: new google.maps.Size(0,0)});
      function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
          zoom: 16,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
        google.maps.event.addListener(map, 'click', function() {
          infowindow.close();
        });
      }

  function clone(obj){
      if(obj == null || typeof(obj) != 'object') return obj;
      var temp = new obj.constructor(); 
      for(var key in obj) temp[key] = clone(obj[key]);
      return temp;
  }


function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      marker.formatted_address = responses[0].formatted_address;
	console.log(responses);   
    } else {
      marker.formatted_address = 'Cannot determine address at this location.';
    }
   // infowindow.setContent(marker.formatted_address+"<br>coordinates: "+marker.getPosition().toUrlValue(6));
    //infowindow.open(map);
	 
    document.getElementById('latlon').value = marker.getPosition();
    document.getElementById('addrress1').value = marker.formatted_address;
  });
}

      function codeAddress() {
        var address = document.getElementById('address').value;
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
	    if (marker) {
               marker.setMap(null);
               if (infowindow) infowindow.close();
            }
            marker = new google.maps.Marker({
                map: map,
		draggable: true,
                position: results[0].geometry.location
            });
            google.maps.event.addListener(marker, 'dragend', function() {
              // updateMarkerStatus('Drag ended');
              geocodePosition(marker.getPosition());
            });
            google.maps.event.addListener(marker, 'click', function() {
              if (marker.formatted_address) {
               // infowindow.setContent(marker.formatted_address+"<br>coordinates: "+marker.getPosition().toUrlValue(6));
              } else  {
             //   infowindow.setContent(address+"<br>coordinates: "+marker.getPosition().toUrlValue(6));
              }
              infowindow.open(map);
            });
            google.maps.event.trigger(marker, 'click');
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    </script>
  </head>
  <body onload="initialize(); codeAddress();" class="main_bg">
  


<div class="main_dv">
 <div class="lft_side">
  
  <div class="tp_header1">
  			<div class="col-md-2 col-sm-2 col-xs-3 tp_menu">
      				<a class="bnr_a"  href="javascript:history.back()">
          				<img src="http://apporio.org/Alakowe/images/back1.png" width="19">
      				</a>
  			</div>
  			<div class="col-md-8 col-sm-8 col-xs-6 tp_logo_txt"><?php echo $this->lang->line('maplocation');?></div>
       			<div class="col-md-2 col-sm-2 col-xs-2"></div>

			<div class="clear"></div>
			 	<form name="myForm" action="<?php echo base_url();?>index.php/Booking_controller/edit_mappickuplocation"  method="post">
          			<input type="hidden" name="address" id="address" type="textbox" value="<?php echo $this->session->userdata('origin_input_city');?>">
	  			<input type="text" id="latlon" name="latlon" hidden>
	  			<input type="text" class="edit_loc_input" id="addrress1" name="addrress1" value="<?php echo $this->session->userdata('origin_input_city');?>" readonly>
	               </div>
    
    <div id="map_canvas"></div>
	
<button type="submit" style="margin-top:25px;" class="con_booking_btn"><?php echo $this->lang->line('maplocationconfirm');?></button>
	 </form>
<script src="https://www.google-analytics.com/urchin.js" type="text/javascript"> 
</script> 
 </div>
</div> 
 <div class="web_lang">
                  <select onchange="javascript:window.location.href='<?php echo base_url(); ?>index.php/LanguageSwitcher/switchLang/'+this.value;">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"';?>>English</option>
                       <option value="french" <?php if($this->session->userdata('site_lang') == 'french') echo 'selected="selected"';?>>Arabic</option>
                </select> 
</div>  
  </body>
</html>
