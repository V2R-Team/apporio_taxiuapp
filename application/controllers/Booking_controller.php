<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
date_default_timezone_set('Europe/London');
class Booking_controller extends CI_Controller {
		function __construct(){
				        parent::__construct();   
				        $this->load->model('User_account');
				        $this->load->model('Booking_model');
				      }	
public function search_cab_city()
{               	
	         $latitude = $this->input->post('latitude');
	         $longitude = $this->input->post('longitude');
	         $destlatitude = $this->input->post('destlatitude');
	         $destlongitude = $this->input->post('destlongitude');
	         $origin_input = $this->input->post('origin-input');
	         $check_page = $this->input->post('index');
	         $check_page1 = $this->input->get('index_ch');
	         $destination_input = $this->input->post('destination-input');
	         $date = $this->input->post('date');
	         $when= $this->input->post('sel1');
	         $time = $this->input->post('time');
				  
if($origin_input != "" )
{	
	$this->session->set_userdata(['origin_input_city'=>$origin_input]);		 
}	
if($destination_input != "" )
{	
	$this->session->set_userdata(['destination_input_city'=>$destination_input]);		 
}
if($latitude != "" )
{	
	$this->session->set_userdata(['lat_city'=>$latitude,]);		 
}
if($longitude != "" )
{	
	$this->session->set_userdata(['long_city'=>$longitude,]);		 
}
if($destlatitude != "" )
{	
	$this->session->set_userdata(['destlatitude_city'=>$destlatitude]);		 
}
if($destlongitude != "" )
{	
	$this->session->set_userdata(['destlongitude_city'=>$destlongitude]);		 
}
if($date != "" )
{	
	$this->session->set_userdata(['date'=>$date]);		 
}
if($when != "" )
{	
	$this->session->set_userdata(['when'=>$when]);		 
}
if($time != "" )
{	
	$this->session->set_userdata(['time'=>$time]);		 
}
 
 
	         $latitude =$this->session->userdata('lat_city');  
	         $longitude = $this->session->userdata('long_city'); 
	         $destlatitude = $this->session->userdata('destlatitude_city'); 
	         $destlongitude = $this->session->userdata('destlongitude_city');
	         $about=$this->User_account->web_home();
		 $city_name = $this->input->post('city_name');
	
$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=false');

$output= json_decode($geocode);
    for($j=0;$j<count($output->results[0]->address_components);$j++){
               $cn=array($output->results[0]->address_components[$j]->types[0]);
           if(in_array("locality", $cn))
           {
            $city= $output->results[0]->address_components[$j]->long_name;
           }
            }
       
		
     if( $latitude != "" && $longitude!= "" && $destlatitude != "" && $destlongitude!= "" )
      { 
          /*  $theta = $destlongitude - $longitude;
            $dist = sin(deg2rad($destlatitude)) * sin(deg2rad($latitude)) +  cos(deg2rad($destlatitude)) * cos(deg2rad($latitude)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $distance=$miles* 1.609344;
            $distance = round($distance,2); */
            
            
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$latitude .",".$longitude."&destinations=".$destlatitude .",".$destlongitude."&mode=driving&language=pl-PL";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    
    $distance = $response_a['rows'][0]['elements'][0]['distance']['value'];
 
    $distance = $distance * 0.00062137;
   
     }
     else
     {  
     $distance= '0';
     }    

 

      if(!empty($latitude) && !empty($longitude))
        {
            $data = $this->Booking_model->cartype($city_name);
			 
            if(!empty($data))
            {
              $get__cars=$data;
			   
            }
			else
			{
                $data = $this->Booking_model->all_driver();
                if (!empty($data))
                {
                    foreach($data as $login3)
                    {
                        $driver_lat = $login3->current_lat;
                        $driver_long = $login3->current_long;
                        $theta = $longitude - $driver_long;
                        $dist = sin(deg2rad($latitude)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($latitude)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
                        $dist = acos($dist);
                        $dist = rad2deg($dist);
                     
                        $miles = $dist * 60 * 1.1515;
                        $km=$miles* 1.609344;
                        if($km <= 30)
                        {
                            $c[] = array("driver_id"=> $login3->driver_id,"distance" =>$km,'city_id'=>$login3->city_id);
                        }
                    }
				 
                    if (!empty($c))
                    {    $distance1=array();
                        foreach ($c as $value)
                        {
                            $distance1[] = $value['distance'];
                        }
                        array_multisort($distance1,SORT_ASC,$c);
                        $c = $c[0]['city_id'];
                        $data = $this->Booking_model->cartype_city_id($c);
					 
                        if(!empty($data))
                        {
                         $get__cars=$data;
						 
                        }
                    }
					 
                }
            }
        } 

		if($distance != 0 )  
		 {
		 $get_city_cars=array();
		 $val=array();
	 
		 foreach($get__cars as $value1)
		 {
		 $car_id=$value1['car_type_id'];
		 $base_distance=$value1['base_distance'];
		 $base_price_per_unit=$value1['base_price_per_unit'];
		 $base_distance_price=$value1['base_distance_price'];
		 /* if($distance > $base_distance)
		 {
			 
		 $distance = $distance-$base_distance;
		 $total_amount = $distance * $base_price_per_unit;
		 $total_amount = $total_amount + $base_distance_price;
		 $total_amount = number_format($total_amount , 2, '.', '');
		 $value1['total_amount']=$total_amount;
		 $get_city_cars[]=$value1;
		 }
		 else
		 {
		 $total_amount = $base_distance_price;
		 $value1['total_amount']=$total_amount;
		 $get_city_cars[]=$value1;
		 } */
		 
		 
		 
		 
		if($distance <= $base_distance)
		{
		 $total_amount = $base_distance_price;
		 $value1['total_amount']=$total_amount;
		 $get_city_cars[]=$value1;
		}
		else
		{
            
            
                 $diff_distance = $distance-$base_distance;
		 $total_amount = ($diff_distance * $base_price_per_unit);
		 $total_amount = $total_amount + $base_distance_price;
		 $total_amount = number_format($total_amount , 2, '.', '');
		 $value1['total_amount']=$total_amount;
		 $get_city_cars[]=$value1;
		} 
		 
		 
		 
		 
                }
		 $this->session->set_userdata('booking_tab','city');  
		
		}
		
//echo "<pre>";
//print_r($get_city_cars);die();
		$header_data=$this->User_account->Get_homepage_data();
	        $this->load->view('booking_page_city',['about'=>$about,'get_city_cars'=>$get_city_cars,'header_data'=>$header_data]);
} 
	
public function search_cab_rental()
	{       
	         $latitude= $this->input->post('latitude');
	         $longitude= $this->input->post('longitude');
	         $destlatitude= $this->input->post('destlatitude');
	         $destlongitude= $this->input->post('destlongitude');
	         $origin_input= $this->input->post('origin-input');
	         $destination_input= $this->input->post('destination-input');
	         $about=$this->User_account->web_home(); 
		 $packages=$this->Booking_model->rental_packages(); 
		 $this->session->set_userdata(['lat_rental'=>$latitude,'long_rental'=>$longitude,'destlatitude_rental'=>$destlatitude,'destlongitude_rental'=>$destlongitude,'origin_input_rental'=>$origin_input,'destination_input_rental'=>$destination_input]);  
		 
		 $rental_category_id=array();
		 foreach($packages as $value)
		 {
		 if (!in_array($value['rental_category_id'], $rental_category_id))
		 $rental_category_id[]=$value['rental_category_id'];
		 }
		  
		 $get_package_rental=array();
		 foreach($rental_category_id as $value1)
		 {
		 $get_package_rental[]=$this->Booking_model->Get_car_type_packages($value1); 
		 }
		 $get_package_rental= array_filter($get_package_rental); 
		 $this->session->set_userdata('booking_tab','rental'); 		 
		 $this->load->view('booking_page_rental',['about'=>$about,'get_package_rental'=>$get_package_rental]);
	 } 
	 
public function location()
	{       
	         $header_data=$this->User_account->Get_homepage_data();
		 $this->load->view('location',['header_data'=>$header_data]);
	} 
	
public function search_cab_login()
	{     
	       $url= $_SERVER['HTTP_REFERER'];
	       $this->session->set_userdata(['login_url'=>$url]);
	       $about=$this->User_account->web_home();
	       $country=$this->Booking_model->get_country();
	       $header_data=$this->User_account->Get_homepage_data();
	       $this->load->view('booking_page_login',['about'=>$about,'country'=>$country,'header_data'=>$header_data]);
	} 
public function search_cab_login1()
	{     
	       echo 'else';die();
	       $about=$this->User_account->web_home();
	       $country=$this->Booking_model->get_country();
	       $header_data=$this->User_account->Get_homepage_data();
	       $this->load->view('booking_page_login',['about'=>$about,'country'=>$country,'header_data'=>$header_data]);
	}
	
public function search_cab_redirect()
	{
	if($this->session->userdata('booking_tab') == 'city')
	{
         redirect('Booking_controller/search_cab_city'); 
        }
        else
        {
         redirect('Booking_controller/search_cab_rental'); 
        }
        }
       
public function search_cab_otp_verify()
	{       	
	         $phone= $this->input->post('phonenum');
	         $phonecode= $this->input->post('phonecode');
	         $password= $this->input->post('password');
	           
	         $phonenumber=$phonecode.$phone;
		 $data = $this->Booking_model->user_check($phonenumber);
		 $login_data = $this->Booking_model->login_check($phonenumber,$password);
		 
		 $header_data=$this->User_account->Get_homepage_data();
		 
		 if (empty($data))
	         {  
	         
	          $this->session->set_userdata('user_registerphone', $phonenumber); 
	           $country=$this->Booking_model->get_country();  
	          $this->load->view('booking_page_register',['header_data'=>$header_data,'country' => $country]);
	         }
	         else
	         {
	         if(!empty($login_data))
	         {
	         $user_id =$login_data['user_id'];
	         $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	     $this->session->set_flashdata('msg', 'Login Succesful');
  	         $url=$this->session->userdata('login_url');  
	         redirect($url);
	         }
	         else
	         {
	           $this->session->set_flashdata('Incorrect_password', 'Category added');
                   redirect('Booking_controller/search_cab_login1');
	         }
	        }
	 } 
	 
public function search_cab_otp_verify21()
	{       	
	          $this->session->set_flashdata('Incorrect_password', 'Category added');
	          $header_data=$this->User_account->Get_homepage_data();
	           $country=$this->Booking_model->get_country();
	          $this->load->view('booking_page_register',['header_data'=>$header_data, 'country'=> $country]);
	        
	 } 
public function search_cab_otpnot()
	{       	
	           $this->session->set_flashdata('Incorrect_phonenumber', 'Category added');
	          $header_data=$this->User_account->Get_homepage_data();
	           $country=$this->Booking_model->get_country();
	          $this->load->view('booking_page_register',['header_data'=>$header_data, 'country'=> $country]);
	        
	 } 

public function search_cab_otpnot1()
	{       	
	           $this->session->set_flashdata('Incorrect_email', 'Category added');
	           $country=$this->Booking_model->get_country();
	          $this->load->view('booking_page_register',['header_data'=>$header_data, 'country'=> $country]);
	        
	 } 
	 
	 
public function get_value()
	{       
	         if($this->input->post('user_phone') == "")
	         {
	         $phonenumber=$this->session->userdata('user_registerphone'); 
	         }
	         else
	         {
	         
	         $phonecode= $this->input->post('phonecode');
	         $phonenumber= $this->input->post('user_phone');
	         $phonenumber=$phonecode.$phonenumber;
	         
	         $this->session->set_userdata(['user_registerphone'=>$phonenumber]);	
	         }
	       
	         $user_email= $this->input->post('user_email');
	         
	         $user_data = $this->Booking_model->User_prvious($phonenumber);
	          
	         if(empty($user_data))
	         {
	         $user_data1 = $this->Booking_model->User_prvious1($user_email);
	         
	         if(empty($user_data1)){
	         $full_name= $this->input->post('full_name');
	         $user_password= $this->input->post('user_password');
	         $user_email= $this->input->post('user_email')?$this->input->post('user_email'):'';
	         $this->session->set_userdata(['user_full_name'=> $full_name,'user_email'=>$user_email,'user_password'=>$user_password]); 
	         $code_ui = substr( str_shuffle( str_repeat( 'abcdefghijklmnopqrstuvwxyz0123456789', 10 ) ), 0, 4 );
	         $code_ui=2017;
  	         $data = $this->Booking_model->otp_Insert($phonenumber,$code_ui);
  	         $header_data=$this->User_account->Get_homepage_data();
  	         $message = urlencode('Your Alakowe Taxi Verification code is:'.$code_ui);
  	         $phonenumber = str_replace("+","",$phonenumber);
	         //$sms_res = file_get_contents("http://nimbusit.net/api.php?username=t4glambin&password=680086&sender=GLAMBN&sendto=$phonenumber&message=$message");  
	        // $url="http://api.smartsmssolutions.com/smsapi.php?username=Alakowe&password=friday&sender=Alakowe&recipient=$phonenumber&message=$message";
                 $ch = curl_init();
                 curl_setopt($ch,CURLOPT_URL,$url);
                 curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                 $output=curl_exec($ch);
                 curl_close($ch);
	         $this->load->view('booking_page_otpverify',['header_data'=>$header_data]);
	         }else
	         {
	         return redirect('Booking_controller/search_cab_otpnot1');
	         }
	         }else
	         {
	         return redirect('Booking_controller/search_cab_otpnot');
	         }
	} 
	
public function search_cab_register_otp()
	{    
	         $phonenumber=$this->session->userdata('user_registerphone'); 
	        // $this->session->set_userdata(['user_full_name'=>$full_name,'user_email'=>$user_email]); 
	         $this->session->set_userdata('user_registerphone', $phonenumber); 
	         $user_full_name=  $this->session->userdata('user_full_name');
	         $user_email=  $this->session->userdata('user_email');
	         $phonenumber=$this->session->userdata('user_registerphone'); 
	          $user_password=  $this->session->userdata('user_password');
	       date_default_timezone_set('Europe/London');
	         $register_date=date('l M d');
	         $data=array(
	                   'user_type'=>4,
	                   'user_name'=>$user_full_name,
	                   'user_email'=>$user_email,
	                   'user_phone'=>$phonenumber,
	                   'user_password'=>$user_password,
	                   'register_date'=>$register_date,
	                   );
	        $user_id=$this->Booking_model->rider_signupweb($data);
	          
	     $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	     $this->session->set_flashdata('msg', 'Login Succesful');
  	        // redirect('Booking_controller/search_cab_city');
			 $url=$this->session->userdata('login_url'); 
  	          redirect($url);
	} 
	
public function search_cab_userregister()
	{       
	         $about=$this->User_account->web_home(); 
	         $rm_userphone=  $this->session->userdata('user_registerphone');
	 	 $user_full_name=  $this->session->userdata('user_full_name');
	         $user_email=  $this->session->userdata('user_email');
	          $user_password=  $this->session->userdata('user_password');
	          
	        date_default_timezone_set('Europe/London');
	         $register_date=date('l M d');
	         $data=array(
	                   'user_type'=>4,
	                   'user_name'=>$user_full_name,
	                   'user_email'=>$user_email,
	                   'user_phone'=>$rm_userphone,
	                   'user_password'=> $user_password,
	                   'register_date'=>$register_date,
	                   );
	         $user_id=$this->Booking_model->rider_signupweb($data);
	          
	         $this->session->set_userdata(['user'=>'rider','user_id'=>$user_id]); 
    	         $this->session->set_flashdata('msg', 'Login Succesful');
  	         redirect('Booking_controller/search_cab_city');
		        
	} 
public function check_login_otp()
	{   
	         $about=$this->User_account->web_home(); 
	         $rm_userphone=  $this->session->userdata('user_registerphone');
	 	 $phonenumber=$rm_userphone;
	 	 $otptext= $this->input->post('otptext');
	 	 $user_detail=$this->Booking_model->otp_user_check($phonenumber,$otptext);
	 	  
	 	 if(!empty($user_detail))
	 	 {
	 	 $user_detail=$this->Booking_model->user_check($phonenumber);
	         $user_id =$user_detail[0]['user_id'];
	         $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	         $this->session->set_flashdata('msg', 'Login Succesful');
  	         $url=$this->session->userdata('login_url');  
	        // $this->load->view('booking_page_otpverify1');
	         redirect($url);
	        }else
	        {
	        $this->session->set_flashdata('otpnotmatch', 'Login Succesful');
	        //redirect('Booking_controller/search_cab_otp_verify');
	        $this->load->view('booking_page_otpverify1');
                }
  	        
	} 


public function check_login_otpregister()
	{   
	         $about=$this->User_account->web_home(); 
	         $rm_userphone=  $this->session->userdata('user_registerphone');
	 	 $phonenumber=$rm_userphone;
	 	 $otptext= $this->input->post('otptext');
	 	 $user_detail=$this->Booking_model->otp_user_check($phonenumber,$otptext);
	 	  
	 	 if(!empty($user_detail))
	 	 {
	 	 $user_detail=$this->Booking_model->user_check($phonenumber);
	         $user_id =$user_detail[0]['user_id'];
	         $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	         $this->session->set_flashdata('msg', 'Login Succesful');
  	         $url=$this->session->userdata('login_url');  
	        // $this->load->view('booking_page_otpverify1');
	         //redirect($url);
	        redirect('Booking_controller/search_cab_register_otp');
	        }else
	        {
	        $this->session->set_flashdata('otpnotmatch', 'Login Succesful');
	        //redirect('Booking_controller/search_cab_otp_verify');
	        $this->load->view('booking_page_otpverify');
                }
  	        
	} 
	
	
	



public function search_cab_user()
	{   
	         $about=$this->User_account->web_home(); 
	         $rm_userphone=  $this->session->userdata('user_registerphone');
	 	 $phonenumber=$rm_userphone;
	 	 
	         $user_detail=$this->Booking_model->user_check($phonenumber);
	         $user_id =$user_detail[0]['user_id'];
	          
	         $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	         $this->session->set_flashdata('msg', 'Login Succesful');
  	         
  	 // echo '<script type="text/javascript">
         // window.location = "http://apporio.org/Alakowe/index.php/Booking_controller/search_cab_city?checkk=success"
            echo '<script>window.location="<?php echo base_url();?>index.php/Booking_controller/search_cab_city?checkk=success"</script>';
         // </script>';
  	        
	} 
	
	
	
public function edit_pickuplocation()
	{   
	   $this->load->view('map');       
  	        
	} 

public function edit_mappickuplocation()
{           
	         $latlon= $this->input->post('latlon');
	         $origin_input= $this->input->post('addrress1');
	         $latlon =  str_replace("(","",$latlon); 
	         $latlon =  str_replace(")","",$latlon);
	         $latlon = (explode(",",$latlon));
	         $latitude = $latlon[0];
	         $longitude = $latlon[1];
	         $latitude= number_format($latitude, 7, '.', '');
	         $longitude = number_format($longitude , 7, '.', '');
 	  
if($origin_input != "" )
{	
	$this->session->set_userdata(['origin_input_city'=>$origin_input]);		 
}	

if($latitude != "" )
{	
	$this->session->set_userdata(['lat_city'=>$latitude,]);		 
}
if($longitude != "" )
{	
	$this->session->set_userdata(['long_city'=>$longitude,]);		 
}
 
 
	         $latitude =$this->session->userdata('lat_city');  
	         $longitude = $this->session->userdata('long_city'); 
	         $destlatitude = $this->session->userdata('destlatitude_city'); 
	         $destlongitude = $this->session->userdata('destlongitude_city');
	         $about=$this->User_account->web_home();
	          $car_type_id = $this->session->userdata('car_type_idcity');
		
	
$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=false');
 
$output= json_decode($geocode);
    for($j=0;$j<count($output->results[0]->address_components);$j++){
               $cn=array($output->results[0]->address_components[$j]->types[0]);
           if(in_array("locality", $cn))
           {
            $city= $output->results[0]->address_components[$j]->long_name;
           }
            }
          $city_name =$city;
	 
		
     if( $latitude != "" && $longitude!= "" && $destlatitude != "" && $destlongitude!= "" )
      { 
            $theta = $destlongitude - $longitude;
            $dist = sin(deg2rad($destlatitude)) * sin(deg2rad($latitude)) +  cos(deg2rad($destlatitude)) * cos(deg2rad($latitude)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $distance=$miles* 1.609344;
            $distance = round($distance,2); 
     }
     else
     {  
     $distance= '0';
     }    
 
 

      if(!empty($latitude) && !empty($longitude))
        {    
             $city_name = $city_name?$city_name:'Gurugram'; 
             $city_ids= $this->Booking_model->getcity_id($city_name);
	     $city_nameId=$city_ids['city_id'];
             $data = $this->Booking_model->cartype_name($car_type_id,  $city_nameId);
        } 
 
 
		if($distance != 0 )  
		 {
		 
		 $car_id=$data['car_type_id'];
		 $base_distance=$data['base_distance'];
		 $base_price_per_unit=$data['base_price_per_unit'];
		 $base_distance_price=$data['base_distance_price'];
		 if($distance > $base_distance)
		 { 
		 $distance = $distance-$base_distance;
		 $total_amount = $distance * $base_price_per_unit;
		 $total_amount = $total_amount + $base_distance_price;
		 
		 }
		 else
		 {
		 $total_amount = $base_distance_price;
		 
		 }
                  
		 $this->session->set_userdata('booking_tab','city');  
		
		}
		 
		 redirect('Booking_controller/booking_ride'.'?car_type='.$car_type_id.'&amo='.$total_amount); 	//booking_ride?car_type=30&amo=153
		 //$this->load->view('booking_page_city',['about'=>$about,'get_city_cars'=>$get_city_cars]);
} 


	
	

public function booking_ride()
	{     
	         $about=$this->User_account->web_home();
	         $payment_option=$this->Booking_model->payment_option();
	         $car_type_id= $this->input->get('car_type', TRUE);
	         $this->session->set_userdata(['car_type_idcity'=>$car_type_id]); 
	         $latitude = $this->session->userdata('lat_city');  
	         $longitude = $this->session->userdata('long_city'); 
	         $destlatitude = $this->session->userdata('destlatitude_city'); 
	         $destlongitude = $this->session->userdata('destlongitude_city');
	         $theta = $destlongitude - $longitude;
                 $dist = sin(deg2rad($destlatitude)) * sin(deg2rad($latitude)) +  cos(deg2rad($destlatitude)) * cos(deg2rad($latitude)) * cos(deg2rad($theta));
                 $dist = acos($dist);
                 $dist = rad2deg($dist);
                 $miles = $dist * 60 * 1.1515;
                 $distance=$miles* 1.609344;
                 $distance = round($distance,2);
				//echo $distance;die();
         if($distance != 0 )  
		 {
		 $get_cars=$this->Booking_model->Get_car_type_city($car_type_id);
         $val=array();
	 
		 $base_distance=$get_cars['base_distance'];
		 $base_price_per_unit=$get_cars['base_price_per_unit'];
		 $base_distance_price=$get_cars['base_distance_price'];
		 if($distance > $base_distance)
		 {
		 $distance=$distance-$base_distance;
		 $total_amount = $distance * $base_price_per_unit;
		 $total_amount = $total_amount + $base_distance_price;
		 $get_cars['total_amount']=$total_amount;
		 $get_city_cars=$get_cars;
		 }
		 else
		 {
		 $total_amount = $base_distance_price;
		 $get_cars['total_amount']=$total_amount ;
		 $get_city_cars=$get_cars;
		 }
		  
		 $this->session->set_userdata('booking_tab','city');  
		 
		}  
		$header_data=$this->User_account->Get_homepage_data();
$this->load->view('booking_ride',['about'=>$about,'get_city_cars'=>$get_city_cars,'payment_option'=>$payment_option,'header_data'=>$header_data]);     
	} 
	
public function submit_confirm_booking()
	{   
	
date_default_timezone_set('Europe/London');
	if($this->session->userdata('user_id') != "")
	{
    $user_id = $this->session->userdata('user_id'); 
    $User_details=$this->Booking_model->Get_user_details($user_id);     
    
    $userphone = $User_details['user_phone'];
   
    $phone = $userphone;
    $username = $User_details['user_name'];
    $email = $User_details['user_email'];
    $pickup_location =$this->session->userdata('origin_input_city');
    $drop_location = $this->session->userdata('destination_input_city');
    $car_type_id = $this->session->userdata('car_type_idcity');
	$language_id=1;
	$date=$this->session->userdata('date');
	if($date = "")
	{
	$date="";
	}
	$time=$this->session->userdata('time');
	if($time= "")
	{
	$time="";
	}
       $payment_option_id=$this->session->userdata('payment_option');
	if($payment_option_id == "")
	{
	$payment_option_id=$this->input->post('payment_option');
	}
	if($this->session->userdata('payment_option') == ""  && $this->input->post('payment_option') == "")
	{
	$payment_option_id=1;
	}
	
	$couponcode=$this->session->userdata('coupon_code');
	if($couponcode != "")
	{
	$result = $this->Booking_model->check_code($couponcode); 
	if(empty($result))
	{
	$couponcode = "";	
	}
	}
	else
	{
	$couponcode = "";	
	}
       $card_id = 0 ;
       $pem_file = 2;
       $pickup_lat = $this->session->userdata('lat_city');
       $pickup_long = $this->session->userdata('long_city'); 
       $drop_lat = $this->session->userdata('destlatitude_city');
       $drop_long = $this->session->userdata('destlongitude_city');
	$when = $this->session->userdata('when');
	if($when == '')
	{
	$ride_mode=1;
	}
	if($when == 'now')
	{
	$ride_mode=1;
	}
	if($when == 'other')
	{
	$ride_mode=2;	
	}
        $time = date("H:i:s");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date1=$day.", ".$data ;
        $last_time_stamp = date("h:i:s A");
        $driver_details = $this->Booking_model->searchnear_driver($car_type_id);
 

	//echo "<pre>";print_r($driver_details);die();
    if(empty($driver_details))
    { 
    $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
        $last_time_stamp = date("h:i:s A");
        $this->session->set_userdata(['payment_option'=>""]); 
	    $this->session->set_userdata(['coupon_code'=>""]); 
        $data=array(
                'later_date' => $date,//$datepicker,
                'later_time' => $time,//$timepicker,
                'user_id' => $user_id,
                'coupon_code' => "",
                'car_type_id' => $car_type_id,
                'pickup_lat' => $pickup_lat,
                'pickup_long' => $pickup_long,
                'pickup_location' => $pickup_location,
                'drop_lat' => $drop_lat,
                'drop_long' => $drop_long,
                'drop_location' => $drop_location,
                'ride_date' => $date1,
                'ride_time' => $time,
                'ride_type' => 1,
                'ride_image' => $image,
                'ride_status' => 1,
                'ride_admin_status' => 1,
                'last_time_stamp' => $last_time_stamp,
                'payment_option_id' => $payment_option_id,
                'card_id' => 0,
               
                 ); 
     $this->Booking_model->insert_noridetable($data);
     $ride_finaldetail =array('ride_id'=>0);
     $driver_allocated = array();
     $model_finaldetail =array();
        
    }
    else {
        $c = array();
        foreach($driver_details as $login3)
        {
            $driver_lat = $login3['current_lat'];
            $driver_long =$login3['current_long'];

            $theta = $pickup_long - $driver_long;
            $dist = sin(deg2rad($pickup_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($pickup_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $km=$miles* 1.609344;
          //  echo $km;
            if($km <= 10)
            {
                $c[] = array("driver_id" => $login3['driver_id'],"distance" => $km,);
            }
        }
    //print_r($c);die();

        if(!empty($c)){
             $this->session->set_userdata(['timer_no' => Count]); 
			// print_r($this->session->userdata());    die();
            $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
             $date = date("Y-m-d");
            $last_time_stamp = date("h:i:s A");
            $this->session->set_userdata(['payment_option'=>""]); 
	    $this->session->set_userdata(['coupon_code'=>""]); 
     $data=array(
                'user_id' => $user_id,
                'pickup_lat' => $pickup_lat,
                'pickup_long' => $pickup_long,
                'pickup_location' => $pickup_location,
                'drop_lat' => $drop_lat,
                'drop_long' => $drop_long,
                'drop_location' => $drop_location,
                'coupon_code' => $couponcode,
                'ride_date' => $date1,
                'ride_time' => $time,
                'ride_type' => 1,
                'ride_status' => 1,
                'ride_image' => $image,
                'car_type_id' => $car_type_id,
                'payment_option_id' => $payment_option_id,
                'card_id' => 0,
                'ride_admin_status' => 1,
                'last_time_stamp' => $last_time_stamp,
                'date' => $date,
                'pem_file' => 1,
               //'ride_platform' => 2
                 );
				  
            
            $last_id = $this->Booking_model->insert_ridetable($data);
            $ride_details = $this->Booking_model->get_ridedetails($last_id);
            $ride_status =$ride_details['ride_status'];
		 	
    foreach($c as $driver){
	$driver_id = $driver['driver_id'];
	//echo $driver_id;die();
	$data=array(
	           'allocated_ride_id' => $last_id,
                   'allocated_driver_id' => $driver_id,
                   'allocated_date' => $date,
	            );
	 $url = 'https://taxiuapp-4edc1.firebaseio.com/Activeride/'.$driver_id.'/.json';            
   // $url = 'https://apporio-taxi.firebaseio.com/Activeride/'.$driver_id.'/.json';
   
   
    $fields = array(
        'ride_id' => (string)$last_id,
        'ride_status'=>"1",
    );

    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
    $response = curl_exec($ch);
	
	$this->Booking_model->rideallocated($data);
    $allocated_driver = $this->Booking_model->ride_allocated($driver_id);
	    
            $device_id=$allocated_driver['device_id'];
            $lang_list=$this->Booking_model->language();  
            $driver_allocated = $this->Booking_model->driverallocated($driver_id);  
        
        if(empty($driver_allocated)){
             $data=array(
	                   'driver_id' => $driver_id,
                        'ride_id' => $last_id,
                        'ride_mode' => $ride_mode ,
	                );
             $this->Booking_model->insertdriverallocated($data);  
        }else{
              $data=array(
                        'allocated_ride_id' => $last_id,
                        'allocated_ride_status' => 0
	                );
             $this->Booking_model->update_driverallocated($data,$driver_id);    
             }

            $message=$lang_list['message_name'];
            $ride_id= (String) $last_id;
            $ride_status= (String) $ride_status;
            $pem_file=2;
            //new_ride($last_id,$driver_id);

            if($device_id!="")
            {
                if($allocated_driver['flag'] == 1)
                {
                  $this->IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);
                }
                else
                {
                   $this->AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                }
            }
          }
             $data=array(
	                'booking_id' => $last_id,
                        'ride_mode' => $ride_mode,
                        'user_id' => $user_id,
	                ); 
	                
	    
	   
	    $drivet_model_id=$driver_allocated['car_model_id'];
            $this->Booking_model->insert_user_rides($data);
            $model_finaldetail = $this->Booking_model->get_driver_model($drivet_model_id);
            $ride_finaldetail = $this->Booking_model->get_ridedetails($last_id);

        }
        else
        {
            $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
            $last_time_stamp = date("h:i:s A");
			$this->session->set_userdata(['payment_option'=>""]); 
	        $this->session->set_userdata(['coupon_code'=>""]); 
            $data=array(
                'later_date' => $date,//$datepicker,
                'later_time' => $time,//$timepicker,
                'user_id' => $user_id,
                'coupon_code' => "",
                'car_type_id' => $car_type_id,
                'pickup_lat' => $pickup_lat,
                'pickup_long' => $pickup_long,
                'pickup_location' => $pickup_location,
                'drop_lat' => $drop_lat,
                'drop_long' => $drop_long,
                'drop_location' => $drop_location,
                'ride_date' => $date1,
                'ride_time' => $time,
                'ride_type' => 1,
                'ride_image' => $image,
                'ride_status' => 1,
                'ride_admin_status' => 1,
                'last_time_stamp' => $last_time_stamp,
                'payment_option_id' => $payment_option_id,
                'card_id' => 1,
               // 'ride_platform' => 2
                 );  
                 $driver_allocated = array();
                 $ride_finaldetail =array();
                  $model_finaldetail =array();
                   
                 $this->Booking_model->insert_noridetable($data);
        }	 
	}
	$header_data=$this->User_account->Get_homepage_data();
$this->load->view('booking_waiting',['ride_finaldetail'=>$ride_finaldetail, 'model_finaldetail' =>  $model_finaldetail ,'driver_allocated'=>$driver_allocated,'header_data' => $header_data]); 
	  }
	  else
	  {
	  $about=$this->User_account->web_home();
	  return redirect('Booking_controller/search_cab_login');

	  }
	}
	
public function booking_status()
	{	 
	        $last_id = base64_decode(urldecode($this->input->get('ride_id')));
	        $header_data=$this->User_account->Get_homepage_data();
	        if( $last_id != 0){
		$ride_finaldetail = $this->Booking_model->get_ridedetails($last_id);
	        $ride_status=$ride_finaldetail['ride_status'];
	        if($ride_status == 3)
	        {
	        $driver_id = $ride_finaldetail['driver_id'];
	        $driver_allocated = $this->Booking_model->driverallocated($driver_id); 
	        $drivet_model_id=$driver_allocated['car_model_id'];
	        $model_finaldetail = $this->Booking_model->get_driver_model($drivet_model_id);
		$cancel_reason = $this->Booking_model->cancel_reasons();
	       
           redirect('Booking_controller/show_ride'.'?last='.$last_id); 	
         $this->IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);		   
		    }
	        else
	        {
			$time_check =$this->session->userdata('timer_no');
			$time_check=$time_check+1;
			$this->session->set_userdata(['timer_no' => $time_check]); 
			if($time_check == 6 )
			{
				$this->session->unset_userdata(['timer_no']);
			       
				redirect('Booking_controller/no_drivers',['header_data' => $header_data]);  
			}
			else
			{
				$this->load->view('booking_waiting',['ride_finaldetail'=>$ride_finaldetail, 'model_finaldetail' =>  $model_finaldetail ,'driver_allocated'=>$driver_allocated,'header_data' => $header_data]); 
			}
	        }  
	        }else
	        {
	        $ride_finaldetail=array('ride_id'=>0);
	        $time_check =$this->session->userdata('timer_no');
			$time_check=$time_check+1;
			$this->session->set_userdata(['timer_no' => $time_check]); 
			if($time_check == 6 )
			{
				$this->session->unset_userdata(['timer_no']);
			    
				redirect('Booking_controller/no_drivers');  
			}
			else
			{
				$this->load->view('booking_waiting',['ride_finaldetail'=>$ride_finaldetail,'header_data' => $header_data]); 
			}
	        }   
	} 

public function no_drivers()
	{	
	       $header_data=$this->User_account->Get_homepage_data();	  
	       $this->load->view('no_rides',['header_data' => $header_data]);    
	} 
	
public function show_ride()
	{	
           
	        $booking_id = $this->input->get('last');
	        $header_data=$this->User_account->Get_homepage_data();	
	        $normal_ride = $this->Booking_model->normal_ride($booking_id);
                $normal_ride= json_decode(json_encode($normal_ride), true);
               
                if(!empty($normal_ride))
                {
                    $normal_done_ride = $this->Booking_model->normal_done_ride($booking_id);
                    $normal_done_ride= json_decode(json_encode($normal_done_ride), true);
                 
                    if (!empty($normal_done_ride))
                    {
                        $begin_lat = $normal_done_ride['begin_lat'];
                        if (empty($begin_lat))
                        {
                            $begin_lat = $normal_ride['pickup_lat'];
                        }
                        $begin_long = $normal_done_ride['begin_long'];
                        if (empty($begin_long))
                        {
                            $begin_long = $normal_ride['pickup_long'];
                        }
                        $begin_location = $normal_done_ride['begin_location'];
                        if(empty($begin_location))
                        {
                            $begin_location = $normal_ride['pickup_location'];
                        }
                        $end_lat = $normal_done_ride['end_lat'];
                        if(empty($end_lat))
                        {
                            $end_lat = $normal_ride['drop_lat'];
                        }
                        $end_long = $normal_done_ride['end_long'];
                        if(empty($end_long))
                        {
                            $end_long = $normal_ride['drop_long'];
                        }
                        $end_location = $normal_done_ride['end_location'];
                        if(empty($end_location))
                        {
                            $end_location = $normal_ride['drop_location'];
                        }
                        $begin_time = $normal_done_ride['begin_time'];
                        $end_time = $normal_done_ride['end_time'];
                        $arrived_time = $normal_done_ride['arrived_time'];
                        $payment_status = $normal_done_ride['payment_status'];
                        $distance = $normal_done_ride['distance'];
                        $waiting_time = $normal_done_ride['waiting_time'];
                        $done_ride_time = $normal_done_ride['tot_time']." "."Min";
                        $time = $normal_done_ride['tot_time']." "."Min";
                        $amount = $normal_done_ride['amount'];
                        $waiting_price = $normal_done_ride['waiting_price'];
                        $ride_time_price =  $normal_done_ride['ride_time_price'];
                        $total_amount = $normal_done_ride['total_amount'];
                        $driver_name = $normal_done_ride['driver_name'];
                        $driver_image = $normal_done_ride['driver_image'];
                        $driver_rating = $normal_done_ride['rating'];
                        $driver_phone = $normal_done_ride['driver_phone'];
                        $driver_lat = $normal_done_ride['current_lat'];
                        $driver_long = $normal_done_ride['current_long'];
                        $car_number = $normal_done_ride['car_number'];
                        $driver_location = $normal_done_ride['current_location'];
                        $coupan_price = $normal_done_ride['coupan_price'];
                        $peak_time_charge = $normal_done_ride['peak_time_charge'];
                        $night_time_charge = $normal_done_ride['night_time_charge'];
                    }else{
                        $driver_iddd=$normal_ride['driver_id'];
                        $driver_details = $this->Booking_model->get_driver_details($driver_iddd);
                        $driver_details= json_decode(json_encode($driver_details), true);
                        $begin_lat = $normal_ride['pickup_lat'];
                        $begin_long = $normal_ride['pickup_long'];
                        $begin_location = $normal_ride['pickup_location'];
                        $end_lat = $normal_ride['drop_lat'];
                        $end_long = $normal_ride['drop_long'];
                        $end_location = $normal_ride['drop_location'];
                        $begin_time = "";
                        $end_time = "";
                        $arrived_time = "";
                        $payment_status = "";
                        $distance = "";
                        $waiting_time = "";
                        $done_ride_time = "";
                        $time = "";
                        $amount = "";
                        $waiting_price = "";
                        $ride_time_price =  "";
                        $coupan_price = "";
                        $total_amount = "";
                        $driver_name = $driver_details['driver_name']?$driver_details['driver_name']:'';
                        $driver_image = $driver_details['driver_image']?$driver_details['driver_image']:'';
                        $rating  = $driver_details['rating']?$driver_details['rating']:'';
                        $driver_phone = $driver_details['driver_phone']?$driver_details['driver_phone']:'';
                        $driver_lat = $driver_details['current_lat']?$driver_details['current_lat']:'';
                        $driver_long = $driver_details['current_long']?$driver_details['current_long']:'';
                        $car_number = $driver_details['car_number']?$driver_details['car_number']:'';
                        $driver_location = $driver_details['current_location']?$driver_details['current_location']:'';
                        $peak_time_charge = "";
                        $night_time_charge = "";
                    }
                    $normal_ride['pickup_lat'] = $begin_lat;
                    $normal_ride['pickup_long'] = $begin_long;
                    $normal_ride['pickup_location'] = $begin_location;
                    $normal_ride['drop_lat'] = $end_lat;
                    $normal_ride['drop_long'] = $end_long;
                    $normal_ride['drop_location'] = $end_location;
                    $normal_ride['total_amount'] = (string)$total_amount;
                    $normal_ride['begin_time'] = $begin_time;
                    $normal_ride['end_time'] = $end_time;
                    $normal_ride['arrived_time'] = $arrived_time;
                    $normal_ride['payment_status'] = $payment_status;
                    $normal_ride['distance'] = $distance;
                    $normal_ride['waiting_time'] = $waiting_time;
                    $normal_ride['done_ride_time'] = $done_ride_time;
                    $normal_ride['time'] = $time;
                    $normal_ride['amount'] = $amount;
                    $normal_ride['waiting_price'] = $waiting_price;
                    $normal_ride['ride_time_price'] = $ride_time_price;
                    $normal_ride['coupan_price'] = $coupan_price;
                    $normal_ride['driver_name'] = $driver_name;
                    $normal_ride['driver_image'] = $driver_image;
                    $normal_ride['driver_rating'] = $driver_rating;
                    $normal_ride['driver_phone'] = $driver_phone;
                    $normal_ride['driver_lat'] = $driver_lat;
                    $normal_ride['driver_long'] = $driver_long;
                    $normal_ride['car_number'] = $car_number;
                    $normal_ride['driver_location'] = $driver_location;
                    $payment_option_id = $normal_ride['payment_option_id'];
                    $payment = $this->Booking_model->payment($payment_option_id);
                    $payment = json_decode(json_encode($payment), true);
                    $payment_option_name = $payment['payment_option_name'];
                    $normal_ride['payment_option_name'] = $payment_option_name;
                    $normal_ride['peak_time_charge'] = $peak_time_charge;
                    $normal_ride['night_time_charge'] = $night_time_charge;
                    
            }  
$cancel_reason = $this->Booking_model->cancel_reasons();
$this->load->view('book_viewride',['normal_ride'=>$normal_ride,'cancel_reason'=>$cancel_reason,'header_data' => $header_data]);		 
			 
	} 
	
	
	
public function cancel_booking()
	{        
           $time = date("H:i:s");
           $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
           $data=$dt->format('M j');
           $day=date("l");
           $date1=$day.", ".$data ;
           $last_time_stamp = date("h:i:s A"); 
           $last_id = base64_decode(urldecode($this->input->get('id')));
           $this->Booking_model->cancel_ridedetails($last_id,$last_time_stamp);
           $ride_finaldetail = $this->Booking_model->get_ridedetails($last_id);
           $driver_id=$ride_finaldetail['driver_id'];
           $driver_allocated = $this->Booking_model->driverallocated($driver_id); 
        
            //$message=$lang_list['message_name'];
            $message='message';
            $ride_id= (String) $ride_finaldetail['ride_id'];
            $ride_status= (String)$ride_finaldetail['ride_status'];
            $pem_file=1;
            $device_id=$driver_allocated['device_id'];
            $drivet_model_id=$driver_allocated['car_model_id'];
	    $model_finaldetail = $this->Booking_model->get_driver_model($drivet_model_id);
	     
            if($device_id!="")
            {
                if($driver_allocated['flag'] == 1)
                {
                 $this->IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);
                }
                else
                {
                    $this->AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                }
            }
         $this->load->view('book_cancellatest',['ride_finaldetail'=>$ride_finaldetail, 'model_finaldetail' =>  $model_finaldetail ,'driver_allocated'=>$driver_allocated]); 
	}

public function cancel_booking1()
	{   
	     
           $time = date("H:i:s");
           $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
           $data=$dt->format('M j');
           $day=date("l");
           $date1=$day.", ".$data ;
           $last_time_stamp = date("h:i:s A"); 
          //$last_id = base64_decode(urldecode($this->input->get('id')));
		  $reason_id = $this->input->post('reason_id');
		  $last_id = $this->input->post('ride_id');
		 $ride_finaldetail = $this->Booking_model->get_ridedetails($last_id);
		 $driver_id=$ride_finaldetail['driver_id']; 
         $this->Booking_model->cancel_ridedetails($last_id,$last_time_stamp,$reason_id,$driver_id);
         $ride_finaldetail = $this->Booking_model->get_ridedetails($last_id);
         $ride_driverclear = $this->Booking_model->get_driverclear($last_id);
          foreach ($ride_driverclear as $value)
        {
            $driver_id = $value['driver_id'];
               $url = 'https://taxiuapp-4edc1.firebaseio.com/Activeride/'.$driver_id.'/.json';  
    $fields = array(
        'ride_id' => "No Ride",
        'ride_status'=>"No Ride Status",
    );
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
    $response = curl_exec($ch);
        }
        
         
         $driver_id=$ride_finaldetail['driver_id'];
         $driver_allocated = $this->Booking_model->driverallocated($driver_id); 
        
            //$message=$lang_list['message_name'];
            $message='message';
            $ride_id= (String) $ride_finaldetail['ride_id'];
            $ride_status= (String)$ride_finaldetail['ride_status'];
            $pem_file=1;
            $device_id=$driver_allocated['device_id'];
           
            $drivet_model_id=$driver_allocated['car_model_id'];
	    $model_finaldetail = $this->Booking_model->get_driver_model($drivet_model_id);
	     
            if($device_id != "")
            {
                if($driver_allocated['flag'] == 1)
                {
                  $this->IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);
                }
                else
                {
                    $this->AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                }
            }
           redirect('Booking_controller/user_allrides'); 
	}
	
	
	
public function user_profile()
	{   
	$user_id = $this->session->userdata('user_id');  
	$user_data = $this->Booking_model->Get_user_details($user_id);
	
	$data =  $this->Booking_model->user_rides($user_id);
	
        $ride_mode = $data['0']['ride_mode'];
        $booking_id = $data['0']['booking_id'];


            if($ride_mode == 1)
            {
                $normal_ride = $this->Booking_model->normal_ride($booking_id);
                $normal_ride= json_decode(json_encode($normal_ride), true);
                
                if(!empty($normal_ride))
                { 
                    $normal_done_ride = $this->Booking_model->normal_done_ride($booking_id);
                     $normal_done_ride= json_decode(json_encode($normal_done_ride), true);
                    if (!empty($normal_done_ride))
                    {
                        $begin_lat = $normal_done_ride['begin_lat'];
                        if (empty($begin_lat))
                        {
                            $begin_lat = $normal_ride['pickup_lat'];
                        }
                        $begin_long = $normal_done_ride['begin_long'];
                        if (empty($begin_long))
                        {
                            $begin_long = $normal_ride['pickup_long'];
                        }
                        $begin_location = $normal_done_ride['begin_location'];
                        if(empty($begin_location))
                        {
                            $begin_location = $normal_ride['pickup_location'];
                        }
                        $end_lat = $normal_done_ride['end_lat'];
                        if(empty($end_lat))
                        {
                            $end_lat = $normal_ride['drop_lat'];
                        }
                        $end_long = $normal_done_ride['end_long'];
                        if(empty($end_long))
                        {
                            $end_long = $normal_ride['drop_long'];
                        }
                        $end_location = $normal_done_ride['end_location'];
                        if(empty($end_location))
                        {
                            $end_location = $normal_ride['drop_location'];
                        }
                        $begin_time = $normal_done_ride['begin_time'];
                        $end_time = $normal_done_ride['end_time'];
                        $arrived_time = $normal_done_ride['arrived_time'];
                        $payment_status = $normal_done_ride['payment_status'];
                        $distance = $normal_done_ride['distance'];
                        $waiting_time = $normal_done_ride['waiting_time'];
                        $done_ride_time = $normal_done_ride['tot_time']." "."Min";
                        $time = $normal_done_ride['tot_time']." "."Min";
                        $amount = $normal_done_ride['amount'];
                        $waiting_price = $normal_done_ride['waiting_price'];
                        $ride_time_price =  $normal_done_ride['ride_time_price'];
                        $total_amount = $normal_done_ride['total_amount'];
                        $driver_name = $normal_done_ride['driver_name'];
                        $driver_image = $normal_done_ride['driver_image'];
                        $driver_rating = $normal_done_ride['rating'];
                        $driver_phone = $normal_done_ride['driver_phone'];
                        $driver_lat = $normal_done_ride['current_lat'];
                        $driver_long = $normal_done_ride['current_long'];
                        $car_number = $normal_done_ride['car_number'];
                        $driver_location = $normal_done_ride['current_location'];
                        $coupan_price = $normal_done_ride['coupan_price'];
                        $peak_time_charge = $normal_done_ride['peak_time_charge'];
                        $night_time_charge = $normal_done_ride['night_time_charge'];
                    }else{
                        $driver_iddd=$normal_ride['driver_id'];
                        $driver_details = $this->Booking_model->get_driver_details($driver_iddd);
                        $driver_details= json_decode(json_encode($driver_details), true);
                        $begin_lat = $normal_ride['pickup_lat'];
                        $begin_long = $normal_ride['pickup_long'];
                        $begin_location = $normal_ride['pickup_location'];
                        $end_lat = $normal_ride['drop_lat'];
                        $end_long = $normal_ride['drop_long'];
                        $end_location = $normal_ride['drop_location'];
                        $begin_time = "";
                        $end_time = "";
                        $arrived_time = "";
                        $payment_status = "";
                        $distance = "";
                        $waiting_time = "";
                        $done_ride_time = "";
                        $time = "";
                        $amount = "";
                        $waiting_price = "";
                        $ride_time_price =  "";
                        $coupan_price = "";
                        $total_amount = "";
                        $driver_name = $driver_details['driver_name']?$driver_details['driver_name']:'';
                        $driver_image = $driver_details['driver_image']?$driver_details['driver_image']:'';
                        $rating  = $driver_details['rating']?$driver_details['rating']:'';
                        $driver_phone = $driver_details['driver_phone']?$driver_details['driver_phone']:'';
                        $driver_lat = $driver_details['current_lat']?$driver_details['current_lat']:'';
                        $driver_long = $driver_details['current_long']?$driver_details['current_long']:'';
                        $car_number = $driver_details['car_number']?$driver_details['car_number']:'';
                        $driver_location = $driver_details['current_location']?$driver_details['current_location']:'';
                        $peak_time_charge = "";
                        $night_time_charge = "";
                    }
                    $normal_ride['pickup_lat'] = $begin_lat;
                    $normal_ride['pickup_long'] = $begin_long;
                    $normal_ride['pickup_location'] = $begin_location;
                    $normal_ride['drop_lat'] = $end_lat;
                    $normal_ride['drop_long'] = $end_long;
                    $normal_ride['drop_location'] = $end_location;
                    $normal_ride['total_amount'] = (string)$total_amount;
                    $normal_ride['begin_time'] = $begin_time;
                    $normal_ride['end_time'] = $end_time;
                    $normal_ride['arrived_time'] = $arrived_time;
                    $normal_ride['payment_status'] = $payment_status;
                    $normal_ride['distance'] = $distance;
                    $normal_ride['waiting_time'] = $waiting_time;
                    $normal_ride['done_ride_time'] = $done_ride_time;
                    $normal_ride['time'] = $time;
                    $normal_ride['amount'] = $amount;
                    $normal_ride['waiting_price'] = $waiting_price;
                    $normal_ride['ride_time_price'] = $ride_time_price;
                    $normal_ride['coupan_price'] = $coupan_price;
                    $normal_ride['driver_name'] = $driver_name;
                    $normal_ride['driver_image'] = $driver_image;
                    $normal_ride['driver_rating'] = $driver_rating;
                    $normal_ride['driver_phone'] = $driver_phone;
                    $normal_ride['driver_lat'] = $driver_lat;
                    $normal_ride['driver_long'] = $driver_long;
                    $normal_ride['car_number'] = $car_number;
                    $normal_ride['driver_location'] = $driver_location;
                    $payment_option_id = $normal_ride['payment_option_id'];
                    $payment = $this->Booking_model->payment($payment_option_id);
                    $payment = json_decode(json_encode($payment), true);
                    $payment_option_name = $payment['payment_option_name'];
                    $normal_ride['payment_option_name'] = $payment_option_name;
                    $normal_ride['peak_time_charge'] = $peak_time_charge;
                    $normal_ride['night_time_charge'] = $night_time_charge;
                    
            }}else{
            
                $rental_ride = $this->Booking_model->rental_ride($booking_id);
                $rental_ride= json_decode(json_encode($rental_ride), true);
            
                if (!empty($rental_ride))
                {
                    $rental_done_ride = $this->Booking_model->rental_done_ride($booking_id);
                    $rental_done_ride= json_decode(json_encode($rental_done_ride), true);
                    
                    if(!empty($rental_done_ride))
                    {
                        $begin_lat = $rental_done_ride['begin_lat'];
                        if (empty($begin_lat))
                        {
                            $begin_lat = $rental_ride['pickup_lat'];
                        }
                        $begin_long = $rental_done_ride['begin_long'];
                        if (empty($begin_long))
                        {
                            $begin_long = $rental_ride['pickup_long'];
                        }
                        $begin_location = $rental_done_ride['begin_location'];
                        if(empty($begin_location))
                        {
                            $begin_location = $rental_ride['pickup_location'];
                        }
                        $end_lat = $rental_done_ride['end_lat'];
                        $end_long = $rental_done_ride['end_long'];
                        $end_location = $rental_done_ride['end_location'];
                        $driver_arive_time = $rental_done_ride['driver_arive_time'];
                        $begin_time = $rental_done_ride['begin_time'];
                        $end_time = $rental_done_ride['end_time'];
                        $total_distance_travel = $rental_done_ride['total_distance_travel'];
                        $total_time_travel = $rental_done_ride['total_time_travel'];
                        $rental_package_price = $rental_done_ride['rental_package_price'];
                        $rental_package_hours = $rental_done_ride['rental_package_hours'];
                        $extra_hours_travel = $rental_done_ride['extra_hours_travel'];
                        $extra_hours_travel_charge = $rental_done_ride['extra_hours_travel_charge'];
                        $rental_package_distance = $rental_done_ride['rental_package_distance'];
                        $extra_distance_travel = $rental_done_ride['extra_distance_travel'];
                        $extra_distance_travel_charge = $rental_done_ride['extra_distance_travel_charge'];
                        $final_bill_amount = $rental_done_ride['final_bill_amount'];
                        $driver_name = $rental_done_ride['driver_name'];
                        $driver_image = $rental_done_ride['driver_image'];
                        $driver_rating = $rental_done_ride['rating'];
                        $driver_phone = $rental_done_ride['driver_phone'];
                        $driver_lat = $rental_done_ride['current_lat'];
                        $driver_long = $rental_done_ride['current_long'];
                        $car_number = $rental_done_ride['car_number'];
                        $driver_location = $rental_done_ride['current_location'];
                        $total_amount = $rental_done_ride['total_amount'];
                        $coupan_price = $rental_done_ride['coupan_price'];
                    }else{
                        $driver_iddd=$rental_ride['driver_id'];
                        $driver_details = $this->Booking_model->get_driver_details($driver_iddd);
                        $driver_details= json_decode(json_encode($driver_details), true);
                        $begin_lat = $rental_ride['pickup_lat'];
                        $begin_long = $rental_ride['pickup_long'];
                        $begin_location = $rental_ride['pickup_location'];
                        $end_lat = "";
                        $end_long = "";
                        $end_location = "";
                        $final_bill_amount = 0;
                        $driver_arive_time = "";
                        $begin_time = "";
                        $end_time = "";
                        $total_distance_travel = "";
                        $total_time_travel = "";
                        $rental_package_price = "";
                        $rental_package_hours = "";
                        $extra_hours_travel = "";
                        $extra_hours_travel_charge = "";
                        $rental_package_distance = "";
                        $extra_distance_travel = "";
                        $extra_distance_travel_charge = "";
                        $driver_name = $driver_details['driver_name']?$driver_details['driver_name']:'';
                        $driver_image = $driver_details['driver_image']?$driver_details['driver_image']:'';
                        $rating  = $driver_details['rating']?$driver_details['rating']:'';
                        $driver_phone = $driver_details['driver_phone']?$driver_details['driver_phone']:'';
                        $driver_lat = $driver_details['current_lat']?$driver_details['current_lat']:'';
                        $driver_long = $driver_details['current_long']?$driver_details['current_long']:'';
                        $car_number = $driver_details['car_number']?$driver_details['car_number']:'';
                        $driver_location = $driver_details['current_location']?$driver_details['current_location']:'';
                        $total_amount = "";
                        $coupan_price = "";
                    }
                    $payment_option_id = $rental_ride->payment_option_id;
                   
                    $payment = $this->Booking_model->payment($payment_option_id);
                    $payment = json_decode(json_encode($payment), true);
                    $payment_option_name = $payment['payment_option_name'];
                    $rental_ride['pickup_lat'] = $begin_lat;
                    $rental_ride['pickup_long'] = $begin_long;
                    $rental_ride['pickup_location'] = $begin_location;
                    $rental_ride['end_lat'] = $end_lat;
                    $rental_ride['end_long'] = $end_long;
                    $rental_ride['end_location'] = $end_location;
                    $rental_ride['final_bill_amount'] = (string)$final_bill_amount;
                    $rental_ride['driver_arive_time'] = $driver_arive_time;
                    $rental_ride['begin_time'] = $begin_time;
                    $rental_ride['end_time'] = $end_time;
                    $rental_ride['total_distance_travel'] = $total_distance_travel;
                    $rental_ride['total_time_travel'] = $total_time_travel;
                    $rental_ride['rental_package_price']  = $rental_package_price;
                    $rental_ride['rental_package_hours'] = $rental_package_hours;
                    $rental_ride['extra_hours_travel'] = $extra_hours_travel;
                    $rental_ride['extra_hours_travel_charge'] = $extra_hours_travel_charge;
                    $rental_ride['rental_package_distance'] = $rental_package_distance;
                    $rental_ride['extra_distance_travel'] = $extra_distance_travel;
                    $rental_ride['extra_distance_travel_charge'] = $extra_distance_travel_charge;
                    $rental_ride['driver_name'] = $driver_name;
                    $rental_ride['driver_image'] = $driver_image;
                    $rental_ride['driver_rating'] = $driver_rating;
                    $rental_ride['driver_phone'] = $driver_phone;
                    $rental_ride['driver_lat'] = $driver_lat;
                    $rental_ride['driver_long'] = $driver_long;
                    $rental_ride['car_number'] = $car_number;
                    $rental_ride['driver_location'] = $driver_location;
                    $rental_ride['total_amount'] = $total_amount;
                    $rental_ride['coupan_price'] = $coupan_price;
                    $rental_ride['payment_option_name'] = $payment_option_name;
                  
                }

            }


 $header_data=$this->User_account->Get_homepage_data();		

	 if($ride_mode == 1)
	 {

	 $type=array('modeid'=>1);
	$this->load->view('book_userprofile',['user_data'=>$user_data,'ride'=>$normal_ride,'type'=>$type,'header_data'=>$header_data]); 
	}
	else
	{
	
	$type=array('modeid' => 2);
	$this->load->view('book_userprofile',['user_data'=>$user_data,'ride'=>$rental_ride,'type'=>$type,'header_data'=>$header_data]); 
	}
	}
	
public function user_allrides()
	{   
	$user_id = $this->session->userdata('user_id');  
	$data =  $this->Booking_model->user_rides($user_id);
	
	     if(!empty($data))
            {
                foreach($data as $key=>$value){
                    $ride_mode = $value['ride_mode'];
                    $booking_id = $value['booking_id'];
                   
                    if($ride_mode == 1)
                    {
                        $normal_ride = $this->Booking_model->normal_ride($booking_id);
                        $normal_done_ride = $this->Booking_model->normal_done_ride($booking_id);
                        if (!empty($normal_done_ride))
                        { $begin_lat = $normal_done_ride->begin_lat;
                            if (empty($begin_lat))
                            {
                                $begin_lat = $normal_ride->pickup_lat;
                            }
                            $begin_long = $normal_done_ride->begin_long;
                            if (empty($begin_long))
                            {
                                $begin_long = $normal_ride->pickup_long;
                            }
                            $begin_location = $normal_done_ride->begin_location;
                            if(empty($begin_location))
                            {
                                $begin_location = $normal_ride->pickup_location;
                            }
                            $end_lat = $normal_done_ride->end_lat;
                            if(empty($end_lat))
                            {
                                $end_lat = $normal_ride->drop_lat;
                            }
                            $end_long = $normal_done_ride->end_long;
                            if(empty($end_long))
                            {
                                $end_long = $normal_ride->drop_long;
                            }
                            $end_location = $normal_done_ride->end_location;
                            if(empty($end_location))
                            {
                                $end_location = $normal_ride->drop_location;
                            }
                            $amount = $normal_done_ride->amount;
                            $waiting_price = $normal_done_ride->waiting_price;
                            $ride_time_price =  $normal_done_ride->ride_time_price;
                            $total_amount = $normal_done_ride->total_amount;
                            $coupan_price = $normal_done_ride->coupan_price;
                        }else{
                            $begin_lat = $normal_ride->pickup_lat;
                            $begin_long = $normal_ride->pickup_long;
                            $begin_location = $normal_ride->pickup_location;
                            $end_lat = $normal_ride->drop_lat;
                            $end_long = $normal_ride->drop_long;
                            $end_location = $normal_ride->drop_location;
                            $total_amount = "0";
                        }
                        $normal_ride->pickup_lat = $begin_lat;
                        $normal_ride->pickup_long = $begin_long;
                        $normal_ride->pickup_location = $begin_location;
                        $normal_ride->drop_lat = $end_lat;
                        $normal_ride->drop_long = $end_long;
                        $normal_ride->drop_location = $end_location;
                        $normal_ride->total_amount = (string)$total_amount;
                        $rental_ride = array(
                            "rental_booking_id"=> "",
                            "user_id"=> "",
                            "rentcard_id"=> "",
                            "car_type_id"=>"",
                            "booking_type"=>"",
                            "driver_id"=>"",
                            "pickup_lat"=> "",
                            "pickup_long"=> "",
                            "pickup_location"=> "",
                            "start_meter_reading"=>"",
                            "start_meter_reading_image"=> "",
                            "end_meter_reading"=> "",
                            "end_meter_reading_image"=> "",
                            "booking_date"=> "",
                            "booking_time"=> "",
                            "user_booking_date_time"=>"",
                            "last_update_time"=>"",
                            "booking_status"=>"",
                            "booking_admin_status"=>"",
                            "car_type_name"=>"",
                            "car_name_arabic"=> "",
                            "car_type_name_french"=> "",
                            "car_type_image"=> "",
                            "ride_mode"=>"",
                            "car_admin_status"=>"",
                            "user_name"=> "",
                            "user_email"=>"",
                            "user_phone"=>"",
                            "user_password"=> "",
                            "user_image"=>"",
                            "register_date"=> "",
                            "device_id"=>"",
                            "flag"=>"",
                            "referral_code"=>"",
                            "free_rides"=> "",
                            "referral_code_send"=>"",
                            "phone_verified"=>"",
                            "email_verified"=> "",
                            "password_created"=>"",
                            "facebook_id"=>"",
                            "facebook_mail"=> "",
                            "facebook_image"=> "",
                            "facebook_firstname"=> "",
                            "facebook_lastname"=> "",
                            "google_id"=> "",
                            "google_name"=> "",
                            "google_mail"=> "",
                            "google_image"=> "",
                            "google_token"=> "",
                            "facebook_token"=> "",
                            "token_created"=> "",
                            "login_logout"=> "",
                            "rating"=> "",
                            "status"=> "",
                            "end_lat"=>"",
                            "end_long"=>"",
                            "end_location"=>"",
                            "final_bill_amount"=> ""
                        );
                    }else{
                        $rental_ride = $this->Booking_model->rental_ride($booking_id);
                        
                        $rental_done_ride = $this->Booking_model->rental_done_ride($booking_id);
                        if(!empty($rental_done_ride))
                        {
                            $begin_lat = $rental_done_ride->begin_lat;
                            if (empty($begin_lat))
                            {
                                $begin_lat = $rental_ride->pickup_lat;
                            }
                            $begin_long = $rental_done_ride->begin_long;
                            if (empty($begin_long))
                            {
                                $begin_long = $rental_ride->pickup_long;
                            }
                            $begin_location = $rental_done_ride->begin_location;
                            if(empty($begin_location))
                            {
                                $begin_location = $rental_ride->pickup_location;
                            }
                            $end_lat = $rental_done_ride->end_lat;
                            $end_long = $rental_done_ride->end_long;
                            $end_location = $rental_done_ride->end_location;
                            $final_bill_amount = $rental_done_ride->final_bill_amount;
                        }else{
                            $begin_lat = $rental_ride->pickup_lat;
                            $begin_long = $rental_ride->pickup_long;
                            $begin_location = $rental_ride->pickup_location;
                            $end_lat = "";
                            $end_long = "";
                            $end_location = "";
                            $final_bill_amount = 0;
                        }
                        $rental_ride->pickup_lat = $begin_lat;
                        $rental_ride->pickup_long = $begin_long;
                        $rental_ride->pickup_location = $begin_location;
                        $rental_ride->end_lat = $end_lat;
                        $rental_ride->end_long = $end_long;
                        $rental_ride->end_location = $end_location;
                        $rental_ride->final_bill_amount = (string)$final_bill_amount;
                        $normal_ride = array(
                            "ride_id"=>"",
                            "user_id"=> "",
                            "coupon_code"=>"",
                            "pickup_lat"=>"",
                            "pickup_long"=>"",
                            "pickup_location"=>"",
                            "drop_lat"=>"",
                            "drop_long"=>"",
                            "drop_location"=>"",
                            "ride_date"=> "",
                            "ride_time"=>"",
                            "last_time_stamp"=>"",
                            "ride_image"=>"" ,
                            "later_date"=> "",
                            "later_time"=>"",
                            "driver_id"=> "",
                            "car_type_id"=>"",
                            "ride_type"=>"",
                            "ride_status"=> "",
                            "reason_id"=> "",
                            "payment_option_id"=>"",
                            "card_id"=> "",
                            "ride_admin_status"=>"",
                            "car_type_name"=> "",
                            "car_name_arabic"=>"",
                            "car_type_name_french"=>"",
                            "car_type_image"=>"",
                            "ride_mode"=> "",
                            "car_admin_status"=>"",
                            "total_amount"=>"",
                            "user_name"=> "",
                            "user_email"=>"",
                            "user_phone"=>"",
                            "user_password"=> "",
                            "user_image"=>"",
                            "register_date"=> "",
                            "device_id"=>"",
                            "flag"=>"",
                            "referral_code"=>"",
                            "free_rides"=> "",
                            "referral_code_send"=>"",
                            "phone_verified"=>"",
                            "email_verified"=> "",
                            "password_created"=>"",
                            "facebook_id"=>"",
                            "facebook_mail"=> "samirgoel3@gmail.com",
                            "facebook_image"=> "facebook imgae url need to send",
                            "facebook_firstname"=> "Samir",
                            "facebook_lastname"=> "Goel",
                            "google_id"=> "112279197400670101492",
                            "google_name"=> "samir goel",
                            "google_mail"=> "samirgoel3@gmail.com",
                            "google_image"=> "google user image",
                            "google_token"=> "",
                            "facebook_token"=> "",
                            "token_created"=> "0",
                            "login_logout"=> "1",
                            "rating"=> "4.0411764705882",
                            "status"=> "1",
                        );
                    }
          
                    $rental_ride= json_decode(json_encode($rental_ride), true);
                    $normal_ride= json_decode(json_encode($normal_ride), true);
                    $data[$key] = $value;
                    $data[$key]["Normal_Ride"] = $normal_ride;
                    $data[$key]["Rental_Ride"] = $rental_ride;
                }
                
            }
        $header_data=$this->User_account->Get_homepage_data();
	$this->load->view('book_alltrips',['data' => $data, 'header_data'=> $header_data]); 
	}
	 
	
public function view_ride()
	{
	$booking_id = base64_decode(urldecode($this->input->get('ride_id')));   
	$ride_mode = base64_decode(urldecode($this->input->get('mode'))); 
	
	$user_id = $this->session->userdata('user_id');  
	
            if($ride_mode == 1)
            {
                $normal_ride = $this->Booking_model->normal_ride($booking_id);
                $normal_ride= json_decode(json_encode($normal_ride), true);
                 
                if(!empty($normal_ride))
                {
                    $normal_done_ride = $this->Booking_model->normal_done_ride($booking_id);
                     $normal_done_ride= json_decode(json_encode($normal_done_ride), true);
                    if (!empty($normal_done_ride))
                    {
                        $begin_lat = $normal_done_ride['begin_lat'];
                        if (empty($begin_lat))
                        {
                            $begin_lat = $normal_ride['pickup_lat'];
                        }
                        $begin_long = $normal_done_ride['begin_long'];
                        if (empty($begin_long))
                        {
                            $begin_long = $normal_ride['pickup_long'];
                        }
                        $begin_location = $normal_done_ride['begin_location'];
                        if(empty($begin_location))
                        {
                            $begin_location = $normal_ride['pickup_location'];
                        }
                        $end_lat = $normal_done_ride['end_lat'];
                        if(empty($end_lat))
                        {
                            $end_lat = $normal_ride['drop_lat'];
                        }
                        $end_long = $normal_done_ride['end_long'];
                        if(empty($end_long))
                        {
                            $end_long = $normal_ride['drop_long'];
                        }
                        $end_location = $normal_done_ride['end_location'];
                        if(empty($end_location))
                        {
                            $end_location = $normal_ride['drop_location'];
                        }
                        $begin_time = $normal_done_ride['begin_time'];
                        $end_time = $normal_done_ride['end_time'];
                        $arrived_time = $normal_done_ride['arrived_time'];
                        $payment_status = $normal_done_ride['payment_status'];
                        $distance = $normal_done_ride['distance'];
                        $waiting_time = $normal_done_ride['waiting_time'];
                        $done_ride_time = $normal_done_ride['tot_time']." "."Min";
                        $time = $normal_done_ride['tot_time']." "."Min";
                        $amount = $normal_done_ride['amount'];
                        $waiting_price = $normal_done_ride['waiting_price'];
                        $ride_time_price =  $normal_done_ride['ride_time_price'];
                        $total_amount = $normal_done_ride['total_amount'];
                        $driver_name = $normal_done_ride['driver_name'];
                        $driver_image = $normal_done_ride['driver_image'];
                        $driver_rating = $normal_done_ride['rating'];
                        $driver_phone = $normal_done_ride['driver_phone'];
                        $driver_lat = $normal_done_ride['current_lat'];
                        $driver_long = $normal_done_ride['current_long'];
                        $car_number = $normal_done_ride['car_number'];
                        $driver_location = $normal_done_ride['current_location'];
                        $coupan_price = $normal_done_ride['coupan_price'];
                        $peak_time_charge = $normal_done_ride['peak_time_charge'];
                        $night_time_charge = $normal_done_ride['night_time_charge'];
                    }else{
                        $driver_iddd=$normal_ride['driver_id'];
                        $driver_details = $this->Booking_model->get_driver_details($driver_iddd);
                        $driver_details= json_decode(json_encode($driver_details), true);
                        $begin_lat = $normal_ride['pickup_lat'];
                        $begin_long = $normal_ride['pickup_long'];
                        $begin_location = $normal_ride['pickup_location'];
                        $end_lat = $normal_ride['drop_lat'];
                        $end_long = $normal_ride['drop_long'];
                        $end_location = $normal_ride['drop_location'];
                        $begin_time = "";
                        $end_time = "";
                        $arrived_time = "";
                        $payment_status = "";
                        $distance = "";
                        $waiting_time = "";
                        $done_ride_time = "";
                        $time = "";
                        $amount = "";
                        $waiting_price = "";
                        $ride_time_price =  "";
                        $coupan_price = "";
                        $total_amount = "";
                        $driver_name = $driver_details['driver_name']?$driver_details['driver_name']:'';
                        $driver_image = $driver_details['driver_image']?$driver_details['driver_image']:'';
                        $rating  = $driver_details['rating']?$driver_details['rating']:'';
                        $driver_phone = $driver_details['driver_phone']?$driver_details['driver_phone']:'';
                        $driver_lat = $driver_details['current_lat']?$driver_details['current_lat']:'';
                        $driver_long = $driver_details['current_long']?$driver_details['current_long']:'';
                        $car_number = $driver_details['car_number']?$driver_details['car_number']:'';
                        $driver_location = $driver_details['current_location']?$driver_details['current_location']:'';
                        $peak_time_charge = "";
                        $night_time_charge = "";
                    }
                    $normal_ride['pickup_lat'] = $begin_lat;
                    $normal_ride['pickup_long'] = $begin_long;
                    $normal_ride['pickup_location'] = $begin_location;
                    $normal_ride['drop_lat'] = $end_lat;
                    $normal_ride['drop_long'] = $end_long;
                    $normal_ride['drop_location'] = $end_location;
                    $normal_ride['total_amount'] = (string)$total_amount;
                    $normal_ride['begin_time'] = $begin_time;
                    $normal_ride['end_time'] = $end_time;
                    $normal_ride['arrived_time'] = $arrived_time;
                    $normal_ride['payment_status'] = $payment_status;
                    $normal_ride['distance'] = $distance;
                    $normal_ride['waiting_time'] = $waiting_time;
                    $normal_ride['done_ride_time'] = $done_ride_time;
                    $normal_ride['time'] = $time;
                    $normal_ride['amount'] = $amount;
                    $normal_ride['waiting_price'] = $waiting_price;
                    $normal_ride['ride_time_price'] = $ride_time_price;
                    $normal_ride['coupan_price'] = $coupan_price;
                    $normal_ride['driver_name'] = $driver_name;
                    $normal_ride['driver_image'] = $driver_image;
                    $normal_ride['driver_rating'] = $driver_rating;
                    $normal_ride['driver_phone'] = $driver_phone;
                    $normal_ride['driver_lat'] = $driver_lat;
                    $normal_ride['driver_long'] = $driver_long;
                    $normal_ride['car_number'] = $car_number;
                    $normal_ride['driver_location'] = $driver_location;
                    $payment_option_id = $normal_ride['payment_option_id'];
                    $payment = $this->Booking_model->payment($payment_option_id);
                    $payment = json_decode(json_encode($payment), true);
                    $payment_option_name = $payment['payment_option_name'];
                    $normal_ride['payment_option_name'] = $payment_option_name;
                    $normal_ride['peak_time_charge'] = $peak_time_charge;
                    $normal_ride['night_time_charge'] = $night_time_charge;
                    
            } }else{
            
                $rental_ride = $this->Booking_model->rental_ride($booking_id);
                $rental_ride= json_decode(json_encode($rental_ride), true);
            
                if (!empty($rental_ride))
                {
                    $rental_done_ride = $this->Booking_model->rental_done_ride($booking_id);
                    $rental_done_ride= json_decode(json_encode($rental_done_ride), true);
                    
                    if(!empty($rental_done_ride))
                    {
                        $begin_lat = $rental_done_ride['begin_lat'];
                        if (empty($begin_lat))
                        {
                            $begin_lat = $rental_ride['pickup_lat'];
                        }
                        $begin_long = $rental_done_ride['begin_long'];
                        if (empty($begin_long))
                        {
                            $begin_long = $rental_ride['pickup_long'];
                        }
                        $begin_location = $rental_done_ride['begin_location'];
                        if(empty($begin_location))
                        {
                            $begin_location = $rental_ride['pickup_location'];
                        }
                        $end_lat = $rental_done_ride['end_lat'];
                        $end_long = $rental_done_ride['end_long'];
                        $end_location = $rental_done_ride['end_location'];
                        $driver_arive_time = $rental_done_ride['driver_arive_time'];
                        $begin_time = $rental_done_ride['begin_time'];
                        $end_time = $rental_done_ride['end_time'];
                        $total_distance_travel = $rental_done_ride['total_distance_travel'];
                        $total_time_travel = $rental_done_ride['total_time_travel'];
                        $rental_package_price = $rental_done_ride['rental_package_price'];
                        $rental_package_hours = $rental_done_ride['rental_package_hours'];
                        $extra_hours_travel = $rental_done_ride['extra_hours_travel'];
                        $extra_hours_travel_charge = $rental_done_ride['extra_hours_travel_charge'];
                        $rental_package_distance = $rental_done_ride['rental_package_distance'];
                        $extra_distance_travel = $rental_done_ride['extra_distance_travel'];
                        $extra_distance_travel_charge = $rental_done_ride['extra_distance_travel_charge'];
                        $final_bill_amount = $rental_done_ride['final_bill_amount'];
                        $driver_name = $rental_done_ride['driver_name'];
                        $driver_image = $rental_done_ride['driver_image'];
                        $driver_rating = $rental_done_ride['rating'];
                        $driver_phone = $rental_done_ride['driver_phone'];
                        $driver_lat = $rental_done_ride['current_lat'];
                        $driver_long = $rental_done_ride['current_long'];
                        $car_number = $rental_done_ride['car_number'];
                        $driver_location = $rental_done_ride['current_location'];
                        $total_amount = $rental_done_ride['total_amount'];
                        $coupan_price = $rental_done_ride['coupan_price'];
                    }else{
                        $driver_iddd=$rental_ride['driver_id'];
                        $driver_details = $this->Booking_model->get_driver_details($driver_iddd);
                        $driver_details= json_decode(json_encode($driver_details), true);
                        $begin_lat = $rental_ride['pickup_lat'];
                        $begin_long = $rental_ride['pickup_long'];
                        $begin_location = $rental_ride['pickup_location'];
                        $end_lat = "";
                        $end_long = "";
                        $end_location = "";
                        $final_bill_amount = 0;
                        $driver_arive_time = "";
                        $begin_time = "";
                        $end_time = "";
                        $total_distance_travel = "";
                        $total_time_travel = "";
                        $rental_package_price = "";
                        $rental_package_hours = "";
                        $extra_hours_travel = "";
                        $extra_hours_travel_charge = "";
                        $rental_package_distance = "";
                        $extra_distance_travel = "";
                        $extra_distance_travel_charge = "";
                        $driver_name = $driver_details['driver_name']?$driver_details['driver_name']:'';
                        $driver_image = $driver_details['driver_image']?$driver_details['driver_image']:'';
                        $rating  = $driver_details['rating']?$driver_details['rating']:'';
                        $driver_phone = $driver_details['driver_phone']?$driver_details['driver_phone']:'';
                        $driver_lat = $driver_details['current_lat']?$driver_details['current_lat']:'';
                        $driver_long = $driver_details['current_long']?$driver_details['current_long']:'';
                        $car_number = $driver_details['car_number']?$driver_details['car_number']:'';
                        $driver_location = $driver_details['current_location']?$driver_details['current_location']:'';
                        $total_amount = "";
                        $coupan_price = "";
                    }
                    $payment_option_id = $rental_ride->payment_option_id;
                   
                    $payment = $this->Booking_model->payment($payment_option_id);
                    $payment = json_decode(json_encode($payment), true);
                    $payment_option_name = $payment['payment_option_name'];
                    $rental_ride['pickup_lat'] = $begin_lat;
                    $rental_ride['pickup_long'] = $begin_long;
                    $rental_ride['pickup_location'] = $begin_location;
                    $rental_ride['end_lat'] = $end_lat;
                    $rental_ride['end_long'] = $end_long;
                    $rental_ride['end_location'] = $end_location;
                    $rental_ride['final_bill_amount'] = (string)$final_bill_amount;
                    $rental_ride['driver_arive_time'] = $driver_arive_time;
                    $rental_ride['begin_time'] = $begin_time;
                    $rental_ride['end_time'] = $end_time;
                    $rental_ride['total_distance_travel'] = $total_distance_travel;
                    $rental_ride['total_time_travel'] = $total_time_travel;
                    $rental_ride['rental_package_price']  = $rental_package_price;
                    $rental_ride['rental_package_hours'] = $rental_package_hours;
                    $rental_ride['extra_hours_travel'] = $extra_hours_travel;
                    $rental_ride['extra_hours_travel_charge'] = $extra_hours_travel_charge;
                    $rental_ride['rental_package_distance'] = $rental_package_distance;
                    $rental_ride['extra_distance_travel'] = $extra_distance_travel;
                    $rental_ride['extra_distance_travel_charge'] = $extra_distance_travel_charge;
                    $rental_ride['driver_name'] = $driver_name;
                    $rental_ride['driver_image'] = $driver_image;
                    $rental_ride['driver_rating'] = $driver_rating;
                    $rental_ride['driver_phone'] = $driver_phone;
                    $rental_ride['driver_lat'] = $driver_lat;
                    $rental_ride['driver_long'] = $driver_long;
                    $rental_ride['car_number'] = $car_number;
                    $rental_ride['driver_location'] = $driver_location;
                    $rental_ride['total_amount'] = $total_amount;
                    $rental_ride['coupan_price'] = $coupan_price;
                    $rental_ride['payment_option_name'] = $payment_option_name;
                  
                }

            }
$cancel_reason = $this->Booking_model->cancel_reasons();
$header_data=$this->User_account->Get_homepage_data();
if($ride_mode == 1) {
$this->load->view('book_viewride',['normal_ride'=>$normal_ride,'cancel_reason'=>$cancel_reason,'header_data'=>$header_data]);
}else {
$this->load->view('book_viewriderental',['rental_ride'=>$rental_ride,'cancel_reason'=>$cancel_reason,'header_data'=>$header_data]);
}
	}

public function view_ratecard()
	{
	 $city_data = $this->Booking_model->Get_city();
	 $city_id=array();
	 $city=array();
	 foreach($city_data as $value)
	 {
	 $city_id[]=$value['city_id'];
	 }   
	 $city_id= array_unique($city_id);
	 foreach($city_id as $data)
	 {
	 $city[] = $this->Booking_model->Get_city_data($data);
	 } 
	 $header_data=$this->User_account->Get_homepage_data();
         $this->load->view('book_ratecard',['city'=>$city,'header_data'=>$header_data]);
	}

public function car_names(){
                     $a = $this->input->post('city_id');
                     
                     $data = $this->Booking_model->Get_carbycity($a);
                      $car_type_id=array();
                        $car_data=array();
                     foreach($data as $value)
	             {
	             $car_type_id[]=$value['car_type_id'];
	             } 
	              foreach($car_type_id as $data)
	 		{
	 		$car_data[] = $this->Booking_model->Get_car_data($data);
	 		} 
	 	echo "<option value=''> ---Please Select Car type--- </option>";	
          foreach($car_data as $data_value)
	      {
	      echo "<option value='".$data_value['car_type_id']."'>".$data_value['car_type_name']."</option>";
	      }
            }

public function car_ratecards(){
                      $car_type_id = $this->input->post('car_type_id');
                      $city_id = $this->input->post('city_id');
                      $data = $this->Booking_model->Get_pricecard($car_type_id,$city_id);
                      $base_distance_price = $data['base_distance_price'];
                      $base_distance = $data['base_distance'];
                      $base_price_per_unit = $data['base_price_per_unit'];
                      $free_ride_minutes = $data['free_ride_minutes'];
                      $price_per_ride_minute = $data['price_per_ride_minute'];
                      $free_waiting_time = $data['free_waiting_time'];
                      $wating_price_minute = $data['wating_price_minute'];
   	
   	
   	echo "<table class='rslt_table'>";
   	                        echo "<tr><td class='rt_cd_tb' colspan='2'>";
   					echo "<img src='http://apporio.org/Alakowe/images/rate_distance.png' width='20' height='20'/>  Ride Distance Charges : ";
   				echo "</td></tr>";
     			        
     			        echo "<tr>";
   		                	echo "<td> <img src='apporioinfolabs.com/apporio_web/webstatic/fuel-pump-icon-23.png'  alt='' style='width:20px; height:20px;'>Base Ride Distance Charges for      </td>";
   		                	echo "<td> €"  .$base_distance_price. " for ".$base_distance." miles</td>";
    			        echo "</tr>";
    			        
    			        echo "<tr>";
   		                echo "<td>After Base Ride Distance Charges   </td>";
   		                echo "<td> €"  .$base_price_per_unit. " per miles</td>";
    			        echo "</tr>";
    			        
    		echo "</table>";
    		echo "<table class='rslt_table'>";	         
    			          
    			        echo "<tr><td class='rt_cd_tb' colspan='2'>";
   				echo "<img src='http://apporio.org/Alakowe/images/speedometer.png' width='20' height='20'/>  Ride Time Charges : ";
   				echo "</td></tr>";
     			        echo "<tr>";
   		                echo "<td>Free Ride Time</td>";
   		                echo "<td>".$free_ride_minutes." min</td>";
    			        echo "</tr>";
    			        echo "<tr>";
   		                echo "<td>After Free Ride Time Charges</td>";
   		                echo "<td> €".$price_per_ride_minute." per min</td>";
    			        echo "</tr>"; 
    		echo "</table>";	        
    		echo "<table class='rslt_table'>";	        
    			        echo "<tr><td class='rt_cd_tb' colspan='2'>";
   				echo "<img src='http://apporio.org/Alakowe/images/ride_time.png' width='20' height='20'/>  Waiting Time Charges : ";
   				echo "</td></tr>";
     			        echo "<tr>";
   		                echo "<td>Free Waiting Time</td>";
   		                echo "<td>".$free_waiting_time." min</td>";
    			        echo "</tr>";
    			        echo "<tr>";
   		                echo "<td>After Free Waiting Time Charges</td>";
   		                echo "<td> €".$wating_price_minute." per min</td>";
    			        echo "</tr>"; 
    		echo "</table>";
    		echo "<table class='rslt_table'>";	        
    			        echo "<tr><td class='rt_cd_tb' colspan='2'>";
   				echo "<img src='http://apporio.org/Alakowe/images/ride_time.png' width='20' height='20'/>  Extra Charges :";
   				echo "</td></tr>";
     			        echo "<tr>";
   		                echo "<td>Peak Time Charges </td>";
   		                echo "<td> €</td>";
    			        echo "</tr>";
    			        echo "<tr>";
   		                echo "<td>Night Time Charges</td>";
   		                echo "<td> € </td>";
    			        echo "</tr>"; 
 	 echo "</table>";
     
     
                     }
                     
public function schedule_later(){
if($this->session->userdata('user_id') != "")
	{ 
    $user_id = $this->session->userdata('user_id'); 
    $User_details=$this->Booking_model->Get_user_details($user_id);     
    $phonecode = '+91';
    $userphone = $User_details['user_phone'];
    $phone = $phonecode.$userphone;
    $username = $User_details['user_name'];
    $email = $User_details['user_email'];
    $pickup_location =$this->session->userdata('origin_input_city');
    $drop_location = $this->session->userdata('destination_input_city');
    $car_type_id = $this->session->userdata('car_type_idcity');
    
    $payment_option_id=$this->session->userdata('payment_option');
	if($payment_option_id == "")
	{
	$payment_option_id=$this->input->post('payment_option');
	}
	if($this->session->userdata('payment_option') == ""  && $this->input->post('payment_option') == "")
	{
	$payment_option_id=1;
	}
	
	$couponcode=$this->session->userdata('coupon_code');
	if($couponcode != "")
	{
	$result = $this->Booking_model->check_code($couponcode); 
	if(empty($result))
	{
	$couponcode = "";	
	}
	}
	else
	{
	$couponcode = "";	
	}
    $date = $this->session->userdata('date');
    $time1 = $this->session->userdata('time'); 
    $pickup_lat = $this->session->userdata('lat_city');
    $pickup_long = $this->session->userdata('long_city'); 
    $drop_lat = $this->session->userdata('destlatitude_city');
    $drop_long = $this->session->userdata('destlongitude_city');
	
        $time = date("H:i:s");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date1=$day.", ".$data ;
        $last_time_stamp = date("h:i:s A");
        $driver_details = $this->Booking_model->searchnear_driver($car_type_id);
    
	
  $image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
        $last_time_stamp = date("h:i:s A");
       
        $data=array(
                'later_date' => $date,//$datepicker,
                'later_time' => $time1,//$timepicker,
                'user_id' => $user_id,
                'coupon_code' => "",
                'car_type_id' => $car_type_id,
                'pickup_lat' => $pickup_lat,
                'pickup_long' => $pickup_long,
                'pickup_location' => $pickup_location,
                'drop_lat' => $drop_lat,
                'drop_long' => $drop_long,
                'drop_location' => $drop_location,
                'ride_date' => $date1,
                'ride_time' => $time,
                'ride_type' => 2,
                'ride_image' => $image,
                'ride_status' => 1,
                'ride_admin_status' => 1,
                'last_time_stamp' => $last_time_stamp,
                'payment_option_id' => $payment_option_id ,
                'card_id' => 0,
               // 'ride_platform' => 2
                 ); 

      $last_id = $this->Booking_model->insert_ridetable($data);   
    
            }
	  else
	  {
		  echo "1";
	  }        
            }

function IphonePushNotificationDriver($did,$msg,$ride_id,$ride_status,$pem_file)
{ 
	
	
	$deviceToken = $did;
	$passphrase = '123456';
	$message = $msg;
       $pem_file=2;
	$ctx = stream_context_create();
	if($pem_file == 1){
        stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/TaxiUDriver.debug.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    }else{
        stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/TaxiUDriver.debug.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    }

	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

	//echo 'Connected to APNS' . PHP_EOL;

	// Create the payload body
	$body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status' => $ride_status);

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));
 
	// Close the connection to the server
	fclose($fp);
	
	
	
	
}




function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
	$url = 'https://fcm.googleapis.com/fcm/send';
	$app_id="2";
	$fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );
	$headers = array (
			'Authorization: key=AAAAeQhvUaw:APA91bGOUpHimzNnTUoMEwmLHOn-kYsA8iFiPSZ-dxiA7Hd9hQJzJ-W5nk1-ape27GHdviHzKQB5JnnDqaRNHGVeG2RN1QO79QLjZHwP6kysBplwckosy7npYXV_c12mzTRyO-tPd9e2',
			'Content-Type: application/json' );
	$ch = curl_init ();
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
	$result = curl_exec ( $ch );
	curl_close ( $ch );
        return $result;
}
 
public function getpicklocation()
{     
	    $origin_input = $this->input->post('origin-input');    
	    $string2 = str_replace(",", "+", urlencode($origin_input));
	
       $details_url2 = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $string2 . "&sensor=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response2 = json_decode(curl_exec($ch), true);
        $geometry2 = $response2['results'][0]['geometry'];
	    $longitude = $geometry2['location']['lng'];	
        $latitude  = $geometry2['location']['lat'];  
		 
		 $this->session->set_userdata(['lat_city'=>$latitude,'long_city'=>$longitude,'origin_input_city'=>$origin_input]); 
		 
	    redirect('Booking_controller/search_cab_city'); 
} 

public function getdroplocation()
{        
        $destination_input = $this->input->post('destination-input');       
	$string2 = str_replace(",", "+", urlencode($destination_input));
        $details_url2 = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $string2 . "&sensor=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response2 = json_decode(curl_exec($ch), true);
        $geometry2 = $response2['results'][0]['geometry'];
	    $destlongitude= $geometry2['location']['lng'];	
        $destlatitude= $geometry2['location']['lat'];
	   
	    $this->session->set_userdata(['destlatitude_city'=>$destlatitude,'destlongitude_city'=>$destlongitude,'destination_input_city'=>$destination_input]); 
 
	    redirect('Booking_controller/search_cab_city'); 
} 

public function update_mode()
{
$mode_id= $this->input->post('mode_id');
$this->session->set_userdata(['when'=>$mode_id]); 

} 
public function update_date()
{
$mode_id= $this->input->post('mode_id');
$this->session->set_userdata(['date'=>$mode_id]); 
}
public function update_time()
{
$mode_id= $this->input->post('mode_id');
$this->session->set_userdata(['time'=>$mode_id]); 
}

public function update_paymentoption()
{
$payment_option= $this->input->post('payment_option');
$this->session->set_userdata(['payment_option'=>$payment_option]); 
}

public function get_codecountry()
{	 
$id= $this->input->post('id');

$list = $this->Booking_model->search_code($id);   
echo $list['phonecode'];
}



	
public function validate_coupon()
{	     
        $coupon_code = $this->input->post('couponcode');
		$user_id = $this->session->userdata('user_id'); 
		$list = $this->Booking_model->check_code($coupon_code); 
	 
		$this->session->set_userdata(['coupon_code'=>$coupon_code]); 
	    if(!empty($list))
		{
		echo "<img src='http://apporio.org/Alakowe/webstatic/checked.png' height='20px' width='20px'/>";	 
		}
		else
		{
			if($coupon_code == "")
		{
			echo "";	 
		}
		else{
			echo "<img src='http://apporio.org/Alakowe/webstatic/error.png' height='20px' width='20px'/>";
		}	
		}
	
}	
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	