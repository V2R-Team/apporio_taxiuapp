<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Welcome extends CI_Controller {

		function __construct(){
				        parent::__construct();   
				        $this->load->model('User_account');
				        $this->load->model('Booking_model');
				        $this->load->model('Drivermodel');
				        $this->load->model('Usermodel');
				        //$Get_homedata=$this->User_account->Get_homepage_data();
				       
				       }
	 
	public function index()
	{        
	      
	        $this->session->set_userdata(['tab'=>"city"]); 
	        $Get_car_type=$this->Booking_model->Get_car_type();
		    $Get_pagedata=$this->User_account->Get_page_data();
		    $Get_homedata=$this->User_account->Get_homepage_data();
		    $Get_heading=$this->User_account->Get_heading_data();

		
		    $about=$this->User_account->web_home(); 		 
		    $this->load->view('index',['about'=>$about,'Get_car_type'=>$Get_car_type,'Get_pagedata'=>$Get_pagedata,'Get_homedata'=>$Get_homedata,'Get_heading'=>$Get_heading]);
	}
	public function index1()
	{     
               $Get_car_type=$this->Booking_model->Get_car_type();
                $Get_pagedata=$this->User_account->Get_page_data();
                $Get_homedata=$this->User_account->Get_homepage_data();
				$Get_heading=$this->User_account->Get_heading_data();
                $this->session->set_userdata(['tab'=>"rental"]); 
		$about=$this->User_account->web_home(); 		 
		$this->load->view('index1',['about'=>$about,'Get_car_type'=>$Get_car_type,'Get_pagedata'=>$Get_pagedata,'Get_homedata'=>$Get_homedata,'Get_heading'=>$Get_heading]); 
	} 
	
	public function signin()
	{    if($this->session->userdata('user') == "") { 
		$this->load->view('signin');
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	public function register()
	{      
	           if($this->session->userdata('user') == "") { 
		  $this->load->view('register');
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	
	public function blog_page()
	{      
	       $page_id = $this->input->get('page'); 
	       $page_data=$this->User_account->blog_data($page_id);  
	       $Get_heading=$this->User_account->Get_heading_data();
	    $header_data=$this->User_account->Get_homepage_data();
	       $this->load->view('blog_page',['page_data'=>$page_data, 'Get_heading' => $Get_heading,'header_data'=>$header_data]);
		
	}
	
	
	public function contact_us()
	{   
	        $about=$this->User_account->web_contact(); 
	        $header_data=$this->User_account->Get_homepage_data();
		$this->load->view('contact-us',['about'=>$about,'header_data'=>$header_data]);
	} 	
	public function about()
	{
		
		 $about_data=$this->User_account->web_about(); 
		  $header_data=$this->User_account->Get_homepage_data();
		 $this->load->view('about-us',['about_data'=>$about_data, 'header_data'=> $header_data]);
	}
	
	public function forget_update()
	{
		 $this->load->view('forgot-password');
	}
	
	
	public function forget_password()
	{      
		$this->load->view('forgot-password');
	}
	public function driver_signin()
	{     if($this->session->userdata('user') == "") { 
	        $country=$this->Booking_model->get_country();
	         $header_data=$this->User_account->Get_homepage_data();
	       $this->load->view('driver-signin',['country'=>$country,'header_data'=>$header_data]);
		 
		
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_signup()
	{      if($this->session->userdata('user') == "") { 
	        $city=$this->User_account->city(); 
	        $car_type=$this->User_account->car_type(); 
	       $country=$this->Booking_model->get_country();
	        $header_data=$this->User_account->Get_homepage_data();
	     
		$this->load->view('driver-signup',['city'=>$city,'car_type'=>$car_type,'country'=>$country,'header_data'=>$header_data]);
		}
		else
		  {
		  echo '<script>window.location="<?php echo base_url();?>"</script>';
		  }
	}
	public function rider_signin()
	{   if($this->session->userdata('user') == "") {     
		$this->load->view('rider-signin');
	}else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function rider_signup()
	{       
	        if($this->session->userdata('user') == "") { 
		$this->load->view('rider-signup');
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	public function rider_later()
	{       
	      if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id); 
	        $data=$this->User_account->rider_trips_booking_later($user_id);
	        
		$this->load->view('rider_later',['profile'=>$profile,'data'=>$data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    } 
	}
	
	
	public function terms()
	{       $header_data=$this->User_account->Get_homepage_data();
		$this->load->view('terms',['header_data'=>$header_data]);
	}
	public function rider_dashboard()
	{  
	         if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id );
	         $car_type=$this->User_account->car_type();  
		$this->load->view('rider-dashboard',['profile'=>$profile,'car_type'=>$car_type]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function rider_trips()
	{       if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id ); 
	        $data=$this->User_account->rider_rides($user_id); 
	        
	        foreach ($data as $key =>$value)
		 {
		  $category_id=$value['ride_id'];
		  $text=$this->User_account->get_doneride_id($category_id);		 
		  $data[$key] = $value;
		  $data[$key]["sub_cat"] = $text;
		 }
	        
	        
	        
	        // echo "<pre>";print_r($data);                               
		  $this->load->view('rider-trips',['profile'=>$profile,'data'=>$data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	
	public function rider_trips_booking_later()
	{       if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id); 
	        $data=$this->User_account->rider_trips_booking_later($user_id); 
	         
		     $this->load->view('rider-trips',['profile'=>$profile,'data'=>$data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	
	
	public function rider_profile()
	{      if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id ); 
	        $this->load->view('rider-profile',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function rider_change_password()
	{        
	        if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id );
		$this->load->view('rider-password',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function rider_payment()
	{
	        if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id );
		 
		$this->load->view('rider-payment',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	public function rider_coupons()
	{
	      if($this->session->userdata('user') == "rider") { 
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_rider_profile($user_id); 
	        $coupons=$this->User_account->view_rider_coupons($user_id); 
	         //print_r($coupons);die();
	       if(count($coupons))
	        {
	        $coupons_data= array();
	        foreach($coupons as $data)
	        {
	        $coupon_code=$data['coupon_code'];
	        if($coupon_code != "")
	        {
	        $coupons_data[]=$this->User_account->view_rider_coupons_data($coupon_code); 
	        }
	        }
	          
	         $this->load->view('rider-coupons',['profile'=>$profile,'coupons_data'=>$coupons_data]);
	        
	        }
	        else
	        {
	        $this->load->view('rider-coupons',['profile'=>$profile]);
	        } 
		
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	public function driver_profile()
	{       
	       if($this->session->userdata('user') == "driver") {   
	       $user_id =  $this->session->userdata('user_id');
		   
	        $profile=$this->User_account->view_driver_profile($user_id );
		 $header_data=$this->User_account->Get_homepage_data();	
		$this->load->view('driver-profile',['profile'=>$profile,'header_data'=>$header_data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_rides()
	{       
	        if($this->session->userdata('user') == "driver") {   
	        $driver_id =  $this->session->userdata('user_id');
	        
	   
            $data11 =  $this->Drivermodel->driver_rides($driver_id);
            $profile=$this->User_account->view_driver_profile($driver_id);
             if(!empty($data11))
            {
                foreach($data11 as $key=>$value){
                    $ride_mode = $value['ride_mode'];
                    $booking_id = $value['booking_id'];
                   
                    if($ride_mode == 1)
                    {
                        $normal_ride = $this->Booking_model->normal_ride($booking_id);
                        $normal_done_ride = $this->Booking_model->normal_done_ride($booking_id);
                        if (!empty($normal_done_ride))
                        { $begin_lat = $normal_done_ride->begin_lat;
                            if (empty($begin_lat))
                            {
                                $begin_lat = $normal_ride->pickup_lat;
                            }
                            $begin_long = $normal_done_ride->begin_long;
                            if (empty($begin_long))
                            {
                                $begin_long = $normal_ride->pickup_long;
                            }
                            $begin_location = $normal_done_ride->begin_location;
                            if(empty($begin_location))
                            {
                                $begin_location = $normal_ride->pickup_location;
                            }
                            $end_lat = $normal_done_ride->end_lat;
                            if(empty($end_lat))
                            {
                                $end_lat = $normal_ride->drop_lat;
                            }
                            $end_long = $normal_done_ride->end_long;
                            if(empty($end_long))
                            {
                                $end_long = $normal_ride->drop_long;
                            }
                            $end_location = $normal_done_ride->end_location;
                            if(empty($end_location))
                            {
                                $end_location = $normal_ride->drop_location;
                            }
                            $amount = $normal_done_ride->amount;
                            $waiting_price = $normal_done_ride->waiting_price;
                            $ride_time_price =  $normal_done_ride->ride_time_price;
                            $total_amount = $normal_done_ride->total_amount;
                            $coupan_price = $normal_done_ride->coupan_price;
                        }else{
                            $begin_lat = $normal_ride->pickup_lat;
                            $begin_long = $normal_ride->pickup_long;
                            $begin_location = $normal_ride->pickup_location;
                            $end_lat = $normal_ride->drop_lat;
                            $end_long = $normal_ride->drop_long;
                            $end_location = $normal_ride->drop_location;
                            $total_amount = "0";
                        }
                        $normal_ride->pickup_lat = $begin_lat;
                        $normal_ride->pickup_long = $begin_long;
                        $normal_ride->pickup_location = $begin_location;
                        $normal_ride->drop_lat = $end_lat;
                        $normal_ride->drop_long = $end_long;
                        $normal_ride->drop_location = $end_location;
                        $normal_ride->total_amount = (string)$total_amount;
                        $rental_ride = array(
                            "rental_booking_id"=> "",
                            "user_id"=> "",
                            "rentcard_id"=> "",
                            "car_type_id"=>"",
                            "booking_type"=>"",
                            "driver_id"=>"",
                            "pickup_lat"=> "",
                            "pickup_long"=> "",
                            "pickup_location"=> "",
                            "start_meter_reading"=>"",
                            "start_meter_reading_image"=> "",
                            "end_meter_reading"=> "",
                            "end_meter_reading_image"=> "",
                            "booking_date"=> "",
                            "booking_time"=> "",
                            "user_booking_date_time"=>"",
                            "last_update_time"=>"",
                            "booking_status"=>"",
                            "booking_admin_status"=>"",
                            "car_type_name"=>"",
                            "car_name_arabic"=> "",
                            "car_type_name_french"=> "",
                            "car_type_image"=> "",
                            "ride_mode"=>"",
                            "car_admin_status"=>"",
                            "user_name"=> "",
                            "user_email"=>"",
                            "user_phone"=>"",
                            "user_password"=> "",
                            "user_image"=>"",
                            "register_date"=> "",
                            "device_id"=>"",
                            "flag"=>"",
                            "referral_code"=>"",
                            "free_rides"=> "",
                            "referral_code_send"=>"",
                            "phone_verified"=>"",
                            "email_verified"=> "",
                            "password_created"=>"",
                            "facebook_id"=>"",
                            "facebook_mail"=> "",
                            "facebook_image"=> "",
                            "facebook_firstname"=> "",
                            "facebook_lastname"=> "",
                            "google_id"=> "",
                            "google_name"=> "",
                            "google_mail"=> "",
                            "google_image"=> "",
                            "google_token"=> "",
                            "facebook_token"=> "",
                            "token_created"=> "",
                            "login_logout"=> "",
                            "rating"=> "",
                            "status"=> "",
                            "end_lat"=>"",
                            "end_long"=>"",
                            "end_location"=>"",
                            "final_bill_amount"=> ""
                        );
                    }else{
                        $rental_ride = $this->Booking_model->rental_ride($booking_id);
                        
                        $rental_done_ride = $this->Booking_model->rental_done_ride($booking_id);
                        if(!empty($rental_done_ride))
                        {
                            $begin_lat = $rental_done_ride->begin_lat;
                            if (empty($begin_lat))
                            {
                                $begin_lat = $rental_ride->pickup_lat;
                            }
                            $begin_long = $rental_done_ride->begin_long;
                            if (empty($begin_long))
                            {
                                $begin_long = $rental_ride->pickup_long;
                            }
                            $begin_location = $rental_done_ride->begin_location;
                            if(empty($begin_location))
                            {
                                $begin_location = $rental_ride->pickup_location;
                            }
                            $end_lat = $rental_done_ride->end_lat;
                            $end_long = $rental_done_ride->end_long;
                            $end_location = $rental_done_ride->end_location;
                            $final_bill_amount = $rental_done_ride->final_bill_amount;
                        }else{
                            $begin_lat = $rental_ride->pickup_lat;
                            $begin_long = $rental_ride->pickup_long;
                            $begin_location = $rental_ride->pickup_location;
                            $end_lat = "";
                            $end_long = "";
                            $end_location = "";
                            $final_bill_amount = 0;
                        }
                        $rental_ride->pickup_lat = $begin_lat;
                        $rental_ride->pickup_long = $begin_long;
                        $rental_ride->pickup_location = $begin_location;
                        $rental_ride->end_lat = $end_lat;
                        $rental_ride->end_long = $end_long;
                        $rental_ride->end_location = $end_location;
                        $rental_ride->final_bill_amount = (string)$final_bill_amount;
                        $normal_ride = array(
                            "ride_id"=>"",
                            "user_id"=> "",
                            "coupon_code"=>"",
                            "pickup_lat"=>"",
                            "pickup_long"=>"",
                            "pickup_location"=>"",
                            "drop_lat"=>"",
                            "drop_long"=>"",
                            "drop_location"=>"",
                            "ride_date"=> "",
                            "ride_time"=>"",
                            "last_time_stamp"=>"",
                            "ride_image"=>"" ,
                            "later_date"=> "",
                            "later_time"=>"",
                            "driver_id"=> "",
                            "car_type_id"=>"",
                            "ride_type"=>"",
                            "ride_status"=> "",
                            "reason_id"=> "",
                            "payment_option_id"=>"",
                            "card_id"=> "",
                            "ride_admin_status"=>"",
                            "car_type_name"=> "",
                            "car_name_arabic"=>"",
                            "car_type_name_french"=>"",
                            "car_type_image"=>"",
                            "ride_mode"=> "",
                            "car_admin_status"=>"",
                            "total_amount"=>"",
                            "user_name"=> "",
                            "user_email"=>"",
                            "user_phone"=>"",
                            "user_password"=> "",
                            "user_image"=>"",
                            "register_date"=> "",
                            "device_id"=>"",
                            "flag"=>"",
                            "referral_code"=>"",
                            "free_rides"=> "",
                            "referral_code_send"=>"",
                            "phone_verified"=>"",
                            "email_verified"=> "",
                            "password_created"=>"",
                            "facebook_id"=>"",
                            "facebook_mail"=> "samirgoel3@gmail.com",
                            "facebook_image"=> "facebook imgae url need to send",
                            "facebook_firstname"=> "Samir",
                            "facebook_lastname"=> "Goel",
                            "google_id"=> "112279197400670101492",
                            "google_name"=> "samir goel",
                            "google_mail"=> "samirgoel3@gmail.com",
                            "google_image"=> "google user image",
                            "google_token"=> "",
                            "facebook_token"=> "",
                            "token_created"=> "0",
                            "login_logout"=> "1",
                            "rating"=> "4.0411764705882",
                            "status"=> "1",
                        );
                    }
          
                    $rental_ride= json_decode(json_encode($rental_ride), true);
                    $normal_ride= json_decode(json_encode($normal_ride), true);
                    $data11[$key] = $value;
                    $data11[$key]["Normal_Ride"] = $normal_ride;
                    $data11[$key]["Rental_Ride"] = $rental_ride;
                }
                
            }
	
	  $header_data=$this->User_account->Get_homepage_data();	   

	$this->load->view('driver-rides',['data11'=>$data11,'profile'=>$profile,'header_data'=>$header_data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_documents()
	{ 
	    if($this->session->userdata('user') == "driver") {  
		    
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id);
			$city_id = $profile[0]['city_id'];
			$list1=$this->User_account->document_list($city_id);
			$list = array();
		    
			$upload_count_list=$this->Booking_model->driver_upload_count($user_id);
			 
			foreach($list1 as $key => $value)
			{
			$document_id=$value['document_id'];
			$upload_list=$this->Booking_model->driver_documentchecklist($user_id,$document_id);
            $list[$key]=$value ;
			$list[$key]['uploaded']=$upload_list ;
			
			}
			$button_hide=array(count($list1),count($upload_count_list));
			
			
			if(!empty($list))
			 {
				 $total_document_need = count($list);
				 $this->User_account->update_count($total_document_need);
			 }
	     $header_data=$this->User_account->Get_homepage_data();	   

		$this->load->view('driver-documents',['profile'=>$profile,'list'=>$list,'button_hide'=>$button_hide,'header_data'=>$header_data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_earnings()
	{        if($this->session->userdata('user') == "driver") {  
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
		$this->load->view('driver-earnings',['profile'=>$profile]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_password()
	{       if($this->session->userdata('user') == "driver") {  
	        $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
	        $header_data=$this->User_account->Get_homepage_data();	
		$this->load->view('driver-password',['profile'=>$profile,'header_data'=>$header_data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	public function driver_suppourt()
	{        
	       if($this->session->userdata('user') == "driver") {  
	       $user_id =  $this->session->userdata('user_id');
	        $profile=$this->User_account->view_driver_profile($user_id );
	        $header_data=$this->User_account->Get_homepage_data();	
		$this->load->view('driver-support',['profile'=>$profile,'header_data'=>$header_data]);
		}
		else
		   {
		    echo '<script>window.location="<?php echo base_url();?>"</script>';
		    }
	}
	
	 
	
	public function car_model(){  
    	                                         $car_type_id= $this->input->post('car_type_id');
    	                                         $data = $this->User_account->get_carmodel($car_type_id);
    	                                         
    	                                          if($data)
             						{
					             echo "<option value>-------- Please Select Model --------</option>";
					            foreach($data as $data_value)
								      {
						      echo "<option value='".$data_value['car_model_id']."'>".$data_value['car_model_name']."</option>";
								      }
        					
    						}
    						}
    						
    	public function Contact_us_form()
	    {        
		            if($this->session->userdata('user') == "driver") {  
	                    $application=2;
					}
				   if($this->session->userdata('user') == "user") {
						$application=1;
		                 }
					if($this->session->userdata('user') == "") {
						$application=1;
		                 }
	                 $uname=$this->input->post('uname');
                     $email= $this->input->post('email');
                     $skypephone= $this->input->post('skypephone');
                     $query= $this->input->post('subject');
					 $date=date('l,M j,h:i A'); 
                     $data=array(
					            'application' => $application,
                                'name'=>$uname,
                                'email'=>$email,               		
                                'phone'=>$skypephone,
                                'query'=>$query,
								'date' => $date,
                                );
					
                    	         
                     $profile=$this->User_account->Contact_us_form($data);
                     $this->session->set_flashdata('Rider_success','Registred Succesfully');
		    $about=$this->User_account->web_contact(); 
		     $header_data=$this->User_account->Get_homepage_data();	
		   $this->load->view('contact-us',['about'=>$about,'header_data'=>$header_data]);
		}
		 
	 
            
	
	 public function logout()
     	 {
      	 $this->session->set_flashdata('Logout',"logout");
       	 $this->session->unset_userdata(['user','user_id','tab1','tab']);
      	 return redirect('http://apporio.org/TaxiUApp/');
         }	
    
	
	
	
}
