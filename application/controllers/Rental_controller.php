<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
date_default_timezone_set('Europe/London');
class Rental_controller extends CI_Controller {
		function __construct(){
				        parent::__construct();   
				        $this->load->model('User_account');
				        $this->load->model('Booking_model');
				        $this->load->model('Rental_model');
				        $this->load->model('Rentalmodel');
				      }	
	
public function search_cab_rental()
	{     
	         $latitude= $this->input->post('latitude');
	         $longitude= $this->input->post('longitude');
	         $origin_input= $this->input->post('origin-input');
	         $date = $this->input->post('date');
	         $when= $this->input->post('sel1');
	         $time = $this->input->post('time');
	         $about=$this->User_account->web_home(); 
		 $packages=$this->Booking_model->rental_packages(); 
	         if($latitude != "")
		 {	
	          $this->session->set_userdata(['lat_rental'=>$latitude ]);		 
                 }	
                  if($longitude != "" )
		 {	
	          $this->session->set_userdata(['long_rental'=>$longitude]);		 
                 }	
                  if($origin_input != "" )
		 {	
	          $this->session->set_userdata(['origin_input_rental'=>$origin_input]);		 
                 }	
                  if($when != "" )
		 {	
	          $this->session->set_userdata(['rentalwhen'=>$when]);		 
                 }	
                  if($date != "" )
		 {	
	          $this->session->set_userdata(['rentaldate'=>$date]);		 
                 }	
                  if($time != "" )
		 {	
	          $this->session->set_userdata(['rentaltime'=>$time]);		 
                 }	
                 $latitude =$this->session->userdata('lat_rental');  
	         $longitude = $this->session->userdata('long_rental');
		 $geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=false');
		 $output= json_decode($geocode);
	         for($j=0;$j<count($output->results[0]->address_components);$j++){
                 $cn=array($output->results[0]->address_components[$j]->types[0]);
                 if(in_array("locality", $cn))
                 {
                  $city= $output->results[0]->address_components[$j]->long_name;
                 }
                 }
               
                $city_name = $city?$city:'Gurugram'; 
            
		$city_ids= $this->Booking_model->getcity_id($city_name);
		
	        $city_id=$city_ids['city_id'];
	        
	          if($city_id != "" )
		 {	
	          $this->session->set_userdata(['rentalcity_id'=>$city_id]);		 
                 }	
	        $data = $this->Rental_model->Rental_Package($city_id);
	       $a = array();
            foreach ($data as $key=>$value)
            {
                $a[$value['rental_category_id']]['rental_category_id']=$value['rental_category_id'];
                $a[$value['rental_category_id']]['rental_category']=$value['rental_category'];
                $a[$value['rental_category_id']]['rental_category_hours'] = $value['rental_category_hours'];
                $a[$value['rental_category_id']]['rental_category_kilometer'] = $value['rental_category_kilometer']." ".$value['rental_category_distance_unit'];
		$a[$value['rental_category_id']]['rental_category_description'] = $value['rental_category_description'];
            }
            if(!empty($a)){
                $get_package_rental = array();
                foreach ($a as $c)
                {
                    $get_package_rental[] = $c;
                }
                foreach($get_package_rental as $keys=>$login)
                {
                    $rental_category_id = $login['rental_category_id'];
                    $data = $this->Rental_model->Rental_Pakage_Car($city_id,$rental_category_id);
                    $get_package_rental[$keys]=$login;
                    $get_package_rental[$keys]["Rental_Pakage_Car"]=$data;
                }
               }
		 $this->session->set_userdata('booking_tab','rental');
		 $header_data=$this->User_account->Get_homepage_data(); 		 
		 $this->load->view('booking_page_rental',['about'=>$about,'get_package_rental'=>$get_package_rental,'header_data'=>$header_data]);
	} 
	
public function getrental_cars() {
                    $rental_category_id= $this->input->post('mode_id');
	            $city_id = $this->session->userdata('rentalcity_id');     
	            $data = $this->Rental_model->Rental_Pakage_Car($city_id,$rental_category_id);
	            $this->session->set_userdata(['rental_category_id'=>$rental_category_id]);	   
	           
	           echo "<h6>Avaliable Rides (". count($data ).")</h6>";
 	           echo "<div class='car_type_full_dlts'>";
 	           foreach($data as $value){
			$site_lang =$this->session->userdata('site_lang');				
	            if($site_lang == 'french'){  			 
			 $car_name= $value['car_description_arabic'];
			 }
			 else{
				$car_name=$value['car_type_name'];
			 }
			 
  		   echo "<a href='".base_url()."index.php/Rental_controller/booking_rental?car_type=".$value['car_type_id']."&amo=".$value['price']."' onClick='checkInputs()' class='car_type_dtls bnr_a'>";
  		     
			 echo "<div class='car_type_img'><img src='".base_url($value['car_type_image'])."' alt='' width='40' height='40'></div>";
			 
			 echo "<div class='car_nm_desc'><h3 class='car_type_nm' style='margin:9px !important'>".$car_name."</h3></div>";
			 
			 echo "<div class='car_type_rgt'><strong class='car_type_price'>$ ".$value['price']."</strong><i class='car_type_next_btn fa fa-angle-right' aria-hidden='true'></i></div>";
			 
    		   
    		 echo "<div class='clear'></div>";
    		   echo "</a>";
    		    }
  		  echo "</div>";
  		   
                }
public function location()
	{       
	         $header_data=$this->User_account->Get_homepage_data(); 	
		 $this->load->view('location_rental',['header_data'=>$header_data]);
	} 
	
public function search_cab_login()
	{     
	       $url= $_SERVER['HTTP_REFERER'];
	       $this->session->set_userdata(['login_url'=>$url]);
	       $about=$this->User_account->web_home();
	       $country=$this->Booking_model->get_country();
	       $this->load->view('booking_page_login',['about'=>$about,'country'=>$country]);
	} 
	
public function search_cab_redirect()
	{
	if($this->session->userdata('booking_tab') == 'city')
	{
         redirect('Booking_controller/search_cab_city'); 
        }
        else
        {
         redirect('Booking_controller/search_cab_rental'); 
        }
        }
       
public function search_cab_otp_verify()
	{       	
	           $phone= $this->input->post('phonenum');
	           $phonecode= $this->input->post('phonecode');
	           $phonenumber=$phonecode.$phone;
		 $data = $this->Booking_model->user_check($phonenumber);
		 
		 if (empty($data))
	         {  
	          $this->session->set_userdata('user_registerphone', $phonenumber);   
	          $this->load->view('booking_page_register');
	         }
	         else
	         {
	         $this->session->set_userdata('user_registerphone', $phonenumber); 
	         $code_ui = substr( str_shuffle( str_repeat( 'abcdefghijklmnopqrstuvwxyz0123456789', 10 ) ), 0, 4 );
  	         $data = $this->Booking_model->otp_Insert($phonenumber,$code_ui);
  	         $message = urlencode('Your Alakowe Taxi Verification code is:'.$code_ui);
  	         $phonenumber = str_replace("+","",$phonenumber);
	        // $sms_res = file_get_contents("http://nimbusit.net/api.php?username=t4glambin&password=680086&sender=GLAMBN&sendto=$phonenumber&message=$message");  
	         $url="http://api.smartsmssolutions.com/smsapi.php?username=Alakowe&password=friday&sender=Alakowe&recipient=$phonenumber&message=$message";
                 $ch = curl_init();
                 curl_setopt($ch,CURLOPT_URL,$url);
                 curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                 $output=curl_exec($ch);
                 curl_close($ch);
	         $this->load->view('booking_page_otpverify1');
	         $user_id =$data[0]['user_id'];
	        // $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	         //$this->session->set_flashdata('msg', 'Login Succesful');
	        // $url=$this->session->userdata('login_url');  
	         //$this->load->view('booking_page_otpverify1');
	        // redirect($url);
	         }
	 } 
	 
public function get_value()
	{        
	         $phonenumber=$this->session->userdata('user_registerphone'); 
	         $full_name= $this->input->post('full_name');
	         $user_email= $this->input->post('user_email')?$this->input->post('user_email'):'';
	         $this->session->set_userdata(['user_full_name'=>"$full_name",'user_email'=>$user_email]); 
	         $code_ui = substr( str_shuffle( str_repeat( 'abcdefghijklmnopqrstuvwxyz0123456789', 10 ) ), 0, 4 );
  	         $data = $this->Booking_model->otp_Insert($phonenumber,$code_ui);
  	         $message = urlencode('Your Alakowe Taxi Verification code is:'.$code_ui);
  	         $phonenumber = str_replace("+","",$phonenumber);
	         //$sms_res = file_get_contents("http://nimbusit.net/api.php?username=t4glambin&password=680086&sender=GLAMBN&sendto=$phonenumber&message=$message");  
	         $url="http://api.smartsmssolutions.com/smsapi.php?username=Alakowe&password=friday&sender=Alakowe&recipient=$phonenumber&message=$message";
                 $ch = curl_init();
                 curl_setopt($ch,CURLOPT_URL,$url);
                 curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                 $output=curl_exec($ch);
                 curl_close($ch);
	         $this->load->view('booking_page_otpverify');
	         // redirect('Booking_controller/check_login_otpregister');
	} 
	
public function search_cab_register_otp()
	{    
	         $phonenumber=$this->session->userdata('user_registerphone'); 
	        // $this->session->set_userdata(['user_full_name'=>$full_name,'user_email'=>$user_email]); 
	         $this->session->set_userdata('user_registerphone', $phonenumber); 
	         $user_full_name=  $this->session->userdata('user_full_name');
	         $user_email=  $this->session->userdata('user_email');
		date_default_timezone_set('Europe/London');
	         $register_date=date('l M d');
	         $data=array(
	                   'user_type'=>4,
	                   'user_name'=>$user_full_name,
	                   'user_email'=>$user_email,
	                   'user_phone'=>$phonenumber,
	                   'register_date'=>$register_date,
	                   );
	        $user_id=$this->Booking_model->rider_signupweb($data);
	          
	     $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	     $this->session->set_flashdata('msg', 'Login Succesful');
  	        // redirect('Booking_controller/search_cab_city');
			 $url=$this->session->userdata('login_url'); 
  	          redirect($url);
	} 
	
public function search_cab_userregister()
	{       
	         $about=$this->User_account->web_home(); 
	         $rm_userphone=  $this->session->userdata('user_registerphone');
	 	 $user_full_name=  $this->session->userdata('user_full_name');
	         $user_email=  $this->session->userdata('user_email');
	    date_default_timezone_set('Europe/London');
	         $register_date=date('l M d');
	         $data=array(
	                   'user_type'=>4,
	                   'user_name'=>$user_full_name,
	                   'user_email'=>$user_email,
	                   'user_phone'=>$rm_userphone,
	                   'register_date'=>$register_date,
	                   );
	         $user_id=$this->Booking_model->rider_signupweb($data);
	          
	         $this->session->set_userdata(['user'=>'rider','user_id'=>$user_id]); 
    	         $this->session->set_flashdata('msg', 'Login Succesful');
  	         redirect('Booking_controller/search_cab_city');
		        
	} 
public function check_login_otp()
	{   
	         $about=$this->User_account->web_home(); 
	         $rm_userphone=  $this->session->userdata('user_registerphone');
	 	 $phonenumber=$rm_userphone;
	 	 $otptext= $this->input->post('otptext');
	 	 $user_detail=$this->Booking_model->otp_user_check($phonenumber,$otptext);
	 	  
	 	 if(!empty($user_detail))
	 	 {
	 	 $user_detail=$this->Booking_model->user_check($phonenumber);
	         $user_id =$user_detail[0]['user_id'];
	         $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	         $this->session->set_flashdata('msg', 'Login Succesful');
  	         $url=$this->session->userdata('login_url');  
	        // $this->load->view('booking_page_otpverify1');
	         redirect($url);
	        }else
	        {
	        $this->session->set_flashdata('otpnotmatch', 'Login Succesful');
	        //redirect('Booking_controller/search_cab_otp_verify');
	        $this->load->view('booking_page_otpverify1');
                }
  	        
	} 


public function check_login_otpregister()
	{   
	         $about=$this->User_account->web_home(); 
	         $rm_userphone=  $this->session->userdata('user_registerphone');
	 	 $phonenumber=$rm_userphone;
	 	 $otptext= $this->input->post('otptext');
	 	 $user_detail=$this->Booking_model->otp_user_check($phonenumber,$otptext);
	 	  
	 	 if(!empty($user_detail))
	 	 {
	 	 $user_detail=$this->Booking_model->user_check($phonenumber);
	         $user_id =$user_detail[0]['user_id'];
	         $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	         $this->session->set_flashdata('msg', 'Login Succesful');
  	         $url=$this->session->userdata('login_url');  
	        // $this->load->view('booking_page_otpverify1');
	         //redirect($url);
	        redirect('Booking_controller/search_cab_register_otp');
	        }else
	        {
	        $this->session->set_flashdata('otpnotmatch', 'Login Succesful');
	        //redirect('Booking_controller/search_cab_otp_verify');
	        $this->load->view('booking_page_otpverify');
                }
  	        
	} 
	
	
	



public function search_cab_user()
	{   
	         $about=$this->User_account->web_home(); 
	         $rm_userphone=  $this->session->userdata('user_registerphone');
	 	 $phonenumber=$rm_userphone;
	 	 
	         $user_detail=$this->Booking_model->user_check($phonenumber);
	         $user_id =$user_detail[0]['user_id'];
	          
	         $this->session->set_userdata(['user'=>"rider",'user_id'=>$user_id]); 
    	         $this->session->set_flashdata('msg', 'Login Succesful');
  	         
  	         echo '<script type="text/javascript">
           window.location = "http://apporio.org/TaxiUApp/index.php/Booking_controller/search_cab_city?checkk=success"
           </script>';
  	        
	} 
	
	
	
public function edit_pickuplocation()
	{   
	   $this->load->view('map');       
  	        
	} 

public function edit_mappickuplocation()
{           
	         $latlon= $this->input->post('latlon');
	         $origin_input= $this->input->post('addrress1');
	         $latlon =  str_replace("(","",$latlon); 
	         $latlon =  str_replace(")","",$latlon);
	         $latlon = (explode(",",$latlon));
	         $latitude = $latlon[0];
	         $longitude = $latlon[1];
	          $latitude= number_format($latitude, 7, '.', '');
	          $longitude = number_format($longitude , 7, '.', '');
 	  
if($origin_input != "" )
{	
	$this->session->set_userdata(['origin_input_city'=>$origin_input]);		 
}	

if($latitude != "" )
{	
	$this->session->set_userdata(['lat_city'=>$latitude,]);		 
}
if($longitude != "" )
{	
	$this->session->set_userdata(['long_city'=>$longitude,]);		 
}
 
 
	         $latitude =$this->session->userdata('lat_city');  
	         $longitude = $this->session->userdata('long_city'); 
	         $destlatitude = $this->session->userdata('destlatitude_city'); 
	         $destlongitude = $this->session->userdata('destlongitude_city');
	         $about=$this->User_account->web_home();
	          $car_type_id = $this->session->userdata('car_type_idcity');
		
	
$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=false');
 
$output= json_decode($geocode);
    for($j=0;$j<count($output->results[0]->address_components);$j++){
               $cn=array($output->results[0]->address_components[$j]->types[0]);
           if(in_array("locality", $cn))
           {
            $city= $output->results[0]->address_components[$j]->long_name;
           }
            }
          $city_name =$city;
	 
		
     if( $latitude != "" && $longitude!= "" && $destlatitude != "" && $destlongitude!= "" )
      { 
            $theta = $destlongitude - $longitude;
            $dist = sin(deg2rad($destlatitude)) * sin(deg2rad($latitude)) +  cos(deg2rad($destlatitude)) * cos(deg2rad($latitude)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $distance=$miles* 1.609344;
            $distance = round($distance,2); 
     }
     else
     {  
     $distance= '0';
     }    
 
 

      if(!empty($latitude) && !empty($longitude))
        {   
             $city_ids= $this->Booking_model->getcity_id($city_name);
	     $city_nameId=$city_ids['city_id'];
             $data = $this->Booking_model->cartype_name($car_type_id,  $city_nameId);
        } 
 
 
		if($distance != 0 )  
		 {
		 
		 $car_id=$data['car_type_id'];
		 $base_distance=$data['base_distance'];
		 $base_price_per_unit=$data['base_price_per_unit'];
		 $base_distance_price=$data['base_distance_price'];
		 if($distance > $base_distance)
		 { 
		 $distance = $distance-$base_distance;
		 $total_amount = $distance * $base_price_per_unit;
		 $total_amount = $total_amount + $base_distance_price;
		 
		 }
		 else
		 {
		 $total_amount = $base_distance_price;
		 
		 }
                  
		 $this->session->set_userdata('booking_tab','city');  
		
		}
		 
		 redirect('Booking_controller/booking_ride'.'?car_type='.$car_type_id.'&amo='.$total_amount); 	//booking_ride?car_type=30&amo=153
		 //$this->load->view('booking_page_city',['about'=>$about,'get_city_cars'=>$get_city_cars]);
} 



public function booking_rental()
	{      
	         $about=$this->User_account->web_home();
	         $payment_option=$this->Booking_model->payment_option();
	         $car_type_id= $this->input->get('car_type', TRUE);
	         $this->session->set_userdata(['rentalcartype_id'=>$car_type_id]); 
	         $latitude = $this->session->userdata('lat_rental');  
	         $longitude = $this->session->userdata('long_rental'); 
	         $city_id = $this->session->userdata('rentalcity_id'); 
	         $rental_category_id = $this->session->userdata('rental_category_id');
	         
	 $data11 = $this->Rental_model->Rental_Package_confirm($city_id,$rental_category_id);
	 $data21 = $this->Rental_model->Rental_Pakage_Car($city_id,$rental_category_id);
	 
	 $get_city_cars=array_merge($data11,$data21);
	 $this->session->set_userdata(['rent_card_id'=> $data21['0']['rentcard_id']]);	
	 $header_data=$this->User_account->Get_homepage_data(); 
$this->load->view('booking_rental',['about'=>$about,'get_city_cars'=>$get_city_cars,'payment_option'=>$payment_option,'header_data' => $header_data]);     
	} 
	
	

public function submit_confirm_booking()
	{ 
	
    date_default_timezone_set('Europe/London');
    if($this->session->userdata('user_id') != "")
{
    $user_id = $this->session->userdata('user_id'); 
    $User_details=$this->Booking_model->Get_user_details($user_id);  
    $userphone = $User_details['user_phone'];
    $phone = $userphone;
    $username = $User_details['user_name'];
    $email = $User_details['user_email'];
    $pickup_location =$this->session->userdata('origin_input_rental');
    $drop_location = $this->session->userdata('destination_input_rental');
    $car_type_id = $this->session->userdata('rentalcartype_id');
    $rent_card_id = $this->session->userdata('rent_card_id');
  
    $language_id = 1;
	$date=$this->session->userdata('rentaldate');
	if($date = "")
	{
	$date="";
	}
	$time21=$this->session->userdata('rentaltime');
	if($time21= "")
	{
	$time21="";
	}
	
        $payment_option_id=$this->session->userdata('rentalpayment_option');
	if($payment_option_id == "")
	{
	$payment_option_id=$this->input->post('payment_option');
	}
	if($this->session->userdata('rentalpayment_option') == ""  && $this->input->post('payment_option') == "")
	{
	$payment_option_id=1;
	}
	
	$couponcode=$this->session->userdata('coupon_code');
	if($couponcode != "")
	{
	$result = $this->Booking_model->check_code($couponcode); 
	if(empty($result))
	{
	$couponcode = "";	
	}
	}
	else
	{
	$couponcode = "";	
	}
       $card_id = 0 ;
       $pem_file = 1;
       $pickup_lat = $this->session->userdata('lat_rental');
       $pickup_long = $this->session->userdata('long_rental'); 
     
       $rental_category_id= $this->session->userdata('rental_category_id');
	$when = $this->session->userdata('rentalwhen');
	if($when == '')
	{
	$ride_mode=1;
	}
	if($when == 'now')
	{
	$ride_mode=1;
	}
	if($when == 'other')
	{
	$ride_mode=2;	
	}
        $time = date("H:i:s");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date1=$day.", ".$data ;
        $last_time_stamp = date("h:i:s A");
       
        $driver_details = $this->Booking_model->searchnear_driver($car_type_id);
        $drivers = $this->Rental_model->nearest_driver($car_type_id);
       
        $date_add=$date1.",".$time;   
       if (!empty($drivers))
                { 
                    foreach($drivers as $login3)
                    {
                        $driver_lat = $login3['current_lat'];
                        $driver_long =$login3['current_long'];

                        $theta = $pickup_long - $driver_long;
 $dist = sin(deg2rad($pickup_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($pickup_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
                        $dist = acos($dist);
                        $dist = rad2deg($dist);
                        $miles = $dist * 60 * 1.1515;
                        $unit = strtoupper("K");
                        if ($unit == "K")
                        {
                            $km=$miles* 1.609344;
                        }
                        else if ($unit == "N")
                        {
                            $miles * 0.8684;
                        }
                        else
                        {
                            $miles;
                        }
                        if($km <= 10)
                        {
                            $c[] = array("driver_id"=> $login3['driver_id'],"distance" => $km,);
                        }
                        
                    }

                if(!empty($c))
                    {
                        foreach ($c as $value)
                        {
                            $distance[] = $value['distance'];
                        }
                        array_multisort($distance,SORT_ASC,$c);
                      
                        $driver_id = $c[0]['driver_id'];
              //  $driver_id=790;   
                  
                        $data = array(
                            'user_id'=>$user_id,
                            'rentcard_id'=>$rent_card_id,
                            'booking_type'=>1,
                            'pickup_lat'=>$pickup_lat,
                            'car_type_id'=>$car_type_id,
                            'pickup_long'=>$pickup_long,
                            'pickup_location'=>$pickup_location,
                            'booking_date'=>$date1,
                            'booking_time'=>$time,
                            'user_booking_date_time'=>$date_add,
                            'last_update_time'=>$last_time_stamp,
                            'booking_status'=>10,
			                'coupan_code'=>$couponcode,
			                'payment_option_id'=>$payment_option_id,
                            'pem_file'=>$pem_file,
                          //  'ride_platform' => 2
                        );
                    
                        $text = $this->Rental_model->booking_now($data);
                        $rental_booking_id = $text->rental_booking_id;
                        $ride_finaldetail=array('rental_booking_id' => $rental_booking_id);
                         
                        $url = 'https://taxiuapp-4edc1.firebaseio.com/Activeride/'.$driver_id.'/.json';  
                         $fields = array(
           			       'ride_id' => (string)$rental_booking_id,
          			       'ride_status'=>"10",
       				       );

      			     $ch = curl_init();
      				  curl_setopt($ch,CURLOPT_URL, $url);
       				  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      				  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      				  curl_setopt($ch,CURLOPT_POST, count($fields));
      				  curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
        			  $response = curl_exec($ch);
      
                        $this->Rental_model->booking_allocated($rental_booking_id,$driver_id,$user_id);
                        $driver = $this->Rental_model->driver_profile($driver_id);
                        $device_id = $driver->device_id;

                        $message = "New Rentel Booking";
                        $ride_id = (String) $rental_booking_id;
                        $ride_status= (String) 10;
                        $pem =  1;
                        if($device_id!="")
                        {
                            if($driver->flag == 1)
                            {
                                $this->IphonePushNotificationDriver($device_id,$message,$ride_id,$ride_status,$pem);
                                
                            }
                            else
                            {
                                $this->AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                   }
                   }
        $header_data=$this->User_account->Get_homepage_data();
	$this->load->view('booking_waitingrental',['ride_finaldetail'=>$ride_finaldetail,'header_data'=>$header_data]); 
	  }
	  else
	  {
	  $about=$this->User_account->web_home();
          redirect('Booking_controller/search_cab_login'); 
     
	  }
	}
	
public function booking_status()
	{	 
	        $last_id = base64_decode(urldecode($this->input->get('ride_id')));
	       
	        if( $last_id != 0){
	        
		$ride_finaldetail = $this->Rental_model->get_ridedetails($last_id);
		
	        $ride_status=$ride_finaldetail['booking_status'];
	        
	        if($ride_status == 11)
	        {
	        $driver_id = $ride_finaldetail['driver_id'];
	        $driver_allocated = $this->Booking_model->driverallocated($driver_id); 
	        $drivet_model_id=$driver_allocated['car_model_id'];
	        $model_finaldetail = $this->Booking_model->get_driver_model($drivet_model_id);
		$cancel_reason = $this->Booking_model->cancel_reasons();
                redirect('Rental_controller/show_ride'.'?last='.$last_id); 	
                $this->IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);		   
		}
	        else
	        {
			$time_check =$this->session->userdata('timer_no');
			$time_check=$time_check+1;
			$this->session->set_userdata(['timer_no' => $time_check]); 
			if($time_check == 6 )
			{
			$this->session->unset_userdata(['timer_no']);
			redirect('Rental_controller/no_drivers');  
			}
			else
			{
	$header_data=$this->User_account->Get_homepage_data();		 
$this->load->view('booking_waitingrental',['ride_finaldetail'=>$ride_finaldetail, 'model_finaldetail' =>  $model_finaldetail ,'driver_allocated'=>$driver_allocated,'header_data'=>$header_data]); 
			}
	        }  
	        }else
	        {
	          
	        $ride_finaldetail=array('ride_id'=>0);
	        $time_check =$this->session->userdata('timer_no');
			$time_check=$time_check+1;
			$this->session->set_userdata(['timer_no' => $time_check]); 
			if($time_check == 6 )
			{
				$this->session->unset_userdata(['timer_no']);
				redirect('Rental_controller/no_drivers');  
			}
			else
			{
				$header_data=$this->User_account->Get_homepage_data();	
				$this->load->view('booking_waitingrental',['ride_finaldetail'=>$ride_finaldetail,'header_data'=>$header_data]); 
			}
	        }   
	} 

public function no_drivers()
	{	
	       $header_data=$this->User_account->Get_homepage_data();		  
	       $this->load->view('no_riderental',['header_data'=>$header_data]);    
	} 
	
public function show_ride()
	{	
	$booking_id = $this->input->get('last');
	$user_id = $this->session->userdata('user_id');  
	$cancel_reason = $this->Booking_model->cancel_reasons();
	
	
	       $rental_ride = $this->Booking_model->rental_ride($booking_id);
         
                if (!empty($rental_ride))
                {
                    $rental_done_ride = $this->Booking_model->rental_done_ride($booking_id);
                   
                    $rental_done_ride= json_decode(json_encode($rental_done_ride), true);
                    $rental_ride= json_decode(json_encode($rental_ride), true);
                    
                    if(!empty($rental_done_ride))
                    {
                        $begin_lat = $rental_done_ride['begin_lat'];
                        if (empty($begin_lat))
                        {
                            $begin_lat = $rental_ride['pickup_lat'];
                        }
                        $begin_long = $rental_done_ride['begin_long'];
                        if (empty($begin_long))
                        {
                            $begin_long = $rental_ride['pickup_long'];
                        }
                        $begin_location = $rental_done_ride['begin_location'];
                        if(empty($begin_location))
                        {
                            $begin_location = $rental_ride['pickup_location'];
                        }
                        $end_lat = $rental_done_ride['end_lat'];
                        $end_long = $rental_done_ride['end_long'];
                        $end_location = $rental_done_ride['end_location'];
                        $driver_arive_time = $rental_done_ride['driver_arive_time'];
                        $begin_time = $rental_done_ride['begin_time'];
                        $end_time = $rental_done_ride['end_time'];
                        $total_distance_travel = $rental_done_ride['total_distance_travel'];
                        $total_time_travel = $rental_done_ride['total_time_travel'];
                        $rental_package_price = $rental_done_ride['rental_package_price'];
                        $rental_package_hours = $rental_done_ride['rental_package_hours'];
                        $extra_hours_travel = $rental_done_ride['extra_hours_travel'];
                        $extra_hours_travel_charge = $rental_done_ride['extra_hours_travel_charge'];
                        $rental_package_distance = $rental_done_ride['rental_package_distance'];
                        $extra_distance_travel = $rental_done_ride['extra_distance_travel'];
                        $extra_distance_travel_charge = $rental_done_ride['extra_distance_travel_charge'];
                        $final_bill_amount = $rental_done_ride['final_bill_amount'];
                        $driver_name = $rental_done_ride['driver_name'];
                        $driver_image = $rental_done_ride['driver_image'];
                        $driver_rating = $rental_done_ride['rating'];
                        $driver_phone = $rental_done_ride['driver_phone'];
                        $driver_lat = $rental_done_ride['current_lat'];
                        $driver_long = $rental_done_ride['current_long'];
                        $car_number = $rental_done_ride['car_number'];
                        $driver_location = $rental_done_ride['current_location'];
                        $total_amount = $rental_done_ride['total_amount'];
                        $coupan_price = $rental_done_ride['coupan_price'];
                    }else{
                        $driver_iddd=$rental_ride['driver_id'];
                        $driver_details = $this->Booking_model->get_driver_details($driver_iddd);
                        $driver_details= json_decode(json_encode($driver_details), true);
                     
                        $begin_lat = $rental_ride['pickup_lat'];
                        $begin_long = $rental_ride['pickup_long'];
                        $begin_location = $rental_ride['pickup_location'];
                        $end_lat = "";
                        $end_long = "";
                        $end_location = "";
                        $final_bill_amount = 0;
                        $driver_arive_time = "";
                        $begin_time = "";
                        $end_time = "";
                        $total_distance_travel = "";
                        $total_time_travel = "";
                        $rental_package_price = "";
                        $rental_package_hours = "";
                        $extra_hours_travel = "";
                        $extra_hours_travel_charge = "";
                        $rental_package_distance = "";
                        $extra_distance_travel = "";
                        $extra_distance_travel_charge = "";                      
                        $driver_name = $driver_details['driver_name']?$driver_details['driver_name']:'';
                        $driver_image = $driver_details['driver_image']?$driver_details['driver_image']:'';
                        $rating  = $driver_details['rating']?$driver_details['rating']:'';
                        $driver_phone = $driver_details['driver_phone']?$driver_details['driver_phone']:'';
                        $driver_lat = $driver_details['current_lat']?$driver_details['current_lat']:'';
                        $driver_long = $driver_details['current_long']?$driver_details['current_long']:'';
                        $car_number = $driver_details['car_number']?$driver_details['car_number']:'';
                        $driver_location = $driver_details['current_location']?$driver_details['current_location']:'';
                      
                        $total_amount = "";
                        $coupan_price = "";
                    }
                    $payment_option_id = $rental_ride->payment_option_id;
                   
                    $payment = $this->Booking_model->payment($payment_option_id);
                    $payment = json_decode(json_encode($payment), true);
                    $payment_option_name = $payment['payment_option_name'];
                    $rental_ride['pickup_lat'] = $begin_lat;
                    $rental_ride['pickup_long'] = $begin_long;
                    $rental_ride['pickup_location'] = $begin_location;
                    $rental_ride['end_lat'] = $end_lat;
                    $rental_ride['end_long'] = $end_long;
                    $rental_ride['end_location'] = $end_location;
                    $rental_ride['final_bill_amount'] = (string)$final_bill_amount;
                    $rental_ride['driver_arive_time'] = $driver_arive_time;
                    $rental_ride['begin_time'] = $begin_time;
                    $rental_ride['end_time'] = $end_time;
                    $rental_ride['total_distance_travel'] = $total_distance_travel;
                    $rental_ride['total_time_travel'] = $total_time_travel;
                    $rental_ride['rental_package_price']  = $rental_package_price;
                    $rental_ride['rental_package_hours'] = $rental_package_hours;
                    $rental_ride['extra_hours_travel'] = $extra_hours_travel;
                    $rental_ride['extra_hours_travel_charge'] = $extra_hours_travel_charge;
                    $rental_ride['rental_package_distance'] = $rental_package_distance;
                    $rental_ride['extra_distance_travel'] = $extra_distance_travel;
                    $rental_ride['extra_distance_travel_charge'] = $extra_distance_travel_charge;
                    $rental_ride['driver_name'] = $driver_name;
                    $rental_ride['driver_image'] = $driver_image;
                    $rental_ride['driver_rating'] = $driver_rating;
                    $rental_ride['driver_phone'] = $driver_phone;
                    $rental_ride['driver_lat'] = $driver_lat;
                    $rental_ride['driver_long'] = $driver_long;
                    $rental_ride['car_number'] = $car_number;
                    $rental_ride['driver_location'] = $driver_location;
                    $rental_ride['total_amount'] = $total_amount;
                    $rental_ride['coupan_price'] = $coupan_price;
                    $rental_ride['payment_option_name'] = $payment_option_name;
                  
                }

	
	
	
	   $cancel_reason = $this->Booking_model->cancel_reasons();
	   $header_data=$this->User_account->Get_homepage_data();	
$this->load->view('book_viewriderental',['rental_ride'=>$rental_ride,'cancel_reason'=> $cancel_reason, 'header_data'=> $header_data]);		 
			 
			 
		 
			 
	} 
	
	
	
public function cancel_booking()
	{        
           $time = date("H:i:s");
           $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
           $data=$dt->format('M j');
           $day=date("l");
           $date1=$day.", ".$data ;
           $last_time_stamp = date("h:i:s A"); 
           $last_id = base64_decode(urldecode($this->input->get('id')));
           $this->Booking_model->cancel_ridedetails($last_id,$last_time_stamp);
           $ride_finaldetail = $this->Booking_model->get_ridedetails($last_id);
           $driver_id=$ride_finaldetail['driver_id'];
           $driver_allocated = $this->Booking_model->driverallocated($driver_id); 
        
            //$message=$lang_list['message_name'];
            $message='message';
            $ride_id= (String) $ride_finaldetail['ride_id'];
            $ride_status= (String)$ride_finaldetail['ride_status'];
            $pem_file=1;
            $device_id=$driver_allocated['device_id'];
            $drivet_model_id=$driver_allocated['car_model_id'];
	    $model_finaldetail = $this->Booking_model->get_driver_model($drivet_model_id);
	     
            if($device_id!="")
            {
                if($driver_allocated['flag'] == 1)
                {
                 $this->IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);
                }
                else
                {
                    $this->AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                }
            }
         $this->load->view('book_cancellatest',['ride_finaldetail'=>$ride_finaldetail, 'model_finaldetail' =>  $model_finaldetail ,'driver_allocated'=>$driver_allocated]); 
	}

public function cancel_booking1()
	{       
      $reason_id = $this->input->post('reason_id');
	   $rental_booking_id = $this->input->post('ride_id');
	   $user_id = $this->session->userdata('user_id'); 
	    $user_id = $this->input->post('user_id');
	 //  $cancel_reason_id = $this->post('cancel_reason_id');
      
            $last_time_stamp = date("h:i:s A");
             
            $text = array(
                'last_update_time'=>$last_time_stamp,
                'booking_status'=>15
            );
            
            $data = $this->Rentalmodel->ride_status_change($rental_booking_id,$text);
            
            if (!empty($data))
            {
                $driver_id = $data->driver_id;
                $pem_file = $data->pem_file;
                
                    $driver = $this->Rentalmodel->driver_profile($driver_id);
                    
                    $did = $driver->device_id;
                    $message = "Ride Cancel By User";
                    $ride_id= (String) $rental_booking_id;
                    $ride_status= (String) 15;
                    if($did !="")
                    {
                        if($driver->flag == 1)
                        {
                            $this->IphonePushNotificationDriver($did,$message,$ride_id,$ride_status,$pem_file);
                        }
                        else
                        {
                            $this->AndroidPushNotificationDriver($did,$message,$ride_id,$ride_status);
                        }
                    }
           
	}
	redirect('Booking_controller/user_allrides'); 
	}
	
	
	
public function user_profile()
	{   
	$user_id = $this->session->userdata('user_id');  
	$user_data = $this->Booking_model->Get_user_details($user_id);
	$ride_data1 = $this->Booking_model->lastride_data($user_id); 
	$ride_data2 = $this->Booking_model->lastride_data1($user_id);
     
	$ridenow=$ride_data1['ride_id'];
	$ridelater=$ride_data2['ride_id'];
	$ridenow = $ridenow?$ridenow:0;
	$ridelater = $ridelater?$ridelater:0;
	 
	if( $ridenow > $ridelater )
		{ 
		$ride_data= $ride_data1;	
		}
	else
		{ 
		$ride_data= $ride_data2;	
		}
	 
	    $driver_id=$ride_data['driver_id'];
        $driver_allocated = $this->Booking_model->driverallocated($driver_id); 
        $drivet_model_id=$driver_allocated['car_model_id'];
        if($drivet_model_id == "")
        { 
        $drivet_model_id=$ride_data['car_type_id'];
        $model_finaldetail = $this->Booking_model->get_driver_type($drivet_model_id);
        }else
        {
	$model_finaldetail = $this->Booking_model->get_driver_model($drivet_model_id);
	}
	$ride_id= $ride_data['ride_id'];
	if($ride_data['ride_status'] == '7')
	 {
	 $done_rideinfo = $this->Booking_model->done_rideinfo($ride_id);
	 }
	 if($ride_data['ride_status'] != '7')
	 {
	 $done_rideinfo=array();
	 }
	 
	$this->load->view('book_userprofile',['user_data'=>$user_data,'ride_data'=>$ride_data, 'model_finaldetail' =>  $model_finaldetail ,'driver_allocated'=>$driver_allocated,'done_rideinfo'=>$done_rideinfo]); 
	}
	
public function user_allrides()
	{   
	$user_id = $this->session->userdata('user_id');  
	$user_data = $this->Booking_model->Get_user_details($user_id);
	$ride_data1 = $this->Booking_model->allrides_data($user_id); 
	$ride_data2 = $this->Booking_model->allrides_data1($user_id);
	$ride_data=array_merge($ride_data1,$ride_data2);
	$complete_array=array();
	foreach($ride_data as $key=>$data)
	{
	 $driver_id=$data['driver_id'];
	 $ride_status = $data['ride_status'];
	 $ride_id = $data['ride_id'];
	 
	 if($ride_status == '7' )
	 {
	 $done_rideinfo = $this->Booking_model->done_rideinfo($ride_id);
	 }
	 if($ride_status != '7')
	 {
	 $done_rideinfo=array();
	 }
	  
	 $driver_allocated = $this->Booking_model->driverallocated($driver_id);
	 $drivet_model_id=$driver_allocated['car_model_id'];

	if($drivet_model_id == "")
        { 
        $drivet_model_id=$data['car_type_id'];
        $model_finaldetail = $this->Booking_model->get_driver_type($drivet_model_id);
        }else
        {
	$model_finaldetail = $this->Booking_model->get_driver_model($drivet_model_id);
	}
	 
	 $complete_array[$key]=$data;			 
         $complete_array[$key]["driver_data"]= $driver_allocated; 
         $complete_array[$key]["amount_data"]= $done_rideinfo; 
         $complete_array[$key]["model_data"]= $model_finaldetail;
	}
	$this->load->view('book_alltrips',['user_data'=>$user_data,'complete_array'=>$complete_array]); 
	}
	 
	
public function view_ride()
	{
	$last_id = base64_decode(urldecode($this->input->get('ride_id')));   
	$user_id = $this->session->userdata('user_id');  
	$user_data = $this->Booking_model->Get_user_details($user_id);
	$ride_finaldetail = $this->Booking_model->get_ridedetails($last_id);
	$cancel_reason = $this->Booking_model->cancel_reasons();
	 
	 $ride_status = $ride_finaldetail['ride_status'];
	 $ride_id = $ride_finaldetail['ride_id'];
	 
	 if($ride_status == '7')
	 {
	 $done_rideinfo = $this->Booking_model->done_rideinfo($ride_id);
	 //print_r($done_rideinfo);die();
	 }
	 if($ride_status != '7')
	 {
	 $done_rideinfo=array();
	 }
	 
	$driver_id=$ride_finaldetail['driver_id'];
	 
    $driver_allocated = $this->Booking_model->driverallocated($driver_id); 
	
	$drivet_model_id=$driver_allocated['car_model_id'];
	if($drivet_model_id == "")
        { 
        $drivet_model_id=$ride_finaldetail['car_type_id'];
        $model_finaldetail = $this->Booking_model->get_driver_type($drivet_model_id);
        }else
        {
	$model_finaldetail = $this->Booking_model->get_driver_model($drivet_model_id);
	}
	 
	//print_r($ride_finaldetail);die(); 
	 
$this->load->view('book_viewride',['ride_finaldetail'=>$ride_finaldetail,'model_finaldetail'=>$model_finaldetail,'driver_allocated'=>$driver_allocated,'done_rideinfo'=>$done_rideinfo,'user_data'=>$user_data,'cancel_reason'=>$cancel_reason]);
	}

public function view_ratecard()
	{
	 $city_data = $this->Booking_model->Get_city();
	 $city_id=array();
	 $city=array();
	 foreach($city_data as $value)
	 {
	 $city_id[]=$value['city_id'];
	 }   
	 $city_id= array_unique($city_id);
	 foreach($city_id as $data)
	 {
	 $city[] = $this->Booking_model->Get_city_data($data);
	 } 
	 
         $this->load->view('book_ratecard',['city'=>$city]);
	}

public function car_names(){
                     $a = $this->input->post('city_id');
                     
                     $data = $this->Booking_model->Get_carbycity($a);
                      $car_type_id=array();
                        $car_data=array();
                     foreach($data as $value)
	             {
	             $car_type_id[]=$value['car_type_id'];
	             } 
	              foreach($car_type_id as $data)
	 		{
	 		$car_data[] = $this->Booking_model->Get_car_data($data);
	 		} 
	 	echo "<option value=''> ---Please Select Car type--- </option>";	
          foreach($car_data as $data_value)
	      {
	      echo "<option value='".$data_value['car_type_id']."'>".$data_value['car_type_name']."</option>";
	      }
            }

public function car_ratecards(){
                      $car_type_id = $this->input->post('car_type_id');
                      $city_id = $this->input->post('city_id');
                      $data = $this->Booking_model->Get_pricecard($car_type_id,$city_id);
                      $base_distance_price = $data['base_distance_price'];
                      $base_distance = $data['base_distance'];
                      $base_price_per_unit = $data['base_price_per_unit'];
                      $free_ride_minutes = $data['free_ride_minutes'];
                      $price_per_ride_minute = $data['price_per_ride_minute'];
                      $free_waiting_time = $data['free_waiting_time'];
                      $wating_price_minute = $data['wating_price_minute'];
   	echo "<table>";
   	                        echo "<tr>";
   				echo "<th> Ride Distance Charges : </th>";
   				echo "</tr>";
     			        echo "<tr>";
   		                echo "<td> <img src='apporioinfolabs.com/apporio_web/webstatic/fuel-pump-icon-23.png'  alt='' style='width:20px; height:20px;'>Base Ride Distance Charges for      </td>";
   		                echo "<td> € "  .$base_distance_price. " for ".$base_distance." miles</td>";
    			        echo "</tr>";
    			        echo "<tr>";
   		                echo "<td>After Base Ride Distance Charges   </td>";
   		                echo "<td>€ "  .$base_price_per_unit. " per miles</td>";
    			        echo "</tr>"; 
    			          
    			        echo "<tr>";
   				echo "<th>Ride Time Charges : </th>";
   				echo "</tr>";
     			        echo "<tr>";
   		                echo "<td>Free Ride Time</td>";
   		                echo "<td>".$free_ride_minutes." min</td>";
    			        echo "</tr>";
    			        echo "<tr>";
   		                echo "<td>After Free Ride Time Charges</td>";
   		                echo "<td> € ".$price_per_ride_minute." per min</td>";
    			        echo "</tr>"; 
    			        
    			        
    			        echo "<tr>";
   				echo "<th>Waiting Time Charges : </th>";
   				echo "</tr>";
     			        echo "<tr>";
   		                echo "<td>Free Waiting Time</td>";
   		                echo "<td>".$free_waiting_time." min</td>";
    			        echo "</tr>";
    			        echo "<tr>";
   		                echo "<td>After Free Waiting Time Charges</td>";
   		                echo "<td> € ".$wating_price_minute." per min</td>";
    			        echo "</tr>"; 
    			        
    			        
    			        echo "<tr>";
   				echo "<th>Extra Charges : </th>";
   				echo "</tr>";
     			        echo "<tr>";
   		                echo "<td>Peak Time Charges </td>";
   		                echo "<td> €</td>";
    			        echo "</tr>";
    			        echo "<tr>";
   		                echo "<td>Night Time Charges</td>";
   		                echo "<td> € </td>";
    			        echo "</tr>"; 
    			        
  		      
 	 echo "</table>";
     
     
                     }
                     
public function schedule_later(){
if($this->session->userdata('user_id') != "")
	{ 
    $user_id = $this->session->userdata('user_id'); 
    $User_details=$this->Booking_model->Get_user_details($user_id);     
    //$phonecode = '+91';
    $userphone = $User_details['user_phone'];
    $phone = $userphone;
    $username = $User_details['user_name'];
    $email = $User_details['user_email'];
    $pickup_location =$this->session->userdata('origin_input_rental');
    //$drop_location = $this->session->userdata('destination_input_city');
    $car_type_id = $this->session->userdata('rentalcartype_id');
    //$ridus = $_POST['radius'];
    //$driver = $_POST['driver'];
    $language_id=1;
  
    $card_id= 0 ;
    $pem_file = 1;
    $date = $this->session->userdata('rentaldate');
    $time1 = $this->session->userdata('rentaltime');
    $rental_category_id= $this->session->userdata('rental_category_id');
    $payment_option_id = $this->session->userdata('rentalpayment_option');
    if($payment_option_id == "")
	{
	$payment_option_id=$this->input->post('payment_option');
	}
	if($this->session->userdata('rentalpayment_option') == ""  && $this->input->post('payment_option') == "")
	{
	$payment_option_id=1;
	}
	
   $couponcode=$this->session->userdata('coupon_code');
	if($couponcode != "")
	{
	$result = $this->Booking_model->check_code($couponcode); 
	if(empty($result))
	{
	$couponcode = "";	
	}
	}
	else
	{
	$couponcode = "";	
	}
    $date_add = $date.",".$time1;
    $pickup_lat = $this->session->userdata('lat_rental');
    $pickup_long = $this->session->userdata('long_rental'); 
    //$drop_lat = $this->session->userdata('destlatitude_city');
    //$drop_long = $this->session->userdata('destlongitude_city');
	
        $time = date("H:i:s");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date1=$day.", ".$data ;
        $last_time_stamp = date("h:i:s A");
        $driver_details = $this->Booking_model->searchnear_driver($car_type_id);
    
	
$image ="https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|".$pickup_lat.",".$pickup_long."&markers=color:red|label:D|".$drop_lat.",".$drop_long."&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4";
        $last_time_stamp = date("h:i:s A");
       
        $data=array(
                    'user_id'=>$user_id,
                    'rentcard_id'=>$rental_category_id,
                    'booking_type'=> 2 ,
                    'pickup_lat'=>$pickup_lat,
                    'car_type_id'=>$car_type_id,
                    'pickup_long'=>$pickup_long,
                    'pickup_location'=>$pickup_location,
                    'booking_date'=>$date,
                    'booking_time'=>$time1 ,
                    'user_booking_date_time'=>$date_add,
                    'last_update_time'=>$last_time_stamp,
                    'booking_status'=>10,
		            'coupan_code'=>$couponcode ,
		            'payment_option_id'=>$payment_option_id,
                    'pem_file'=>$pem_file
                    ); 
                    
 $this->Rental_model->booking_later_rental($data,$user_id);
 
    
            }
	  else
	  {
		  echo "1";
	   

	  }        
            }
	

   
 
 
function IphonePushNotificationDriver($did,$msg,$ride_id,$ride_status,$pem_file)
{
	 
	$deviceToken = $device_id;
	$passphrase = 'programmer';
	$message = $message;
	$pem_file=2;
 //echo $ride_status;die();
	$ctx = stream_context_create();
	if($pem_file == 1){
                stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/TaxiUDriver.debug.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    }else{
              stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/TaxiUDriver.debug.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    }

	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

	//echo 'Connected to APNS' . PHP_EOL;

	// Create the payload body
	$body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status' => $ride_status);

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));
 
	// Close the connection to the server
	fclose($fp);
}


 

 function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
	// Set POST variables
	$url = 'https://fcm.googleapis.com/fcm/send';
	
	$app_id="2";
	
	
	
	$fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );
$headers = array (
			'Authorization: key=AAAAeQhvUaw:APA91bGOUpHimzNnTUoMEwmLHOn-kYsA8iFiPSZ-dxiA7Hd9hQJzJ-W5nk1-ape27GHdviHzKQB5JnnDqaRNHGVeG2RN1QO79QLjZHwP6kysBplwckosy7npYXV_c12mzTRyO-tPd9e2',
			'Content-Type: application/json' );
	// Open connection
	$ch = curl_init ();
	// Set the url, number of POST vars, POST data
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
	// Execute post
	$result = curl_exec ( $ch );
	//print_r($result);die();
	// Close connection
	curl_close ( $ch );
        return $result;
}
  
 
 
 
public function getpicklocation()
{     
	    $origin_input = $this->input->post('origin-input');    
	    $string2 = str_replace(",", "+", urlencode($origin_input));
	
         $details_url2 = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $string2 . "&sensor=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response2 = json_decode(curl_exec($ch), true);
        $geometry2 = $response2['results'][0]['geometry'];
	    $longitude = $geometry2['location']['lng'];	
        $latitude  = $geometry2['location']['lat'];  
		 
		 $this->session->set_userdata(['lat_rental'=>$latitude,'long_rental'=>$longitude,'origin_input_rental'=>$origin_input]); 
		 
	    redirect('Rental_controller/search_cab_rental'); 
} 

public function getdroplocation()
{        
        $destination_input = $this->input->post('destination-input');       
	$string2 = str_replace(",", "+", urlencode($destination_input));
        $details_url2 = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $string2 . "&sensor=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response2 = json_decode(curl_exec($ch), true);
        $geometry2 = $response2['results'][0]['geometry'];
	    $destlongitude= $geometry2['location']['lng'];	
        $destlatitude= $geometry2['location']['lat'];
	   
	    $this->session->set_userdata(['destlatitude_city'=>$destlatitude,'destlongitude_city'=>$destlongitude,'destination_input_city'=>$destination_input]); 
 
	    redirect('Booking_controller/search_cab_city'); 
} 

public function update_mode()
{
$mode_id= $this->input->post('mode_id');
$this->session->set_userdata(['rentalwhen'=>$mode_id]); 

} 
public function update_date()
{
$mode_id= $this->input->post('mode_id');
$this->session->set_userdata(['rentaldate'=>$mode_id]); 
}
public function update_time()
{
$mode_id= $this->input->post('mode_id');
$this->session->set_userdata(['rentaltime'=>$mode_id]); 
}

public function update_paymentoption()
{
$payment_option= $this->input->post('payment_option');
$this->session->set_userdata(['rentalpayment_option'=>$payment_option]); 
}

public function get_codecountry()
{	 
$id= $this->input->post('id');

$list = $this->Booking_model->search_code($id);   
echo $list['phonecode'];
}


public function coupon()
{	 
$this->load->view('Coupon');
}	
	
public function validate_coupon()
{	     
        $coupon_code = $this->input->post('couponcode');
		$user_id = $this->session->userdata('user_id'); 
		$list = $this->Booking_model->check_code($coupon_code); 
	 
		$this->session->set_userdata(['rentalcoupon_code'=>$coupon_code]); 
	    if(!empty($list))
		{
		echo "<img src='http://apporio.org/Alakowe/webstatic/checked.png' height='20px' width=20px'/>";	 
		}
		else
		{
			if($coupon_code == "")
		{
			echo "";	 
		}
		else{
			echo "<img src='http://apporio.org/Alakowe/webstatic/error.png' height='20px' width='20px'/>";
		}	
		}
	
}		
	
public function Car_Type()
    { 
        $city_name = $this->input->post('city_name');
        $latitude = $this->input->post('latitude');
        $longitude = $this->input->post('longitude');
        if(!empty($latitude) && !empty($longitude))
        {
            $data = $this->Booking_model->cartype($city_name);
			 
            if(!empty($data))
            {
              $get_city_cars=$data;
			   print_r($get_city_cars); 
            }
			else
			{
                $data = $this->Booking_model->all_driver();
                if (!empty($data))
                {
                    foreach($data as $login3)
                    {
                        $driver_lat = $login3->current_lat;
                        $driver_long = $login3->current_long;
                        $theta = $longitude - $driver_long;
                        $dist = sin(deg2rad($latitude)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($latitude)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
                        $dist = acos($dist);
                        $dist = rad2deg($dist);
                        $miles = $dist * 60 * 1.1515;
                        $km=$miles* 1.609344;
                        if($km <= 30)
                        {
                            $c[] = array("driver_id"=> $login3->driver_id,"distance" =>$km,'city_id'=>$login3->city_id);
                        }
                    }
				 
                    if (!empty($c))
                    {
                        foreach ($c as $value)
                        {
                            $distance[] = $value['distance'];
                        }
                        array_multisort($distance,SORT_ASC,$c);
                        $c = $c[0]['city_id'];
                        $data = $this->Booking_model->cartype_city_id($c);
					 
                        if(!empty($data))
                        {
                         $get_city_cars=$data;
						 print_r($get_city_cars);die();
                        }
                    }
					 
                }
            }
        } 
    }
	
	
	
	
	
	
	
	
 function new_firebase_ride($last_id,$driver_id)
    {
        $admin = $this->Rentalmodel->admin_settings();
        $url = 'https://'.$admin->admin_panel_firebase_id.'.firebaseio.com/Activeride/'.$driver_id.'/.json';
        $fields = array(
            'ride_id' => (string)$last_id,
            'ride_status'=>"10",
        );

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        if(!$response) {
            return false;
        }else{
            return true;
        }
    }

    function Cler_firebase_ride($driver_id)
    {
        $admin = $this->Rentalmodel->admin_settings();
        $url = 'https://'.$admin->admin_panel_firebase_id.'.firebaseio.com/Activeride/'.$driver_id.'/.json';
        $fields = array(
            'ride_id' => "No Ride",
            'ride_status'=>"No Ride Status",
        );

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        if(!$response) {
            return false;
        }else{
            return true;
        }
    }

	
function Send_Sms($phone,$message)
    {
        $message = urlencode($message);
        $url="http://api.smartsmssolutions.com/smsapi.php?username=Alakowe&password=friday&sender=Alakowe&recipient=$phone&message=$message";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $output=curl_exec($ch);
        curl_close($ch);
    }
	
	
	
	
	
	
	
	
	
	 function AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status)
	  
    {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $app_id="1";

        $fields = array ('to' => $device_id,'priority' => 'high','data' => array('message' => $message,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

$headers = array (
			'Authorization: key=AAAAeQhvUaw:APA91bGOUpHimzNnTUoMEwmLHOn-kYsA8iFiPSZ-dxiA7Hd9hQJzJ-W5nk1-ape27GHdviHzKQB5JnnDqaRNHGVeG2RN1QO79QLjZHwP6kysBplwckosy7npYXV_c12mzTRyO-tPd9e2',
			'Content-Type: application/json' );

        // Open connection
        $ch = curl_init ();
        // Set the url, number of POST vars, POST data
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
        // Execute post
        $result = curl_exec ( $ch );
        // Close connection
        curl_close ( $ch );
        return $result;
    }  
	
	 function AndroidPushNotificationDriver21($did, $msg,$ride_id,$ride_status) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $app_id="2";
        $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );
$headers = array (
			'Authorization: key=AAAAeQhvUaw:APA91bGOUpHimzNnTUoMEwmLHOn-kYsA8iFiPSZ-dxiA7Hd9hQJzJ-W5nk1-ape27GHdviHzKQB5JnnDqaRNHGVeG2RN1QO79QLjZHwP6kysBplwckosy7npYXV_c12mzTRyO-tPd9e2',
			'Content-Type: application/json' );
        // Open connection
        $ch = curl_init ();
        // Set the url, number of POST vars, POST data
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
        // Execute post
        $result = curl_exec ( $ch );
        // Close connection
        curl_close ( $ch );
        return $result;
    }

    function IphonePushNotificationDriver21($did,$msg,$ride_id,$ride_status,$pem)
    {
	$deviceToken = $did;
	$passphrase = '123456';
	$message = $msg;
        $pem_file=2;
	$ctx = stream_context_create();
	if($pem_file == 1){
          stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/TaxiUDriver.debug.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    }else{
              stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/TaxiUDriver.debug.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    }

	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

	//echo 'Connected to APNS' . PHP_EOL;

	// Create the payload body
	$body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status' => $ride_status);

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));

	// Close the connection to the server
	fclose($fp);
	
    }

    function AndroidPushNotificationCustomer21($did, $msg,$ride_id,$ride_status)
    {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $app_id="1";

        $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

$headers = array (
			'Authorization: key=AAAAeQhvUaw:APA91bGOUpHimzNnTUoMEwmLHOn-kYsA8iFiPSZ-dxiA7Hd9hQJzJ-W5nk1-ape27GHdviHzKQB5JnnDqaRNHGVeG2RN1QO79QLjZHwP6kysBplwckosy7npYXV_c12mzTRyO-tPd9e2',
			'Content-Type: application/json' );

        // Open connection
        $ch = curl_init ();
        // Set the url, number of POST vars, POST data
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
        // Execute post
        $result = curl_exec ( $ch );
        // Close connection
        curl_close ( $ch );
        return $result;
    }

    function IphonePushNotificationCustomer21($did,$msg,$ride_id,$ride_status,$pem)
    {
        
	$deviceToken = $did;
	$passphrase = '123456';
	$message = $msg;
         $pem_file=2;
	$ctx = stream_context_create();
	if($pem_file == 1){
                stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/TaxiUDriver.debug.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    }else{
                stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/TaxiUDriver.debug.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

    }

	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

	//echo 'Connected to APNS' . PHP_EOL;

	// Create the payload body
	$body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status' => $ride_status);

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));

	// Close the connection to the server
	fclose($fp);
	
    }

	
	
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	