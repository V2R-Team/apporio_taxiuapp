<?php
class Booking_model extends CI_Model{
					
    	 
public function Get_car_type(){
        		       $query = $this->db->select('*')
        					  ->from('car_type')
        					  ->where(['car_admin_status' => 1 ,'ride_mode' => 1])
        					   ->get();
        	               return $query->result_array();
    			      }
public function payment_option(){
        		       $query = $this->db->select('*')
        					  ->from('payment_option')
        					  ->where(['status' => 1])
        					   ->get();
        	               return $query->result_array();
    			      }
					  
public function paymentmail($payment_option_id){
        		       $query = $this->db->select('*')
        					  ->from('payment_option')
        					  ->where(['payment_option_id' => $payment_option_id])
        					   ->get();
        	               return $query->row_array();
    			      }
					  
					  



 public function user_rides($user_id)
    {
        $data = $this->db->select('*')
                         ->from('table_user_rides')
                         ->order_by("user_ride_id", "desc")
                         ->where(['user_id'=>$user_id])
                         ->get();
        return $data->result_array();
    }
    
 public function normal_ride($booking_id)
    {
          $data = $this->db->select('*')
                          ->from('ride_table')
                          ->join('car_type','car_type.car_type_id=ride_table.car_type_id','inner')
                         ->join('user','user.user_id=ride_table.user_id','inner')
                         ->where(['ride_table.ride_id'=>$booking_id])
                         ->get();
        return $data->row();
    }   



    public function normal_done_ride($booking_id)
    {
        $data = $this->db->select('*')
                        ->from('done_ride')
                        ->join('driver','driver.driver_id=done_ride.driver_id','inner')
                        ->where(['done_ride.ride_id'=>$booking_id])
                        ->get();
        return $data->row();
    }
    
    public function rental_ride($booking_id)
    {
        $data = $this->db->select('*')
                        ->from('rental_booking')
                        ->join('car_type','car_type.car_type_id=rental_booking.car_type_id','inner')
                        ->join('user','user.user_id=rental_booking.user_id','inner')
                        ->where(['rental_booking.rental_booking_id'=>$booking_id])
                        ->get();
        return $data->row();
    }
    
   
   
   
  public function rental_done_ride($booking_id)
    {
        $data = $this->db->select('*')
                        ->from('table_done_rental_booking')
                        ->join('driver','driver.driver_id=table_done_rental_booking.driver_id','inner')
                        ->where(['rental_booking_id'=>$booking_id])
                        ->get();
        return $data->row();
    }    
        				  
public function Get_car_type_driver($value1){
        		             $query = $this->db->select('*')
        					 	->from('driver')
        					 	->where(['login_logout' => 1 ,'online_offline'=> 1,'car_type_id'=> $value1])
        					 	->get();
        			     return $query->row_array();
     				     }
 
 
 
 
 public function get_driver_details($driver_iddd){
        		             $query = $this->db->select('*')
        					 	->from('driver')
        					 	->where(['driver_id'=> $driver_iddd])
        					 	->get();
        			     return $query->row_array();
     				     }
     				     
     				     
     				        				     
public function Get_nearcar_type_driver($car_id){
        		             $query = $this->db->select('*')
        					 	->from('driver')
        					 	->where(['login_logout' => 1 ,'online_offline'=> 1,'car_type_id'=> $car_id])
        					 	->get();
        			     return $query->result_array();
    				     }
    				     
    				     
public function Get_user_details($user_id){
        		             $query = $this->db->select('*')
        					 	->from('user')
        					 	->where(['user_id'=> $user_id])
        					 	->get();
        			     return $query->row_array();
 
    				     }



function payment($payment_option_id)
	{
		$data = $this->db->select('*')
                         ->from('payment_option')          
                         ->where(['payment_option_id'=>$payment_option_id])
                         ->get();
        return $data->row();
	}
	
	
	
	
	
public function Get_car_type_city($value1){
        		             $query = $this->db->select('*')
        					 	->from('car_type')
        					 	->join('price_card','price_card.car_type_id=car_type.car_type_id','inner')
        					 	->where(['car_admin_status' => 1,'ride_mode' => 1 ,'car_type.car_type_id'=>$value1])
        					 	->get();
        			     return $query->row_array();
    				     }   
						 
public function Get_car_type_mail($car_type_id){
        		             $query = $this->db->select('*')
        					 	->from('car_type')
        					 	->where(['car_type.car_type_id'=>$car_type_id])
        					 	->get();
        			     return $query->row_array();
    				     } 

public function get_driver_model($drivet_model_id){
 
        		             $query = $this->db->select('*')
        					 	->from('car_model')
        					 	->join('car_type','car_type.car_type_id=car_model.car_type_id','inner')
        					 	->where(['car_model_id' => $drivet_model_id])
        					 	->get();
        			     return $query->row_array();
    				     }  
    				         				     


public function get_driver_type($drivet_model_id){
 
        		             $query = $this->db->select('*')
        					 	->from('car_type')
        					 	//->join('car_type','car_type.car_type_id=car_model.car_type_id','inner')
        					 	->where(['car_type_id' => $drivet_model_id])
        					 	->get();
        			     return $query->row_array();
    				     }  
    				         
    				         
 public function Get_car_type_rental(){
        		             $query = $this->db->select('*')
        					 	->from('car_type')
        					 	//->join('car_type','car_type.car_type_id=rentcard.car_type_id','inner')
        					 	->where(['car_type.ride_mode' => 2 ])
        					 	->get();
        			     return $query->result_array();
    				     } 
    				     
 public function get_rentalcars($rm_userphone){
        		             $query = $this->db->select('*')
        					 	->from('rentcard')
        					 	->join('car_type','car_type.car_type_id=rentcard.car_type_id','inner')
        					 	->where(['rentcard.rentcard_admin_status' => 1,'car_type.ride_mode' => 2 ,'rentcard.rental_category_id'=>$rm_userphone])
        					 	->get();
        			     return $query->result_array();
    				     } 				     
    				        			      
public function Get_car_type_packages($value1){
        		             $query = $this->db->select('*')
        					 	->from('rental_category')
        					 	->where(['rental_category_admin_status' => 1,'rental_category_id'=>$value1])
        					 	->get();
        			     return $query->row_array();
    				     }
    				     
public function get_carmodel(){
        		       $query = $this->db->select('*')
        					  ->from('car_model')
                          			   ->where('car_admin_status',1)
	                 			    ->get();
			      return $query->result_array(); 
    			      }
    						
public function user_check($phonenumber){
	                                 $query = $this->db->select('*')
        					            ->from('user')
        					 	     ->where('user_phone', $phonenumber)
        					 	      ->get();
        				  return $query->result_array();
    					 }				
    	 
public function otp_check($rm_userphone){
	                                $query = $this->db->select('*')
        					 	   ->from('booking-otp')
        					 	    ->where('user_phone', $rm_userphone )
        					 	     ->get();
        				return $query->row_array();
    					}
 
 public function login_check($phonenumber,$password){

	                                 $query = $this->db->select('*')
        					            ->from('user')
        					 	     ->where(['user_phone'=>$phonenumber,'user_password'=>$password])
        					 	     ->get();
        				  return $query->row_array();
    					 }	 	 
						 
public function User_prvious($phonenumber){
        		       $query = $this->db->select('*')
        					  ->from('user')
        					  ->where(['user_phone' => $phonenumber])
        					   ->get();
        	               return $query->row_array();
    			      }
    			      
public function User_prvious1($user_email){
        		       $query = $this->db->select('*')
        					  ->from('user')
        					  ->where(['user_email' => $user_email])
        					   ->get();
        	               return $query->row_array();
    			      } 
					  
public function otp_user_check($phonenumber,$otptext){
 
 
                   //$phonenumber=(string)$phonenumber;
                   //$otptext=(string)$otptext;
	                                $query = $this->db->select('*')
        					 	   ->from('booking-otp')
        					 	    ->where(['user_phone'=> $phonenumber, 'code' => $otptext ])
        					 	     ->get();
        					 	     
        				return $query->row_array();
    					}
    					    					
public function otp_Insert($phonenumber,$code_ui){
	                                         
	                                          $query = $this->db->select('*')
        					 	        ->from('booking-otp')
        					 	        ->where('user_phone', $phonenumber)
        					 	        ->get();
        				         $check=$query->row_array();
        				         
	                                         if(empty($check))
	                                         {
	                                         $query=$this->db->set(['user_phone'=>$phonenumber,'code'=>$code_ui ])
			                                          ->insert('booking-otp');
				                 $id=$this->db->insert_id();   
				                 return $id;
				                 }else
				                 {                                     
				                 $data=array('code'=>$code_ui);
				                 $this->db->where(['user_phone' => $phonenumber]);
						 $this->db->update('booking-otp', $data);
		                                 return $this->db->affected_rows();
				                 }
    						 }

public function otp_update($phonenumber,$code_ui){
	                                         $data=array('code'=>$code_ui);
				                 $this->db->where(['user_phone' => $phonenumber]);
						         $this->db->update('booking-otp', $data);
		                                         return $this->db->affected_rows();
    						 }    						 
public function rider_signupweb($data){
 
   				      $query=$this->db->set($data)
			                               ->insert('user');
				      $id=$this->db->insert_id();   
				      return $id;
				      } 
	
public function rental_packages(){
	                          $query = $this->db->select('*')
        			                     ->from('rental_category')
        			                     ->join('rentcard','rentcard.rental_category_id=rental_category.rental_category_id','inner')
                                                     ->where(['rental_category.rental_category_admin_status' => 1 , 'rentcard.rentcard_admin_status' => 1])
        					     ->get();
        				return $query->result_array();
        			
    				 }
    					
  	
public function city_search_cars(){
	                          $query = $this->db->select('*')
        			                     ->from('car_type')
        			                     ->join('price_card','price_card.car_type_id=car_type.car_type_id','inner')
                                         ->where(['car_type.ride_mode' => 1 , 'car_type.car_admin_status' => 1])
        					             ->get();
        				        return $query->result_array();
    				 }  					
    					
    					
    					
public function insert_ridetable($data)
   				{
   			         $query=$this->db->set($data)
			                         ->insert('ride_table');
				$id=$this->db->insert_id();   
				return $id;
				         }  	


public function insert_noridetable($data)
   				{
   			         $query=$this->db->set($data)
			                         ->insert('no_driver_ride_table');
				$id=$this->db->insert_id();   
				return $id;
				         }  

						 
public function insert_userrides($data1)	
   				{
   			         $query=$this->db->set($data1)
			                         ->insert('table_user_rides');
				}     					
    					
    					
public function searchnear_driver($car_type_id){
        		       $query = $this->db->select('*')
        					  ->from('driver')
        					  ->where(['car_type_id' => $car_type_id,'online_offline' => 1, 'driver_admin_status' => 1, 'busy' => 0, 'login_logout' => 1])
        					 // ->where('driver_category != ', 2)
        					  ->get();
        	               return $query->result_array();
    			      }   					
    				        			      
public function get_ridedetails($last_id){
        		             $query = $this->db->select('*')
        					 	->from('ride_table')
        					 	->where(['ride_id' => $last_id])
        					 	->get();
        			     return $query->row_array();
    				     }   					
    					
public function rideallocated($data)	
   				{
   			         $query=$this->db->set($data)
			                         ->insert('ride_allocated');
				}     					
    					
public function ride_allocated($driver_id){
        		             $query = $this->db->select('*')
        					 	->from('driver')
        					 	->where(['driver_id' => $driver_id])
        					 	->get();
        		                      
        					 	
        			     return $query->row_array();
    				     }  	
    				     
public function driverallocated($driver_id){
        		             $query = $this->db->select('*')
        					 	->from('driver')
        					 	->where(['driver_id' => $driver_id ])
        					 	->get();
        			     return $query->row_array();
    				     } 		
    				     
    				     
public function get_driverclear($last_id){
        		             $query = $this->db->select('*')
        					 	->from('driver_ride_allocated')
        					 	->where(['ride_id' => $last_id ])
        					 	->get();
        			     return $query->result_array();
    				     } 	
    				     
    				     		
    					
public function language(){
        		             $query = $this->db->select('*')
        					 	->from('messages')
        					 	->where(['language_id' => 1 , 'message_id' => 32])
        					 	->get();
        			     return $query->row_array();
    				     }   					
    					
    					
public function insertdriverallocated($data)	
   				{
   			         $query=$this->db->set($data)
			                         ->insert('driver_ride_allocated');
				}    					
    					
public function update_driverallocated($data,$driver_id){
                   				         $this->db->where(['allocated_driver_id' => $driver_id]);
						         $this->db->update('ride_allocated', $data);
		                                         return $this->db->affected_rows();
    							}	

    					
public function insert_user_rides($data)
   				{
   			         $query=$this->db->set($data)
			                         ->insert('table_user_rides');
				} 
				
public function cancel_ridedetails($last_id,$last_time_stamp,$reason_id,$driver_id){
                                            $data=array('ride_status' => 2,'last_time_stamp'=>$last_time_stamp,'reason_id'=>$reason_id);
                   			    $this->db->where(['ride_id' => $last_id]);
					    $this->db->update('ride_table', $data);
					    
					      $data=array('busy' => 0);
        				      $this->db->where(['driver_id' => $driver_id]);
					      $this->db->update('driver', $data);
		                             return $this->db->affected_rows();
		                            
		                            
    					   }				
				
				
public function lastride_data($user_id){
        		             $query = $this->db->select('*')
        					 	->from('ride_table')
        					 	->where(['user_id' => $user_id ,'ride_type' => 1])
								->where('driver_id != ',0)
        					 	->order_by('ride_id' , 'desc')
        					 	->get();
								
        			        return $query->row_array();
    				     } 	

public function lastride_data1($user_id){
        		             $query = $this->db->select('*')
        					 	->from('ride_table')
        					 	->where(['user_id' => $user_id , 'ride_type' => 2])
        					    ->order_by('ride_id' , 'desc')
        					 	->get();
								 
        			     return $query->row_array();
    				     } 						 

public function allrides_data($user_id){
        		             $query = $this->db->select('*')
        					 	->from('ride_table')
        					 	->where(['user_id' => $user_id ,'ride_type' => 1])
								->where('driver_id != ',0)
        					 	->order_by('ride_id' , 'desc')
        					 	->get();
        			     return $query->result_array();
    				     } 		

public function allrides_data1($user_id){
        		             $query = $this->db->select('*')
        					 	->from('ride_table')
        					 	->where(['user_id' => $user_id , 'ride_type' => 2])
								->order_by('ride_id' , 'desc')
        					 	->get();
        			     return $query->result_array();
    				     } 						 
			
function delete_ridedetails($last_id){
                                   // $this->db->where('ride_id', $last_id);
				                    //$this->db->delete('ride_table');
					                //$data=array('user_id'=>0);
					  	            //$this->db->where('ride_id', $last_id);
						           //$this->db->update('ride_table', $data);
		                           //return $this->db->affected_rows();
                                     }


public function done_rideinfo($ride_id){
        		                        $query = $this->db->select('total_amount,distance,arrived_time,end_time')
														   ->from('done_ride')
														   ->where(['ride_id' => $ride_id])
														   ->get();
											    return $query->row_array();
    				     } 		

public function done_rideinfomail($ride_id){
        		                                                                 $query = $this->db->select('*')
														   ->from('done_ride')
														   ->where(['ride_id' => $ride_id])
														   ->get();
											    return $query->row_array();
    				     } 
    				     
public function Get_city(){
        		             $query = $this->db->select('*')
        			                     ->from('city')
        			                     ->join('price_card','price_card.city_id=city.city_id','inner')
                                                     ->where(['city.city_admin_status' => 1])
        					     ->get();
        				return $query->result_array();
    				     } 		
    				     
    				     
public function Get_city_data($data){
        		             $query = $this->db->select('*')
        			                     ->from('city')
                                                     ->where(['city_id' => $data])
        					     ->get();
        				return $query->row_array();
    				     }		
				
public function Get_carbycity($a){
        		               $query = $this->db->select('*')
        			                     ->from('price_card')
        			                     ->join('car_type','car_type.car_type_id=price_card.car_type_id','inner')
                                                     ->where(['price_card.city_id' => $a])
        					     ->get();
        				return $query->result_array();
    				     }	
    				     		
public function Get_car_data($data)	{
        		               $query = $this->db->select('*')
        			                     ->from('car_type')
        			                     ->where(['car_type_id' => $data])
        					     ->get();
        				return $query->row_array();
    				     }		
public function Get_pricecard($car_type_id,$city_id){
        		               $query = $this->db->select('*')
        			                     ->from('price_card')
        			                     ->where(['car_type_id' => $car_type_id,'city_id'=>$city_id])
        					     ->get();
        				return $query->row_array();
    				     }									
     				   							



public function get_country(){
        		             $query = $this->db->select('*')
        					 	->from('country')
        					 	->get();
        			     return $query->result_array();
    				     }
						 
public function search_code($id){
        		               $query = $this->db->select('*')
        			                     ->from('country')
        			                    ->where(['id' => $id])
        					     ->get();
        				return $query->row_array();
    				     }	

public function check_code($coupon_code){
                               $cur_date= date('Y-m-d');
        		               $query = $this->db->select('*')
        			                     ->from('coupons')
        			                     ->where(['coupons_code  like binary' =>  $coupon_code])
						     ->where('expiry_date >= ' , $cur_date)
						    ->where('start_date  <=' , $cur_date)
								         ->get();
        				return $query->row_array();
    				     }	

						 
						 
function cartype($city_name)
    {
        $data = $this->db->select('*')
            ->from('price_card ')
            ->join('car_type','car_type.car_type_id=price_card.car_type_id','inner')
            ->join('city','city.city_id=price_card.city_id','inner')
            ->where(['city.city_name'=>$city_name,'car_type.car_admin_status'=>1])
            ->get();
        return $data->result_array();
    }
    
    
    
function cartype_name($car_type_id, $city_nameId)
    {
        $data = $this->db->select('*')
            ->from('price_card')
            ->join('car_type','car_type.car_type_id=price_card.car_type_id','inner')
            ->join('city','city.city_id=price_card.city_id','inner')
            ->where(['price_card.car_type_id'=>$car_type_id ,'city.city_id'=>$city_nameId])
            ->get();
        return $data->row_array();
    }
    
    
    
function cartype_city_id($city_id)
    {
        $data = $this->db->select('*')
            ->from('price_card')
            ->join('car_type','car_type.car_type_id=price_card.car_type_id','inner')
            ->join('city','city.city_id=price_card.city_id','inner')
            ->where(['city.city_id'=>$city_id,'car_type.car_admin_status'=>1])
            ->get();
        return $data->result_array();
    }


    
function getcity_id($city_name)
    {
        $data = $this->db->select('*')
            ->from('city')
           // ->where(['city_name'=>$city_name])
           ->where(['city_name  like ' =>  $city_name])
            ->get();
        return $data->row_array();
    }
    
    
function rent_package($city_id)
    {
        $data = $this->db->select('*')
                        ->from('rentcard ')
                        ->join('rental_category','rental_category.rental_category_id=rentcard.rental_category_id','inner')
                        ->where(['rentcard.city_id'=>$city_id,'rentcard.rentcard_admin_status'=>1])
                        ->get();
        return $data->result_array();
    }
	
function all_driver()
    {
        $data = $this->db->select('*')
            ->from('driver')
            ->where(['current_lat !='=>"",'current_long !='=>""])
            ->get();
        return $data->result();
    }	


public function driver_documentupload($data){
 
   				      $query=$this->db->set($data)
			                       ->insert('table_driver_document');
				     
				      } 	


function driver_documentcheck($driver_id,$document_id)
    {
        $data = $this->db->select('*')
                        ->from('table_driver_document ')
                        ->where(['driver_id'=>$driver_id,'document_id'=>$document_id])
                        ->get();
        return $data->row_array();
    }

function driver_documentchecklist($user_id,$document_id)
    {
		 
        $data = $this->db->select('*')
                        ->from('table_driver_document')
                        ->where(['driver_id'=>$user_id,'document_id'=>$document_id])
                        ->get();
        return $data->row_array();
    }
	

function driver_upload_count($user_id)
    {
		 
        $data = $this->db->select('*')
                        ->from('table_driver_document')
                        ->where(['driver_id'=>$user_id])
                        ->get();
        return $data->result_array();
    }
	
	
function cancel_reasons()
    {
        $data = $this->db->select('*')
                        ->from('cancel_reasons ')
                        ->where(['reason_type'=> 1 , 'cancel_reasons_status'=> 1])
                        ->get();
        return $data->result_array();
    }
	
	
public function driver_documentupdate($data,$driver_id,$document_id){
	                                          
				                 $this->db->where(['driver_id' => $driver_id,'document_id' => $document_id]);
						         $this->db->update('table_driver_document', $data);
		                                         return $this->db->affected_rows();
    						 }   
							 



    					
public function driver_updatedoccount($total,$driver_id){
	$data=array('detail_status'=>3,'total_document_need'=>$total);
                   				         $this->db->where(['driver_id' => $driver_id]);
						              $this->db->update('driver', $data);
		                                         return $this->db->affected_rows();
    							}	


								
}
































