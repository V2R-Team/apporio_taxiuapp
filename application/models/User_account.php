<?php
class User_account extends CI_Model{

	 public function rider_signup($data)
   					 {
   					  $query=$this->db->set($data)
			                             ->insert('user');
				          $id=$this->db->insert_id();   
				          return $id;
				         } 
    public function driver_signup($data)
   					 {
   					  $query=$this->db->set($data)
			                             ->insert('driver');
				          $id=$this->db->insert_id();   
				          return $id;
				         } 
				         
	public function login($email,$pass){
	 
        					 $query = $this->db->select('*')
        					 		  ->from('user')
        					 		  ->where('user_email', $email)
        					 		  ->or_where('user_phone',$email)
        					 		  ->where('user_password', $pass)
        					 		  ->get();
        					                   return $query->result_array();
    						}
	
	
		public function Get_homepage_data(){
	 
	  $site_lang =$this->session->userdata('site_lang');
		  
		  if($site_lang == 'french')
		  {
			   $head_id=2;  
		  }
		  else
		  {
			  $head_id=1;
		  }
		  
        					 $query = $this->db->select('*')
        					 		  ->from('web_home')
									  ->where('id', $head_id)
        					 		  ->get();
        					                 return $query->row_array();
    						   }
    public function Get_heading_data(){
	 
	      $site_lang =$this->session->userdata('site_lang');
		 
		  if($site_lang == 'french')
		  {
			   $head_id=2;  
		  }
		  else
		  {
			  $head_id=1;
		  }
        					 $query = $this->db->select('*')
        					 		  ->from('web_headings')
									   ->where('heading_id', $head_id)
        					 		  ->get();
        					        return $query->row_array();
    						   }						
    						
    						
	public function Get_page_data()
	                      {
        					 $query = $this->db->select('*')
        					 		  ->from('web_home_pages')
        					 		  ->get();
        					        return $query->result_array();
    						}
							
							
    	
    	public function login_driver($email,$pass){
	 
        					 $query = $this->db->select('*')
        					 		  ->from('driver')
        					 		  ->where('driver_phone', $email)
        					 		 // ->or_where('driver_phone',$email)
        					 		  ->where('driver_password', $pass)
        					 		  ->get();
        					                   return $query->result_array();
    						}
    						
      	
    	public function login_driver1($email,$pass){
	 
        					 $query = $this->db->select('*')
        					 		  ->from('driver')
        					 		  ->where('driver_email', $email)
        					 		//  ->or_where('driver_phone',$email)
        					 		  ->where('driver_password', $pass)
        					 		  ->get();
        					                   return $query->result_array();
    						}
    						
    public function sign_drivercheck($user_phone){
	 
        					 $query = $this->db->select('*')
        					 		  ->from('driver')
        					 		  ->where('driver_phone',$user_phone)
        					 		  ->get();
        					                   return $query->row_array();
    						}
    						
    
    
        public function blog_data($page_id){
	 
	 
	 
        					 $query = $this->db->select('*')
        					 		  ->from('web_home_pages')
        					 		  ->where('page_id',$page_id)
        					 		  ->get();
        					                   return $query->row_array();
    						}
    						
    						
    												
    						
    	public function about_us(){
        					 $query = $this->db->select('*')
        					 		  ->from('pages')
        					 		  ->where('page_id', 2)
        					 		  ->get();
        					          return $query->result_array();
    						}
		public function about_data(){
        					 $query = $this->db->select('*')
        					 		  ->from('pages')
        					 		  ->where('page_id', 1)
        					 		  ->get();
        					          return $query->result_array();
    						}
							
    	public function city(){
        					 $query = $this->db->select('*')
        					 		  ->from('city')
        					 		  ->where('city_admin_status', 1)
        					 		  ->get();
        					                   return $query->result_array();
    						}
    	public function car_type(){
        					 $query = $this->db->select('*')
        					 		  ->from('car_type')
        					 		  ->where('car_admin_status', 1)
        					 		  ->get();
        					                   return $query->result_array();
    						}
    	public function get_carmodel($car_type_id){
    	                                        
    	                                          
        					  $query = $this->db->select('*')
                          						->where('car_type_id',$car_type_id)
	                 						->get('car_model');
									return $query->result_array(); 
    						}
    						
    						
    	public function view_rider_profile($user_id ){
        					 $query = $this->db->select('*')
        					 		  ->from('user')
        					 		  ->where('user_id', $user_id)
        					 		  ->get();
        					                   return $query->result_array();
    						}
    						
    	public function view_driver_profile($user_id ){
			
        					 $query = $this->db->select('*')
        					 		  ->from('driver')
        					 		   ->join('car_model','car_model.car_model_id=driver.car_model_id','inner')
        					 		  ->where('driver_id', $user_id)
        					 		  ->get();
        					            return $query->result_array();
    						}
    	    						
    	public function view_rider_coupons($user_id){
        					 $query = $this->db->select('*')
        					 		  ->from('ride_table')
        					 		  ->where('user_id', $user_id)
        					 		  ->get();
        					                   return $query->result_array();
    						}
    	
    	public function view_rider_coupons_data($coupon_code){
        					      $query = $this->db->select('*')
        					 		  ->from('coupons')
        					 		  ->where('coupons_code', $coupon_code)
        					 		  ->get();
        					                   return $query->result_array();
    						}
    						
    	public function update_rider_password($user_id,$old_password,$data){
                   							$this->db->where(['user_id'=> $user_id,'user_password'=>$old_password]);
						                        $this->db->update('user', $data);
						                        return $this->db->affected_rows();
    									}		 

 	


   	public function update_count($total_document_need){
			                              $data=array('total_document_need'=>$total_document_need);
                   							   $this->db->where(['driver_id'=> $driver_id]);
						                        $this->db->update('driver', $data);
						                        return $this->db->affected_rows();
    									}

										
    	public function update_driver_password($user_id,$old_password,$data){
                   							    $this->db->where(['driver_id'=> $user_id,'driver_password'=>$old_password]);
						                        $this->db->update('driver', $data);
						                        return $this->db->affected_rows();
    									}
										
										
		public function check_driver_password($user_id,$old_password){
        		             $query = $this->db->select('*')
        					 	->from('driver')
        					 	->where(['driver_id'=> $user_id,'driver_password'=>$old_password])
        					 	->get();
        			     return $query->row_array();
    				     } 
						 
						 
						 
        public function update_rider_profile($user_id,$data){
         							$this->db->where(['user_id'=> $user_id]);
						                $this->db->update('user', $data);
						                return $this->db->affected_rows();
    									}     
           
        public function update_driver_profile($user_id,$data){
         							$this->db->where(['driver_id'=> $user_id]);
						                $this->db->update('driver', $data);
						                return $this->db->affected_rows();
    									}
    
    public function update_driver_docs($user_id,$data){
                   							$this->db->where(['driver_id'=> $user_id]);
						                        $this->db->update('driver', $data);
						                        return $this->db->affected_rows();
    									}	
    									
    	public function view_rides($user_id){ 
        					 $query = $this->db->select('*')
        					 		   ->from('ride_table')
        					 		   ->join('driver','driver.driver_id=ride_table.driver_id','inner')  
        		   					   ->join('user','user.user_id =ride_table.user_id','inner') 
        		   					   //->join('payment','user.user_id =ride_table.user_id','inner')  
        					 		   ->where(['ride_type'=>1,'ride_table.driver_id'=>$user_id])
									  
        					 		   ->get();
        					                   return $query->result_array();
    						} 
    	 /*	public function view_rides($user_id){ 
        					 $query = $this->db->select('*')
        					 		   ->from('table_driver_document')
        					 		   ->join('table_documents','table_documents.driver_id=table_driver_document.driver_id','inner')  
        		   					   ->where(['driver_id'=>$user_id])
									   ->get();
        					          return $query->result_array();
    						          } */
    						
    	 public function driver_suppourt($data)
   					 {
   					  $query=$this->db->set($data)
			                             ->insert('customer_support');
				          $id=$this->db->insert_id();   
				          return $id;
				         } 
public function Contact_us_form($data)
                                    {
   				    $query=$this->db->set($data)
			                        ->insert('customer_support');
				         $id=$this->db->insert_id();   
				          return $id;
				         } 
						 
public function allrides_data($user_id){
        		             $query = $this->db->select('*')
        					 	->from('ride_table')
        					 	->where(['driver_id' => $user_id])
								->order_by('ride_id' , 'desc')
        					 	->get();
        			     return $query->result_array();
    				     } 
						 
						 
public function driverallocated($user_id_ride){
        		             $query = $this->db->select('*')
        					 	->from('user')
        					 	->where(['user_id' => $user_id_ride ])
        					 	->get();
        			     return $query->row_array();
    				     } 	


public function done_rideinfo($ride_id){
        		                        $query = $this->db->select('total_amount,distance,arrived_time,end_time')
														   ->from('done_ride')
														   ->where(['ride_id' => $ride_id])
														   ->get();
											    return $query->row_array();
    				     } 						 
    									
     	public function rider_rides($user_id)
	{
	 
	 $query = $this->db->select('*')
	 		    ->from('ride_table')
        		    //->join('done_ride','done_ride.ride_id = ride_table.ride_id','inner')  
        		    ->join('driver','driver.driver_id=ride_table.driver_id','inner')  
        		    ->join('user','user.user_id =ride_table.user_id','inner')  
        		    ->join('car_type','car_type.car_type_id=ride_table.car_type_id','inner')  
        		    ->join('car_model','car_model.car_type_id=driver.car_type_id','inner')  
        		    //->join('rate_card','rate_card.car_type_id=car_type.car_type_id','inner')  
        		    ->where('ride_table.user_id',$user_id)
        		    //->where('ride_table.ride_type !=',1)
        		    ->get();
        		    return $query->result_array();
        		  
	} 
	
	public function get_doneride_id($category_id)
	{
	 $query = $this->db->select('*')
	 		    ->from('done_ride')
        		    ->where('done_ride.ride_id',$category_id)
        		   ->get();
        		   return $query->result_array();
	} 
	
	public function rider_rides_pdf($ride_id)
	{
	 
	 $query = $this->db->select('*')
	 		   ->from('done_ride')
        		   ->join('ride_table','ride_table.ride_id =done_ride.ride_id','inner')  
        		   ->join('driver','driver.driver_id=done_ride.driver_id','inner')  
        		   ->join('user','user.user_id =ride_table.user_id','inner')  
        		   ->join('car_type','car_type.car_type_id=ride_table.car_type_id','inner')  
        		   ->join('car_model','car_model.car_type_id=driver.car_type_id','inner')  
        		   ->join('rate_card','rate_card.car_type_id=car_type.car_type_id','inner')  
        		   ->where('done_ride.ride_id',$ride_id)
        		   ->get();
        		   return $query->result_array();	
        		  
	} 
    public function rider_book_later($data)
   					 {
   					  $query=$this->db->set($data)
			                             ->insert('ride_table');
				          $id=$this->db->insert_id();   
				          return $id;
				         }   
				        
   public function rider_trips_booking_later($user_id)
   					             {
   					                      $query = $this->db->select('*')
	 		  				         ->from('ride_table')
				        		          ->join('user','user.user_id =ride_table.user_id','inner')  
				        		          ->join('car_type','car_type.car_type_id=ride_table.car_type_id','inner')  
							          ->where(['ride_table.user_id'=>$user_id,'ride_table.ride_type'=>2,'ride_table.ride_status'=>1])
							          ->get();
        		                       return $query->result_array();
				                      } 



    public function rider_book_later_user_data($user_id)
   					 { 
   					   $query = $this->db->select('*')
        					 		  ->from('user')
        					 		  ->where('user_id', $user_id)
        					 		  
        					 		  ->get();
        				  return $query->result_array();
				         }  
   public function rider_book_later_cartype_data($car_type_id)
   					 { 
   					   $query = $this->db->select('*')
        					 		  ->from('car_type')
        					 		  ->where('car_type_id', $car_type_id)
        					 		  ->get();
        				  return $query->result_array();
				         }  

 	public function rider_book_later_carmodel_data($car_type_id)
   					 { 
   					   $query = $this->db->select('*')
        					 		  ->from('car_model')
        					 		  ->where('car_type_id', $car_type_id)
        					 		  ->get();
        				  return $query->result_array();
				         }  

	public function login_rider_forget($email)
   					 { 
   					   $query = $this->db->select('*')
        					 		  ->from('user')
        					 		  ->where('user_email',$email)
        					 		  ->get();
        				  return $query->result_array();
				         }  
						 
	public function Get_carbycity($city_id,$car_type_id){
        		               $query = $this->db->select('*')
        			                     ->from('price_card')
        			                     ->where(['city_id' => $city_id,'car_type_id'=>$car_type_id])
        					             ->get();
        				return $query->row_array();
    				     }	
    				     	
		public function document_list($city_id){
        		                   $query = $this->db->select('*')
	 		  				         ->from('table_document_list')
				        		     ->join('table_documents','table_documents.document_id =table_document_list.document_id','inner')  
				        		     ->where(['table_document_list.city_id'=>$city_id ,'table_document_list.city_document_status'=>1])
							          ->get();
        		                       return $query->result_array();
    				     }	
    				     	
							
							
							
							
   public function login_driver_forget($email)
   					 { 
   					   $query = $this->db->select('*')
        					 		  ->from('driver')
        					 		  ->where('driver_email', $email)
        					 		  ->get();
        				  return $query->result_array();
				         }  
				         
				         public function web_home()
   					 { 
   					   $query = $this->db->select('*')
        					 		  ->from('web_home')
        					 		  ->where('id', 1)
        					 		  ->get();
        				  return $query->result_array();
				         } 
				         
				         public function web_about()
   					 { 
   					   $query = $this->db->select('*')
        					 		  ->from('web_about')
        					 		  ->where('id', 1)
        					 		  ->get();
        				  return $query->result_array();
				         }
				         
				         public function web_contact()
   					 { 
   					   $query = $this->db->select('*')
        					 		  ->from('web_contact')
        					 		  ->where('id', 1)
        					 		  ->get();
        				  return $query->result_array();
				         } 						 
           
            	    }