<?php
class Rentalmodel extends CI_Model{

    function Authcode($user_id)
    {
        $data = $this->db->select('*')
                         ->from('table_paystack')
                         ->where(['user_id'=>$user_id])
                         ->get();
        return $data->row();
    }

    function update_earing($driver_id,$earn_array)
    {
        $date = date("Y-m-d");
        $this->db->where(['driver_id'=>$driver_id,'date'=>$date,'ride_mode'=>2])
                 ->update('driver_earnings',$earn_array);
    }

    function insert_earing($earn_array)
    {
       $this->db->insert('driver_earnings',$earn_array);
    }

    function driver_earning($driver_id)
    {
        $date = date("Y-m-d");
        $data = $this->db->select('*')
                        ->from('driver_earnings')
                        ->where(['driver_id'=>$driver_id,'date'=>$date,'ride_mode'=>2])
                        ->get();
        return $data->row();
    }

    function admin_settings()
    {
        $data = $this->db->select('*')
                        ->from('admin_panel_settings')
                        ->where(['admin_panel_setting_id'=>1])
                        ->get();
        return $data->row();
    }

    function check_payment($rental_booking_id)
	{
		$data = $this->db->select('*')
                         ->from('rental_payment')
                         ->where(['rental_booking_id'=>$rental_booking_id])
                         ->get();
        return $data->row();
	}
	
   function payment($payment,$rental_booking_id)
   {
	   $this->db->insert('rental_payment',$payment);
       $this->db->where(['rental_booking_id'=>$rental_booking_id])
                ->update('table_done_rental_booking',['payment_status'=>1]);
       $this->db->where(['rental_booking_id'=>$rental_booking_id])
                ->update('rental_booking',['payment_status'=>1]);

   }
   
    function coupan($coupan_code)
    {
        $data = $this->db->select('*')
                         ->from('coupons')
                         ->where(['coupons_code'=>$coupan_code])
                         ->get();
        return $data->row();
    }

    function rencard_details($rentcard_id)
    {
        $data = $this->db->select('*')
            ->from('rentcard')
            ->join('rental_category','rental_category.rental_category_id=rentcard.rental_category_id','inner')
            ->join('car_type','car_type.car_type_id=rentcard.car_type_id','inner')
            ->where(['rentcard.rentcard_id'=>$rentcard_id])
            ->get();
        return $data->row();
    }

    function driver_ride($rental_booking_id,$driver_id)
    {
        $this->db->where(['booking_id'=>$rental_booking_id,'ride_mode'=>2])
            ->update('table_user_rides',['driver_id'=>$driver_id]);
    }

    function rating($rating)
    {
        $this->db->insert('table_rental_rating',$rating);
    }

    function done_ride_info($rental_booking_id)
    {
        $data = $this->db->select('*')
            ->from('table_done_rental_booking')
            ->join('rental_booking','rental_booking.rental_booking_id=table_done_rental_booking.rental_booking_id','inner')
            ->where(['table_done_rental_booking.rental_booking_id'=>$rental_booking_id])
            ->get();
        return $data->row();
    }

    function rental_ratecard($rentcard_id)
    {
        $data = $this->db->select('*')
            ->from('rentcard')
            ->join('rental_category','rental_category.rental_category_id=rentcard.rental_category_id','inner')
            ->where('rentcard.rentcard_id',$rentcard_id)
            ->get();
        return $data->row();
    }
    function done_ride($ride_id,$last_time_stamp,$driver_id)
    {
        $data = array('rental_booking_id'=>$ride_id,'driver_arive_time'=>$last_time_stamp,'driver_id'=>$driver_id);
        $this->db->insert('table_done_rental_booking',$data);
    }

    function done_ride_update($rental_booking_id,$query)
    {
        $this->db->where('rental_booking_id',$rental_booking_id)
            ->update('table_done_rental_booking',$query);
    }

    function cartype($city_name)
    {
        $data = $this->db->select('*')
            ->from('price_card ')
            ->join('car_type','car_type.car_type_id=price_card.car_type_id','inner')
            ->join('city','city.city_id=price_card.city_id','inner')
            ->where(['city.city_name'=>$city_name,'car_type.car_admin_status'=>1])
            ->get();
        return $data->result_array();
    }


    function cartype_city_id($city_id)
    {
        $data = $this->db->select('*')
            ->from('price_card ')
            ->join('car_type','car_type.car_type_id=price_card.car_type_id','inner')
            ->join('city','city.city_id=price_card.city_id','inner')
            ->where(['city.city_id'=>$city_id,'car_type.car_admin_status'=>1])
            ->get();
        return $data->result_array();
    }


    function ride_info($rental_booking_id)
    {
        $data = $this->db->select('*')
                         ->from('rental_booking')
                         ->join('user','user.user_id=rental_booking.user_id','inner')
                         ->where('rental_booking.rental_booking_id',$rental_booking_id)
                         ->get();
        return $data->row();
    }

    function ride_status_change($rental_booking_id,$text)
    {
        $this->db->where('rental_booking_id',$rental_booking_id)
            ->update('rental_booking',$text);
        $data = $this->db->select('*')
            ->from('rental_booking')
            ->join('user','user.user_id=rental_booking.user_id','inner')
            ->where('rental_booking.rental_booking_id',$rental_booking_id)
            ->get();
        return $data->row();
    }

    function rent_package($city_id)
    {
        $data = $this->db->select('*')
                        ->from('rentcard ')
                        ->join('rental_category','rental_category.rental_category_id=rentcard.rental_category_id','inner')
                        ->where(['rentcard.city_id'=>$city_id,'rentcard.rentcard_admin_status'=>1])
                        ->get();
        return $data->result_array();
    }

    function Rental_Package($city_id)
    {
        $data = $this->db->select('*')
            ->from('rental_category')
            ->join('rentcard','rentcard.rental_category_id=rental_category.rental_category_id','inner')
            ->where(['rentcard.city_id'=>$city_id])
            ->get();
        return $data->result_array();
    }

    function Rental_Pakage_Car($city_id,$rental_category_id)
    {
        $data = $this->db->select('*')
            ->from('rentcard')
            ->join('car_type','car_type.car_type_id=rentcard.car_type_id','inner')
            ->where(['rentcard.city_id'=>$city_id,'rentcard.rental_category_id'=>$rental_category_id])
            ->get();
        return $data->result_array();
    }

    function booking_later($data,$user_id)
    {
        $this->db->insert('rental_booking',$data);
        $id = $this->db->insert_id();
        $this->db->insert('table_user_rides',['booking_id'=>$id,'ride_mode'=>2,'user_id'=>$user_id]);
    }

    function booking_now($data)
    {
        $this->db->insert('rental_booking',$data);
        $id = $this->db->insert_id();
        $data = $this->db->get_where('rental_booking',['rental_booking_id'=>$id]);
        return $data->row();
    }

    function booking_allocated($rental_booking_id,$driver_id,$user_id)
    {
        $this->db->insert('booking_allocated',['rental_booking_id'=>$rental_booking_id,'driver_id'=>$driver_id]);
        $this->db->insert('table_user_rides',['booking_id'=>$rental_booking_id,'ride_mode'=>2,'user_id'=>$user_id]);
    }

    function driver_profile($driver_id)
    {
        $data = $this->db->select('*')
            ->from('driver')
            ->join('car_type','car_type.car_type_id=driver.car_type_id','inner')
            ->join('car_model','car_model.car_model_id=driver.car_model_id','inner')
            ->where(['driver.driver_id'=>$driver_id])
            ->get();
        return $data->row();
    }

    function update_driver($driver_id,$ear)
    {
        $this->db->where('driver_id',$driver_id)
            ->update('driver',$ear);
    }

    function all_driver()
    {
        $data = $this->db->select('*')
            ->from('driver')
            ->where(['current_lat !='=>"",'current_long !='=>""])
            ->get();
        return $data->result();
    }

    function driver_details($driver_id,$driver_token)
    {
        $data = $this->db->select('*')
            ->where(['driver_id'=>$driver_id,'driver_token'=>$driver_token])
            ->get('driver');
        return $data->row();
    }

    function nearest_driver($car_type_id)
    {
        $categories = array('2','3');
        $data = $this->db->select('*')
            ->where(['online_offline'=>1,'busy'=>0,'login_logout'=>1,'car_type_id'=>$car_type_id,'driver_admin_status'=>1])
			->where(['current_lat !='=>"",'current_long !='=>""])
            ->where_in('driver_category', $categories)
            ->get('driver');
        return $data->result_array();

    }

    function user_device($user_id)
    {
        $data = $this->db->select('*')
            ->where(['user_id'=>$user_id,'login_logout'=>1])
            ->get('user_device');
        return $data->result();

    }

}