<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*******ENGLISH_LANGUAGE*******/
/******************************/
	/*****Headers**********/
		$lang['sign'] = 'تسجيل الدخول';
		$lang['become_driver'] = 'تصبح سائق';
		$lang['book_now'] = 'احجز الآن';
		$lang['logout'] = 'الخروج';
		$lang['dashboard'] = 'لوحة القيادة';
		$lang['myrides'] = 'بلدي ركوب الخيل';
	

	/******Index_page_start***********/
  	/*****Index_BannerImage**********/
	  
	    $lang['bannertxt1'] = 'حجز أبوريو تاكسي إلى وجهتك في المدينة';
		$lang['bannertxt2'] = 'الاختيار من بين مجموعة من الفئات والأسعار';
        $lang['bnrsearchbox1'] = 'شريك السفر اليومي'; 
        $lang['bnrsearchbox2'] = 'أس سيارات الأجرة لنقطة إلى نقطةالسفر';

    /***********form data************/	
    	$lang['cityheader'] = 'تاكسي تاكسي';	
		$lang['rentalheader'] = 'تأجير';
        $lang['pickup'] = 'امسك';	
		$lang['drop'] = 'قطرة';
		$lang['when'] = 'متى';
		$lang['depart'] = 'DEPART';
		$lang['enterpick'] = 'أدخل موقع بيك اب الخاص بك';
		$lang['enterdrop'] = 'أدخل إسقاط الموقع ل ركوب تقدير';
		$lang['schlater'] = 'الجدول الزمني لاحقا';
		$lang['now'] = 'الآن';
		$lang['searchbtn'] = 'بحث المركبات';
        
 
    /*****Index_Footer**********/
     
        $lang['Home'] ='الصفحة الرئيسية';
        $lang['about_us'] ='معلومات عنا';
        $lang['contact_us'] ='اتصل بنا';
	    $lang['iphone'] ='متاح على';
		$lang['android'] ='الروبوت أب أون';
        $lang['iphone1'] ='متجر التطبيقات';
		$lang['android1'] ='تحميل التطبيق';   
        
   
        
    /**************Sign_in_page************/
          $lang['driver_desc'] = 'العثور على كل ما تحتاجه لتتبع نجاحك على الطريق.';
          $lang['rider_desc'] = 'يمكنك إدارة خيارات الدفع ومراجعة سجل الرحلة والمزيد.';
          $lang['driver_signin'] = 'سائق تسجيل الدخول';
          $lang['rider_signin'] = 'رايدر تسجيل الدخول';
          
  /**************DriverSignUp page************/
          $lang['driversignup'] = 'سائق الاشتراك';
          $lang['firstname'] = 'الاسم الاول';
		  $lang['lastname'] = 'الكنية';
		  $lang['youremail'] = 'بريدك الالكتروني';
		  $lang['yourpass'] = 'كلمة السر خاصتك';
		  $lang['selectcity'] = 'الرجاء اختيار المدينة';
		  $lang['selectcar'] = 'الرجاء اختيار نوع السيارة';
		  $lang['selectmodel'] = 'الرجاء اختيار نموذج';
		  $lang['carnumber'] = 'رقم السياره';
		  $lang['ridetype'] = 'الرجاء تحديد نوع ركوب';
		  $lang['signup'] = 'سجل';
		  $lang['alreadysignin'] = 'هل لديك حساب بالفعل؟';
          $lang['normal'] = 'ركوب عادي';
		  $lang['rental'] = 'تأجير';
		  $lang['both'] = 'كلا';  
		  
  /**************DriverSignIN_in_page************/
          $lang['driversignin'] = 'سائق تسجيل الدخول';
          $lang['entermob'] = 'أدخل رقم هاتفك المحمول';
		  $lang['enteremail'] = 'أدخل الرقم التعريفي للبريد الإلكتروني';
		  $lang['password'] = 'كلمه السر';
		  $lang['next'] = 'التالى';
		  $lang['dontsignup'] = 'ليس لديك حساب؟';
  /**************DriverProfile_page************/
          $lang['drprofile'] = 'الملف الشخصي';
          $lang['drmyrides'] = 'بلدي ركوب الخيل';
		  $lang['drdocument'] = 'مستندات';
		  $lang['drchpass'] = 'تغيير كلمة السر';
		  $lang['drsupport'] = 'دعم العملاء';
		  $lang['drabout'] = 'معلومات عنا';
		  $lang['drlogout'] = 'الخروج';
		  $lang['drName'] = 'اسم';
		  $lang['drName1'] = 'اسمك';
          $lang['dremail'] = 'البريد الإلكتروني';
		  $lang['dremail1'] = 'بريدك الالكتروني';
		  $lang['drphone'] = 'رقم الهاتف';
		  $lang['drphone1'] = 'رقم تليفونك';
		  $lang['updatepro'] = 'تحديث الملف';
		  $lang['online'] = 'عبر الانترنت';
		  $lang['offline'] = 'غير متصل على الانترنت';
		  
		  
  /**************DriverRides_page************/
          $lang['myridesprofile'] = 'الملف الشخصي';
          $lang['myridesmyride'] = 'بلدي ركوب الخيل';
		  $lang['myridesridername'] = 'اسم رايدر';
		  $lang['myridesphone'] = 'هاتف';
		  $lang['myridesdate'] = 'تاريخ';
		  $lang['myridesstatus'] = 'الحالة';
		  
  /**************DriverDocuments_page************/
          $lang['dryourdocs'] = 'المستندات الخاصة بك';
       
  /**************DriverChangePassword_page************/
          $lang['drchangepass'] = 'تغيير كلمة السر';	
          $lang['drchangeoldpass'] = 'كلمة المرور القديمة';	
		  $lang['drchangenewpass'] = 'كلمة السر الجديدة';	
		  $lang['drchangeconpass'] = 'تأكيد كلمة المرور';	  		  
	
  /**************User Sign IN_page************/
          $lang['usrloginmob'] = 'أدخل رقم هاتفك المحمول';	
          $lang['usrloginnxt'] = 'التالى';

  /**************User Sign UP_page************/
          $lang['usrsigninacc'] = 'إنشاء حساب أبوريو تاكسي الخاص بك';	
          $lang['usrsigninacc1'] = 'أدخل تفاصيلك لإنشاء حساب';	
          $lang['usrsigninfullname'] = 'أدخل الاسم الكامل';	
          $lang['usrsigninemail'] = 'أدخل عنوان البريد الإلكتروني(اختياري)';	
          $lang['usrsigninsave'] = 'حفظ التفاصيل';	
          $lang['usrsigninterms'] = 'من خلال التسجيل، فإنك توافقعلى أبوريو تاكسي';	
          $lang['usrsigninterms1'] = 'البنود و الظروف';		


  /**************Normal Booking Main Page************/
          $lang['normalfrom'] = 'من عند';	
          $lang['normalto'] = 'إلى';	
          $lang['normalwhen'] = 'متى';	
          $lang['normalavaliable'] = 'متاح ركوب الخيل';	
		  $lang['normallogin'] = 'تسجيل الدخول';
     			  

  /**************Rental Booking Main Page************/
          $lang['rentalfrom'] = 'من عند';	
          $lang['rentalto'] = 'إلى';	
          $lang['rentalwhen'] = 'متى';	
          $lang['rentalavaliable'] = 'متاح ركوب الخيل';	
		  $lang['rentallogin'] = 'تسجيل الدخول';
		  $lang['rentalpkg'] = 'صفقة';
		  $lang['rentalselpkg'] = 'الرجاء اختيار حزمة';
     			  


  /**************Navigation Bar Page************/
          $lang['navbarhome'] = 'الصفحة الرئيسية';	
          $lang['navbarbookride'] = 'حجز ركوب الخاص بك';	
          $lang['navbaryourride'] = 'ركوب الخيل الخاص بك';	
          $lang['navbarratecard'] = 'سعر البطاقة';	
		  $lang['navbarsupport'] = 'الدعم';
		  $lang['navbarappdown'] = 'تحميل التطبيق';

  /**************Normal Booking Location Page************/
          $lang['normallocfrom'] = 'أدخل موقع بيك آب';	
          $lang['normallocto'] = 'أدخل إسقاط الموقع';	
        

  /**************Rental Booking Location Page************/
          $lang['rentallocfrom'] = 'أدخل موقع بيك آب';	
          $lang['rentallocto'] = 'أدخل إسقاط الموقع';	
        		 
  /**************User Profile Page************/
          $lang['useryourprofile'] = 'ملفك الشخصي';	
          $lang['userseeallridesprofile'] = 'رؤية جميع ركوب الخيل';
          $lang['userseelogoutprofile'] = 'الخروج';
          $lang['userseenoridesprofile'] = 'ليس لديك ركوب، يرجى حجز رحلة الآن ..';
		   $lang['userseeshowconfirmprofile'] = 'هل أنت متأكد أنك تريد تسجيلالخروج ؟';
		  
  /**************User All Trips Page************/
          $lang['userallridesyourride'] = 'ركوب الخاص بك';	
		  $lang['userallridesnoride'] = 'لا ركوب الخيل الآن، يرجى القيامركوب الآن ..';
         

  /**************BookingConfirm pages Normal************/
          $lang['bookingpick'] = 'امسك';	
		  $lang['bookingdrop'] = 'قطرة';	
		  $lang['bookingfare'] = 'أجرة';
		  $lang['bookingfare1'] = '(فارس أعلى من المعتاد)';
		  $lang['bookingpayby'] = 'ادفع عن طريق';
          $lang['bookingcoupon'] = 'القسيمة';
		  $lang['bookingcoupon1'] = 'أدخل رمز القسيمة (اختياري)';	
          $lang['bookingmodel1'] = 'تاكيد الحجز';	
		  $lang['bookingmodel2'] = 'يول تلقي تفاصيل ركوب 15 دقيقة قبل وقت بيك اب.';		  
		  $lang['bookingcofirm'] = 'تأكيد الحجز';			  
		  $lang['bookingpickupon'] = 'لاقط على';		

  /**************BookingConfirm pages Rental************/
          $lang['bookingrentalpick'] = 'امسك';	
		  $lang['bookingrentalpayby'] = 'ادفع عن طريق';
		  $lang['bookingrentalcoupon'] = 'القسيمة';
		  $lang['bookingrentalcoupon1'] = 'أدخل رمز القسيمة (اختياري)';
		  $lang['bookingrentalmodel1'] = 'تاكيد الحجز';	
		  $lang['bookingrentalmodel2'] = 'يول تلقي تفاصيل ركوب 15 دقيقةقبل وقت بيك اب.';		  
		  $lang['bookingrentalcofirm'] = 'تأكيد الحجز';			  
		  $lang['bookingrentalpickupon'] = 'لاقط على';			  

		  $lang['bookingrentalhour'] = 'ساعات';	
		  $lang['bookingrentalmiles'] = 'اميال';
		  $lang['bookingrentalmiles1'] = 'اميال';
		  $lang['bookingrentalbasefare'] = 'الاجرة الاساسية';
		  $lang['bookingrentalincludes'] = 'يشمل';	
		  $lang['bookingrentaladditionaldis'] = 'إضافية المسافة أجرة';	
          $lang['bookingrentaladditionaltime'] = 'الوقت الاضافي';			  
		  $lang['bookingrentalafterfirst'] = 'بعد أول';			  
		 	
  /**************Location From Map Page************/
          $lang['maplocation'] = 'حرك الخريطة لضبط الموقع';	
		  $lang['maplocationconfirm'] = 'تأكيد الموقع';	
		  
		  
  /**************Book View Ride************/
          $lang['mailsuccess'] = 'تم إرسال الفاتورة بنجاح إلى معرف البريد.';	
		  $lang['mailclose'] = 'قريب';	
		  $lang['mailinvoice'] ='فاتورة البريد';
		  $lang['mailinvoice1'] = 'أدخل معرف البريد الإلكتروني';	
		  $lang['sendinvoice'] = 'إرسال الفاتورة';	
		  $lang['mailinvoice'] ='فاتورة البريد';
		  $lang['selectcancel'] ='حدد إلغاء السبب';
		  $lang['cancelride'] = 'إلغاء ركوب';	
		  $lang['suppourt_ride'] = 'الدعم';	
		  $lang['calldriver'] = 'استدعاء سائق';	
		  $lang['callbelow'] = 'اتصل على الرقم أدناه';	
		  $lang['later_ride'] ='وسيتم تقاسم التفاصيل 15 دقيقةقبل وقت بيك اب. '; 
		  
		  
   /*************No Drivers Page************/
          $lang['noridesorry'] = 'عذرا، لا يتوفر برنامج تشغيل الآن';	
		  $lang['noridegoback'] = 'عُد';	

   /*************Statuses************/
          $lang['Scheduled'] = 'المقرر';	
		  $lang['New_Booking'] = 'حجز جديد';	
		  $lang['Cancelled_By_User'] = 'تم الإلغاء بواسطة المستخدم';	
		  $lang['Accepted_by_Driver'] = 'قبلها سائق';	 
		  $lang['Cancelled_by_driver'] = 'تم إلغاؤها بواسطة برنامج التشغيل';	
		  $lang['Driver_Arrived'] = 'وصل السائق';	
		  $lang['Trip_Started'] = 'بدأت الرحلة';	
		  $lang['Trip_Completed'] = 'اكتملت الرحلة';	 
		  $lang['Trip_Book_By_Admin'] = 'كتاب الرحلة من قبل المشرف';	
		  $lang['Trip_Cancel_By_Admin'] = 'رحلة إلغاء بواسطة المشرف';	 
		  $lang['Trip_Reject_By_Driver'] = 'رحلة رفض بواسطة سائق';	
		  $lang['Trip_Cancel_By_User'] = 'رحلة إلغاء بواسطة المستخدم';
          $lang['Trip_Reject_By_User'] = 'رحلة رفض من قبل المستخدم';		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		
		  
		  