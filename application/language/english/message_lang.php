<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*******ENGLISH_LANGUAGE*******/
/******************************/
	/*****Headers**********/
		$lang['sign'] = 'Sign in';
		$lang['become_driver'] = 'BECOME A DRIVER';
		$lang['book_now'] = 'Book Now';
		$lang['logout'] = 'Log Out';
		$lang['dashboard'] = 'Dashboard';
		$lang['myrides'] = 'My Rides';
	

	/******Index_page_start***********/
  	/*****Index_BannerImage**********/
	  
	    $lang['bannertxt1'] = 'Book an TaxiUapp to your destination in town';
		$lang['bannertxt2'] = 'Choose from a range of categories and prices';
        $lang['bnrsearchbox1'] = 'Your Everyday Travel Partner'; 
        $lang['bnrsearchbox2'] = 'AC Cabs for point to point travel';

    /***********form data************/	
    	$lang['cityheader'] = 'TaxiUapp';	
		$lang['rentalheader'] = 'RENTAL';
        $lang['pickup'] = 'PICKUP';	
		$lang['drop'] = 'DROP';
		$lang['when'] = 'WHEN';
		$lang['depart'] = 'DEPART';
		$lang['enterpick'] = 'Enter Your Pickup Location';
		$lang['enterdrop'] = 'Enter Drop off Location for Ride Estimate';
		$lang['schlater'] = 'Schedule for Later';
		$lang['now'] = 'Now';
		$lang['searchbtn'] = 'Search Vehicles';
        
 
    /*****Index_Footer**********/
     
        $lang['Home'] ='Home';
        $lang['about_us'] ='About Us';
        $lang['contact_us'] ='Contact Us';
	    $lang['iphone'] ='AVAILABLE ON THE';
		$lang['android'] ='ANDROID APP ON';
        $lang['iphone1'] ='App Store';
		$lang['android1'] ='Download App';   
        
   
        
    /**************Sign_in_page************/
          $lang['driver_desc'] = 'Find everything you need to track your success on the road.';
          $lang['rider_desc'] = 'Manage your payment options, review trip history, and more.';
          $lang['driver_signin'] = 'Driver Sign in';
          $lang['rider_signin'] = 'Rider Sign in';
          
  /**************DriverSignUp page************/
          $lang['driversignup'] = 'Driver Sign up';
          $lang['firstname'] = 'First Name';
		  $lang['lastname'] = 'Last Name';
		  $lang['youremail'] = 'Your Email';
		  $lang['yourpass'] = 'Your password';
		  $lang['selectcity'] = 'Please Select City';
		  $lang['selectcar'] = 'Please Select Car Type';
		  $lang['selectmodel'] = 'Please Select Model';
		  $lang['carnumber'] = 'Car Number';
		  $lang['ridetype'] = 'Please Select Type of Ride';
		  $lang['signup'] = 'SIGN UP';
		  $lang['alreadysignin'] = 'Already have an account ?';
          $lang['normal'] = 'Normal Ride';
		  $lang['rental'] = 'Rental';
		  $lang['both'] = 'Both';  
		  
  /**************DriverSignIN_in_page************/
          $lang['driversignin'] = 'Driver Sign In';
          $lang['entermob'] = 'Enter your mobile number';
		  $lang['enteremail'] = 'Enter your email ID';
		  $lang['password'] = 'Password';
		  $lang['next'] = 'Next';
		  $lang['dontsignup'] = 'Dont have an account ?';
  /**************DriverProfile_page************/
          $lang['drprofile'] = 'Profile';
          $lang['drmyrides'] = 'My Rides';
		  $lang['drdocument'] = 'Documents';
		  $lang['drchpass'] = 'Change Password';
		  $lang['drsupport'] = 'Customer Support';
		  $lang['drabout'] = 'About us';
		  $lang['drlogout'] = 'Logout';
		  $lang['drName'] = 'Name';
		  $lang['drName1'] = 'Your Name';
          $lang['dremail'] = 'Email';
		  $lang['dremail1'] = 'Your Email';
		  $lang['drphone'] = 'Phone Number';
		  $lang['drphone1'] = 'Your Phone Number';
		  $lang['updatepro'] = 'Update Profile';
		  $lang['online'] = 'Online';
		  $lang['offline'] = 'Offline';
		  
		  
  /**************DriverRides_page************/
          $lang['myridesprofile'] = 'Profile';
          $lang['myridesmyride'] = 'My Rides';
		  $lang['myridesridername'] = 'Rider Name';
		  $lang['myridesphone'] = 'Phone';
		  $lang['myridesdate'] = 'Date';
		  $lang['myridesstatus'] = 'Status';
		  
  /**************DriverDocuments_page************/
          $lang['dryourdocs'] = 'Your Documents';
       
  /**************DriverChangePassword_page************/
          $lang['drchangepass'] = 'Change Password';	
          $lang['drchangeoldpass'] = 'Old Password';	
		  $lang['drchangenewpass'] = 'New Password';	
		  $lang['drchangeconpass'] = 'Confirm Password';	  		  
	
  /**************User Sign IN_page************/
          $lang['usrloginmob'] = 'Enter Your Mobile Number';	
          $lang['usrloginnxt'] = 'Next';

  /**************User Sign UP_page************/
          $lang['usrsigninacc'] = 'Create your TaxiUapp account';	
          $lang['usrsigninacc1'] = 'Enter your details to create an account';	
          $lang['usrsigninfullname'] = 'Enter Full Name';	
          $lang['usrsigninemail'] = 'Enter Email Address(Optional)';	
          $lang['usrsigninsave'] = 'Save Details';	
          $lang['usrsigninterms'] = 'By registering, you are agreeing to TaxiUapp';	
          $lang['usrsigninterms1'] = 'Terms & Conditions';		


  /**************Normal Booking Main Page************/
          $lang['normalfrom'] = 'FROM';	
          $lang['normalto'] = 'TO';	
          $lang['normalwhen'] = 'WHEN';	
          $lang['normalavaliable'] = 'Avaliable Rides ';	
		  $lang['normallogin'] = 'LOGIN';
     			  

  /**************Rental Booking Main Page************/
          $lang['rentalfrom'] = 'FROM';	
          $lang['rentalto'] = 'TO';	
          $lang['rentalwhen'] = 'WHEN';	
          $lang['rentalavaliable'] = 'Avaliable Rides ';	
		  $lang['rentallogin'] = 'LOGIN';
		  $lang['rentalpkg'] = 'PACKAGE';
		  $lang['rentalselpkg'] = 'Please Select a Package';
     			  


  /**************Navigation Bar Page************/
          $lang['navbarhome'] = 'Home';	
          $lang['navbarbookride'] = 'Book Your Ride';	
          $lang['navbaryourride'] = 'Your rides';	
          $lang['navbarratecard'] = 'Rate Card';	
		  $lang['navbarsupport'] = 'Support';
		  $lang['navbarappdown'] = 'Download App';

  /**************Normal Booking Location Page************/
          $lang['normallocfrom'] = 'Enter Pickup Location ';	
          $lang['normallocto'] = 'Enter Drop Location';	
        

  /**************Rental Booking Location Page************/
          $lang['rentallocfrom'] = 'Enter Pickup Location';	
          $lang['rentallocto'] = 'Enter Drop Location';	
        		 
  /**************User Profile Page************/
          $lang['useryourprofile'] = 'Your Profile';	
          $lang['userseeallridesprofile'] = 'See all rides';
          $lang['userseelogoutprofile'] = 'Log out';
          $lang['userseenoridesprofile'] = 'You have no rides, please book a ride now..';
		   $lang['userseeshowconfirmprofile'] = 'Are you sure you want to log out ?';
		  
  /**************User All Trips Page************/
          $lang['userallridesyourride'] = 'Your Rides';	
		  $lang['userallridesnoride'] = 'No rides til now , Please Do a Ride now..';
         

  /**************BookingConfirm pages Normal************/
          $lang['bookingpick'] = 'PICKUP';	
		  $lang['bookingdrop'] = 'DROP';	
		  $lang['bookingfare'] = 'FARE';
		  $lang['bookingfare1'] = '(Fares are higher than usual)';
		  $lang['bookingpayby'] = 'PAYBY';
          $lang['bookingcoupon'] = 'COUPON';
		  $lang['bookingcoupon1'] = 'Enter coupon code(optional)';	
          $lang['bookingmodel1'] = 'Booking Confirmed';	
		  $lang['bookingmodel2'] = 'Youll recieve ride details 15 min before pickup time.';		  
		  $lang['bookingcofirm'] = 'Confirm Booking';			  
		  $lang['bookingpickupon'] = 'Pickup on';		

  /**************BookingConfirm pages Rental************/
          $lang['bookingrentalpick'] = 'PICKUP';	
		  $lang['bookingrentalpayby'] = 'PAYBY';
		  $lang['bookingrentalcoupon'] = 'COUPON';
		  $lang['bookingrentalcoupon1'] = 'Enter coupon code(optional)';
		  $lang['bookingrentalmodel1'] = 'Booking Confirmed';	
		  $lang['bookingrentalmodel2'] = 'Youll recieve ride details 15 min before pickup time.';		  
		  $lang['bookingrentalcofirm'] = 'Confirm Booking';			  
		  $lang['bookingrentalpickupon'] = 'Pickup on';			  

		  $lang['bookingrentalhour'] = 'hours';	
		  $lang['bookingrentalmiles'] = 'Miles';
		  $lang['bookingrentalmiles1'] = 'miles';
		  $lang['bookingrentalbasefare'] = 'Base Fare';
		  $lang['bookingrentalincludes'] = 'includes';	
		  $lang['bookingrentaladditionaldis'] = 'Additional Distance Fare';	
          $lang['bookingrentaladditionaltime'] = 'Additional Time Fare';			  
		  $lang['bookingrentalafterfirst'] = 'After first';			  
		 	
  /**************Location From Map Page************/
          $lang['maplocation'] = 'Move Map to Adjust Location';	
		  $lang['maplocationconfirm'] = 'Confirm Location';	
		  
		  
  /**************Book View Ride************/
          $lang['mailsuccess'] = 'Invoice Succesfully sent to your mail-Id.';	
		  $lang['mailclose'] = 'Close';	
		  $lang['mailinvoice'] ='Mail Invoice';
		  $lang['mailinvoice1'] = 'Enter your Email Id';	
		  $lang['sendinvoice'] = 'Send Invoice';	
		  $lang['mailinvoice'] ='Mail Invoice';
		  $lang['selectcancel'] ='Select cancel reason';
		  $lang['cancelride'] = 'Cancel ride';	
		  $lang['suppourt_ride'] = 'Support';	
		  $lang['calldriver'] = 'Call Driver';	
		  $lang['callbelow'] = 'Call On below Number';	
		  $lang['later_ride'] ='Details will be shared 15 min before pickup time. '; 
		  
		  
   /*************No Drivers Page************/
          $lang['noridesorry'] = 'Sorry , no driver available now';	
		  $lang['noridegoback'] = 'Go back';	

   /*************Statuses************/
          $lang['Scheduled'] = 'Scheduled';	
		  $lang['New_Booking'] = 'New Booking';	
		  $lang['Cancelled_By_User'] = 'Cancelled By User';	
		  $lang['Accepted_by_Driver'] = 'Accepted by Driver';	 
		  $lang['Cancelled_by_driver'] = 'Cancelled by driver';	
		  $lang['Driver_Arrived'] = 'Driver Arrived';	
		  $lang['Trip_Started'] = 'Trip Started';	
		  $lang['Trip_Completed'] = 'Trip Completed';	 
		  $lang['Trip_Book_By_Admin'] = 'Trip Book By Admin';	
		  $lang['Trip_Cancel_By_Admin'] = 'Trip Cancel By Admin';	 
		  $lang['Trip_Reject_By_Driver'] = 'Trip Reject By Driver';	
		  $lang['Trip_Cancel_By_User'] = 'Trip Cancel By User';
          $lang['Trip_Reject_By_User'] = 'Trip Reject By User';		  
		  
		  
		  

 
		  		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		
		  
		  