Rental - Car Type Api: October 28, 2017, 7:14 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => STANDARD
            [car_type_image] => uploads/car/editcar_2.jpg
            [city_id] => 121
            [base_fare] => 8 Per 1 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.jpg
            [city_id] => 121
            [base_fare] => 10 Per 1 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => MINIVAN
            [car_type_image] => uploads/car/editcar_4.jpg
            [city_id] => 121
            [base_fare] => 12 Per 1 Km
            [ride_mode] => 1
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Almere
latitude: 52.353966140686225
longitude: 5.154028236865997
-------------------------
Rental - Car Type Api: October 28, 2017, 9:22 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => STANDARD
            [car_type_image] => uploads/car/editcar_2.jpg
            [city_id] => 121
            [base_fare] => 8 Per 1 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.jpg
            [city_id] => 121
            [base_fare] => 10 Per 1 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => MINIVAN
            [car_type_image] => uploads/car/editcar_4.jpg
            [city_id] => 121
            [base_fare] => 12 Per 1 Km
            [ride_mode] => 1
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Almere
latitude: 52.353967164588525
longitude: 5.154002420604229
-------------------------
Rental - Car Type Api: October 28, 2017, 9:22 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => STANDARD
            [car_type_image] => uploads/car/editcar_2.jpg
            [city_id] => 121
            [base_fare] => 8 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => EUR
            [currency_unicode] => 20AC
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.jpg
            [city_id] => 121
            [base_fare] => 10 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => EUR
            [currency_unicode] => 20AC
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => MINIVAN
            [car_type_image] => uploads/car/editcar_4.jpg
            [city_id] => 121
            [base_fare] => 12 Per 1 Km
            [ride_mode] => 1
            [currency_iso_code] => EUR
            [currency_unicode] => 20AC
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Amsterdam
latitude: 52.379128336607884
longitude: 4.900272153317929
-------------------------
Rental - Car Type Api: October 28, 2017, 11:21 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => STANDARD
            [car_type_image] => uploads/car/editcar_2.jpg
            [city_id] => 121
            [base_fare] => 5 Per 1 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.jpg
            [city_id] => 121
            [base_fare] => 8 Per 1 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => MINIVAN
            [car_type_image] => uploads/car/editcar_4.jpg
            [city_id] => 121
            [base_fare] => 10 Per 1 Km
            [ride_mode] => 1
        )

    [3] => Array
        (
            [car_type_id] => 001
            [car_type_name] => Rental
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 121
            [distance] => 
            [base_fare] => 
            [ride_mode] => 2
        )

)

city_name: Almere
latitude: 52.3539242958024
longitude: 5.15427373640698
-------------------------
