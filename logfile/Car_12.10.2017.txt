Rental - Car Type Api: October 12, 2017, 11:29 am
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => STANDARD
            [car_type_image] => uploads/car/editcar_2.jpg
            [city_id] => 56
            [base_fare] => 100 Per 4 Km
            [ride_mode] => 1
            [currency_iso_code] => AUD
            [currency_unicode] => 0024
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.jpg
            [city_id] => 56
            [base_fare] => 100 Per 4 Km
            [ride_mode] => 1
            [currency_iso_code] => AUD
            [currency_unicode] => 0024
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => MINIVAN
            [car_type_image] => uploads/car/editcar_4.jpg
            [city_id] => 56
            [base_fare] => 178 Per 4 Km
            [ride_mode] => 1
            [currency_iso_code] => AUD
            [currency_unicode] => 0024
        )

)

city_name: Gurugram
latitude: 28.412192247401716
longitude: 77.04318970441818
-------------------------
Rental - Car Type Api: October 12, 2017, 8:21 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => STANDARD
            [car_type_image] => uploads/car/editcar_2.jpg
            [city_id] => 121
            [base_fare] => 3 Per 1 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => LUXURY
            [car_type_image] => uploads/car/editcar_3.jpg
            [city_id] => 121
            [base_fare] => 5 Per 1 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => MINIVAN
            [car_type_image] => uploads/car/editcar_4.jpg
            [city_id] => 121
            [base_fare] => 6 Per 1 Km
            [ride_mode] => 1
        )

)

city_name: Almere
latitude: 52.35143231937175
longitude: 5.176226869225502
-------------------------
