<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Driver extends REST_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('Drivermodel');
        $this->load->model('Usermodel');
    }

    public function Driver_Report_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        if(!empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
               $all_rides = $this->Drivermodel->all_rides($driver_id);
               if($all_rides != 0)
               {
                    $acceptance_rides = $this->Drivermodel->all_acceptance_rides($driver_id);
                    $daily_acceptance = ($acceptance_rides/$all_rides)*100;
                    $daily_acceptance = round($daily_acceptance);
                    $daily_acceptance = $daily_acceptance." %";
               }else{
                    $daily_acceptance = "";
               }
               $avrage_rating = $this->Drivermodel->avarage_rating($driver_id);
               $avrage_rating = round($avrage_rating);
               $online = $this->Drivermodel->online_time($driver_id);
               if(!empty($online)){
                  $total_time  = $online->total_time;
                  $online_time = $online->online_time;
                  if(empty($total_time)){
                      $new_date = date("Y-m-d H:i:s");
                      $datetime1 = new DateTime($online_time);
                      $datetime2 = new DateTime($new_date);
                      $interval = $datetime1->diff($datetime2);
                      $total_time = $interval->format('%h')." Hours ".$interval->format('%i')." Minutes";
                  }
               }else{
                   $total_time = "";
               }
               $data = array('daily_acceptance_rate'=>$daily_acceptance,'avrage_rating'=>$avrage_rating,'online_time'=>$total_time);
                $this->response([
                    'status' => 1,
                    'message' => 'Driver Daily Report',
                    'details' =>$data
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }

    public function Signup_Stap_One_post()
    {
        $firstname = $this->post('firstname');
        $lastname = $this->post('lastname');
        $phone = $this->post('phone');
        $email = $this->post('email');
        $password = $this->post('password');
        if(!empty($firstname) && !empty($lastname) && !empty($phone) && !empty($email) && !empty($password))
        {
            $phone_check = $this->Drivermodel->check_phone($phone);
            if(empty($phone_check))
            {
                $driver_token = $this->generateRandomString();
                $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                $data = $dt->format('M j');
                $day = date("l");
                $date = $day . ", " . $data;
                $time = date("h:i A");
                $date_add = $date . ", " . $time;

                $data = array(
                    'driver_firstname'=>$firstname,
                    'driver_lastname'=>$lastname,
                    'driver_phone'=>$phone,
                    'driver_email'=>$email,
                    'driver_password'=>$password,
                    'driver_token'=>$driver_token,
                    'driver_signup_date'=>$date_add
                );
                $text = $this->Drivermodel->Signup_Stap_One($data);
                $text->car_type_name ="";
                $text->car_model_name ="";
                $text->car_model_image ="";
                $this->set_response([
                    'status' => 1,
                    'message' => 'Signup Step One Complete',
                    'details'=>$text
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 0,
                    'message' => 'Phone Number Already Register'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }


    public function Signup_Stap_Two_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        if(!empty($driver_id) && !empty($driver_token) && !empty($_FILES['image']['name']))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $config = [
                    'upload_path' => './uploads/',
                    'allowed_types' => '*'
                ];
                $this->load->library('upload', $config);
                if($this->upload->do_upload('image') == TRUE)
                {
                    $data = $this->upload->data();
                    $image = base_url("uploads/" . $data['raw_name'] . $data['file_ext']);
                    $this->Drivermodel->Signup_Stap_Two($image,$driver_id);
                    $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
                    $driver->car_type_name ="";
                    $driver->car_model_name ="";
                    $driver->car_model_image ="";
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Signup Step Two Complete',
                        'details'=>$driver
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'status' => 0,
                        'message' => 'Image Not Uploaded'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }

    function Signup_Stap_Three_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $city_id = $this->post('city_id');
        $car_type_id = $this->post('car_type_id');
        $car_model_id  = $this->post('car_model_id');
        $car_number = $this->post('car_number');
        $car_model_date = $this->post('car_model_date');
        if(!empty($driver_id) && !empty($driver_token) && !empty($city_id) && !empty($car_type_id) && !empty($car_model_id) && !empty($car_number) && !empty($car_model_date) && !empty($_FILES['car_number_plate_image']['name']))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $config = [
                    'upload_path' => './uploads/',
                    'allowed_types' => '*'
                ];
                $this->load->library('upload', $config);
                $this->upload->do_upload('car_number_plate_image');
                $data = $this->upload->data();
                $car_number_plate_image = base_url("uploads/" . $data['raw_name'] . $data['file_ext']);
                $this->upload->do_upload('car_image');
                $car = $this->upload->data();
                $car_image = base_url("uploads/" . $car['raw_name'] . $car['file_ext']);
                $data = array(
                    'driver_city_id'=>$city_id,
                    'driver_car_type_id'=>$car_type_id,
                    'driver_car_model_id'=>$car_model_id,
                    'driver_car_number'=>$car_number,
                    'driver_car_model_date'=>$car_model_date,
                    'driver_car_number_plate_image'=>$car_number_plate_image,
                    'driver_car_image'=>$car_image,
                    'signup_step'=>3
                );
                $text = array(
                    'driver_id'=>$driver_id,
                    'driver_car_type_id'=>$car_type_id,
                    'driver_car_model_id'=>$car_model_id,
                    'driver_car_number'=>$car_number,
                    'driver_car_model_date'=>$car_model_date,
                    'driver_car_number_plate_image'=>$car_number_plate_image,
                    'driver_car_image'=>$car_image,
                    'driver_car_activate'=>1
                );
                $this->Drivermodel->add_new_car($text);
                $this->Drivermodel->Signup_Stap_Three($data,$driver_id);
                $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
                $car_type = $this->Drivermodel->car_type($car_type_id);
                $driver->car_type_name = $car_type->car_type_name;
                $car_model = $this->Drivermodel->car_model($car_model_id);
                $driver->car_model_name = $car_model->car_model_name;
                $driver->car_model_image = $car_model->car_model_image;
                $this->set_response([
                    'status' => 1,
                    'message' => 'Signup Step Three Complete',
                    'details'=>$driver
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Signup_Stap_Four_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $account_number = $this->post('account_number');
        $account_holder_number = $this->post('account_holder_number');
        $bank_name = $this->post('bank_name');
        $ifsc_code  = $this->post('ifsc_code');
        if(!empty($driver_id) && !empty($driver_token) && !empty($account_number) && !empty($account_holder_number) && !empty($bank_name) && !empty($ifsc_code))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            
            if(!empty($driver))
            {
                $data = array(
                    'driver_account_number'=>$account_number,
                    'driver_bank_account_holder_number'=>$account_holder_number,
                    'driver_bank_name'=>$bank_name,
                    'driver_bank_ifsc_code'=>$ifsc_code,
                    'signup_step'=>4
                );
                $this->Drivermodel->Signup_Stap_Four($data,$driver_id);
                $car_type_id = $driver->driver_car_type_id;
                $car_model_id = $driver->driver_car_model_id;
                $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
                $car_type = $this->Drivermodel->car_type($car_type_id);
                $driver->car_type_name = $car_type->car_type_name;
                $car_model = $this->Drivermodel->car_model($car_model_id);
                $driver->car_model_name = $car_model->car_model_name;
                $driver->car_model_image = $car_model->car_model_image;
                $this->set_response([
                    'status' => 1,
                    'message' => 'Signup Step Four Complete',
                    'details'=>$driver
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Login_post()
    {
        $phone = $this->post('phone');
        $password = $this->post('password');
        if(!empty($phone) && !empty($password))
        {
            $data = $this->Drivermodel->login($phone,$password);
            if(!empty($data))
            {
                $driver_id = $data->driver_id;
                $driver_token = $this->generateRandomString();
                $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                $data = $dt->format('M j');
                $day = date("l");
                $date = $day . ", " . $data;
                $time = date("h:i A");
                $date_add = $date . ", " . $time;

                $data = array(
                    'driver_token'=>$driver_token,
                    'driver_last_login'=>$date_add,
                    'driver_login_logout'=>1
                );
                $data = $this->Drivermodel->update_driver_token($data,$driver_id);
                $car_type_id = $data->driver_car_type_id;
                $car_model_id = $data->driver_car_model_id;
                if($car_type_id == 0)
                {
                    $car_type_name = "";
                    $car_type_image = "";
                }else{
                    $car_type = $this->Drivermodel->car_type($car_type_id);
                    $car_type_name = $car_type->car_type_name;
                    $car_type_image = $car_type->car_type_image;
                }
                if ($car_model_id == 0)
                {
                    $car_model_name = "";
                    $car_model_image = "";
                }else{
                    $car_model = $this->Drivermodel->car_model($car_model_id);
                    $car_model_name = $car_model->car_model_name;
                    $car_model_image = $car_model->car_model_image;
                }
                $data->car_type_name = $car_type_name;
                $data->car_type_image = $car_type_image;
                $data->car_model_name = $car_model_name;
                $data->car_model_image = $car_model_image;
                $this->set_response([
                    'status' => 1,
                    'message' => 'Login Succesfully',
                    'details'=>$data
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 0,
                    'message' => 'Phone Number And Password Not Match'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Device_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $device_id = $this->post('device_id');
        $flag = $this->post('flag');
        $device = $this->post('device');
        if(!empty($driver_id) && !empty($driver_token) && !empty($device_id) && !empty($flag) && !empty($device))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $data = array(
                    'driver_device_id'=>$device_id,
                    'driver_device_flag'=>$flag,
                    'driver_phone_info'=>$device,
                );
                $this->Drivermodel->add_device($data,$driver_id);
                $this->set_response([
                    'status' => 1,
                    'message' => 'Device ID Updated'
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function online_offline_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $status = $this->post('status');
        if(!empty($driver_id) && !empty($driver_token) && !empty($status))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                $data = $dt->format('M j');
                $day = date("l");
                $date = $day . ", " . $data;
                $time = date("h:i A");
                $date_add = $date . ", " . $time;
                if($status == 1)
                {
                    $data = array(
                        'driver_online_offline'=>1,
                        'driver_last_online'=>$date_add
                    );
                    $this->Drivermodel->driver_online($data,$driver_id);
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Online Now'
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $data = array(
                        'driver_online_offline'=>2,
                        'driver_last_offline'=>$date_add
                    );
                    $this->Drivermodel->driver_offline($data,$driver_id);
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Offline Now'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' => 0,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function Document_List_post()
    {
        $driver_id = $this->post('driver_id');
        $city_id = $this->post('city_id');
        $car_type_id = $this->post('car_type_id');
        if (!empty($driver_id) && !empty($car_type_id) && !empty($city_id))
        {
            $text = $this->Drivermodel->Document_List($car_type_id,$city_id);
            if(!empty($text)){
                foreach ($text as $key=>$value)
                {
                    $document_id = $value['document_id'];
                    $data = $this->Drivermodel->check_document($document_id,$driver_id);
                    if(!empty($data))
                    {
                        $documnet_varification_status = $data->documnet_varification_status;
                    }else{
                        $documnet_varification_status = "0";
                    }
                    $text[$key] = $value;
                    $text[$key]['documnet_varification_status'] = $documnet_varification_status;
                }
                $query = $this->Drivermodel->check_bankdetails($driver_id);
                $driver_account_number = $query->driver_account_number;
                if(!empty($driver_account_number))
                {
                    $a = array(
                        "document_id"=> "00",
                        "document_name"=> "Bank Details ",
                        "car_document_id"=> "00",
                        "city_id"=>"00",
                        "car_type_id"=> "00",
                        "car_document_status"=> "00",
                        "documnet_varification_status"=> "1"
                    );
                }else{
                    $a = array(
                        "document_id"=> "00",
                        "document_name"=> "Bank Details ",
                        "car_document_id"=> "00",
                        "city_id"=>"00",
                        "car_type_id"=> "00",
                        "car_document_status"=> "00",
                        "documnet_varification_status"=> "0"
                    );
                }
                array_push($text,$a);
                $this->set_response([
                    'status' => 1,
                    'message' => 'Document To Be Upload',
                    'details'=>$text
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->set_response([
                    'status' => 0,
                    'message' => 'No List Found',

                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function Document_Upload_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $document_id = $this->post('document_id');
        if(!empty($driver_id) && !empty($driver_token) && !empty($_FILES['document_image']['name']) && !empty($document_id))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $config = [
                    'upload_path' => './uploads/',
                    'allowed_types' => '*'
                ];
                $this->load->library('upload', $config);
                if($this->upload->do_upload('document_image') == TRUE)
                {
                    $data = $this->upload->data();
                    $image = base_url("uploads/" . $data['raw_name'] . $data['file_ext']);
                    $this->Drivermodel->document_upload($image,$driver_id,$document_id);
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Document Uploaded Successfully'
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'status' => 0,
                        'message' => 'Image Not Uploaded'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }

    function Driver_Ride_History_post()
    {
        $driver_id = $this->post('driver_id');
        if (!empty($driver_id))
        {
            $data =  $this->Drivermodel->driver_rides($driver_id);
            if(!empty($data))
            {
                foreach($data as $key=>$value){
                    $ride_mode = $value['ride_mode'];
                    $booking_id = $value['booking_id'];
                    if($ride_mode == 1)
                    {
                        $normal_ride = $this->Usermodel->normal_ride($booking_id);
                        $normal_done_ride = $this->Usermodel->normal_done_ride($booking_id);
                        if (!empty($normal_done_ride))
                        { $begin_lat = $normal_done_ride->begin_lat;
                            if (empty($begin_lat))
                            {
                                $begin_lat = $normal_ride->pickup_lat;
                            }
                            $begin_long = $normal_done_ride->begin_long;
                            if (empty($begin_long))
                            {
                                $begin_long = $normal_ride->pickup_long;
                            }
                            $begin_location = $normal_done_ride->begin_location;
                            if(empty($begin_location))
                            {
                                $begin_location = $normal_ride->pickup_location;
                            }
                            $end_lat = $normal_done_ride->end_lat;
                            if(empty($end_lat))
                            {
                                $end_lat = $normal_ride->drop_lat;
                            }
                            $end_long = $normal_done_ride->end_long;
                            if(empty($end_long))
                            {
                                $end_long = $normal_ride->drop_long;
                            }
                            $end_location = $normal_done_ride->end_location;
                            if(empty($end_location))
                            {
                                $end_location = $normal_ride->drop_location;
                            }
                            $amount = $normal_done_ride->amount;
                            $waiting_price = $normal_done_ride->waiting_price;
                            $ride_time_price =  $normal_done_ride->ride_time_price;
                            $total_amount = $normal_done_ride->total_amount;
                
                        }else{
                            $begin_lat = $normal_ride->pickup_lat;
                            $begin_long = $normal_ride->pickup_long;
                            $begin_location = $normal_ride->pickup_location;
                            $end_lat = $normal_ride->drop_lat;
                            $end_long = $normal_ride->drop_long;
                            $end_location = $normal_ride->drop_location;
                            $total_amount = "0";
                        }
                        $normal_ride->pickup_lat = $begin_lat;
                        $normal_ride->pickup_long = $begin_long;
                        $normal_ride->pickup_location = $begin_location;
                        $normal_ride->drop_lat = $end_lat;
                        $normal_ride->drop_long = $end_long;
                        $normal_ride->drop_location = $end_location;
                        $normal_ride->total_amount = (string)$total_amount;

                        $rental_ride = array(
                            "rental_booking_id"=> "",
                            "user_id"=> "",
                            "rentcard_id"=> "",
                            "car_type_id"=>"",
                            "booking_type"=>"",
                            "driver_id"=>"",
                            "pickup_lat"=> "",
                            "pickup_long"=> "",
                            "pickup_location"=> "",
                            "start_meter_reading"=>"",
                            "start_meter_reading_image"=> "",
                            "end_meter_reading"=> "",
                            "end_meter_reading_image"=> "",
                            "booking_date"=> "",
                            "booking_time"=> "",
                            "user_booking_date_time"=>"",
                            "last_update_time"=>"",
                            "booking_status"=>"",
                            "booking_admin_status"=>"",
                            "car_type_name"=>"",
                            "car_name_arabic"=> "",
                            "car_type_name_french"=> "",
                            "car_type_image"=> "",
                            "ride_mode"=>"",
                            "car_admin_status"=>"",
                            "user_name"=> "",
                            "user_email"=>"",
                            "user_phone"=>"",
                            "user_password"=> "",
                            "user_image"=>"",
                            "register_date"=> "",
                            "device_id"=>"",
                            "flag"=>"",
                            "referral_code"=>"",
                            "free_rides"=> "",
                            "referral_code_send"=>"",
                            "phone_verified"=>"",
                            "email_verified"=> "",
                            "password_created"=>"",
                            "facebook_id"=>"",
                            "facebook_mail"=> "",
                            "facebook_image"=> "",
                            "facebook_firstname"=> "",
                            "facebook_lastname"=> "",
                            "google_id"=> "",
                            "google_name"=> "",
                            "google_mail"=> "",
                            "google_image"=> "",
                            "google_token"=> "",
                            "facebook_token"=> "",
                            "token_created"=> "",
                            "login_logout"=> "",
                            "rating"=> "",
                            "status"=> "",
                            "end_lat"=>"",
                            "end_long"=>"",
                            "end_location"=>"",
                            "final_bill_amount"=> ""
                        );
                    }else{
                        $rental_ride = $this->Usermodel->rental_ride($booking_id);
                        $rental_done_ride = $this->Usermodel->rental_done_ride($booking_id);
                        if(!empty($rental_done_ride))
                        {
                            $begin_lat = $rental_done_ride->begin_lat;
                            if (empty($begin_lat))
                            {
                                $begin_lat = $rental_ride->pickup_lat;
                            }
                            $begin_long = $rental_done_ride->begin_long;
                            if (empty($begin_long))
                            {
                                $begin_long = $rental_ride->pickup_long;
                            }
                            $begin_location = $rental_done_ride->begin_location;
                            if(empty($begin_location))
                            {
                                $begin_location = $rental_ride->pickup_location;
                            }
                            $end_lat = $rental_done_ride->end_lat;
                            $end_long = $rental_done_ride->end_long;
                            $end_location = $rental_done_ride->end_location;
                            $final_bill_amount = $rental_done_ride->final_bill_amount;
                        }else{
                            $begin_lat = $rental_ride->pickup_lat;
                            $begin_long = $rental_ride->pickup_long;
                            $begin_location = $rental_ride->pickup_location;
                            $end_lat = "";
                            $end_long = "";
                            $end_location = "";
                            $final_bill_amount = 0;
                        }
                        $rental_ride->pickup_lat = $begin_lat;
                        $rental_ride->pickup_long = $begin_long;
                        $rental_ride->pickup_location = $begin_location;
                        $rental_ride->end_lat = $end_lat;
                        $rental_ride->end_long = $end_long;
                        $rental_ride->end_location = $end_location;
                        $rental_ride->final_bill_amount = (string)$final_bill_amount;
                        $normal_ride = array(
                            "ride_id"=>"",
                            "user_id"=> "",
                            "coupon_code"=>"",
                            "pickup_lat"=>"",
                            "pickup_long"=>"",
                            "pickup_location"=>"",
                            "drop_lat"=>"",
                            "drop_long"=>"",
                            "drop_location"=>"",
                            "ride_date"=> "",
                            "ride_time"=>"",
                            "last_time_stamp"=>"",
                            "ride_image"=>"" ,
                            "later_date"=> "",
                            "later_time"=>"",
                            "driver_id"=> "",
                            "car_type_id"=>"",
                            "ride_type"=>"",
                            "ride_status"=> "",
                            "reason_id"=> "",
                            "payment_option_id"=>"",
                            "card_id"=> "",
                            "ride_admin_status"=>"",
                            "car_type_name"=> "",
                            "car_name_arabic"=>"",
                            "car_type_name_french"=>"",
                            "car_type_image"=>"",
                            "ride_mode"=> "",
                            "car_admin_status"=>"",
                            "total_amount"=>"",
                            "user_name"=> "",
                            "user_email"=>"",
                            "user_phone"=>"",
                            "user_password"=> "",
                            "user_image"=>"",
                            "register_date"=> "",
                            "device_id"=>"",
                            "flag"=>"",
                            "referral_code"=>"",
                            "free_rides"=> "",
                            "referral_code_send"=>"",
                            "phone_verified"=>"",
                            "email_verified"=> "",
                            "password_created"=>"",
                            "facebook_id"=>"",
                            "facebook_mail"=> "",
                            "facebook_image"=> "",
                            "facebook_firstname"=> "",
                            "facebook_lastname"=> "",
                            "google_id"=> "",
                            "google_name"=> "",
                            "google_mail"=> "",
                            "google_image"=> "",
                            "google_token"=> "",
                            "facebook_token"=> "",
                            "token_created"=> "",
                            "login_logout"=> "",
                            "rating"=> "",
                            "status"=> "",
                        );
                    }
                    $data[$key] = $value;
                    $data[$key]["Normal_Ride"] = $normal_ride;
                    $data[$key]["Rental_Ride"] = $rental_ride;
                }
                $this->response([
                    'status' =>1,
                    'message' => 'Ride History',
                    'details'=> $data
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' =>0,
                    'message' => 'Sorry You Have Not Rides'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Driver_Active_Ride_History_post()
    {
        $driver_id = $this->post('driver_id');
        if (!empty($driver_id))
        {
            $data =  $this->Drivermodel->driver_rides($driver_id);
            if(!empty($data))
            {
                foreach ($data as $active_rides)
                {
                    $ride_mode = $active_rides['ride_mode'];
                    $booking_id = $active_rides['booking_id'];
                    if($ride_mode == 1)
                    {
                        $active_normal_ride = $this->Usermodel->driver_active_normal_ride($booking_id);
                        if(!empty($active_normal_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }else{
                        $active_rental_ride = $this->Usermodel->driver_active_rental_ride($booking_id);
                        if(!empty($active_rental_ride))
                        {
                            $c[] = array('user_ride_id'=>$active_rides['user_ride_id'],'ride_mode'=>$active_rides['ride_mode'],'user_id'=>$active_rides['user_id'],'driver_id'=>$active_rides['driver_id'],'booking_id'=>$active_rides['booking_id']);
                        }
                    }
                }
                if(!empty($c))
                {
                    foreach($c as $key=>$value){
                        $ride_mode = $value['ride_mode'];
                        $booking_id = $value['booking_id'];
                        if($ride_mode == 1)
                        {
                            $normal_ride = $this->Usermodel->normal_ride($booking_id);
                            $normal_done_ride = $this->Usermodel->normal_done_ride($booking_id);
                            if (!empty($normal_done_ride))
                            { $begin_lat = $normal_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $normal_ride->pickup_lat;
                                }
                                $begin_long = $normal_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $normal_ride->pickup_long;
                                }
                                $begin_location = $normal_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $normal_ride->pickup_location;
                                }
                                $end_lat = $normal_done_ride->end_lat;
                                if(empty($end_lat))
                                {
                                    $end_lat = $normal_ride->drop_lat;
                                }
                                $end_long = $normal_done_ride->end_long;
                                if(empty($end_long))
                                {
                                    $end_long = $normal_ride->drop_long;
                                }
                                $end_location = $normal_done_ride->end_location;
                                if(empty($end_location))
                                {
                                    $end_location = $normal_ride->drop_location;
                                }
                                $amount = $normal_done_ride->amount;
                                $waiting_price = $normal_done_ride->waiting_price;
                                $ride_time_price =  $normal_done_ride->ride_time_price;
                                $total_amount = $normal_done_ride->total_amount;

                            }else{
                                $begin_lat = $normal_ride->pickup_lat;
                                $begin_long = $normal_ride->pickup_long;
                                $begin_location = $normal_ride->pickup_location;
                                $end_lat = $normal_ride->drop_lat;
                                $end_long = $normal_ride->drop_long;
                                $end_location = $normal_ride->drop_location;
                                $total_amount = "0";
                            }
                            $normal_ride->pickup_lat = $begin_lat;
                            $normal_ride->pickup_long = $begin_long;
                            $normal_ride->pickup_location = $begin_location;
                            $normal_ride->drop_lat = $end_lat;
                            $normal_ride->drop_long = $end_long;
                            $normal_ride->drop_location = $end_location;
                            $normal_ride->total_amount = (string)$total_amount;

                            $rental_ride = array(
                                "rental_booking_id"=> "",
                                "user_id"=> "",
                                "rentcard_id"=> "",
                                "car_type_id"=>"",
                                "booking_type"=>"",
                                "driver_id"=>"",
                                "pickup_lat"=> "",
                                "pickup_long"=> "",
                                "pickup_location"=> "",
                                "start_meter_reading"=>"",
                                "start_meter_reading_image"=> "",
                                "end_meter_reading"=> "",
                                "end_meter_reading_image"=> "",
                                "booking_date"=> "",
                                "booking_time"=> "",
                                "user_booking_date_time"=>"",
                                "last_update_time"=>"",
                                "booking_status"=>"",
                                "booking_admin_status"=>"",
                                "car_type_name"=>"",
                                "car_name_arabic"=> "",
                                "car_type_name_french"=> "",
                                "car_type_image"=> "",
                                "ride_mode"=>"",
                                "car_admin_status"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "",
                                "facebook_image"=> "",
                                "facebook_firstname"=> "",
                                "facebook_lastname"=> "",
                                "google_id"=> "",
                                "google_name"=> "",
                                "google_mail"=> "",
                                "google_image"=> "",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "",
                                "login_logout"=> "",
                                "rating"=> "",
                                "status"=> "",
                                "end_lat"=>"",
                                "end_long"=>"",
                                "end_location"=>"",
                                "final_bill_amount"=> ""
                            );
                        }else{
                            $rental_ride = $this->Usermodel->rental_ride($booking_id);
                            $rental_done_ride = $this->Usermodel->rental_done_ride($booking_id);
                            if(!empty($rental_done_ride))
                            {
                                $begin_lat = $rental_done_ride->begin_lat;
                                if (empty($begin_lat))
                                {
                                    $begin_lat = $rental_ride->pickup_lat;
                                }
                                $begin_long = $rental_done_ride->begin_long;
                                if (empty($begin_long))
                                {
                                    $begin_long = $rental_ride->pickup_long;
                                }
                                $begin_location = $rental_done_ride->begin_location;
                                if(empty($begin_location))
                                {
                                    $begin_location = $rental_ride->pickup_location;
                                }
                                $end_lat = $rental_done_ride->end_lat;
                                $end_long = $rental_done_ride->end_long;
                                $end_location = $rental_done_ride->end_location;
                                $final_bill_amount = $rental_done_ride->final_bill_amount;
                            }else{
                                $begin_lat = $rental_ride->pickup_lat;
                                $begin_long = $rental_ride->pickup_long;
                                $begin_location = $rental_ride->pickup_location;
                                $end_lat = "";
                                $end_long = "";
                                $end_location = "";
                                $final_bill_amount = 0;
                            }
                            $rental_ride->pickup_lat = $begin_lat;
                            $rental_ride->pickup_long = $begin_long;
                            $rental_ride->pickup_location = $begin_location;
                            $rental_ride->end_lat = $end_lat;
                            $rental_ride->end_long = $end_long;
                            $rental_ride->end_location = $end_location;
                            $rental_ride->final_bill_amount = (string)$final_bill_amount;
                            $normal_ride = array(
                                "ride_id"=>"",
                                "user_id"=> "",
                                "coupon_code"=>"",
                                "pickup_lat"=>"",
                                "pickup_long"=>"",
                                "pickup_location"=>"",
                                "drop_lat"=>"",
                                "drop_long"=>"",
                                "drop_location"=>"",
                                "ride_date"=> "",
                                "ride_time"=>"",
                                "last_time_stamp"=>"",
                                "ride_image"=>"" ,
                                "later_date"=> "",
                                "later_time"=>"",
                                "driver_id"=> "",
                                "car_type_id"=>"",
                                "ride_type"=>"",
                                "ride_status"=> "",
                                "reason_id"=> "",
                                "payment_option_id"=>"",
                                "card_id"=> "",
                                "ride_admin_status"=>"",
                                "car_type_name"=> "",
                                "car_name_arabic"=>"",
                                "car_type_name_french"=>"",
                                "car_type_image"=>"",
                                "ride_mode"=> "",
                                "car_admin_status"=>"",
                                "total_amount"=>"",
                                "user_name"=> "",
                                "user_email"=>"",
                                "user_phone"=>"",
                                "user_password"=> "",
                                "user_image"=>"",
                                "register_date"=> "",
                                "device_id"=>"",
                                "flag"=>"",
                                "referral_code"=>"",
                                "free_rides"=> "",
                                "referral_code_send"=>"",
                                "phone_verified"=>"",
                                "email_verified"=> "",
                                "password_created"=>"",
                                "facebook_id"=>"",
                                "facebook_mail"=> "",
                                "facebook_image"=> "",
                                "facebook_firstname"=> "",
                                "facebook_lastname"=> "",
                                "google_id"=> "",
                                "google_name"=> "",
                                "google_mail"=> "",
                                "google_image"=> "",
                                "google_token"=> "",
                                "facebook_token"=> "",
                                "token_created"=> "",
                                "login_logout"=> "",
                                "rating"=> "",
                                "status"=> "",
                            );
                        }
                        $c[$key] = $value;
                        $c[$key]["Normal_Ride"] = $normal_ride;
                        $c[$key]["Rental_Ride"] = $rental_ride;
                    }
                    $this->response([
                        'result' =>"1",
                        'message' => 'Active Ride History',
                        'details'=> $c
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'result' =>"0",
                        'message' => 'Sorry You Have Active Rides'
                    ], REST_Controller::HTTP_CREATED);
                }

            }else{
                $this->response(['result'=>"0",'message'=>'No Rides'],REST_Controller::HTTP_CREATED);
            }
            }else{
                    $this->response([
                        'result' =>"0",
                        'message' => 'Required Field Missing'
                    ], REST_Controller::HTTP_CREATED);
        }
    }


    function Edit_Profile_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $driver_firstname = $this->post('driver_firstname');
        $driver_lastname = $this->post('driver_lastname');
        if(!empty($driver_id) && !empty($driver_token) && !empty($driver_firstname) && !empty($driver_lastname))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $data =  array('driver_firstname'=>$driver_firstname,'driver_lastname'=>$driver_lastname);
                $this->Drivermodel->edit_profile($driver_id,$data);
                if(!empty($_FILES['driver_profile_image']['name']))
                {
                    $config = [
                        'upload_path' => './uploads/',
                        'allowed_types' => '*'
                    ];
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('driver_profile_image');
                    $data = $this->upload->data();
                    $image = base_url("uploads/" . $data['raw_name'] . $data['file_ext']);
                    $data = array('driver_profile_image'=>$image);
                    $this->Drivermodel->edit_profile($driver_id,$data);
                }
                $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
                $car_type_id = $driver->driver_car_type_id;
                $car_model_id = $driver->driver_car_model_id;
                if($car_type_id == 0)
                {
                    $car_type_name = "";
                    $car_type_image = "";
                }else{
                    $car_type = $this->Drivermodel->car_type($car_type_id);
                    $car_type_name = $car_type->car_type_name;
                    $car_type_image = $car_type->car_type_image;
                }
                if ($car_model_id == 0)
                {
                    $car_model_name = "";
                    $car_model_image = "";
                }else{
                    $car_model = $this->Drivermodel->car_model($car_model_id);
                    $car_model_name = $car_model->car_model_name;
                    $car_model_image = $car_model->car_model_image;
                }
                $driver->car_type_name = $car_type_name;
                $driver->car_type_image = $car_type_image;
                $driver->car_model_name = $car_model_name;
                $driver->car_model_image = $car_model_image;
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Your Profile Update Successfully Successfully',
                        'details'=>$driver
                    ], REST_Controller::HTTP_CREATED);



            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Change_Password_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $new_password = $this->post('new_password');
        $old_password = $this->post('old_password');
        if(!empty($driver_id) && !empty($driver_token) && !empty($old_password) && !empty($new_password))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $data = $this->Drivermodel->change_password($driver_id,$new_password,$old_password);
                if (!empty($data))
                {
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Your Password Change Successfully'
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->set_response([
                        'status' => 0,
                        'message' => 'Password Not Change'
                    ], REST_Controller::HTTP_CREATED);
                }


            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Add_New_Car_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $car_type_id = $this->post('car_type_id');
        $car_model_id  = $this->post('car_model_id');
        $car_number = $this->post('car_number');
        $car_model_date = $this->post('car_model_date');
        if(!empty($driver_id) && !empty($driver_token)  && !empty($car_type_id) && !empty($car_model_id) && !empty($car_number) && !empty($car_model_date) && !empty($_FILES['car_number_plate_image']['name']))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $config = [
                    'upload_path' => './uploads/',
                    'allowed_types' => '*'
                ];
                $this->load->library('upload', $config);
                $this->upload->do_upload('car_number_plate_image');
                $data = $this->upload->data();
                $car_number_plate_image = base_url("uploads/" . $data['raw_name'] . $data['file_ext']);
                $this->upload->do_upload('car_image');
                $car = $this->upload->data();
                $car_image = base_url("uploads/" . $car['raw_name'] . $car['file_ext']);
                $data = array(
                        'driver_id'=>$driver_id,
                        'driver_car_type_id'=>$car_type_id,
                        'driver_car_model_id'=>$car_model_id,
                        'driver_car_number'=>$car_number,
                        'driver_car_model_date'=>$car_model_date,
                        'driver_car_number_plate_image'=>$car_number_plate_image,
                        'driver_car_image'=>$car_image,
                );
                $this->Drivermodel->add_new_car($data);
                $this->set_response([
                    'status' => 1,
                    'message' => 'Car Added Successfully',
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Logout_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        if(!empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
                $data = $dt->format('M j');
                $day = date("l");
                $date = $day . ", " . $data;
                $time = date("h:i A");
                $date_add = $date . ", " . $time;
                $data = array(
                            'driver_login_logout'=>2,
                            'driver_last_offline'=>$date_add,
                            'driver_online_offline'=>2,
                );
                $this->Drivermodel->add_device($data,$driver_id);
                $this->set_response([
                    'status' => 1,
                    'message' => 'Logout Successfully',
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Cars_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        if(!empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $data = $this->Drivermodel->driver_cars($driver_id);
                if(!empty($data))
                {
                    $this->set_response([
                                    'status' => 1,
                                    'message' => 'Car List',
                                     'detail'=>$data
                                ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->set_response([
                                        'status' => 0,
                                        'message' => 'Sorry You have No Cars',
                                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Change_Car_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $driver_car_id = $this->post('driver_car_id');
        if(!empty($driver_id) && !empty($driver_token) && !empty($driver_car_id))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $data = $this->Drivermodel->driver_single_cars($driver_id,$driver_car_id);
                if(!empty($data))
                {
                    $text = array(
                                'driver_car_type_id'=>$data->driver_car_type_id,
                                'driver_car_model_id'=>$data->driver_car_model_id,
                                'driver_car_number'=>$data->driver_car_number,
                                'driver_car_model_date'=>$data->driver_car_model_date,
                                'driver_car_image'=>$data->driver_car_image,
                                'driver_car_number_plate_image'=>$data->driver_car_number_plate_image
                    );
                    $this->Drivermodel->add_device($text,$driver_id);
                    $data = $this->Drivermodel->driver_profile($driver_id,$driver_token);
                    $car_type_id = $data->driver_car_type_id;
                    $car_model_id = $data->driver_car_model_id;
                    if($car_type_id == 0)
                    {
                        $car_type_name = "";
                        $car_type_image = "";
                    }else{
                        $car_type = $this->Drivermodel->car_type($car_type_id);
                        $car_type_name = $car_type->car_type_name;
                        $car_type_image = $car_type->car_type_image;
                    }
                    if ($car_model_id == 0)
                    {
                        $car_model_name = "";
                        $car_model_image = "";
                    }else{
                        $car_model = $this->Drivermodel->car_model($car_model_id);
                        $car_model_name = $car_model->car_model_name;
                        $car_model_image = $car_model->car_model_image;
                    }
                    $data->car_type_name = $car_type_name;
                    $data->car_type_image = $car_type_image;
                    $data->car_model_name = $car_model_name;
                    $data->car_model_image = $car_model_image;
                    $this->set_response([
                        'status' => 1,
                        'message' => 'Car Activate Successfully',
                        'details'=>$data
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->set_response([
                        'status' => 0,
                        'message' => 'This Car Not Exit'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Update_Lat_Long_post()
    {
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $current_latitude = $this->post('current_latitude');
        $current_longitude = $this->post('current_longitude');
        $current_location = $this->post('current_location');
        if(!empty($driver_id) && !empty($driver_token) && !empty($current_latitude) && !empty($current_longitude) && !empty($current_location))
        {
            $driver = $this->Drivermodel->driver_profile($driver_id,$driver_token);
            if(!empty($driver))
            {
                $time = date("h:i:s A");
                $text = array(
                            'driver_current_latitude'=>$current_latitude,
                            'driver_current_longitude'=>$current_longitude,
                            'driver_current_location'=>$current_location,
                            'driver_last_timestamp'=>$time,
                            );
                $this->Drivermodel->add_device($text,$driver_id);
                $this->response([
                                'status' => 1,
                                'message' => 'Location Update Successfully'
                            ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    public function generateRandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
?>