<?php
//status
// 10 new rental booking
// 11 rental booking acepted by driver
// 12 rental driver arrived
// 13 rental ride started by driver
// 14 rental ride reject by driver
// 15 rental ride cancelled by user
// 16 rental ride end by driver
// 17 Ride Cancel by admin
// 18 rental Ride Cancel by admin
// 21 rental ride payment done
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Rental extends REST_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Rentalmodel');
    }

	function Payment_post()
	{
		$rental_booking_id =$this->post('rental_booking_id');
		$amount_paid =$this->post('amount_paid');
		$payment_status =$this->post('payment_status');
		 if (!empty($rental_booking_id) && !empty($amount_paid) && !empty($payment_status))
        {
			$data =  $this->Rentalmodel->check_payment($rental_booking_id);
			if(empty($data)){
				$payment = array(
							'rental_booking_id'=>$rental_booking_id,
							'amount_paid'=>$amount_paid,
							'payment_status'=>$payment_status
						);
                $this->Rentalmodel->payment($payment,$rental_booking_id);
				$this->response([
							'status' =>1,
							'message' => 'Payment Details save'
						], REST_Controller::HTTP_CREATED);
			}else{
				$this->response([
							'status' =>0,
							'message' => 'Payment Already Done'
						], REST_Controller::HTTP_CREATED);
			}
			
		}else{
			 $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
		}
	}

    function Rating_post()
    {
        $rating_star = $this->post('rating_star');
        $rental_booking_id = $this->post('rental_booking_id');
        $comment = $this->post('comment');
        $user_id = $this->post('user_id');
        $driver_id = $this->post('driver_id');
        $app_id = $this->post('app_id');
        if (!empty($rating_star) && !empty($rental_booking_id) && !empty($user_id) && !empty($driver_id) && !empty($app_id))
        {
            $rating = array(
                'rating_star'=> $rating_star,
                'rental_booking_id'=>$rental_booking_id,
                'comment'=>$comment,
                'user_id'=>$user_id,
                'driver_id'=>$driver_id,
                'app_id'=>$app_id
            );
            $this->Rentalmodel->rating($rating);
            $this->response([
                'status' =>1,
                'message' => 'Rating Successfully Done'
            ], REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Done_Ride_Info_post()
    {
        $rental_booking_id = $this->post('rental_booking_id');
        if (!empty($rental_booking_id))
        {
            $data = $this->Rentalmodel->done_ride_info($rental_booking_id);
            if(!empty($data))
            {
                $text = $this->Rentalmodel->ride_info($rental_booking_id);
                $data->user_name = $text->user_name;
                $data->user_email = $text->user_email;
                $data->user_phone = $text->user_phone;
                $this->response([
                    'status' =>1,
                    'message' => 'Done Ride Info',
                    'details' => $data
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' =>0,
                    'message' => 'No Record Found'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Rental_Driver_End_Ride_post()
    {
        $rental_booking_id = $this->post('rental_booking_id');
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $meter_reading = $this->post('meter_reading');
        $end_lat = $this->post('end_lat');
        $end_long = $this->post('end_long');
        if (!empty($end_lat) && !empty($end_long)  && !empty($rental_booking_id) && !empty($driver_id) && !empty($driver_token) && !empty($meter_reading) && !empty($_FILES['meter_reading_image']['name']))
        {
            $driver = $this->Rentalmodel->driver_details($driver_id,$driver_token);
            if (!empty($driver))
            {
                $data = $this->Rentalmodel->ride_info($rental_booking_id);
                if (!empty($data))
                {
                    $start_meter_reading = $data->start_meter_reading;
                    if($meter_reading > $start_meter_reading)
                    {
                        $last_time_stamp = date("h:i:s A");
                        $config = [
                            'upload_path' => './uploads/',
                            'allowed_types' => '*'
                        ];
                        $this->load->library('upload', $config);
                        $this->upload->do_upload('meter_reading_image');
                        $data = $this->upload->data();
                        $image = base_url("uploads/" . $data['raw_name'] . $data['file_ext']);
                        $text = array(
                            'end_meter_reading_image'=>$image,
                            'end_meter_reading'=>$meter_reading,
                            'last_update_time'=>$last_time_stamp,
                            'booking_status'=>16
                        );
                        $data = $this->Rentalmodel->ride_status_change($rental_booking_id,$text);
                        $pem_file = $data->pem_file;
                        $rentcard_id = $data->rentcard_id;
                        $coupan_code = $data->coupan_code;
                        $text = $this->Rentalmodel->done_ride_info($rental_booking_id);
                        $fare = $this->Rentalmodel->rental_ratecard($rentcard_id);
						$rental_category_distance_unit = $fare->rental_category_distance_unit;
                        $price = $fare->price;
                        $price_per_hrs = $fare->price_per_hrs;
                        $price_per_kms = $fare->price_per_kms;
                        $rental_category_hours = $fare->rental_category_hours;
                        $rental_category_kilometer = $fare->rental_category_kilometer;
                        $begin_date = $text->begin_date;
                        $end_date = date("Y-m-d");
                        $begin_time = $begin_date." ".$text->begin_time;
                        $end_time = $end_date." ".$last_time_stamp;
                        $datetime1 = new DateTime($begin_time);
                        $datetime2 = new DateTime($end_time);
                        $interval = $datetime1->diff($datetime2);
                        $ride_time =  $interval->format('%h')." Hr ".$interval->format('%i')." Min";
                        $ride_hours = $interval->format('%h').".".$interval->format('%i');
                        $ride_hr = $interval->format('%h');
                        $ride_mn =  $interval->format('%i');
                        $distance = $meter_reading-$start_meter_reading;
                        if($ride_hours > $rental_category_hours)
                        {
                            $extra_hours = $ride_hours-$rental_category_hours;
                        }else{
                            $extra_hours = "0";
                        }
                        $extra = $ride_hr-$rental_category_hours;
                        if($ride_mn > 0)
                        {
                            $extra = $extra+1;
                        }
                        if($extra > 0)
                        {
                            $extra_hours_travel_charge = $price_per_hrs*$extra;
                        }else{
                            $extra_hours_travel_charge = 0;
                        }
                        $extra_distance = $distance-$rental_category_kilometer;
                        if($extra_distance > 0)
                        {
                            $extra_distance_travel_charge = $extra_distance*$price_per_kms;
                        }else{
                            $extra_distance_travel_charge = 0;
                        }
                        $total_amount = $price+$extra_distance_travel_charge+$extra_hours_travel_charge;
                        if($coupan_code != "")
                        {
                            $coupan_details = $this->Rentalmodel->coupan($coupan_code);
                            $coupon_type = $coupan_details->coupon_type;
                            $coupon_code = $coupan_details->coupons_code;
                            $coupan_price = $coupan_details->coupons_price;
                            if($coupon_type == "Nominal"){
								if($coupan_price > $total_amount){
									$final_bill_amount = "0.00";
								}else{
									$final_bill_amount = $total_amount-$coupan_price;
								}
                                
                            }else{
                                $coupan_price = ($total_amount*$coupan_price)/100;
                                $final_bill_amount =  $total_amount-$coupan_price;
                            }
                        }else{
                            $coupan_price = "00.00";
							$final_bill_amount = $total_amount;
                        }
                        $end_location = $this->getAddress($end_lat,$end_long);
                        $end_location = $end_location?$end_location:'Address Not found';
                        $query = array(
                            'end_lat'=>$end_lat,
                            'end_long'=>$end_long,
                            'end_location'=>$end_location,
                            'end_date'=>$end_date,
                            'end_time'=>$last_time_stamp,
                            'total_distance_travel'=>$distance." ".$rental_category_distance_unit,
                            'total_time_travel'=>$ride_time,
                            'rental_package_price'=>$price,
                            'rental_package_hours'=>$rental_category_hours,
                            'extra_hours_travel'=>$extra_hours,
                            'extra_hours_travel_charge'=>$extra_hours_travel_charge,
                            'rental_package_distance'=>$rental_category_kilometer." ".$rental_category_distance_unit,
                            'extra_distance_travel'=>$extra_distance." ".$rental_category_distance_unit,
                            'extra_distance_travel_charge'=>$extra_distance_travel_charge,
							'total_amount'=>$total_amount,
                            'coupan_price'=>$coupan_price,
                            'final_bill_amount'=>$final_bill_amount
                        );
                        $this->Rentalmodel->done_ride_update($rental_booking_id,$query);
                        $message = "Driver End Ride";
                        $ride_id= (String) $rental_booking_id;
                        $ride_status= (String) 16;
                        $user_id = $data->user_id;
                        $query = $this->Rentalmodel->user_device($user_id);
                        if (!empty($query))
                        {
                            foreach ($query as $item)
                            {
                                $device_id = $item->device_id;
                                $flag = $item->flag;
                                if ($flag == 1)
                                {
                                    $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                                }
                                else
                                {
                                    $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                                }
                            }
                        }else{
                            $device_id = $data->device_id;
                            $flag = $data->flag;
                            if ($flag == 1)
                            {
                                $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                            }
                            else
                            {
                                $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                        $this->response([
                            'status' => 1,
                            'message' => 'Driver End Ride',
                            'details'=>$data
                        ], REST_Controller::HTTP_CREATED);
                    }else{
                        $this->response([
                            'status' => 0,
                            'message' => 'Meter Readings Are Wrong'
                        ], REST_Controller::HTTP_CREATED);
                    }

                }else{
                    $this->response([
                        'status' => 0,
                        'message' => 'No Details Found'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Rental_User_Cancel_Ride_post()
    {
        $rental_booking_id = $this->post('rental_booking_id');
        $user_id = $this->post('user_id');
        $cancel_reason_id = $this->post('cancel_reason_id');
        if (!empty($rental_booking_id) && !empty($user_id))
        {
            $last_time_stamp = date("h:i:s A");
            $text = array(
                'last_update_time'=>$last_time_stamp,
                'booking_status'=>15
            );
            $data = $this->Rentalmodel->ride_status_change($rental_booking_id,$text);
            if (!empty($data))
            {
                $driver_id = $data->driver_id;
                $pem_file = $data->pem_file;
                if($driver_id == 0)
                {
                    $this->response([
                        'status' => 1,
                        'message' => 'Your Ride Cancel'
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $driver = $this->Rentalmodel->driver_profile($driver_id);
                    $device_id = $driver->device_id;
                    $message = "Ride Cancel By User";
                    $ride_id= (String) $rental_booking_id;
                    $ride_status= (String) 15;
                    if($device_id!="")
                    {
                        if($driver->flag == 1)
                        {
                            $this->IphonePushNotificationDriver($device_id,$message,$ride_id,$ride_status,$pem_file);
                        }
                        else
                        {
                            $this->AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                        }
                    }
                    $this->response([
                        'status' => 1,
                        'message' => 'Your Ride Cancel'
                    ], REST_Controller::HTTP_CREATED);
                }

            }else{
                $this->response([
                    'status' => 0,
                    'message' => 'No Details Found'
                ], REST_Controller::HTTP_CREATED);
            }
        }
        else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Rental_Driver_Cancel_Ride_post()
    {
        $rental_booking_id = $this->post('rental_booking_id');
        $driver_id = $this->post('driver_id');
        if (!empty($rental_booking_id) && !empty($driver_id))
        {
            $last_time_stamp = date("h:i:s A");
            $text = array(
                'last_update_time'=>$last_time_stamp,
                'booking_status'=>18
            );
            $data = $this->Rentalmodel->ride_status_change($rental_booking_id,$text);
            if (!empty($data))
            {
                $message = "Ride Cancel By Driver";
                $ride_id= (String) $rental_booking_id;
                $ride_status= (String) 18;
                $user_id = $data->user_id;
                $pem_file = $data->pem_file;
                $query = $this->Rentalmodel->user_device($user_id);
                if (!empty($query))
                {
                    foreach ($query as $item)
                    {
                        $device_id = $item->device_id;
                        $flag = $item->flag;
                        if ($flag == 1)
                        {
                            $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                        }
                        else
                        {
                            $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                        }
                    }
                }else{
                    $device_id = $data->device_id;
                    $flag = $data->flag;
                    if ($flag == 1)
                    {
                        $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                    }
                    else
                    {
                        $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                    }
                }
                $this->response([
                    'status' => 1,
                    'message' => 'Your Ride Cancel'
                ], REST_Controller::HTTP_CREATED);


            }else{
                $this->response([
                    'status' => 0,
                    'message' => 'No Details Found'
                ], REST_Controller::HTTP_CREATED);
            }
        }
        else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Rental_Driver_Reject_Ride_post()
    {
        $rental_booking_id = $this->post('rental_booking_id');
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        if (!empty($rental_booking_id) && !empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Rentalmodel->driver_details($driver_id,$driver_token);
            if (!empty($driver))
            {
				$text = $this->Rentalmodel->ride_info($rental_booking_id);
				$booking_status = $text->booking_status;
				if($booking_status == 10){
					$last_time_stamp = date("h:i:s A");
                $text = array(
                    'last_update_time'=>$last_time_stamp,
                    'booking_status'=>14
                );
                $data = $this->Rentalmodel->ride_status_change($rental_booking_id,$text);
                if (!empty($data))
                {
                    $message = "Driver Reject Ride  Request";
                    $ride_id= (String) $rental_booking_id;
                    $ride_status= (String) 14;
                    $user_id = $data->user_id;
                    $pem_file = $data->pem_file;
                    $query = $this->Rentalmodel->user_device($user_id);
                    if (!empty($query))
                    {
                        foreach ($query as $item)
                        {
                            $device_id = $item->device_id;
                            $flag = $item->flag;
                            if ($flag == 1)
                            {
                                $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                            }
                            else
                            {
                                $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                    }else{
                        $device_id = $data->device_id;
                        $flag = $data->flag;
                        if ($flag == 1)
                        {
                            $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                        }
                        else
                        {
                            $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                        }
                    }
                    $this->response([
                        'status' => 1,
                        'message' => 'Driver Reject Ride'
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'status' => 0,
                        'message' => 'No Details Found'
                    ], REST_Controller::HTTP_CREATED);
                }
				}else{
					$this->response([
                        'status' => 0,
                        'message' => 'Ride Expire'
                    ], REST_Controller::HTTP_CREATED);
				}
                
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Rental_Driver_Start_Ride_post()
    {
        $rental_booking_id = $this->post('rental_booking_id');
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        $meter_reading = $this->post('meter_reading');
        $begin_lat = $this->post('begin_lat');
        $begin_long = $this->post('begin_long');
        $begin_location = $this->post('begin_location');
        if (!empty($begin_lat) && !empty($begin_long) && !empty($begin_location) && !empty($rental_booking_id) && !empty($driver_id) && !empty($driver_token) && !empty($meter_reading) && !empty($_FILES['meter_reading_image']['name']))
        {
            $driver = $this->Rentalmodel->driver_details($driver_id,$driver_token);
            if (!empty($driver))
            {
                $last_time_stamp = date("h:i:s A");
                $config = [
                    'upload_path' => './uploads/',
                    'allowed_types' => '*'
                ];
                $this->load->library('upload', $config);
                $this->upload->do_upload('meter_reading_image');
                $data = $this->upload->data();
                $image = base_url("uploads/" . $data['raw_name'] . $data['file_ext']);
                $begin_date = date("Y-m-d");
                $begin_location = $this->getAddress($begin_lat,$begin_long);
                $begin_location = $begin_location?$begin_location:'Address Not found';
                $query = array(
                    'begin_lat'=>$begin_lat,
                    'begin_long'=>$begin_long,
                    'begin_location'=>$begin_location,
                    'begin_date'=>$begin_date,
                    'begin_time'=>$last_time_stamp
                );
                $this->Rentalmodel->done_ride_update($rental_booking_id,$query);
                $text = array(
                    'start_meter_reading_image'=>$image,
                    'start_meter_reading'=>$meter_reading,
                    'last_update_time'=>$last_time_stamp,
                    'booking_status'=>13
                );
                $data = $this->Rentalmodel->ride_status_change($rental_booking_id,$text);
                if (!empty($data))
                {
                    $user_id = $data->user_id;
                    $pem_file = $data->pem_file;
                    $query = $this->Rentalmodel->user_device($user_id);
                    $message = "Driver Start Ride";
                    $ride_id= (String) $rental_booking_id;
                    $ride_status= (String) 13;
                    if (!empty($query))
                    {
                        foreach ($query as $item)
                        {
                            $device_id = $item->device_id;
                            $flag = $item->flag;
                            if ($flag == 1)
                            {
                                $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                            }
                            else
                            {
                                $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                    }else{
                        $device_id = $data->device_id;
                        $flag = $data->flag;
                        if ($flag == 1)
                        {
                            $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                        }
                        else
                        {
                            $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                        }
                    }
                    $this->response([
                        'status' => 1,
                        'message' => 'Driver Start Ride',
                        'details'=>$data
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'status' => 0,
                        'message' => 'No Details Found'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Rental_Driver_Arrive_post()
    {
        $rental_booking_id = $this->post('rental_booking_id');
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        if (!empty($rental_booking_id) && !empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Rentalmodel->driver_details($driver_id,$driver_token);
            if (!empty($driver))
            {
                $last_time_stamp = date("h:i:s A");
                $text = array(
                    'last_update_time'=>$last_time_stamp,
                    'booking_status'=>12
                );
                $data = $this->Rentalmodel->ride_status_change($rental_booking_id,$text);
                if (!empty($data))
                {
                    $message = "Driver Arrived";
                    $ride_id= (String) $rental_booking_id;
                    $pem_file = $data->pem_file;
                    $this->Rentalmodel->done_ride($ride_id,$last_time_stamp,$driver_id);
                    $ride_status= (String) 12;
                    $user_id = $data->user_id;
                    $query = $this->Rentalmodel->user_device($user_id);
                    if (!empty($query))
                    {
                        foreach ($query as $item)
                        {
                            $device_id = $item->device_id;
                            $flag = $item->flag;
                            if ($flag == 1)
                            {
                                $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                            }
                            else
                            {
                                $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                    }else{
                        $device_id = $data->device_id;
                        $flag = $data->flag;
                        if ($flag == 1)
                        {
                            $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                        }
                        else
                        {
                            $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                        }
                    }
                    $this->response([
                        'status' => 1,
                        'message' => 'Driver Arrive',
                        'details'=>$data
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'status' => 0,
                        'message' => 'No Details Found'
                    ], REST_Controller::HTTP_CREATED);
                }
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Rental_Ride_Accept_post()
    {
        $rental_booking_id = $this->post('rental_booking_id');
        $driver_id = $this->post('driver_id');
        $driver_token = $this->post('driver_token');
        if (!empty($rental_booking_id) && !empty($driver_id) && !empty($driver_token))
        {
            $driver = $this->Rentalmodel->driver_details($driver_id,$driver_token);
            if (!empty($driver))
            {
				$text = $this->Rentalmodel->ride_info($rental_booking_id);
				$booking_status = $text->booking_status;
				if($booking_status == 10){
					 $last_time_stamp = date("h:i:s A");
                $text = array(
                    'driver_id'=>$driver_id,
                    'last_update_time'=>$last_time_stamp,
                    'booking_status'=>11
                );
                $this->Cler_firebase_ride($driver_id);
                $data = $this->Rentalmodel->ride_status_change($rental_booking_id,$text);
                $this->Rentalmodel->driver_ride($rental_booking_id,$driver_id);
                if (!empty($data))
                {
                    $user_id = $data->user_id;
                    $pem_file = $data->pem_file;
                    $query = $this->Rentalmodel->user_device($user_id);
                    $message = "Your Booking is confirm by driver";
                    $ride_id= (String) $rental_booking_id;
                    $ride_status= (String) 11;
                    if (!empty($query))
                    {
                        foreach ($query as $item)
                        {
                            $device_id = $item->device_id;
                            $flag = $item->flag;
                            if ($flag == 1)
                            {
                                $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                            }
                            else
                            {
                                $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                    }else{
                        $device_id = $data->device_id;
                        $flag = $data->flag;
                        if ($flag == 1)
                        {
                            $this->IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                        }
                        else
                        {
                            $this->AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                        }
                    }


                    $this->response([
                        'status' => 1,
                        'message' => 'Driver Accept',
                        'details'=>$data
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                        'status' => 0,
                        'message' => 'No Details Found'
                    ], REST_Controller::HTTP_CREATED);
                }
				}else{
					 $this->response([
									'status' => 0,
									'message' => 'Ride Expire'
								], REST_Controller::HTTP_CREATED);
				}
               
            }else{
                $this->response([
                    'status' => 419,
                    'message' => 'Login On Other Device'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Ride_Sync_post()
    {
        $app_id = $this->post('app_id');
        $rental_booking_id = $this->post('rental_booking_id');
        if(!empty($app_id) && !empty($rental_booking_id))
        {
            $data = $this->Rentalmodel->ride_info($rental_booking_id);
            if (!empty($data))
            {
                $booking_status = $data->booking_status;
                if($app_id == 2)
                {
                    if($booking_status == 10)
                    {
                        $this->response([
                            'status' =>1,
                            'message' => 'New Ride Allocated',
                            'details'=>array('rental_booking_id'=>$data->rental_booking_id,'booking_status'=>$booking_status)
                        ], REST_Controller::HTTP_CREATED);
                    }else{
                        $this->response([
                            'status' =>0,
                            'message' => 'Booking Session Expired'
                        ], REST_Controller::HTTP_CREATED);
                    }
                }else{
                    switch ($booking_status) {
                        case "11":
                            $this->response([
                                'status' =>1,
                                'message' => 'Driver Accpted Ride',
                                'details'=>array('rental_booking_id'=>$data->rental_booking_id,'booking_status'=>$booking_status)
                            ], REST_Controller::HTTP_CREATED);
                            break;
                        case "12":
                            $this->response([
                                'status' =>1,
                                'message' => 'Driver Arrived',
                                'details'=>array('rental_booking_id'=>$data->rental_booking_id,'booking_status'=>$booking_status)
                            ], REST_Controller::HTTP_CREATED);
                            break;
                        case "13":
                            $this->response([
                                'status' =>1,
                                'message' => 'ride started by driver',
                                'details'=>array('rental_booking_id'=>$data->rental_booking_id,'booking_status'=>$booking_status)
                            ], REST_Controller::HTTP_CREATED);
                            break;
                        case "14":
                            $this->response([
                                'status' =>1,
                                'message' => 'ride reject by driver',
                                'details'=>array('rental_booking_id'=>$data->rental_booking_id,'booking_status'=>$booking_status)
                            ], REST_Controller::HTTP_CREATED);
                            break;
                        case "15":
                            $this->response([
                                'status' =>1,
                                'message' => 'ride cancelled by user',
                                'details'=>array('rental_booking_id'=>$data->rental_booking_id,'booking_status'=>$booking_status)
                            ], REST_Controller::HTTP_CREATED);
                            break;
                        case "16":
                            $this->response([
                                'status' =>1,
                                'message' =>'ride end by driver',
                                'details'=>array('rental_booking_id'=>$data->rental_booking_id,'booking_status'=>$booking_status)
                            ], REST_Controller::HTTP_CREATED);
                            break;
                        case "18":
                            $this->response([
                                'status' =>1,
                                'message' =>'Ride Cancel by driver',
                                'details'=>array('rental_booking_id'=>$data->rental_booking_id,'booking_status'=>$booking_status)
                            ], REST_Controller::HTTP_CREATED);
                            break;
                        default:
                            $this->response([
                                'status' =>0,
                                'message' => 'Booking Session Expire'
                            ], REST_Controller::HTTP_CREATED);
                    }
                }

            }else{
                $this->response([
                    'status' =>0,
                    'message' => 'No Details Found'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Ride_Info_post()
    {
        $rental_booking_id = $this->post('rental_booking_id');
        if (!empty($rental_booking_id))
        {
            $data = $this->Rentalmodel->ride_info($rental_booking_id);
            if (!empty($data))
            {
                $driver_id = $data->driver_id;
                $rentcard_id = $data->rentcard_id;
                $query = $this->Rentalmodel->rencard_details($rentcard_id);
                if($driver_id == 0)
                {
                    $driver_name = "";
                    $driver_email = "";
                    $driver_phone = "";
                    $driver_image = "";
                    $car_number = "";
                    $car_model_name = "";
                    $car_model_image = "";
                }else{
                    $text = $this->Rentalmodel->driver_profile($driver_id);
                    $driver_name = $text->driver_name;
                    $driver_email = $text->driver_email;
                    $driver_phone = $text->driver_phone;
                    $driver_image = $text->driver_image;
                    $car_number = $text->car_number;

                    $car_model_name = $text->car_model_name;
                    $car_model_image = $text->car_model_image;
                }
                $car_type_name = $query->car_type_name;
                $car_type_image = $query->car_type_image;

                $package_name = $query->rental_category;
                $package_price = $query->price;

                $data->driver_name =  $driver_name;
                $data->driver_email = $driver_email;
                $data->driver_phone = $driver_phone;
                $data->driver_image = $driver_image;
                $data->car_number = $car_number;
                $data->car_type_name = $car_type_name;
                $data->car_type_image = $car_type_image;
                $data->car_model_name = $car_model_name;
                $data->car_model_image = $car_model_image;
                $data->package_name= $package_name;
                $data->package_price = $package_price;
                $this->response([
                    'status' =>1,
                    'message' => 'Ride Information',
                    'details' => $data
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' =>0,
                    'message' => 'Wrong Rental Booking Id'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function new_firebase_ride($last_id,$driver_id)
    {
        $url = 'https://apporio-taxi.firebaseio.com/Activeride/'.$driver_id.'/.json';
        $fields = array(
            'ride_id' => (string)$last_id,
            'ride_status'=>"10",
        );

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        if(!$response) {
            return false;
        }else{
            return true;
        }
    }

    function Cler_firebase_ride($driver_id)
    {
        $url = 'https://apporio-taxi.firebaseio.com/Activeride/'.$driver_id.'/.json';
        $fields = array(
            'ride_id' => "No Ride",
            'ride_status'=>"No Ride Status",
        );

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        if(!$response) {
            return false;
        }else{
            return true;
        }
    }

    function Book_Car_post()
    {
        $booking_type = $this->post('booking_type');
        $pickup_lat = $this->post('pickup_lat');
        $pickup_long = $this->post('pickup_long');
        $pickup_location = $this->post('pickup_location');
        $booking_date = $this->post('booking_date');
        $booking_time = $this->post('booking_time');
        $car_type_id = $this->post('car_type_id');
        $rentcard_id = $this->post('rentcard_id');
        $user_id = $this->post('user_id');
		$coupan_code = $this->post('coupan_code');
		$coupan = $coupan_code == ""?"":$coupan_code;
        $pem_file = $this->post('pem_file');
        $payment_option_id = $this->post('payment_option_id');
        $pem =	$pem_file==""?1:$pem_file;
	    $payment_id =	$payment_option_id==""?1:$payment_option_id;
        if(!empty($car_type_id) && !empty($booking_type) && !empty($pickup_lat) && !empty($pickup_long) && !empty($pickup_location) && !empty($rentcard_id) && !empty($user_id))
        {
            $dt = DateTime::createFromFormat('!d/m/Y',date("d/m/Y"));
            $data=$dt->format('M j');
            $day=date("l");
            $date=$day.", ".$data ;
            $time=date("h:i A");
            $date_add=$date.", ".$time;
            $last_time_stamp = date("h:i:s A");
            if($booking_type == 1)
            {
                $drivers = $this->Rentalmodel->nearest_driver($car_type_id);
                if (!empty($drivers))
                {
                    foreach($drivers as $login3)
                    {
                        $driver_lat = $login3['current_lat'];
                        $driver_long =$login3['current_long'];

                        $theta = $pickup_long - $driver_long;
 $dist = sin(deg2rad($pickup_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($pickup_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
                        $dist = acos($dist);
                        $dist = rad2deg($dist);
                        $miles = $dist * 60 * 1.1515;
                        $unit = strtoupper("K");
                        if ($unit == "K")
                        {
                            $km=$miles* 1.609344;
                        }
                        else if ($unit == "N")
                        {
                            $miles * 0.8684;
                        }
                        else
                        {
                            $miles;
                        }
                        if($km <= 10)
                        {
                            $c[] = array("driver_id"=> $login3['driver_id'],"distance" => $km,);
                        }
                    }

                    if(!empty($c))
                    {
                        foreach ($c as $value)
                        {
                            $distance[] = $value['distance'];
                        }
                        array_multisort($distance,SORT_ASC,$c);
                        $driver_id = $c[0]['driver_id'];

                        $data = array(
                            'user_id'=>$user_id,
                            'rentcard_id'=>$rentcard_id,
                            'booking_type'=>$booking_type,
                            'pickup_lat'=>$pickup_lat,
                            'car_type_id'=>$car_type_id,
                            'pickup_long'=>$pickup_long,
                            'pickup_location'=>$pickup_location,
                            'booking_date'=>$date,
                            'booking_time'=>$time,
                            'user_booking_date_time'=>$date_add,
                            'last_update_time'=>$last_time_stamp,
                            'booking_status'=>10,
							'coupan_code'=>$coupan,
							'payment_option_id'=>$payment_id,
                            'pem_file'=>$pem
                        );
                        $text = $this->Rentalmodel->booking_now($data);
                        $rental_booking_id = $text->rental_booking_id;
                        $this->new_firebase_ride($rental_booking_id,$driver_id);
                        $this->Rentalmodel->booking_allocated($rental_booking_id,$driver_id,$user_id);
                        $driver = $this->Rentalmodel->driver_profile($driver_id);

                        $device_id = $driver->device_id;

                        $message = "New Rentel Booking";
                        $ride_id= (String) $rental_booking_id;
                        $ride_status= (String) 10;
                        if($device_id!="")
                        {
                            if($driver->flag == 1)
                            {
                                $this->IphonePushNotificationDriver($device_id,$message,$ride_id,$ride_status,$pem);
                            }
                            else
                            {
                                $this->AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                        $this->response([
                            'status' =>1,
                            'message' => 'Car Booked',
                            'details'=>$text
                        ], REST_Controller::HTTP_CREATED);
                    }else{
                        $this->response([
                            'status' =>0,
                            'message' => 'Sorry No Driver Available'
                        ], REST_Controller::HTTP_CREATED);
                    }
                }else{
                    $this->response([
                        'status' =>0,
                        'message' => 'Sorry No Driver Available'
                    ], REST_Controller::HTTP_CREATED);
                }

            }else{
                $data = array(
                    'user_id'=>$user_id,
                    'rentcard_id'=>$rentcard_id,
                    'booking_type'=>$booking_type,
                    'pickup_lat'=>$pickup_lat,
                    'car_type_id'=>$car_type_id,
                    'pickup_long'=>$pickup_long,
                    'pickup_location'=>$pickup_location,
                    'booking_date'=>$booking_date,
                    'booking_time'=>$booking_time,
                    'user_booking_date_time'=>$date_add,
                    'last_update_time'=>$last_time_stamp,
                    'booking_status'=>10,
					'coupan_code'=>$coupan,
					'payment_option_id'=>$payment_id,
                    'pem_file'=>$pem
                );
                $this->Rentalmodel->booking_later($data,$user_id);
                $this->response([
                    'status' =>1,
                    'message' => 'Your Car Book Successfully'
                ], REST_Controller::HTTP_CREATED);
            }
        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }

    }


    function Rental_Package_post()
    {
        $city_id = $this->post('city_id');
        if (!empty($city_id))
        {
            $data = $this->Rentalmodel->Rental_Package($city_id);
            $a = array();
            foreach ($data as $key=>$value)
            {
                $a[$value['rental_category_id']]['rental_category_id']=$value['rental_category_id'];
                $a[$value['rental_category_id']]['rental_category']=$value['rental_category'];
                $a[$value['rental_category_id']]['rental_category_hours'] = $value['rental_category_hours'];
                $a[$value['rental_category_id']]['rental_category_kilometer'] = $value['rental_category_kilometer']." ".$value['rental_category_distance_unit'];
				$a[$value['rental_category_id']]['rental_category_description'] = $value['rental_category_description'];
            }
            if(!empty($a)){
                $d = array();
                foreach ($a as $c)
                {
                    $d[] = $c;
                }
                foreach($d as $keys=>$login)
                {
                    $rental_category_id = $login['rental_category_id'];
                    $data = $this->Rentalmodel->Rental_Pakage_Car($city_id,$rental_category_id);
                    $d[$keys]=$login;
                    $d[$keys]["Rental_Pakage_Car"]=$data;

                }
                $this->response([
                    'status' =>1,
                    'message' => 'Rental Package',
                    'details' =>$d
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                    'status' =>0,
                    'message' => 'No Rental Package In This City',
                ], REST_Controller::HTTP_CREATED);
            }

        }else{
            $this->response([
                'status' =>0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function Car_Type_post()
    {
        $unique_number = $this->post('unique_number');
        $city_name = $this->post('city_name');
        $latitude = $this->post('latitude');
        $longitude = $this->post('longitude');
        if(!empty($latitude) && !empty($longitude))
        {
            $data = $this->Rentalmodel->cartype($city_name);
            if(!empty($data))
            {
                $d = array();
                foreach($data as $login) {
                    $car_type_id = $login['car_type_id'];
                    $car_type_name = $login['car_type_name'];
                    $car_type_image = $login['car_type_image'];
                    $ride_mode = $login['ride_mode'];
                    $city_id = $login['city_id'];
                    $currency= $login['currency'];
                    $currency_iso_code = $login['currency_iso_code'];
                    $currency_unicode = $login['currency_unicode'];
                    $distance_unit = $login['distance_unit'];
                    $base_distance = $login['base_distance'];
                    $base_fare = $login['base_distance_price']." Per ".$base_distance." ".$distance_unit;
                    $d[]=array('car_type_id'=>$car_type_id,'car_type_name'=>$car_type_name,'car_type_image'=>$car_type_image,'city_id'=>$city_id,'base_fare'=>$base_fare,'ride_mode'=>$ride_mode,'currency_iso_code'=>$currency_iso_code,'currency_unicode'=>$currency_unicode);
                }
                $city_id = $d[0]['city_id'];
                $query = $this->Rentalmodel->rent_package($city_id);
                if(!empty($query))
                {
                    $a = array(
                        'car_type_id'=>"001",
                        'car_type_name'=>"Rental",
                        'car_type_image'=>"uploads/car/editcar_2.png",
                        'city_id'=>$city_id,
                        'distance'=>"",
                        'base_fare'=>"",
                        'ride_mode'=>"2"
                    );
                    array_push($d,$a);
                    $log  = "Rental - Car Type Api: ".date("F j, Y, g:i a").PHP_EOL.
                        "Response: ".print_r($d, true).PHP_EOL.
                        "city_name: ".$city_name.PHP_EOL.
                        "latitude: ".$latitude.PHP_EOL.
                        "longitude: ".$longitude.PHP_EOL.
                        "-------------------------".PHP_EOL;
                    file_put_contents('../logfile/Car_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
                    $this->response([
                        'status' => 1,
                        'currency'=>$currency,
                        'currency_iso_code'=>$currency_iso_code,
                        'currency_unicode'=>$currency_unicode,
                        'message' => 'Home Screens Cars',
                        'details' => $d
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $log  = "Rental - Car Type Api: ".date("F j, Y, g:i a").PHP_EOL.
                        "Response: ".print_r($d, true).PHP_EOL.
                        "city_name: ".$city_name.PHP_EOL.
                        "latitude: ".$latitude.PHP_EOL.
                        "longitude: ".$longitude.PHP_EOL.
                        "-------------------------".PHP_EOL;
                    file_put_contents('../logfile/Car_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
                    $this->response([
                        'status' => 1,
                        'currency'=>$currency,
                        'currency_iso_code'=>$currency_iso_code,
                        'currency_unicode'=>$currency_unicode,
                        'message' => 'Home Screens Cars',
                        'details' => $d
                    ], REST_Controller::HTTP_CREATED);
                }
            }else {
                $data = $this->Rentalmodel->all_driver();
                if (!empty($data))
                {
                    foreach($data as $login3)
                    {
                        $driver_lat = $login3->current_lat;
                        $driver_long = $login3->current_long;
                        $theta = $longitude - $driver_long;
                        $dist = sin(deg2rad($latitude)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($latitude)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
                        $dist = acos($dist);
                        $dist = rad2deg($dist);
                        $miles = $dist * 60 * 1.1515;
                        $km=$miles* 1.609344;
                        if($km <= 30)
                        {
                            $c[] = array("driver_id"=> $login3->driver_id,"distance" =>$km,'city_id'=>$login3->city_id);
                        }
                    }
					
                    if (!empty($c))
                    {
                        foreach ($c as $value)
                        {
                            $distance[] = $value['distance'];
                        }
                        array_multisort($distance,SORT_ASC,$c);
                        $c = $c[0]['city_id'];
                        $data = $this->Rentalmodel->cartype_city_id($c);
                        if(!empty($data))
                        {
                            $d = array();
                            foreach($data as $login) {
                                $car_type_id = $login['car_type_id'];
                                $car_type_name = $login['car_type_name'];
                                $car_type_image = $login['car_type_image'];
                                $ride_mode = $login['ride_mode'];
                                $city_id = $login['city_id'];
                                $currency= $login['currency'];
                                $currency_iso_code = $login['currency_iso_code'];
                                $currency_unicode = $login['currency_unicode'];
                                $distance_unit = $login['distance_unit'];
                                $base_distance = $login['base_distance'];
                                $base_fare = $login['base_distance_price']." Per ".$base_distance." ".$distance_unit;
                                $d[]=array('car_type_id'=>$car_type_id,'car_type_name'=>$car_type_name,'car_type_image'=>$car_type_image,'city_id'=>$city_id,'base_fare'=>$base_fare,'ride_mode'=>$ride_mode);
                            }
                            $city_id = $d[0]['city_id'];
                            $query = $this->Rentalmodel->rent_package($city_id);
                            if(!empty($query))
                            {
                                $a = array(
                                    'car_type_id'=>"001",
                                    'car_type_name'=>"Rental",
                                    'car_type_image'=>"uploads/car/editcar_2.png",
                                    'city_id'=>$city_id,
                                    'distance'=>"",
                                    'base_fare'=>"",
                                    'ride_mode'=>"2"
                                );
                                array_push($d,$a);
                                $log  = "Rental - Car Type Api: ".date("F j, Y, g:i a").PHP_EOL.
                                    "Response: ".print_r($d, true).PHP_EOL.
                                    "city_name: ".$city_name.PHP_EOL.
                                    "latitude: ".$latitude.PHP_EOL.
                                    "longitude: ".$longitude.PHP_EOL.
                                    "-------------------------".PHP_EOL;
                                file_put_contents('../logfile/Car_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
                                $this->response([
                                    'status' => 1,
                                    'currency'=>$currency,
                                    'currency_iso_code'=>$currency_iso_code,
                                    'currency_unicode'=>$currency_unicode,
                                    'message' => 'Home Screens Cars',
                                    'details' => $d
                                ], REST_Controller::HTTP_CREATED);
                            }else{
                                $log  = "Rental - Car Type Api: ".date("F j, Y, g:i a").PHP_EOL.
                                    "Response: ".print_r($d, true).PHP_EOL.
                                    "city_name: ".$city_name.PHP_EOL.
                                    "latitude: ".$latitude.PHP_EOL.
                                    "longitude: ".$longitude.PHP_EOL.
                                    "-------------------------".PHP_EOL;
                                file_put_contents('../logfile/Car_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
                                $this->response([
                                    'status' => 1,
                                    'currency'=>$currency,
                                    'currency_iso_code'=>$currency_iso_code,
                                    'currency_unicode'=>$currency_unicode,
                                    'message' => 'Home Screens Cars',
                                    'details' => $d
                                ], REST_Controller::HTTP_CREATED);
                            }
                        }else{
                            $this->response([
                                'status' => 0,
                                'message' => 'No Cars'
                            ], REST_Controller::HTTP_CREATED);
                        }

                    }else{

                        $this->response([
                            'status' => 0,
                            'message' => 'No Cars'
                        ], REST_Controller::HTTP_CREATED);
                    }
                }else{

                    $this->response([
                        'status' => 0,
                        'message' => 'No Cars'
                    ], REST_Controller::HTTP_CREATED);
                }
            }
        }else{
            $this->response([
                'status' => 0,
                'message' => 'Required Field Missing'
            ], REST_Controller::HTTP_CREATED);
        }
    }

    function demo_city_car()
    {
        $data = $this->Rentalmodel->cartype_city_id(3);
        if(!empty($data))
        {
            $d = array();
            foreach($data as $login) {
                $car_type_id = $login['car_type_id'];
                $car_type_name = $login['car_type_name'];
                $car_type_image = $login['car_type_image'];
                $ride_mode = $login['ride_mode'];
                $city_id = $login['city_id'];
                $currency= $login['currency'];
                $currency_iso_code = $login['currency_iso_code'];
                $currency_unicode = $login['currency_unicode'];
                $distance_unit = $login['distance_unit'];
                $base_distance = $login['base_distance'];
                $base_fare = $login['base_distance_price']." Per ".$base_distance." ".$distance_unit;
                $d[]=array('car_type_id'=>$car_type_id,'car_type_name'=>$car_type_name,'car_type_image'=>$car_type_image,'city_id'=>$city_id,'base_fare'=>$base_fare,'ride_mode'=>$ride_mode);
            }
            $city_id = $d[0]['city_id'];
            $query = $this->Rentalmodel->rent_package($city_id);
            if(!empty($query))
            {
                $a = array(
                    'car_type_id'=>"001",
                    'car_type_name'=>"Rental",
                    'car_type_image'=>"uploads/car/editcar_2.png",
                    'city_id'=>$city_id,
                    'distance'=>"",
                    'base_fare'=>"",
                    'ride_mode'=>"2"
                );
                array_push($d,$a);

            }
            return $d;
        }
    }

    function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $app_id="2";
        $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

        $headers = array (
            'Authorization: key=AAAAScCQu44:APA91bGv5aF9Nc7VwoD1BiwEXOlzUmduxil63c6TrYymDtBWlT91AL7oSoe9yW-ihdibptX4X-g3pVq10gNC3swN_pWI1QGTmTqIc7DoYjr6gqJTDF0aNaJhMpTQJaeOhQbQQnPUoDxb',
            'Content-Type: application/json' );
        // Open connection
        $ch = curl_init ();
        // Set the url, number of POST vars, POST data
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
        // Execute post
        $result = curl_exec ( $ch );
        // Close connection
        curl_close ( $ch );
        return $result;
    }

    function IphonePushNotificationDriver($did,$msg,$ride_id,$ride_status,$pem)
    {
        $passphrase = 'programmer';
        $message = $msg;

        $ctx = stream_context_create();
        if($pem == 1){
            stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/taxi_driver_distri.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        }else{
            stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/taxi_driver_debug.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        }

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status'=>$ride_status);

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        // Close the connection to the server
        fclose($fp);
    }

    function AndroidPushNotificationCustomer($did, $msg,$ride_id,$ride_status)
    {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $app_id="1";

        $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

        $headers = array (
            'Authorization: key=AAAAScCQu44:APA91bGv5aF9Nc7VwoD1BiwEXOlzUmduxil63c6TrYymDtBWlT91AL7oSoe9yW-ihdibptX4X-g3pVq10gNC3swN_pWI1QGTmTqIc7DoYjr6gqJTDF0aNaJhMpTQJaeOhQbQQnPUoDxb',
            'Content-Type: application/json' );

        // Open connection
        $ch = curl_init ();
        // Set the url, number of POST vars, POST data
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
        // Execute post
        $result = curl_exec ( $ch );
        // Close connection
        curl_close ( $ch );
        return $result;
    }

    function IphonePushNotificationCustomer($did,$msg,$ride_id,$ride_status,$pem)
    {
        $passphrase = 'programmer';
        $message = $msg;

        $ctx = stream_context_create();
        if($pem == 1)
        {
            stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/taxi_user_distri.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        }else{
            stream_context_set_option($ctx, 'ssl', 'local_cert', APPPATH.'controllers/taxi_user_debug.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        }

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status' => $ride_status);

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        // Close the connection to the server
        fclose($fp);
    }

    function getAddress($latitude,$longitude){
        if(!empty($latitude) && !empty($longitude)){
            $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&key=AIzaSyDdN4fqXPnnGWuCs2d5ncpDBnGgKfDo1fM');
            $output = json_decode($geocodeFromLatLong);
            $status = $output->status;
            $address = ($status=="OK")?$output->results[0]->formatted_address:'';
            if(!empty($address)){
                return $address;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}