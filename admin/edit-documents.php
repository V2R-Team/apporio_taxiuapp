<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$city_id = $_GET['id'];
$query="SELECT * FROM table_document_list WHERE city_id='$city_id'";
$result=$db->query($query);
$list=$result->rows;
foreach($list as $value){
    $c = $value['document_id'];
    $a[] = $c;
}
$query2="select * from table_documents";
$result2 = $db->query($query2);
$list2=$result2->rows;
$total = count($list);
if(isset($_POST['save'])) {
    $city_id = $_POST['city_id'];
    $query1="DELETE from table_document_list WHERE city_id='$city_id'";
    $result1 = $db->query($query1);

    foreach ($_POST['docList'] as $row) {
        $query3 = "INSERT INTO table_document_list (city_id,document_id) VALUES ('$city_id',$row)";
        $db->query($query3);
    }
    $msg = "Document Edit Successfully";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    $db->redirect("home.php?pages=view-documents");

}
?>
<script>
    function validatelogin() {
        var doc_list = document.getElementById('doc_list').value;
        if(doc_list == "")
        {
            alert("Select Documents");
            return false;
        }
    }
</script>

<style>
    .dropdown-menu{ position: inherit !important;}
</style>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Driver Document</h3>
        <span class="tp_rht">
            <a href="home.php?pages=view-documents" data-toggle="tooltip" title="Back" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Document List*</label>
                                <div class="col-lg-6">
                                    <select class="selectpicker" name="docList[]" id="doc_list" data-width="100%" data-live-search="true" multiple data-actions-box="true" multiple>
                                        <option value=""  disabled>--Please Select Documents--</option>
                                        <?php foreach($list2 as $document){ ?>
                                            <option value="<?php echo $document['document_id'];?>" <?php if(in_array($document['document_id'],$a)){ ?> selected <?php } ?>  ><?php echo $document['document_name'];?></option>
                                            <?php
                                        } ?>
                                    </select>
                                    <input type="hidden" name="city_id" value="<?php echo $_GET['id']; ?>">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->
</section>
<!-- Main Content Ends -->
<script>
    $(document).ready(function () {
        $('.selectpicker').selectpicker();
    });
</script>

</body>
</html>

