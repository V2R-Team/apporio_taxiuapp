<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');


$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$list1 = $result->row;

if(isset($_POST['save']))
{
    $admin_panel_city = $_POST['admin_panel_city'];
    $address = str_replace(" ", "+", $admin_panel_city);
    $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response);
    $lat = $response_a->results[0]->geometry->location->lat;
    $long = $response_a->results[0]->geometry->location->lng;
    $admin_panel_name = $_POST['admin_panel_name'];
    $admin_panel_email = $_POST['admin_panel_email'];
    $admin_panel_map_key = $_POST['admin_panel_map_key'];
    $admin_panel_firebase_id = $_POST['admin_panel_firebase_id'];
    $query2="UPDATE admin_panel_settings SET admin_panel_firebase_id='$admin_panel_firebase_id',admin_panel_map_key='$admin_panel_map_key',admin_panel_city='$admin_panel_city',admin_panel_latitude='$lat',admin_panel_longitude='$long',admin_panel_name='$admin_panel_name',admin_panel_email='$admin_panel_email' WHERE admin_panel_setting_id=1";
    $db->query($query2);
    if(!empty($_FILES['admin_panel_logo']))
    {
        $img_name = $_FILES['admin_panel_logo']['name'];

        $filedir  = "../uploads/logo/";
        if(!is_dir($filedir)) mkdir($filedir, 0755, true);
        $fileext = strtolower(substr($_FILES['admin_panel_logo']['name'],-4));
        if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg")
        {
            if($fileext=="jpeg")
            {
                $fileext=".jpg";
            }
            $pfilename = "logo_".uniqid().$fileext;
            $filepath1 = "uploads/logo/".$pfilename;
            $filepath = $filedir.$pfilename;
            copy($_FILES['admin_panel_logo']['tmp_name'], $filepath);
            $upd_qry = "UPDATE admin_panel_settings SET admin_panel_logo ='$filepath1' where admin_panel_setting_id=1";
            $db->query($upd_qry);
        }
    }
    $msg = "Details Save Successfully";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    $db->redirect("home.php?pages=settings");
}
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>
<script>
    function initialize() {

        var input = document.getElementById('admin_panel_city');
        var options = {
            types: ['(cities)'],
        };

        var autocomplete = new google.maps.places.Autocomplete(input,options);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>
    function validatelogin() {
        var admin_panel_name = document.getElementById('admin_panel_name').value;
        var admin_panel_email = document.getElementById('admin_panel_email').value;
        var admin_panel_city = document.getElementById('admin_panel_city').value;
        var admin_panel_map_key = document.getElementById('admin_panel_map_key').value;
        var admin_panel_firebase_id =document.getElementById('admin_panel_firebase_id').value;
        if(admin_panel_name == "")
        {
            alert("Enter Application Name");
            return false;
        }
        if(admin_panel_email == "")
        {
            alert("Enter Sender Email");
            return false;
        }
        if(admin_panel_city == "")
        {
            alert("Enter City");
            return false;
        }
        if(admin_panel_map_key == "")
        {
            alert("Enter Google Map Key");
            return false;
        }
        if(admin_panel_firebase_id == "")
        {
            alert("Enter Firebase Project Id");
            return false;
        }
    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Configuration Of Admin Panel</h3>
        <span>
            <a href="home.php?pages=add-currency" class="btn btn-primary btn-lg" id="add-button"  title="add-currency" role="button">add-currency</a>
      </span>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">Name Of Application*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Name Of Application" name="admin_panel_name" value="<?php echo $list1['admin_panel_name'] ?>" id="admin_panel_name" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Application Logo*</label>
                                <div class="col-lg-6">
                                    <input type="file" class="form-control"  placeholder="Application Logo" name="admin_panel_logo" id="admin_panel_logo" >
                                </div>
                            </div>


                            <div class="form-group ">
                                <label class="control-label col-lg-2">Sender Email*</label>
                                <div class="col-lg-6">
                                    <input type="email" class="form-control"  placeholder="Sender Email" name="admin_panel_email" value="<?php echo $list1['admin_panel_email'] ?>" id="admin_panel_email" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Map City*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Map City" name="admin_panel_city" value="<?php echo $list1['admin_panel_city'] ?>" id="admin_panel_city" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Google Map Key*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Google Map Key" name="admin_panel_map_key" value="<?php echo $list1['admin_panel_map_key'] ?>" id="admin_panel_map_key" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Firebase Projrct Id*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Firebase Projrct Id" name="admin_panel_firebase_id" value="<?php echo $list1['admin_panel_firebase_id'] ?>" id="admin_panel_firebase_id" >
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
    </div>
</div>
</section>
</body>
</html>
