<?php
function IphonePushNotificationCustomer($did,$msg,$ride_id,$ride_status) 
{
	$deviceToken = $did;
	$passphrase = '123456';
	$message = $msg;

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', 'TaxiUser.debug.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	// Open a connection to the APNS server
	$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

	//echo 'Connected to APNS' . PHP_EOL;

	// Create the payload body
	$body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status' => $ride_status);

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));

	// Close the connection to the server
	fclose($fp);
}

function IphonePushNotificationDriver($did,$msg,$ride_id,$ride_status) 
{
	$deviceToken = $did;
	$passphrase = '123456';
	$message = $msg;

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', 'TaxiUDriver.debug.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	// Open a connection to the APNS server
	$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

	//echo 'Connected to APNS' . PHP_EOL;

	// Create the payload body
	$body['aps'] = array('alert' => $message,'sound' => 'default','ride_id' => $ride_id,'ride_status' => $ride_status);

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $did) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));

	// Close the connection to the server
	fclose($fp);
}