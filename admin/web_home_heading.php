<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from web_headings WHERE heading_id=1";
$result = $db->query($query);
$list = $result->row;
$query1 = "select * from web_headings WHERE heading_id=2";
$result1 = $db->query($query1);
$list1 = $result1->row;
 
if(isset($_POST['save']))
{
    $heading1= $_POST['heading1'];
     $heading2= $_POST['heading2'];
      $heading3= $_POST['heading3'];
       $heading4= $_POST['heading4'];
        $heading5= $_POST['heading5'];
         $heading6= $_POST['heading6'];
		 
	$heading11= $_POST['heading1arabic1'];
     $heading21= $_POST['heading2arabic1'];
      $heading31= $_POST['heading3arabic1'];
       $heading41= $_POST['heading4arabic1'];
        $heading51= $_POST['heading5arabic1'];
         $heading61= $_POST['heading6arabic1'];
           
    $query2="UPDATE web_headings SET heading_1='$heading1',heading1='$heading2',heading2='$heading3',heading3='$heading4',heading4='$heading5',heading5='$heading6' WHERE heading_id=1";
    
    $db->query($query2);
	  $query3="UPDATE web_headings SET heading_1='$heading11',heading1='$heading21',heading2='$heading31',heading3='$heading41',heading4='$heading51',heading5='$heading61' WHERE heading_id=2";
	$db->query($query3);
    $db->redirect("home.php?pages=web-heading");
}

?>

<script>
    function validatelogin() {
        var title = document.getElementById('title').value;
        var description = document.getElementById('description').value;
        if(title == "")
        {
            alert("Enter Title");
            return false;
        }
        if(description == "")
        {
            alert("Enter Description");
            return false;
        }
    }
</script>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Website Home headings</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">Heading1 In English*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="heading1" name="heading1" value="<?= $list['heading_1'] ?>" id="title" >
                                </div>
                            </div>
							  <div class="form-group ">
                                <label class="control-label col-lg-2">Heading1 In Arabic*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="heading1" name="heading1arabic1" value="<?= $list1['heading_1'] ?>" id="title" >
                                </div>
                            </div>


                            <div class="form-group ">
                                <label class="control-label col-lg-2">Heading2 In English*</label>
                                <div class="col-lg-6">
                                    <textarea  class="form-control" id="heading2" name="heading2"   placeholder="Description"><?php echo $list['heading1'];?></textarea>
                                </div>
                            </div>
							 <div class="form-group ">
                                <label class="control-label col-lg-2">Heading2 In Arabic*</label>
                                <div class="col-lg-6">
                                    <textarea  class="form-control" id="heading2" name="heading2arabic1"   placeholder="Description"><?php echo $list1['heading1'];?></textarea>
                                </div>
                            </div>
                            
                             <div class="form-group ">
                                <label class="control-label col-lg-2">Heading3 In English*</label>
                                <div class="col-lg-6">
                                   <input type="text" class="form-control" id="heading3" name="heading3" value="<?php echo $list['heading2'];?>"  placeholder="Description">
                                </div>
                            </div>
							 <div class="form-group ">
                                <label class="control-label col-lg-2">Heading3 In Arabic*</label>
                                <div class="col-lg-6">
                                   <input type="text" class="form-control" id="heading3arabic" name="heading3arabic1" value="<?php echo $list1['heading2'];?>"  placeholder="Description">
                                </div>
                            </div>
                            
                            
                             <div class="form-group ">
                                <label class="control-label col-lg-2">Heading4 In English*</label>
                                <div class="col-lg-6">
                                    <textarea  class="form-control" id="heading4" name="heading4"   placeholder="Description"><?php echo $list['heading3'];?></textarea>
                                </div>
                            </div>
							<div class="form-group ">
                                <label class="control-label col-lg-2">Heading4 In Arabic*</label>
                                <div class="col-lg-6">
                                    <textarea  class="form-control" id="heading4arabic" name="heading4arabic1"   placeholder="Description"><?php echo $list1['heading3'];?></textarea>
                                </div>
                            </div>
                            
                            
                             <div class="form-group ">
                                <label class="control-label col-lg-2">Heading5 In English*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="heading5" name="heading5"  value="<?php echo $list['heading4'];?>"   placeholder="Description">
                                </div>
                            </div>
							 <div class="form-group ">
                                <label class="control-label col-lg-2">Heading5 In Arabic*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="heading5arabic" name="heading5arabic1"  value="<?php echo $list1['heading4'];?>"   placeholder="Description">
                                </div>
                            </div>
							
                              <div class="form-group ">
                                <label class="control-label col-lg-2">Heading6 In English*</label>
                                <div class="col-lg-6">
                                    <textarea  class="form-control" id="heading6" name="heading6"   placeholder="Description"><?php echo $list['heading5'];?></textarea>
                                </div>
                            </div>
							  <div class="form-group ">
                                <label class="control-label col-lg-2">Heading6 In Arabic*</label>
                                <div class="col-lg-6">
                                    <textarea  class="form-control" id="heading6arabic" name="heading6arabic1"   placeholder="Description"><?php echo $list1['heading5'];?></textarea>
                                </div>
                            </div>
                            
                            
                            

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</body>
</html>
