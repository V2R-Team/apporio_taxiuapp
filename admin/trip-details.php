<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id where ride_table.ride_id='".$_GET['id']."'";
$result = $db->query($query);
$ride=$result->row;
$driver_id = $ride['driver_id'];
if($driver_id == 0)
{
    $driver_name = "Not Assgin";
    $driver_image = "Not Assgin";
    $driver_email = "Not Assgin";
    $driver_phone = "Not Assgin";
}else{
    $query1 = "select * from driver where driver_id ='$driver_id'";
    $result1 = $db->query($query1);
    $list1 = $result1->row;
    $driver_name = $list1['driver_name'];
    $driver_image = $list1['driver_image'];
    $driver_email = $list1['driver_email'];
    $driver_phone = $list1['driver_phone'];
}
$ride["driver_name"] = $driver_name;
$ride["driver_image"] = $driver_image;
$ride["driver_email"] = $driver_email;
$ride["driver_phone"] = $driver_phone;
$lat = $ride['pickup_lat'];
$pickup_long = $ride['pickup_long'];
$pickup_location = $ride['pickup_location'];
$drop_lat = $ride['drop_lat'];
$drop_long = $ride['drop_long'];
$drop_location = $ride['drop_location'];
$query = "select * from done_ride where ride_id='".$_GET['id']."'";
$result = $db->query($query);
$done_ride=$result->row;
?>
<style>
    .divmap{ height: 500px}
</style>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCDl845ZGSDvGV5BzufLsjRQC04JARqErg"></script>
<script type="text/javascript">
    var markers = [
        {
            "title": 'Pick Up Location',
            "lat": '<?php echo $lat;?>',
            "lng": '<?php echo $pickup_long;?>',
            "description": 'Pick Up Location'
        }
        ,
        {
            "title": 'Drop Up Location',
            "lat": '<?php echo $drop_lat;?>',
            "lng": '<?php echo $drop_long;?>',
            "description": 'Drop Up Location'
        }
    ];
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
        var path = new google.maps.MVCArray();
        var service = new google.maps.DirectionsService();


        var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
        for (var i = 0; i < lat_lng.length; i++) {
            if ((i + 1) < lat_lng.length) {
                var src = lat_lng[i];
                var des = lat_lng[i + 1];
                path.push(src);
                poly.setPath(path);
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }
                    }
                });
            }
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="row">

        <div id="dvMap" class="divmap col-md-4"></div>
        <div class="col-md-8">

            <table class="table table-striped table-hover table-bordered" id="">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td><strong>Rider Name</strong></td><td><?php
                                       echo $ride['user_name'];
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Rider Email</strong></td><td><?php
                                        $user_email = $ride['user_email'];
                                        echo $user_email;
                                        ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Rider Phone</strong></td><td><?php
                                        $user_phone=$ride['user_phone'];
                                        echo $user_phone;
                                        ?></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td><strong>Driver Name</strong></td><td><?= $ride['driver_name'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Driver Email</strong></td><td><?php
                                        $driver_email = $ride['driver_email'];
                                        echo $driver_email;
                                    ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Driver Phone</strong></td><td><?php
                                        $driver_phone = $ride['driver_phone'];
                                        echo $driver_phone;
                                        ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>


</table>
            <table class="table table-striped table-hover table-bordered" id="">
                <tbody>
            <tr>
                <td width="150"><strong>Pickup Up Location</strong></td>
                <td><?= $ride['pickup_location'];?></td>
            </tr>
            <tr>
                <td><strong>Drop Up Location</strong></td>
                <td><?= $ride['drop_location'];?></td>
            </tr>
            <tr>
                <td><strong>Car type</strong></td>
                <td><?= $ride['car_type_name'];?></td>
            </tr>
                <?php if(!empty($done_ride)){?>
                    <tr>
                        <td><strong>Arrived Time</strong></td>
                        <td><?= $done_ride['arrived_time'];?></td>
                    </tr>
                    <tr>
                        <td><strong>Begin Time</strong></td>
                        <td><?= $done_ride['begin_time'];?></td>
                    </tr>
                    <tr>
                        <td><strong>End Time</strong></td>
                        <td><?= $done_ride['end_time'];?></td>
                    </tr>
               <?php } ?>
            </tbody>
            </table>
        </div>

    </div>
</div>

