<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="TaxiUapp Panel">
            
        <title>TaxiUapp Admin</title>
        <link href="images/favicon.png" rel="icon"/>
        <?php include('style.php'); ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>


</head>
    <body>
        <?php include('sidebar.php'); ?>
        <section class="content">
            <?php include('header.php'); ?>
            <?php include('footer.php'); ?>
            <?php include('design.php'); ?>