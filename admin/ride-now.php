<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']))
{
    $db->redirect("index.php");
}
include('common.php');
require 'pn_android.php';
require 'pn_iphone.php';
$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$admin_settings = $result->row;
$admin_panel_firebase_id = $admin_settings['admin_panel_firebase_id'];
$where = "";
if(isset($_POST['user_phone']) && isset($_POST['user_email']) && isset($_POST['car_type_id']) && isset($_POST['ride_status'])) {
    $user_phone = $_POST['user_phone'];
    $user_email = $_POST['user_email'];
    $car_type_id = $_POST['car_type_id'];
    $ride_status = $_POST['ride_status'];
    $where .= " and user.user_email LIKE '%$user_email%'";
    $where .= " and user.user_phone LIKE '%$user_phone%'";
    $where .= " and car_type.car_type_id LIKE '%$car_type_id%'";
    $where .= " and ride_table.ride_status LIKE '%$ride_status%'";
    $where .= " and ride_table.ride_status Not IN(7,17,2,4)";
    $where .= " and ride_table.ride_type=1";
}else{
    $where .= " and ride_table.ride_status IN (1,3,5,6)";
    $where .= " and ride_table.ride_type=1";
}
$query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id where 1=1 $where ORDER BY ride_table.ride_id DESC";
$result = $db->query($query);
$list=$result->rows;
foreach ($list as $key=>$value)
{
    $driver_id = $value['driver_id'];
    $payment_option_id = $value['payment_option_id'];
    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];

    if($driver_id == 0)
    {
        $driver_name = "";
        $driver_email = "";
        $driver_phone = "";
    }else{
        $query1 = "select * from driver where driver_id ='$driver_id'";
        $result1 = $db->query($query1);
        $list1 = $result1->row;
        $driver_name = $list1['driver_name'];
        $driver_email = $list1['driver_email'];
        $driver_phone = $list1['driver_phone'];
    }
    $list[$key]=$value;
    $list[$key]["driver_name"] = $driver_name;
    $list[$key]["driver_email"] = $driver_email;
    $list[$key]["driver_phone"] = $driver_phone;
    $list[$key]["payment_option_name"]=$payment_option_name;
}
if(isset($_POST['user_phone1']) && isset($_POST['user_email1'])  && isset($_POST['car_type_id1']) && isset($_POST['ride_status1'])) {
    $user_phone = $_POST['user_phone1'];
    $user_email = $_POST['user_email'];
    $car_type_id = $_POST['car_type_id'];
    $ride_status = $_POST['ride_status'];
    $where .= " and user.user_email LIKE '%$user_email%'";
    $where .= " and user.user_phone LIKE '%$user_phone%'";
    $where1 .= " and car_type.car_type_id LIKE '%$car_type_id%'";
    $where1 .= " and ride_table.ride_status LIKE '%$ride_status%'";
    $where1 .= " and ride_table.ride_status Not IN(7,17,2,4)";
    $where1 .= " and ride_table.ride_type=2";
}else{
    $where1 .= " and ride_status IN (1,3,5,6)";
    $where1 .= " and ride_table.ride_type=2";
}
$query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id where 1=1 $where1 ORDER BY ride_table.ride_id DESC";
$result = $db->query($query);
$ride_later=$result->rows;
foreach ($ride_later as $key=>$value)
{
    $driver_id = $value['driver_id'];
    $payment_option_id = $value['payment_option_id'];
    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];

    if($driver_id == 0)
    {
        $driver_name = "";
        $driver_email = "";
        $driver_phone = "";
    }else{
        $query1 = "select * from driver where driver_id ='$driver_id'";
        $result1 = $db->query($query1);
        $list1 = $result1->row;
        $driver_name = $list1['driver_name'];
        $driver_name = $list1['driver_name'];
        $driver_email = $list1['driver_email'];
        $driver_phone = $list1['driver_phone'];
    }
    $ride_later[$key]=$value;
    $ride_later[$key]["driver_name"] = $driver_name;
    $ride_later[$key]["driver_email"] = $driver_email;
    $ride_later[$key]["driver_phone"] = $driver_phone;
    $ride_later[$key]["payment_option_name"]=$payment_option_name;
}
if(isset($_GET['driver_id']) && isset($_GET['ride_id']) && isset($_GET['status'])){
    $driver_id = $_GET['driver_id'];
    $ride_id=$_GET['ride_id'];
    $time1 = date("h:i:s A");
    $query1="UPDATE ride_table SET last_time_stamp='$time1',ride_status=1 WHERE ride_id='$ride_id'";
    $db->query($query1);
    $query1234="select * from driver where driver_id='$driver_id'";
    $result1234 = $db->query($query1234);
    $list1234=$result1234->row;
    $device_id=$list1234['device_id'];

    $query5="INSERT INTO ride_allocated (allocated_ride_id,allocated_driver_id,allocated_ride_status) VALUES ('$ride_id','$driver_id','8')";
    $db->query($query5);
    $message="New Ride Allocated";
    $ride_id= (String) $ride_id;
    $ride_status= "1";
    if($device_id!="")
    {
        if($list1234['flag'] == 1)
        {
            IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
        }
        else
        {
            AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
        }
    }
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/'.$driver_id.'/.json';
    $fields = array(
        'ride_id' => (string)$ride_id,
        'ride_status'=>"1",
    );

    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
    $response = curl_exec($ch);
    $db->redirect("home.php?pages=ride-now");
}
$sql = "SELECT * FROM `driver` WHERE `driver_admin_status`=1 AND `online_offline`=1 AND`busy`=0 AND `login_logout`=1";
$result3=$db->query($sql);
$drivers=$result3->rows;
$query1234 ="select * from cancel_reasons where cancel_reasons_status=1 AND reason_type=3";
$result1234 = $db->query($query1234);
$list123=$result1234->rows;

$query="select * from car_type";
$result = $db->query($query);
$car_type=$result->rows;

?>

<style>
    .clear{clear:both !important;}
    .bookind-id {
        cursor: pointer;
        text-decoration: underline;
    }

</style>
<script>
    function Validate(){
        if(document.getElementById("user_phone").value == "" && document.getElementById("user_email").value == "" && document.getElementById("car_type_id").value == "" && document.getElementById("ride_status").value == ""){
            alert("Plz Enter Anything For Search");
            return false;
        }
    }
    function Validate1(){
        if(document.getElementById("user_phone1").value == "" && document.getElementById("user_email1").value == "" && document.getElementById("car_type_id1").value == "" && document.getElementById("ride_status1").value == ""){
            alert("Plz Enter Anything For Search");
            return false;
        }
    }
</script>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Active Rides</h3>
        <span>
            <a href="home.php?pages=manual-rides" class="btn btn-primary btn-lg" id="add-button"  title="Manual Rides" role="button">Manual Rides</a>
      </span>
    </div>
    <div class="row m-t-30">
        <div class="col-sm-12">
            <ul class="nav-tabs navi navi_tabs">
                <li class="active"><a data-toggle="tab" href="#personal"><i class="fa fa-car" aria-hidden="true"></i>
                        <b>Ride Now Bookings</b></a></li>
                <li class=""><a data-toggle="tab" href="#address"><i class="fa fa-clock-o" aria-hidden="true"></i> <b>Ride Later Bookings</b></a></li>
            </ul>
            <div class="panel panel-default p-0">

                <div class="panel-body p-0">

                    <div class="tab-content m-0">
                        <div id="personal" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                        <form method="post" onSubmit="return Validate()">
                                            <div class="" style="margin: 10px 0px 30px 0px;">

                                                <div class="form-group col-md-2">
                                                    <input type="text" class="form-control" name="user_phone" id="user_phone" style="width: 100%" title="Type in a User Phone Number" placeholder="Search for User Phone Number..">
                                                </div>

                                                <div class="form-group col-md-2">
                                                    <input type="text" class="form-control" name="user_email" id="user_email" style="width: 100%" title="Type in a User Email" placeholder="Search for User Email..">
                                                </div>

                                                <div class="form-group col-md-3">
                                                    <select class="form-control" name="car_type_id" id="car_type_id">
                                                        <option value=""> ---Select Car Type--- </option>
                                                        <?php foreach($car_type as $value){ ?>
                                                            <option value="<?php echo $value['car_type_id'];?>"><?php echo $value['car_type_name'];?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <select class="form-control" name="ride_status" id="ride_status">
                                                        <option value=""> ---Select Current Status--- </option>
                                                        <option value="1">New Booking</option>
                                                        <option value="2">Cancelled By User</option>
                                                        <option value="3">Accepted by Driver</option>
                                                        <option value="4">Cancelled by driver</option>
                                                        <option value="5">Driver Arrived</option>
                                                        <option value="6">Trip Started</option>
                                                    </select>
                                                </div>

                                                <button class="btn btn-primary" type="submit" name="seabt12"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                        </form>

                                    </div>
                                    <table id="datatable1" class="table table-striped table-bordered table-responsive">
                                        <thead>
                                        <tr>
                                            <th width="5">Sr.No</th>
                                            <th width="5">Ride Id</th>
                                            <th width="5">Rider Details</th>
                                            <th width="5">Driver Details</th>
                                            <th>Pickup Address</th>
                                            <th>Drop Address</th>
                                            <th width="">Payment Mode</th>
                                            <th width="10%">Ride booked time</th>
                                            <th width="5%">Current Status</th>
                                            <th width="8%">Ride Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $a = 1;
                                        foreach($list as $ridenow){
                                            ?>
                                            <tr>
                                                <td> <?php echo $a;?></td>
                                                <td><a href="home.php?pages=trip-details&id=<?=$ridenow['ride_id']?>" ><span title="Full Details" class="bookind-id"> <?=$ridenow['ride_id']?> </span></a></td>
                                                <td><?php
                                                    $user_name = $ridenow['user_name'];
                                                    $user_phone = $ridenow['user_phone'];
                                                    $user_email = $ridenow['user_email'];
                                                echo nl2br($user_name."\n".$user_phone."\n".$user_email);
                                                    ?></td>
                                                <td>
                                                    <?php  $driver_name = $ridenow['driver_name'];
                                                    if($driver_name == "")
                                                    { ?>
                                                        <h4 style="color:red;">Not Assign</h4>

                                                    <?php }else{
                                                        echo nl2br($ridenow['driver_name']."\n".$ridenow['driver_phone']."\n".$ridenow['driver_email']);
                                                    } ?></td>

                                                <td>
                                                    <?php
                                                    $pickup_location = $ridenow['pickup_location'];
                                                    echo $pickup_location;
                                                    ?>
                                                </td>

                                                <td>
                                                    <?php
                                                    $drop_location = $ridenow['drop_location'];
                                                    echo $drop_location;
                                                    ?>
                                                </td>

                                                <td>
                                                    <?php
                                                    $payment_option_name = $ridenow['payment_option_name'];
                                                    echo $payment_option_name;
                                                    ?>
                                                </td>

                                                <td>
                                                    <?php
                                                    $ride_date = $ridenow['ride_date'];
                                                    echo $ride_date.",".$ridenow['ride_time'];
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $ride_status = $ridenow['ride_status'];
                                                    $timestap = $ridenow['last_time_stamp'];
                                                    switch ($ride_status){
                                                        case "1":
                                                            echo nl2br("New Booking \n ".$timestap);
                                                            break;
                                                        case "2":
                                                            echo nl2br("Cancelled By User  \n ".$timestap);
                                                            break;
                                                        case "3":
                                                            echo nl2br("Accepted by Driver  \n ".$timestap);
                                                            break;
                                                        case "4":
                                                            echo nl2br("Cancelled by driver  \n ".$timestap);
                                                            break;
                                                        case "5":
                                                            echo nl2br("Driver Arrived  \n ".$timestap);
                                                            break;
                                                        case "6":
                                                            echo nl2br("Trip Started  \n ".$timestap);
                                                            break;
                                                        case "7":
                                                            echo nl2br("Trip Completed  \n ".$timestap);
                                                            break;
                                                        case "8":
                                                            echo nl2br("Trip Book By Admin  \n ".$timestap);
                                                            break;
                                                        case "17":
                                                            echo nl2br("Trip Cancel By Admin  \n ".$timestap);
                                                            break;
                                                        default:
                                                            echo "----";
                                                    }
                                                    ?>
                                                </td>

                                                <td align="center">
                                                    <?php
                                                    $ride_status = $ridenow['ride_status'];
                                                    if ($ride_status == 1)
                                                    { ?>
                                                        <span data-target="#cancel<?php echo $ridenow['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>

                                                    <?php }else{ ?>
                                                    <span data-target="#cancel<?php echo $ridenow['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                                    <a target="_blank" href="home.php?pages=track-ride&id=<?=$ridenow['ride_id']?>" data-original-title="Track" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="ion-android-locate"></i> </a>
                                               <?php } ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $a++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="clear"></div>

                    <div id="address" class="tab-pane">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">

                                    <div class="" style="margin: 10px 0px 30px 0px;">
                                        <form method="post" onSubmit="return Validate1()">
                                            <div class="form-group col-md-2">
                                                <input type="text" class="form-control" name="user_phone1" id="user_phone1" style="width: 100%" title="Type in a User Phone Number" placeholder="Search for User Phone Number..">
                                            </div>

                                            <div class="form-group col-md-2">
                                                <input type="text" class="form-control" name="user_email1" id="user_email1" style="width: 100%" title="Type in a User Email" placeholder="Search for User Email..">
                                            </div>

                                            <div class="form-group col-md-3">
                                                <select class="form-control" name="car_type_id1" id="car_type_id1">
                                                    <option value=""> ---Select Car Type--- </option>
                                                    <?php foreach($car_type as $value){ ?>
                                                        <option value="<?php echo $value['car_type_id'];?>"><?php echo $value['car_type_name'];?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <select class="form-control" name="ride_status1" id="ride_status1">
                                                    <option value=""> ---Select Current Status--- </option>
                                                    <option value="1">New Booking</option>
                                                    <option value="2">Cancelled By User</option>
                                                    <option value="3">Accepted by Driver</option>
                                                    <option value="4">Cancelled by driver</option>
                                                    <option value="5">Driver Arrived</option>
                                                    <option value="6">Trip Started</option>
                                                </select>
                                            </div>

                                            <button class="btn btn-primary" type="submit" name="seabt12"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                        </form>

                                        <table id="datatable" class="table table-striped table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <th width="46px">Sr.No</th>
                                                <th width="5">Ride Id</th>
                                                <th width="5">Rider Details</th>
                                                <th width="5">Driver Details</th>
                                                <th>Pickup Address</th>
                                                <th>Drop Address</th>
                                                <th width="5%">Payment Mode</th>
                                                <th width="10%">Ride booked time</th>
                                                <th width="5%">Current Status</th>
                                                <th width="120px">Ride Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $b = 1;
                                            foreach($ride_later as $ride){
                                                ?>
                                                <tr>
                                                    <td> <?php echo $b;?></td>
                                                    <td><a href="home.php?pages=trip-details&id=<?=$ride['ride_id']?>" ><span title="Full Details" class="bookind-id"> <?=$ride['ride_id']?> </span></a></td>
                                                    <td><?php
                                                        $user_name = $ride['user_name'];
                                                        $user_phone = $ride['user_phone'];
                                                        $user_email = $ride['user_email'];
                                                        echo nl2br($user_name."\n".$user_phone."\n".$user_email);
                                                        ?></td>
                                                    <td><?php  $driver_name = $ride['driver_name'];
                                                        if($driver_name == "")
                                                        { ?>
                                                            <button  class="btn btn-success"  data-target="#DriverAssign<?php echo $ride['ride_id'];?>" data-toggle="modal" >Assign</button>
                                                        <?php }else{
                                                            echo nl2br($ride['driver_name']."\n".$ride['driver_phone']."\n".$ride['driver_email']);
                                                        } ?></td>

                                                    <td>
                                                        <?php
                                                        $pickup_location = $ride['pickup_location'];
                                                        echo $pickup_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $drop_location = $ride['drop_location'];
                                                        echo $drop_location;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $payment_option_name = $ride['payment_option_name'];
                                                        echo $payment_option_name;
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $ride_date = $ride['later_date'];
                                                        echo $ride_date.",".$ride['later_time'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $ride_status = $ride['ride_status'];
                                                        $timestap = $ride['last_time_stamp'];
                                                        switch ($ride_status){
                                                            case "1":
                                                                echo nl2br("New Booking \n ".$timestap);
                                                                break;
                                                            case "2":
                                                                echo nl2br("Cancelled By User  \n ".$timestap);
                                                                break;
                                                            case "3":
                                                                echo nl2br("Accepted by Driver  \n ".$timestap);
                                                                break;
                                                            case "4":
                                                                echo nl2br("Cancelled by driver  \n ".$timestap);
                                                                break;
                                                            case "5":
                                                                echo nl2br("Driver Arrived  \n ".$timestap);
                                                                break;
                                                            case "6":
                                                                echo nl2br("Trip Started  \n ".$timestap);
                                                                break;
                                                            case "7":
                                                                echo nl2br("Trip Completed  \n ".$timestap);
                                                                break;
                                                            case "8":
                                                                echo nl2br("Trip Book By Admin  \n ".$timestap);
                                                                break;
                                                            case "17":
                                                                echo nl2br("Trip Cancel By Admin  \n ".$timestap);
                                                                break;
                                                            default:
                                                                echo "----";
                                                        }
                                                        ?>
                                                    </td>

                                                    <td>
                                                        <?php
                                                        $ride_status = $ride['ride_status'];
                                                        if ($ride_status == 1){
                                                            ?>
                                                            <span data-target="#ridelatercancel<?php echo $ride['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                                            <?php
                                                        }else{
                                                        ?>
                                                        <span data-target="#ridelatercancel<?php echo $ride['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                                        <a target="_blank" href="home.php?pages=track-ride&id=<?=$ride['ride_id']?>" data-original-title="Track" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="ion-android-locate"></i> </a>
<?php } ?>

                                                    </td>
                                                </tr>
                                                <?php
                                                $b++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Assign Driver-->
<?php foreach($ride_later as $ride){?>
    <div class="modal fade"  id="DriverAssign<?php echo $ride['ride_id'];?>" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign Driver for Ride Id #<?php echo $ride['ride_id'];?></h4>
                    <div id="assign_driver_success" style="display:none;"> <br>
                        <div class="alert alert-succ$pages->driver_idess"> <strong>Success!</strong> Driver Assigned for Ride Id #<?php echo $ride['ride_id'];?> </div>
                    </div>
                </div>
                <div class="modal-body" >
                    <div class="tab-content">
                        <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                <tr>
                                    <th>Driver Name</th>
                                    <th>Assign</th>
                                </tr>
                                <?php foreach($drivers as $login) { ?>
                                    <tr>
                                        <td><?php echo $login['driver_name'];?></td>
                                        <td><a href="home.php?pages=ride-now&status=8&driver_id=<?php echo $login['driver_id']?>&ride_id=<?php echo $ride['ride_id']?>" class="" title="Active">
                                                <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" >Assign</button>
                                            </a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php foreach($ride_later as $ride){?>
    <div class="modal fade"  id="ridelatercancel<?php echo $ride['ride_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cancel Ride #<?php echo $ride['ride_id']; ?></h4>
                </div>
                <form method="get" action="cancel_booking.php">
                    <div class="modal-body" >
                        <div class="tab-content">
                            <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                                <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                    <tbody>
                                    <?php
                                    foreach($list123 as $reasons){?>
                                        <tr><td><input type="radio" name="cancel_reason_id" value="<?php echo $reasons['reason_id']; ?>"> &nbsp; <?php echo $reasons['reason_name']; ?><br></td></tr>
                                    <?php } ?>
                                    <input type="hidden" name="booking_id" value="<?php echo $ride['ride_id'];?>">
                                    <input type="hidden" name="status" value="17">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success br2 btn-xs fs12 activebtn" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
<?php foreach($list as $ridenow) { ?>
    <div class="modal fade"  id="cancel<?php echo $ridenow['ride_id'];?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cancel Ride #<?php echo $ridenow['ride_id']; ?></h4>
                </div>
                <form method="get" action="cancel_booking.php">
                    <div class="modal-body" >
                        <div class="tab-content">
                            <div id="booking_status_on" class="tab-pane fade in active" style="max-height: 400px; overflow-x: auto;">
                                <table class="table table-striped table-hover table-bordered" id="sample_editable_1" >
                                    <tbody>
                                    <?php
                                    foreach($list123 as $reasons){?>
                                        <tr><td><input type="radio" name="cancel_reason_id" value="<?php echo $reasons['reason_id']; ?>"> &nbsp; <?php echo $reasons['reason_name']; ?><br></td></tr>
                                    <?php } ?>
                                    <input type="hidden" name="booking_id" value="<?php echo $ridenow['ride_id'];?>">
                                    <input type="hidden" name="status" value="17">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success br2 btn-xs fs12 activebtn" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
</section>
<!-- Main Content Ends -->

</body></html>