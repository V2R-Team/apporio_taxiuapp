<?php
include_once '../apporioconfig/connection.php';
$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$admin_settings = $result->row;
$role = $_SESSION['ADMIN']['Role'];
switch ($role){
    case "1"; ?>
        <aside class="left-panel">
            <div class="logo"> <a href="home.php?pages=ride-now" class="logo-expanded"><img src="<?php echo '../'.$admin_settings['admin_panel_logo'] ?>"  width="60px" height="60px" alt="logo"> <span class="nav-label"><?php echo $admin_settings['admin_panel_name'] ?></span> </a> </div>
            <nav class="navigation">
                <ul class="list-unstyled">
                    <!-- Dashboard Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "dashboard" ) {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=dashboard"><i class="ion-android-home" aria-hidden="true"></i> <span class="nav-label" >Dashboard</span><span class="selected"></span></a></li>

                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "admin" || $_REQUEST['pages'] == "add-admin") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=admin"><i class="fa fa-user-secret" aria-hidden="true"></i> <span class="nav-label">Manage Sub-Admin</span><span class="selected"></span></a></li>



                    <!--Ride Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "ride-now" || $_REQUEST['pages'] == "manual-rides" || $_REQUEST['pages'] == "track-ride" || $_REQUEST['pages'] == "ride-later" || $_REQUEST['pages'] == "trip-details" || $_REQUEST['pages'] == "invoice") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="ion-person" aria-hidden="true"></i> <span class="nav-label" >Ride Management&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled selected" id="submenu">
                            
							<?php 
								$arr_open = $color ="";
								if(@$_REQUEST['pages'] == "ride-now") {
								$arr_open   = "open";
								$color = "#111111";
							  }
							?>
                    
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=ride-now"><i class="fa fa-caret-right"></i>Active Rides</a></li>
							<?php 
								$arr_open = $color ="";
								if(@$_REQUEST['pages'] == "ride-later") {
								$arr_open   = "open";
								$color = "#111111";
							  }
							?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=ride-later"><i class="fa fa-caret-right"></i>Completed Rides</a></li>
                        </ul>
                    </li>

                    <!-- user Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "rider" || $_REQUEST['pages'] == "add-rider") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=rider"><i class="ion-android-people" aria-hidden="true"></i> <span class="nav-label" >Riders
  Management</span><span class="selected"></span></a></li>


                    <!-- book a ride -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "view-company" || $_REQUEST['pages'] == "add-company") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-company"><i class="fa fa-building-o" aria-hidden="true"></i> <span class="nav-label" >Taxi Companies</span><span class="selected"></span></a></li>

                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "booking_now") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=booking_now"><i class="ion-android-car" aria-hidden="true"></i> <span class="nav-label" >Manual Taxi Dispatch</span><span class="selected"></span></a></li>



                    <!--Driver Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "drivers" || $_REQUEST['pages'] == "upload_document" || $_REQUEST['pages'] == "map" || $_REQUEST['pages'] == "pending-driver-approvals" || $_REQUEST['pages'] == "verify-driver") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="ion-android-person" aria-hidden="true"></i> <span class="nav-label" >Driver Management&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled" id="submenu">
                            
							<?php 
								$arr_open = $color ="";
								if(@$_REQUEST['pages'] == "drivers") {
								$arr_open   = "open";
								$color = "#111111";
							  }
							?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=drivers"><i class="fa fa-caret-right"></i>Drivers</a></li>
                            <?php 
								$arr_open = $color ="";
								if(@$_REQUEST['pages'] == "map") {
								$arr_open   = "open";
								$color = "#111111";
							  }
							?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=map"><i class="fa fa-caret-right"></i>Drivers Map</a></li>
                        </ul>
                    </li>

                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "accounts" || $_REQUEST['pages'] == "driver-bill") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=accounts"><i class="fa fa-money" aria-hidden="true"></i> <span class="nav-label" >Driver Accounts</span><span class="selected"></span></a></li>


                    <!--Document Management-->

                    <!--Driver Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "documents" || $_REQUEST['pages'] == "edit-documents" || $_REQUEST['pages'] == "add-category-document" || $_REQUEST['pages'] == "view-documents" || $_REQUEST['pages'] == "add-document") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="ion-document-text" aria-hidden="true"></i> <span class="nav-label" >Document Management&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled" id="submenu">

                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "documents" || $_REQUEST['pages'] == "add-document") {
                                $arr_open   = "open";
                                $color = "#111111";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=documents"><i class="fa fa-caret-right"></i>Documents</a></li>
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "view-documents" || $_REQUEST['pages'] == "add-category-document") {
                                $arr_open   = "open";
                                $color = "#111111";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=view-documents"><i class="fa fa-caret-right"></i>Driver Documents</a></li>
                        </ul>
                    </li>



                    <!--Language Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "edit-languages") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=edit-languages"><i class="fa fa-tasks" aria-hidden="true"></i> <span class="nav-label" >Language  Management</span><span class="selected"></span></a></li>



                    <!-- Fleet Management -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "manage-fleet") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=manage-fleet"><i class="fa fa-tasks" aria-hidden="true"></i> <span class="nav-label" >Fleet Management</span><span class="selected"></span></a></li>





                    <!-- Transactions Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "transactions") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=transactions"><i class="fa fa-address-book-o" aria-hidden="true"></i> <span class="nav-label" >Transactions</span><span class="selected"></span></a></li>


                    <!-- Add city model Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "add-city" || $_REQUEST['pages'] == "view-city") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-city"><i class="fa fa-building" aria-hidden="true"></i> <span class="nav-label" >City</span><span class="selected"></span></a></li>




                    <!-- Add Rate card Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "add-rate-card" || $_REQUEST['pages'] == "view-rate-card" || $_REQUEST['pages'] == "extra-charges") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-rate-card"><i class="fa fa-credit-card" aria-hidden="true"></i> <span class="nav-label" >Fare Management</span><span class="selected"></span></a></li>


                    <!--Rental Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "rental-category" || $_REQUEST['pages'] == "rental_trip-details" || $_REQUEST['pages'] == "rental-car" || $_REQUEST['pages'] == "rental-ride") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="fa fa-inr" aria-hidden="true"></i> <span class="nav-label" >Rental Management&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled" id="submenu">
                            <?php 
								$arr_open = $color ="";
								if(@$_REQUEST['pages'] == "rental-category") {
								$arr_open   = "open";
								$color = "#111111";
							  }
							?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=rental-category"><i class="fa fa-caret-right"></i>Rental Package</a></li>
                            <?php 
								$arr_open = $color ="";
								if(@$_REQUEST['pages'] == "rental-car") {
								$arr_open   = "open";
								$color = "#111111";
							  }
							?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=rental-car"><i class="fa fa-caret-right"></i>Rental Fare</a></li>
                            <?php 
								$arr_open = $color ="";
								if(@$_REQUEST['pages'] == "rental-ride") {
								$arr_open   = "open";
								$color = "#111111";
							  }
							?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=rental-ride"><i class="fa fa-caret-right"></i>Rental Rides</a></li>
                        </ul>
                    </li>

                    <!-- heatmapStart -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "heatmap") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=heatmap"><i class="ion-android-map" aria-hidden="true"></i> <span class="nav-label" >Heat Map</span><span class="selected"></span></a></li>


                    <!-- Add Coupons Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "add-coupons" || $_REQUEST['pages'] == "view-coupons") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-coupons"><i class="fa fa-ticket" aria-hidden="true"></i> <span class="nav-label" >Promo Codes</span><span class="selected"></span></a></li>
 <!-- sos -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "sos" || $_REQUEST['pages'] == "sos-location") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=sos"><i class="fa fa-universal-access" aria-hidden="true"></i> <span class="nav-label" >SOS</span><span class="selected"></span></a></li>

                    <!-- Add CancelReason Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "add-cancel" || $_REQUEST['pages'] == "view-cancel") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=view-cancel"><i class="ion-close-round" aria-hidden="true"></i> <span class="nav-label" >Cancel Reason</span><span class="selected"></span></a></li>



                    <!-- Send Notifications -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "send-notification") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=send-notification"><i class="ion-android-notifications" aria-hidden="true"></i> <span class="nav-label" >Send Notification</span><span class="selected"></span></a></li>




                    <!--Pages Starts-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "edit-pages" || $_REQUEST['pages'] == "pages") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=pages"><i class="ion-android-open" aria-hidden="true"></i> <span class="nav-label" >CMS Pages</span><span class="selected"></span></a></li>

                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "app-version") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=app-version"><i class="fa fa-refresh" aria-hidden="true"></i> <span class="nav-label" >Application Version</span><span class="selected"></span></a></li>

<!--website Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "web-home" || $_REQUEST['pages'] == "web-rider-signup" || $_REQUEST['pages'] == "web-driver-signup" || $_REQUEST['pages'] == "web-about-us" || $_REQUEST['pages'] == "web-contact-us") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="web-home"><a href=""><i class="fa fa-globe" aria-hidden="true"></i> <span class="nav-label" >Website&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled" id="submenu">

                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "web-home") {
                                $arr_open   = "open";
                                $color = "#c9c5c5";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=web-home"><i class="fa fa-caret-right"></i>Website Home Page</a></li>
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "web-about-us") {
                                $arr_open   = "open";
                                $color = "#c9c5c5";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=web-about-us"><i class="fa fa-caret-right"></i>Website About Us Page</a></li>
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "web-contact-us") {
                                $arr_open   = "open";
                                $color = "#c9c5c5";
                            }
                            ?>
							<li style="background-color:<?php echo $color ?>"><a href="home.php?pages=web-heading"><i class="fa fa-caret-right"></i>Website Home Headings</a></li>
							<?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "web-heading") {
                                $arr_open   = "open";
                                $color = "#9404dd";
                            }
                            ?>
							  <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "web-blog") {
                                $arr_open   = "open";
                                $color = "#9404dd";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=web-blog"><i class="fa fa-caret-right"></i>Website Blog Page</a></li>

                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=web-contact-us"><i class="fa fa-caret-right"></i>Website Contact Us</a></li>
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "web-driver-signup") {
                                $arr_open   = "open";
                                $color = "#c9c5c5";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=web-driver-signup"><i class="fa fa-caret-right"></i>Website Driver Signup</a></li>
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "web-rider-signup") {
                                $arr_open   = "open";
                                $color = "#c9c5c5";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=web-rider-signup"><i class="fa fa-caret-right"></i>Website Rider Signup</a></li>

                        </ul>
                    </li>
                    <!--Report Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "driver-reports" || $_REQUEST['pages'] == "user-reports" || $_REQUEST['pages'] == "sales-reports") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="fa fa-bar-chart" aria-hidden="true"></i> <span class="nav-label" >Reports&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled" id="submenu">
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "driver-reports") {
                                $arr_open   = "open";
                                $color = "#111111";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=driver-reports"><i class="fa fa-caret-right"></i>Driver Reports</a></li>
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "user-reports") {
                                $arr_open   = "open";
                                $color = "#111111";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=user-reports"><i class="fa fa-caret-right"></i>User Reports</a></li>
                            <?php
                            $arr_open = $color ="";
                            if(@$_REQUEST['pages'] == "sales-reports") {
                                $arr_open   = "open";
                                $color = "#111111";
                            }
                            ?>
                            <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=sales-reports"><i class="fa fa-caret-right"></i>Sales Reports</a></li>
                        </ul>
                    </li>
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "customer-support") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=customer-support"><i class="fa fa-paper-plane" aria-hidden="true"></i> <span class="nav-label" >Customer Support</span><span class="selected"></span></a></li>

                    <!--Pages Starts-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "ratings" ) {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=ratings"><i class="fa fa-star-o" aria-hidden="true"></i> <span class="nav-label" >Ratings And Reviews</span><span class="selected"></span></a></li>
                </ul>
                <ul class="list-unstyled">
                </ul>

            </nav>
        </aside>
<?php
      break;
    case "2";
    ?>
<aside class="left-panel">
    <div class="logo"> <a href="home.php?pages=ride-now" class="logo-expanded"><img src="<?php echo '../'.$admin_settings['admin_panel_logo'] ?>"  width="60px" height="60px"  alt="logo"> <span class="nav-label"><?php echo $admin_settings['admin_panel_name'] ?></span> </a> </div>
    <nav class="navigation">
        <ul class="list-unstyled">
            <!-- Dashboard Start -->
            <?php
            $li_open = $arr_open = $ul_open = "";
            if(@$_REQUEST['pages'] == "dashboard" ) {
                $li_open    = "active open";
                $arr_open   = "open";
                $ul_open    = "style='display: block'";
            }
            ?>
            <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=dashboard"><i class="ion-android-home" aria-hidden="true"></i> <span class="nav-label" >Dashboard</span><span class="selected"></span></a></li>




            <!--Ride Management-->
            <?php
            $li_open = $arr_open = $ul_open = "";
            if(@$_REQUEST['pages'] == "ride-now" || $_REQUEST['pages'] == "track-ride" || $_REQUEST['pages'] == "ride-later" || $_REQUEST['pages'] == "trip-details" || $_REQUEST['pages'] == "invoice") {
                $li_open    = "active open";
                $arr_open   = "open";
                $ul_open    = "style='display: block'";
            }
            ?>
            <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="ion-person" aria-hidden="true"></i> <span class="nav-label" >Ride Management&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                <ul class="list-unstyled selected" id="submenu">

                    <?php
                    $arr_open = $color ="";
                    if(@$_REQUEST['pages'] == "ride-now") {
                        $arr_open   = "open";
                        $color = "#111111";
                    }
                    ?>

                    <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=ride-now"><i class="fa fa-caret-right"></i>Active Rides</a></li>
                    <?php
                    $arr_open = $color ="";
                    if(@$_REQUEST['pages'] == "ride-later") {
                        $arr_open   = "open";
                        $color = "#111111";
                    }
                    ?>
                    <li style="background-color:<?php echo $color ?>"><a href="home.php?pages=ride-later"><i class="fa fa-caret-right"></i>Completed Rides</a></li>
                </ul>
            </li>


            <?php
            $li_open = $arr_open = $ul_open = "";
            if(@$_REQUEST['pages'] == "booking_now") {
                $li_open    = "active open";
                $arr_open   = "open";
                $ul_open    = "style='display: block'";
            }
            ?>
            <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=booking_now"><i class="ion-android-car" aria-hidden="true"></i> <span class="nav-label" >Manual Taxi Dispatch</span><span class="selected"></span></a></li>

            <!-- Transactions Start -->
            <?php
            $li_open = $arr_open = $ul_open = "";
            if(@$_REQUEST['pages'] == "transactions") {
                $li_open    = "active open";
                $arr_open   = "open";
                $ul_open    = "style='display: block'";
            }
            ?>
            <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=transactions"><i class="fa fa-address-book-o" aria-hidden="true"></i> <span class="nav-label" >Transactions</span><span class="selected"></span></a></li>
        </ul>
        <ul class="list-unstyled">
        </ul>

    </nav>
</aside>
<?php
        break;
    case "3";
    ?>
        <aside class="left-panel">
            <div class="logo"> <a href="home.php?pages=ride-now" class="logo-expanded"><img src="<?php echo '../'.$admin_settings['admin_panel_logo'] ?>"  width="60px" height="60px"  alt="logo"> <span class="nav-label"><?php echo $admin_settings['admin_panel_name'] ?></span> </a> </div>
            <nav class="navigation">
                <ul class="list-unstyled">
                    <!-- Dashboard Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "dashboard" ) {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=dashboard"><i class="ion-android-home" aria-hidden="true"></i> <span class="nav-label" >Dashboard</span><span class="selected"></span></a></li>




                    <!--Ride Management-->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "ride-now" || $_REQUEST['pages'] == "track-ride" || $_REQUEST['pages'] == "ride-later" || $_REQUEST['pages'] == "trip-details" || $_REQUEST['pages'] == "invoice") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>" id="languagetab"><a href=""><i class="ion-person" aria-hidden="true"></i> <span class="nav-label" >Ride Management&nbsp;&nbsp;<span id="plus" class="fa fa-plus"></span></span><span class="selected"></span></a>
                        <ul class="list-unstyled selected" id="submenu">
                            <li><a href="home.php?pages=ride-later"><i class="fa fa-caret-right"></i>Completed Rides</a></li>
                        </ul>
                    </li>

                    <!-- Transactions Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "transactions") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=transactions"><i class="fa fa-address-book-o" aria-hidden="true"></i> <span class="nav-label" >Transactions</span><span class="selected"></span></a></li>


                    <!-- Add Message Start -->
                    <?php
                    $li_open = $arr_open = $ul_open = "";
                    if(@$_REQUEST['pages'] == "accounts") {
                        $li_open    = "active open";
                        $arr_open   = "open";
                        $ul_open    = "style='display: block'";
                    }
                    ?>
                    <li class="has-submenu <?php echo $li_open ?>"><a href="home.php?pages=accounts"><i class="ion-android-settings"></i> <span class="nav-label">Driver Accounts</span><span class="selected"></span> <span class="arrow <?php echo $arr_open ?> "></span></a>
                    </li>

                </ul>
                <ul class="list-unstyled">
                </ul>

            </nav>
        </aside>
      <?php
        break;
}
?>

