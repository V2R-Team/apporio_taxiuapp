<?php
function AndroidPushNotificationCustomer($did, $msg,$ride_id,$ride_status)
{
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $app_id="1";

    $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

    $headers = array (
        'Authorization: key=AAAAeQhvUaw:APA91bGOUpHimzNnTUoMEwmLHOn-kYsA8iFiPSZ-dxiA7Hd9hQJzJ-W5nk1-ape27GHdviHzKQB5JnnDqaRNHGVeG2RN1QO79QLjZHwP6kysBplwckosy7npYXV_c12mzTRyO-tPd9e2',
        'Content-Type: application/json' );

    // Open connection
    $ch = curl_init ();
    // Set the url, number of POST vars, POST data
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
    // Execute post
    $result = curl_exec ( $ch );
    // Close connection
    curl_close ( $ch );
    return $result;
}

function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $app_id="2";



    $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

    $headers = array (
        'Authorization: key=AAAAeQhvUaw:APA91bGOUpHimzNnTUoMEwmLHOn-kYsA8iFiPSZ-dxiA7Hd9hQJzJ-W5nk1-ape27GHdviHzKQB5JnnDqaRNHGVeG2RN1QO79QLjZHwP6kysBplwckosy7npYXV_c12mzTRyO-tPd9e2',
        'Content-Type: application/json' );

    // Open connection
    $ch = curl_init ();
    // Set the url, number of POST vars, POST data
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
    // Execute post
    $result = curl_exec ( $ch );
    // Close connection
    curl_close ( $ch );

    return $result;

}
?>