<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from ride_table INNER JOIN done_ride ON ride_table.ride_id=done_ride.ride_id INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id where ride_table.ride_id='".$_GET['id']."'";
$result = $db->query($query);
$ride=$result->row;

$city_id = $ride['city_id'];
$query="select * from city WHERE city_id='$city_id'";
$result = $db->query($query);
$list=$result->row;
$currency = $list['currency'];
$begin_location =  $ride['begin_location'];
?>

<style>
    #map {
        height: 200px;
        width: 100%;
        }
    #left {
        width: 48%;
        border: 1px solid #e7e5e5;
        margin: 0 0 0 10px;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    #fare tr{
        margin-top: 40px;
        margin-bottom: 30px;
    }
</style>


<div class="wraper container-fluid">
    <div class="page-title">
       <h2 class="col-md-4" class="title">Invoice</h2>
    </div>
    <hr>
    <div class="panel panel-default">
        <div class="panel-heading">Your Trip <?= $ride['ride_time'];?> on <?= $ride['ride_date'];?></div>
        <div class="panel-body">

<!--FOR MAP-->

        <div class="col-md-6" id="left">
            <div id="map"></div>
            <br>
            <sapn>
                <i style="float: left;"><img style="height: 20px; width: 20px;" src="../admin/images/start.png"></i>
                <b><?= $ride['begin_time'];?></b>
                    <p><?= $ride['begin_location'];?></p>
            </sapn>
                    <br>
            <span>
                <i style="float: left;"><img style="height: 20px; width: 20px;" src="../admin/images/end.png"></i>
                <b><?= $ride['end_time'];?></b>
                <p><?= $ride['end_location'];?></p>
            <hr>
            <div class="col-sm-4" align="center">
                Car
                <br>
                <b><?= $ride['car_type_name'];?></b>
            </div>
            <div class="col-sm-4" align="center">
                Distance
                <br>
                <b><?= $ride['distance'];?></b>
            </div>
            <div class="col-sm-4" align="center">
                Trip Total Time
                <br>
                <b><?= $ride['tot_time'];?></b>
            </div>
        </div>

<!--FARE BREAKDOWN-->

        <div class="col-md-6">
            <h4 style="text-align:center;">	Fare Breakdown For Ride No :<?= $ride['ride_id'];?></h4>
            <hr>
            <table id="fare" style="width:100%; font-size: 18px;" cellpadding="5" cellspacing="0" border="0">
                <tbody>
                <tr>
                    <td>Distance Charges</td>
                    <td align="right"> <?= $currency." ".$ride['amount'];?></td>
                </tr>
                <tr>
                    <td>Time Charges</td>
                    <td align="right"> <?= $currency." ".$ride['ride_time_price'];?></td>
                </tr>
                <tr>
                    <td>Waiting Charges</td>
                    <td align="right"> <?= $currency." ".$ride['waiting_price'];?></td>
                </tr>
                <tr>
                    <td>Peak Time Charge</td>
                    <td align="right"> <?= $currency." ".$ride['peak_time_charge'];?></td>
                </tr>
                <tr>
                    <td>Night Time Charge</td>
                    <td align="right"> <?= $currency." ".$ride['night_time_charge'];?></td>
                </tr>
                <tr>
                    <td>Coupan Price</td>
                    <td align="right"> <?= $currency." ".$ride['coupan_price'];?></td>
                </tr>
                <tr>
                    <td colspan="2"><hr style="margin-bottom:0px"></td>
                </tr>
                <tr>
                    <td><b>Total Fare Paid By Customer</b></td>
                    <td align="right"><b> <?= $currency." ".$ride['total_amount'];?></b></td>
                </tr>
                <tr>
                    <td>Company Commission </td>
                    <td align="right">  <?= $currency." ".$ride['company_commision'];?></td>
                </tr>
                <tr>
                    <td>Driver Earn </td>
                    <td align="right">  <?= $currency." ".$ride['driver_amount'];?></td>
                </tr>
                </tbody></table>

        </div>
        <?php    if($ride['user_email'] == ""){ ?>
            <div class="col-md-12" align="right">
                <button class="btn btn-white" onclick="myfunction()">E-Mail Invoice</button>
            </div>
           <?php }else{ ?>
            <div class="col-md-12" align="right">
                <a href="home.php?pages=email-send&id=<?= $ride['ride_id'] ?>"><button class="btn btn-white">E-Mail Invoice</button></a>
            </div>
       <?php } ?>

    </div>
    </div>
</div>
<script>
    function myfunction()
    {
        alert("User Email Not Found");
    }
</script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCDl845ZGSDvGV5BzufLsjRQC04JARqErg"></script>
<script type="text/javascript">
    var markers = [
        {
            "title": 'Pick Up Location',
            "lat": <?php echo $ride['begin_lat']?>,
            "lng": <?php echo $ride['begin_long']?>,
            "description": 'Pick Up Location'
        }
        ,
        {
            "title": 'Drop Up Location',
            "lat": <?php echo $ride['end_lat']?>,
            "lng": <?php echo $ride['end_long']?>,
            "description": 'Drop Up Location',
        }
    ];
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();
        for (i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            lat_lng.push(myLatlng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
        var path = new google.maps.MVCArray();
        var service = new google.maps.DirectionsService();


        var poly = new google.maps.Polyline({ map: map, strokeColor: '#4986E7' });
        for (var i = 0; i < lat_lng.length; i++) {
            if ((i + 1) < lat_lng.length) {
                var src = lat_lng[i];
                var des = lat_lng[i + 1];
                path.push(src);
                poly.setPath(path);
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }
                    }
                });
            }
        }
    }
</script>

</section>
</body></html>