<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
  $query = "select * from currency";
  $result = $db->query($query);
  $list = $result->rows;

$query = "select * from application_currency";
$result = $db->query($query);
$list1 = $result->rows;


if(isset($_POST['save'])) 
     {
         $city_name = $_POST['city_name'];
$address = str_replace(" ", "+", $city_name);
$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
$response_a = json_decode($response);
$lat = $response_a->results[0]->geometry->location->lat;

$long = $response_a->results[0]->geometry->location->lng;

	$city = explode(",",$city_name);
	$currency = $_POST['currency'];
	$distance = $_POST['distance'];
	$application_currency = $_POST['application_currency'];
         $query = "select * from application_currency WHERE application_currency_id ='$application_currency'";
         $result = $db->query($query);
         $list1 = $result->row;
         $currency_iso_code = $list1['currency_iso_code'];
         $currency_unicode = $list1['currency_unicode'];

	$query2="INSERT INTO city (city_name,currency,distance,city_latitude,city_longitude,currency_iso_code,currency_unicode) VALUES ('".$city[0]."','$currency','$distance','$lat','$long','$currency_iso_code','$currency_unicode')";
	$db->query($query2); 
	$msg = "City Details Save Successfully";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
	$db->redirect("home.php?pages=add-city");
	}

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB21baMnSAvRrcZ3IKymum3_vJFN4hm7xU&v=3.exp&libraries=places"></script>
<script>
function initialize() {

var input = document.getElementById('city_name');
var options = {
  types: ['(cities)'],
};

var autocomplete = new google.maps.places.Autocomplete(input,options);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>
    function validatelogin() {
        var city_name = document.getElementById('city_name').value;
		var currency = document.getElementById('currency').value;
		var distance = document.getElementById('distance').value;
		var application_currency = document.getElementById('application_currency').value;
        if(city_name == "")
        {
            alert("Enter City Name");
            return false;
        }
		if(currency == "")
		{
			alert("Select Currency");
            return false;
		}
		if(distance == "")
		{
			alert("Select Distance Unit");
            return false;
		}
        if(application_currency == "")
        {
            alert("Select Application Currency");
            return false;
        }
    }
</script>
  
<div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add City</h3>
        
      <span class="tp_rht">
         <a href="home.php?pages=view-city" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class="form">
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                <div class="form-group ">
                  <label class="control-label col-lg-2">City*</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control"  placeholder="City Name" name="city_name" id="city_name" >
                  </div>
                </div>
				
				
				 <div class="form-group ">
                  <label class="control-label col-lg-2">Currency*</label>
                  <div class="col-lg-6">
                     <select class="form-control" name="currency" id="currency" >
                        <option value="">--Please Select Currency--</option>
						<?php foreach($list as $currency):?>
                           <option value="<?php echo $currency['symbol'];?>"><?php echo $currency['name'];?></option>
                           <?php endforeach;?>
                    </select>
                  </div>
                </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Application Currency*</label>
                      <div class="col-lg-6">
                          <select class="form-control" name="application_currency" id="application_currency" >
                              <option value="">--Please Select Application Currency--</option>
                              <?php foreach($list1 as $app_currency):?>
                                  <option value="<?php echo $app_currency['application_currency_id'];?>"><?php echo $app_currency['currency_name'];?></option>
                              <?php endforeach;?>
                          </select>
                      </div>
                  </div>

                  <div class="form-group ">
                  <label class="control-label col-lg-2">Distance Unit*</label>
                  <div class="col-lg-6">
                     <select class="form-control" name="distance" id="distance" >
                        <option value="">--Please Select Distance Unit--</option>
                           <option value="Miles">Miles</option>
                           <option value="Km">Kilometers</option>
                    </select>
                  </div>
                </div> 
				
				

				
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
