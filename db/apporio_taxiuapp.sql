-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:24 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_taxiuapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir', 'Brar Sahab', 'appppp', '', '', '1234567', '98238923929', 'hello@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 2),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(24, 'Ali', 'Alkarori', 'ali', '', '', 'Isudana', '4803221216', 'alikarori@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(25, 'Ali', 'Karori', 'aliKarori', '', '', 'apporio7788', '480-322-1216', 'sudanzol@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(26, 'Shilpa', 'Goyal', 'shilpa', '', '', '123456', '8130039030', 'shilpa@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(27, 'siavash', 'rezayi', 'siavash', '', '', '123456', '+989123445028', 'siavash55r@yahoo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(28, 'Murt', 'Omer', 'Murtada', '', '', '78787878', '2499676767676', 'murtada@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(29, 'tito', 'reyes', 'tito', '', '', 'tito123', '9999999999', 'tito@reyes.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(30, 'ANDRE ', 'FREITAS', 'MOTOTAXISP', '', '', '14127603', '5511958557088', 'ANDREFREITASALVES2017@GMAIL.COM', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(31, 'exeerc', 'exeerv', 'exeerc', '', '', 'exeeerv', '011111111111', 'exeer@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(32, 'anurag', 'upadhyay', 'anurag', '', '', 'anurag', '9205414742', 'anurag@apporio.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL,
  `admin_panel_firebase_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`, `admin_panel_firebase_id`) VALUES
(1, 'TaxiUapp', 'uploads/logo/logo_59db7da59ca1f.png', 'Hello@taxiuapp.com', 'Amsterdam, Netherlands', 'AIzaSyAP7-oE7YEjtqIhiAbKVP1MO4Xv_mZAd-k', '52.3702157', '4.8951679', ' taxiuapp-4edc1');

-- --------------------------------------------------------

--
-- Table structure for table `application_currency`
--

CREATE TABLE `application_currency` (
  `application_currency_id` int(11) NOT NULL,
  `currency_name` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_currency`
--

INSERT INTO `application_currency` (`application_currency_id`, `currency_name`, `currency_iso_code`, `currency_unicode`) VALUES
(1, 'DOLLAR SIGN', 'AUD', '0024'),
(2, 'EURO', 'EUR', '20A0');

-- --------------------------------------------------------

--
-- Table structure for table `application_version`
--

CREATE TABLE `application_version` (
  `application_version_id` int(11) NOT NULL,
  `ios_current_version` varchar(255) NOT NULL,
  `ios_mandantory_update` int(11) NOT NULL,
  `android_current_version` varchar(255) NOT NULL,
  `android_mandantory_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_version`
--

INSERT INTO `application_version` (`application_version_id`, `ios_current_version`, `ios_mandantory_update`, `android_current_version`, `android_mandantory_update`) VALUES
(1, '233', 1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_allocated`
--

INSERT INTO `booking_allocated` (`booking_allocated_id`, `rental_booking_id`, `driver_id`, `status`) VALUES
(1, 1, 7, 1),
(2, 2, 7, 1),
(3, 3, 12, 1),
(4, 4, 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refuse to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(4, 'I got a lift', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`card_id`, `customer_id`, `user_id`) VALUES
(2, 'cus_BagcJcMuwPlTPn', 8),
(3, 'cus_BagcJSRlMiT4ih', 8),
(4, 'cus_BagcGjuLWAxDwF', 8),
(5, 'cus_BagcTzvMzT28sa', 8),
(6, 'cus_BagcGZRst0RB1j', 8),
(7, 'cus_BagcmtZoG1JjNI', 8),
(8, 'cus_Bagc0dwrYmNWCT', 8),
(9, 'cus_BagcQKk98ybqFm', 8),
(10, 'cus_Bagc6786uviHy1', 8),
(11, 'cus_Bagc3fFlo4YBr5', 8),
(13, 'cus_Bagd8yFhNP7ZUX', 8),
(14, 'cus_Bancysg4s73sM0', 2);

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 1),
(16, 'VOLVO', 'uploads/car/car_16.jpg', 1),
(5, 'MERCEDES BENZ', 'uploads/car/car_5.png', 1),
(6, 'TESLA', 'uploads/car/car_6.png', 1),
(15, 'TOYOTA', 'uploads/car/car_15.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(44, 'E Class', '', '', 'Mercedes', '', '', '', '', 3, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(46, 'PRIUS', '', '', 'TOYOTA', '', '', '', '', 2, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1),
(43, '5 Serie', '', '', 'BMW', '', '', '', '', 3, 1),
(45, 'V Class', '', '', 'Mercedes', '', '', '', '', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `cartype_big_image` varchar(255) NOT NULL DEFAULT '',
  `car_longdescription` text NOT NULL,
  `car_longdescription_arabic` text CHARACTER SET utf8 NOT NULL,
  `car_description` varchar(255) NOT NULL DEFAULT '',
  `car_description_arabic` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `cartype_big_image`, `car_longdescription`, `car_longdescription_arabic`, `car_description`, `car_description_arabic`, `ride_mode`, `car_admin_status`) VALUES
(2, 'STANDARD', 'اساسي', 'ECONOMIC', 'uploads/car/editcar_2.jpg', '2584', 'webstatic/img/fleet-image/prime-play.png', 'The all too familiar STANDARD, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'هاتشباك مألوفة جدا، ناقص الازعاج من الانتظار والمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'Get an STANDARD at your doorstep', 'الحصول على معيار على عتبة داركم', 1, 1),
(3, 'LUXURY', 'LUXURY', 'LUXURY', 'uploads/car/editcar_3.jpg', '3937', 'webstatic/img/fleet-image/lux.png', 'The all too familiar LUXURY, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'لوكسوري مألوفة جدا، ناقص الازعاج من الانتظار والمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'Get an LUXURY at your doorstep', 'الحصول على لوكسوري على عتبة داركم', 1, 1),
(4, 'MINIVAN', 'حافلة صغيرة', 'VAN', 'uploads/car/editcar_4.jpg', '3190', 'webstatic/img/fleet-image/shuttle.png', 'The all too familiar MINIVAN, minus the hassle of waiting and haggling for price. A convenient way to travel everyday.', 'مينيفان مألوفة جدا، ناقص الازعاج من الانتظار والمساومة للسعر. وهناك طريقة مريحة للسفر كل يوم.', 'Get an MINIVAN at your doorstep', 'الحصول على مينيفان على عتبة داركم', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `currency_iso_code`, `currency_unicode`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 'Gurugram', '', '', '$', 'EUR', '20AC', 'Km', '', '', 1),
(121, 'Amsterdam', '52.3702157', '4.8951679', 'â‚¬', 'EUR', '20AC', 'Km', '', '', 1),
(122, 'Tanger', '35.7594651', '-5.8339543', 'MAD', 'AUD', '0024', 'Km', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_email`, `company_phone`, `company_address`, `country_id`, `city_id`, `company_contact_person`, `company_password`, `vat_number`, `company_image`, `company_status`) VALUES
(44, 'Taxicompagnons B.V.', 'info@taxicompagnons.nl', '0368411376', 'Katernstraat, Almere, Nederland', 'NETHERLANDS', 121, 'R. Kebdi', 'Sabrine@05', 'NL8554254545.B01', 'uploads/company/company_44.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 63, 'MOTO TAXI SP JÃ', 1, 2, 1, 2, 'Apporio Taxi', 'Apporio Infolabs', 'Apporio', 'apporio@info.com2', '', 'tawsila', '', 'tawsila', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '++91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `total_usage_limit`, `per_user_limit`, `start_date`, `expiry_date`, `coupon_type`, `status`) VALUES
(74, 'APPORIO', '10', 0, 0, '2017-08-14', '2017-11-15', 'Nominal', 1),
(112, 'TAXIUAPP', '10', 1000, 10, '2017-10-15', '2018-10-31', 'Percentage', 1),
(115, 'FREERIDE', '100', 10, 1, '2017-11-09', '2017-11-30', 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`) VALUES
(1, 'Doller', '&#36', '&#36'),
(2, 'POUND', '&#163', '&#163'),
(3, 'Indian Rupees', '&#8377', '&#8377'),
(4, 'Euro', '&#8364;', '&#8364;'),
(5, 'MAD', 'MAD', 'MAD');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`) VALUES
(1, 2, 'Rich', 'rkebdi@gmail.com', '0624372264', 'Testing Support massage ', 'Tuesday, Oct 17, 10:47 PM'),
(2, 1, 'Rachid', 'boutaina@kpnmail.nl', '0624372264', 'Test', 'Wednesday,Nov 1,11:26 PM');

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 1, '28.4122189', '77.0432347', '28.4122189', '77.0432347', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '08:05:53 AM', '08:06:05 AM', '08:06:17 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(2, 2, '28.4122144', '77.0432398', '28.4122144', '77.0432398', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '08:13:12 AM', '08:13:15 AM', '08:13:18 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(3, 3, '28.4122138', '77.0432394', '28.4122138', '77.0432394', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '08:31:25 AM', '08:31:27 AM', '08:31:28 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(4, 4, '52.353966', '5.1537157', '52.3539851', '5.1529466', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Aphroditestraat 4, 1363 Almere, Netherlands', '09:51:45 PM', '09:52:14 PM', '09:54:28 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '90', '9.00', '81.00', '90.00', '0.00', '0.00 Km', '0.0', '2', '90', 1, '', 1, 1, '0000-00-00'),
(5, 7, '52.3539055', '5.1541773', '52.3539371', '5.1546438', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 33A, 1363 VN Almere, Netherlands', '06:24:27 AM', '06:24:55 AM', '06:26:08 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '90', '9.00', '81.00', '90.00', '0.00', '0.00 Km', '0.0', '1', '90', 1, '', 1, 1, '0000-00-00'),
(6, 8, '52.3539631', '5.1541246', '52.3539709', '5.1540446', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '07:22:36 AM', '07:22:54 AM', '07:45:53 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '180.9', '18.09', '162.81', '180.90', '0.00', '12.09 Km', '5370.0', '23', '180.9', 1, '', 1, 1, '0000-00-00'),
(7, 10, '52.3537643', '5.1537616', '52.3539122', '5.1538548', 'Achillesstraat 27, 1363 Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '08:34:42 AM', '08:34:52 AM', '08:39:39 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '3', '0.30', '2.70', '3.00', '0.00', '0.00 Km', '0.0', '5', '3', 1, '', 1, 1, '0000-00-00'),
(8, 11, '52.3539258', '5.154118', '52.3539293', '5.1541164', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '08:43:31 AM', '08:44:28 AM', '08:44:45 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '3', '0.30', '2.70', '3.00', '0.00', '0.00 Km', '0.0', '0', '3', 1, '', 1, 1, '0000-00-00'),
(9, 12, '52.3467503', '5.1515232', '52.3539753', '5.1540635', 'Duitslandstraat 1, 1363 BG Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '09:02:06 AM', '09:02:14 AM', '09:04:56 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '6.84', '0.68', '6.16', '6.84', '0.00', '4.84 Km', '740.0', '3', '6.84', 1, 'Payment complete.', 1, 1, '0000-00-00'),
(10, 15, '52.351433', '5.1762139', '52.351434', '5.176208', 'Katernstraat 31, 1321 NC Almere, Netherlands', 'Katernstraat 31, 1321 NC Almere, Netherlands', '11:57:59 AM', '11:58:07 AM', '11:58:43 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '3', '0.30', '2.70', '3.00', '0.00', '0.00 Km', '0.0', '1', '3', 1, '', 1, 1, '0000-00-00'),
(11, 16, '52.3514319', '5.1762241', '52.3514321', '5.1762242', 'Katernstraat 31, 1321 NC Almere, Netherlands', 'Katernstraat 31, 1321 NC Almere, Netherlands', '04:18:00 PM', '04:18:19 PM', '04:19:09 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '3', '0.30', '2.70', '3.00', '0.00', '0.00 Km', '0.0', '1', '3', 1, '', 1, 0, '0000-00-00'),
(12, 18, '52.353753', '5.154219', '52.3534159', '5.154207', 'Jupitersingel, 1363 Almere, Netherlands', 'Jupitersingel, 1363 Almere, Netherlands', '05:54:37 PM', '05:55:48 PM', '05:57:20 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '3', '0.30', '2.70', '3.00', '0.00', '0.00 Km', '0.0', '2', '3', 1, '', 1, 0, '0000-00-00'),
(13, 19, '52.3533426', '5.1529161', '52.3533014', '5.1530589', 'Poseidonsingel 103, 1363 Almere, Netherlands', 'Achillesstraat 30, 1363 Almere, Netherlands', '06:05:21 PM', '06:05:26 PM', '06:05:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '3', '0.30', '2.70', '3.00', '0.00', '0.00 Km', '0.0', '0', '3', 1, '', 1, 1, '0000-00-00'),
(14, 20, '52.3543255', '5.1550474', '52.3529885', '5.1513212', 'Venusstraat 17, 1363 Almere, Netherlands', 'Freyjaplantsoen, 1363 Almere, Netherlands', '06:09:10 PM', '06:09:14 PM', '06:10:35 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '3', '0.30', '2.70', '3.00', '0.00', '0.33 Km', '160.0', '1', '3', 1, '', 1, 1, '0000-00-00'),
(15, 21, '52.3539826', '5.1542855', '52.3539854', '5.1540371', 'Venusstraat 33, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '06:48:34 PM', '06:49:11 PM', '06:49:55 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '3', '0.30', '2.70', '3.00', '0.00', '0.00 Km', '0.0', '1', '3', 1, '', 1, 1, '0000-00-00'),
(16, 5, '52.3539536', '5.1543404', '', '', 'Venusstraat 33, 1363 Almere, Netherlands', '', '07:15:39 PM', '07:15:48 PM', '', '0', '0', '0', '0.00', '0.00', '0.00', 2, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(17, 22, '52.353828', '5.1536719', '52.3538204', '5.1536674', 'Horusstraat 2, 1363 Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '07:50:17 PM', '07:50:29 PM', '07:50:34 PM', '0', '0.00', '00.00', '0.00', '45', '0.00', 2, '50', '5.00', '45.00', '5.00', '0.00', '0.00 Km', '0.0', '0', '50', 1, '', 1, 1, '0000-00-00'),
(18, 23, '52.353837', '5.1541571', '52.3535645', '5.1545564', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Jupitersingel, 1363 Almere, Netherlands', '04:30:11 PM', '04:31:21 PM', '04:36:36 PM', '1', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '5', '5', 1, '', 1, 0, '0000-00-00'),
(19, 26, '52.3540262', '5.1538751', '52.3540327', '5.1538683', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '04:47:19 PM', '04:47:44 PM', '04:49:24 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '136', '13.60', '122.40', '136.00', '0.00', '132.00 Km', '132000.0', '2', '136', 1, '', 1, 1, '0000-00-00'),
(20, 27, '52.3540848', '5.1536185', '52.3540952', '5.1535245', 'Achillesstraat 9, 1363 Almere, Netherlands', 'Achillesstraat 14, 1363 Almere, Netherlands', '04:54:33 PM', '04:55:36 PM', '05:02:27 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '7', '5', 1, '', 1, 1, '0000-00-00'),
(21, 28, '52.3539408', '5.1541031', '52.354042', '5.1542346', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '05:04:30 PM', '05:04:51 PM', '05:06:02 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '233', '23.30', '209.70', '233.00', '0.00', '229.00 Km', '229000.0', '1', '233', 1, '', 1, 0, '0000-00-00'),
(22, 30, '52.3538864', '5.1540754', '52.353879', '5.1540863', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '05:41:35 PM', '05:41:49 PM', '05:42:23 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '1', '5', 1, '', 1, 0, '0000-00-00'),
(23, 32, '52.3535919', '5.1538967', '52.3536151', '5.1538196', 'Jupitersingel, 1363 Almere, Netherlands', 'Achillesstraat 27, 1363 Almere, Netherlands', '05:47:14 PM', '05:47:38 PM', '05:47:49 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(24, 33, '52.3533973', '5.1542444', '52.3533999', '5.1542423', 'Jupitersingel, 1363 Almere, Netherlands', 'Jupitersingel, 1363 Almere, Netherlands', '05:48:45 PM', '05:48:48 PM', '05:48:52 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(25, 35, '', '', '', '', '', '', '06:58:55 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 2, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(26, 36, '52.3538141', '5.1543045', '52.3538258', '5.1543659', 'Venusstraat 33A, 1363 VN Almere, Netherlands', 'Venusstraat 33A, 1363 VN Almere, Netherlands', '07:04:09 PM', '07:04:16 PM', '07:05:19 PM', '0', '0.00', '00.00', '0.00', '0.00', '7.3', 2, '65.7', '6.57', '59.13', '73.00', '0.00', '69.00 Km', '69000.0', '1', '65.7', 1, '', 0, 1, '0000-00-00'),
(27, 37, '52.3514324', '5.1762268', '52.3514324', '5.1762268', 'Katernstraat 31, 1321 NC Almere, Netherlands', 'Katernstraat 31, 1321 NC Almere, Netherlands', '03:53:31 PM', '03:53:39 PM', '03:54:12 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '64', '6.40', '57.60', '64.00', '0.00', '60.00 Km', '60000.0', '1', '64', 1, '', 1, 1, '0000-00-00'),
(28, 42, '28.4121745017253', '77.043181963967', '28.4121205655629', '77.0432705869775', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:23:52 AM', '10:23:57 AM', '10:24:03 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(29, 43, '28.4121888658848', '77.0432854427173', '28.4121888658848', '77.0432854427173', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:31:20 AM', '10:31:25 AM', '10:31:31 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(30, 44, '28.4121174693242', '77.0432366718621', '28.438888922661', '77.0857560262084', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'HUDA Sector Rd, Sector 24, Sarswati Kunj II, Wazirabad, Sector 52, Gurugram, Haryana 122413, India', '10:32:32 AM', '10:32:36 AM', '10:32:40 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(31, 45, '52.3538213', '5.1539989', '52.3538094', '5.1539922', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '06:05:16 PM', '06:05:45 PM', '06:06:11 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(32, 48, '52.353749', '5.1539438', '52.353741', '5.1538426', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Achillesstraat 27, 1363 Almere, Netherlands', '06:41:31 PM', '06:41:35 PM', '06:41:56 PM', '0', '0.00', '00.00', '0.00', '0.00', '5.5', 2, '49.5', '4.95', '44.55', '55.00', '0.00', '51.00 Km', '51000.0', '0', '49.5', 1, '', 1, 0, '0000-00-00'),
(33, 49, '52.353833', '5.154129', '52.3538195', '5.1541267', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '07:01:05 PM', '07:01:27 PM', '07:01:53 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(34, 50, '52.3538861', '5.1542019', '52.353909', '5.1541745', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '08:29:03 AM', '08:29:29 AM', '08:31:12 AM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '2', '5', 1, '', 1, 1, '0000-00-00'),
(35, 52, '52.3538919', '5.1540426', '52.3538919', '5.1540426', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '08:37:14 AM', '08:37:18 AM', '08:37:20 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(36, 53, '52.3537835', '5.1537817', '52.3537877', '5.1537631', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '10:52:55 AM', '10:53:12 AM', '10:53:25 AM', '0', '0.00', '00.00', '0.00', '0.00', '100', 2, '0.00', '0.00', '0.00', '19.00', '0.00', '15.00 Km', '15000.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(37, 54, '52.3537564', '5.1536981', '52.3537404', '5.153712', 'Achillesstraat 27, 1363 Almere, Netherlands', 'Achillesstraat 27, 1363 Almere, Netherlands', '10:55:24 AM', '10:55:33 AM', '10:55:46 AM', '0', '0.00', '00.00', '0.00', '0.00', '100', 2, '0.00', '0.00', '0.00', '34.00', '0.00', '30.00 Km', '30000.0', '0', '0.00', 1, '', 1, 0, '0000-00-00'),
(38, 55, '52.3537005', '5.1540393', '52.3537104', '5.154085', 'Achillesstraat 27, 1363 Almere, Netherlands', 'Achillesstraat 27, 1363 Almere, Netherlands', '11:27:57 AM', '11:28:02 AM', '11:28:09 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.5', 2, '4.5', '0.45', '4.05', '5.00', '0.00', '0.00 Km', '0.0', '0', '4.5', 1, '', 1, 0, '0000-00-00'),
(39, 55, '', '', '', '', '', '', '11:28:56 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 2, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(40, 56, '52.3537196', '5.154103', '52.3537953', '5.15406', 'Achillesstraat 27, 1363 Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '03:13:11 PM', '03:13:19 PM', '03:13:30 PM', '0', '0.00', '00.00', '0.00', '0.00', '100', 2, '0.00', '0.00', '0.00', '5.00', '0.00', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 0, '0000-00-00'),
(41, 57, '52.3541352', '5.1539032', '52.3541884', '5.1539602', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '04:21:45 PM', '04:21:52 PM', '04:22:18 PM', '0', '0.00', '00.00', '0.00', '0.00', '1.4', 2, '12.6', '1.26', '11.34', '14.00', '0.00', '10.00 Km', '10000.0', '0', '12.6', 1, '', 1, 0, '0000-00-00'),
(42, 58, '52.3539609', '5.1544402', '52.3539609', '5.1544402', 'Venusstraat 33A, 1363 VN Almere, Netherlands', 'Venusstraat 33A, 1363 VN Almere, Netherlands', '04:41:30 PM', '04:41:33 PM', '04:41:36 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.5', 2, '4.5', '0.45', '4.05', '5.00', '0.00', '0.00 Km', '0.0', '0', '4.5', 1, '', 1, 0, '0000-00-00'),
(43, 59, '52.3537063', '5.1544582', '52.3537061', '5.1544426', 'Jupitersingel, 1363 Almere, Netherlands', 'Jupitersingel, 1363 Almere, Netherlands', '04:51:56 PM', '04:52:01 PM', '04:52:06 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.5', 2, '4.5', '0.45', '4.05', '5.00', '0.00', '0.00 Km', '0.0', '0', '4.5', 1, '', 1, 0, '0000-00-00'),
(44, 60, '28.4121367', '77.0432276', '28.4121376', '77.0432266', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:54:44 AM', '07:54:48 AM', '07:55:14 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 0, '0000-00-00'),
(45, 63, '28.4121573', '77.0432119', '28.4121607', '77.0432058', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '09:03:26 AM', '09:04:00 AM', '09:04:34 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Km', '0.0', '1', '0.00', 1, '', 1, 0, '0000-00-00'),
(46, 64, '28.4120953469589', '77.043297455243', '28.449781844863193', '77.0867782831192', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '129, St Thomas Marg, Sector 52A, Gurugram, Haryana 122003, India', '10:10:59 AM', '10:11:19 AM', '10:13:16 AM', '0', '0.00', '15.00', '0.00', '0.00', '0.00', 4, '115', '4.60', '110.40', '100.00', '0.00', '0.00 Km', '0.0', '2', '115', 1, '', 1, 1, '0000-00-00'),
(47, 66, '28.4120908168129', '77.0433089353992', '28.4121041367005', '77.0433018184019', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:15:27 AM', '10:15:36 AM', '10:17:59 AM', '0', '0.00', '15.00', '0.00', '0.00', '0.00', 4, '115', '4.60', '110.40', '100.00', '0.00', '0.00 Km', '0.0', '2', '115', 1, '', 1, 0, '0000-00-00'),
(48, 67, '28.4121497', '77.0432197', '28.4121485', '77.0432214', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:37:21 AM', '10:37:58 AM', '10:39:55 AM', '0', '0.00', '15.00', '0.00', '0.00', '0.00', 5, '115', '4.60', '110.40', '100.00', '0.00', '0.00 Km', '0.0', '2', '115', 1, '', 1, 0, '0000-00-00'),
(49, 69, '28.4121496', '77.0432198', '28.4121498', '77.0432195', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:54:12 AM', '10:54:31 AM', '10:55:35 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 5, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Km', '0.0', '1', '100', 1, 'Payment complete.', 1, 0, '0000-00-00'),
(50, 70, '52.3539043', '5.154047', '52.3538661', '5.1540042', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '05:59:17 PM', '06:00:41 PM', '06:01:24 PM', '1', '0.00', '00.00', '0.00', '0.00', '6.4', 2, '57.6', '5.76', '51.84', '64.00', '0.00', '60.00 Km', '60000.0', '1', '57.6', 1, '', 1, 0, '0000-00-00'),
(51, 74, '52.3538358854794', '5.15418215668253', '52.3538416199573', '5.15409546216331', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '04:45:13 PM', '04:45:29 PM', '04:46:26 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '6', '0.00', '6', '6.00', '0.00', '0.00 Km', '0.0', '1', '6', 1, '', 1, 0, '0000-00-00'),
(52, 76, '52.3537811', '5.1542691', '52.3537766', '5.1543099', 'Venusstraat 33A, 1363 VN Almere, Netherlands', 'Venusstraat 33A, 1363 VN Almere, Netherlands', '04:51:35 PM', '04:52:29 PM', '04:52:52 PM', '0', '0.00', '00.00', '0.00', '0.00', '2.623', 2, '23.607', '2.36', '21.25', '26.23', '0.00', '22.23 Km', '22230.0', '0', '23.607', 1, '', 1, 1, '0000-00-00'),
(53, 77, '52.3537654', '5.1543681', '52.3537644', '5.1543938', 'Venusstraat 33A, 1363 VN Almere, Netherlands', 'Venusstraat 33A, 1363 VN Almere, Netherlands', '04:54:01 PM', '04:54:07 PM', '04:54:13 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 1, '0000-00-00'),
(54, 78, '52.3537993', '5.1542372', '52.353793', '5.1542379', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '04:57:13 PM', '04:57:23 PM', '04:57:51 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.5', 2, '4.5', '0.45', '4.05', '5.00', '0.00', '0.00 Km', '0.0', '0', '4.5', 1, '', 1, 1, '0000-00-00'),
(55, 79, '52.3538428853802', '5.15410728919921', '52.3538428853802', '5.15410728919921', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '05:01:08 PM', '05:01:44 PM', '05:02:31 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.6', 7, '5.4', '0.00', '5.4', '6.00', '0.00', '0.00 Km', '0.0', '1', '5.4', 1, '', 1, 1, '0000-00-00'),
(56, 80, '52.3538768210459', '5.15413679778872', '52.3538768210459', '5.15413679778872', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '05:27:27 PM', '05:27:55 PM', '05:28:11 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.6', 7, '5.4', '0.00', '5.4', '6.00', '0.00', '0.00 Km', '0.0', '0', '5.4', 1, '', 1, 1, '0000-00-00'),
(57, 72, '52.3538549608898', '5.15384801163865', '52.3538563889183', '5.15385116980868', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '06:09:02 PM', '06:09:35 PM', '06:09:45 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '6', '0.00', '6', '6.00', '0.00', '0.00 Km', '0.0', '0', '6', 1, '', 1, 0, '0000-00-00'),
(58, 82, '52.3539873617325', '5.15415284179543', '52.3539904910749', '5.15415244549115', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '08:08:26 PM', '08:08:36 PM', '08:08:45 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.6', 7, '5.4', '0.00', '5.4', '6.00', '0.00', '0.00 Km', '0.0', '0', '5.4', 1, '', 0, 1, '0000-00-00'),
(59, 83, '52.3538515154886', '5.15399526691584', '52.3515216964084', '5.17634649691761', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Katernstraat 33, 1321 NC Almere, Netherlands', '08:17:59 PM', '08:18:46 PM', '08:25:33 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.6', 7, '5.4', '0.00', '5.4', '6.00', '0.00', '0.00 Km', '1534.31171815597', '7', '5.4', 1, '', 1, 1, '0000-00-00'),
(60, 84, '', '', '', '', '', '', '08:40:27 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 7, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(61, 87, '52.3532698', '5.1522605', '52.3535222', '5.1527542', 'Poseidonsingel 136, 1363 Almere, Netherlands', 'Zeussingel 97, 1363 Almere, Netherlands', '06:19:36 PM', '06:20:08 PM', '06:21:38 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '16', '1.60', '14.40', '16.00', '0.00', '12.00 Km', '12000.0', '2', '16', 1, '', 1, 1, '0000-00-00'),
(62, 88, '52.3538686956406', '5.15420433490034', '52.3515643080684', '5.17629028046803', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Katernstraat 33, 1321 NC Almere, Netherlands', '06:49:23 PM', '06:49:39 PM', '07:00:56 PM', '0', '0.00', '0.00', '0.00', '0.00', '0.6', 7, '5.4', '0.00', '5.4', '6.00', '0.00', '0.00 Km', '1526.81508818268', '11', '5.4', 1, '', 1, 0, '0000-00-00'),
(63, 90, '52.3539271125826', '5.15416915014043', '52.2773216933216', '5.18482061187682', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Amersfoortsestraatweg 47, 1402 GP Bussum, Netherlands', '06:06:05 PM', '06:13:49 PM', '06:31:18 PM', '7', '0.00', '0.00', '0.00', '0.00', '1', 7, '9', '0.00', '9', '10.00', '0.00', '0.00 Km', '8905.70227417246', '17', '9', 1, '', 1, 1, '0000-00-00'),
(64, 91, '52.3538558', '5.1541051', '52.2774022', '5.1848611', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Amersfoortsestraatweg 47, 1402 GP Bussum, Netherlands', '06:12:08 PM', '06:13:41 PM', '06:31:41 PM', '1', '0.00', '0.00', '0.00', '0.00', '0.00', 2, '28.04', '2.80', '25.24', '28.04', '0.00', '21.04 Km', '0.0', '18', '28.04', 1, '', 1, 1, '0000-00-00'),
(65, 93, '28.412153734852', '77.0432951488318', '28.412153734852', '77.0432951488318', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:35:12 AM', '11:35:17 AM', '11:35:30 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 0, '0000-00-00'),
(66, 94, '28.4120998811241', '77.0431217272552', '28.4121403657165', '77.0432436001273', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '11:39:52 AM', '11:39:57 AM', '11:40:01 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 0, 1, '0000-00-00'),
(67, 95, '28.4121376415979', '77.0431534108492', '28.4122419962924', '77.0432923828038', '1, Sohna Rd, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122001, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '12:03:03 PM', '12:03:06 PM', '12:03:11 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 0, 1, '0000-00-00'),
(68, 97, '28.4121082859564', '77.0432670069178', '28.4121082859564', '77.0432670069178', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '01:27:28 PM', '01:27:34 PM', '01:27:40 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 8, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(69, 98, '28.4121499770609', '77.0432522879994', '28.4121472313687', '77.0432255822546', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:41:25 PM', '02:41:34 PM', '02:42:21 PM', '0', '0.00', '00.00', '0.00', '0.00', '100', 9, '0', '0.00', '0.00', '100.00', '0.00', '0.00 Km', '0.0', '1', '0', 1, '', 1, 1, '0000-00-00'),
(70, 99, '28.412112630513', '77.043235510478', '28.4121401939904', '77.0432488377741', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '02:46:32 PM', '02:46:38 PM', '02:46:57 PM', '0', '0.00', '00.00', '0.00', '0.00', '113', 9, '0.00', '0.00', '0.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 1, '0000-00-00'),
(71, 100, '28.4122985', '77.043445', '28.4123007', '77.0434425', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '03:41:21 PM', '03:41:43 PM', '03:41:48 PM', '0', '0.00', '00.00', '0.00', '0.00', '10', 1, '90', '3.60', '86.40', '100.00', '0.00', '0.00 Km', '0.0', '0', '90', 1, '', 1, 0, '0000-00-00'),
(72, 109, '', '', '', '', '', '', '11:20:33 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 8, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(73, 116, '28.4123461', '77.0433922', '28.4122282', '77.0434484', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '07:18:16 AM', '07:18:19 AM', '07:18:24 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(74, 118, '', '', '', '', '', '', '11:08:39 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 11, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(75, 120, '52.3539209309928', '5.15414151809854', '52.3515316410654', '5.17625760998441', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Katernstraat 33, 1321 NC Almere, Netherlands', '12:34:15 PM', '12:34:42 PM', '01:17:12 PM', '0', '0.00', '14.70', '0.00', '0.00', '0.00', 7, '24.7', '0.00', '24.7', '10.00', '0.00', '0.00 Km', '0.0', '43', '24.7', 1, '', 1, 1, '0000-00-00'),
(76, 122, '52.3538716301442', '5.15212071536007', '52.3539602187626', '5.15420203004885', 'Zeussingel 97, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '08:10:37 PM', '08:10:46 PM', '08:11:01 PM', '0', '0.00', '00.00', '0.00', '0.00', '1.2', 7, '10.8', '0.00', '10.8', '12.00', '0.00', '0.00 Km', '0.0', '0', '10.8', 1, '', 1, 1, '0000-00-00'),
(77, 123, '52.3541063', '5.1543012', '52.3541063', '5.1543012', 'Venusstraat 17, 1363 Almere, Netherlands', 'Venusstraat 17, 1363 Almere, Netherlands', '08:20:29 PM', '08:20:51 PM', '08:21:23 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '10', '1.00', '9.00', '10.00', '0.00', '0.00 Km', '0.0', '1', '10', 1, '', 1, 1, '0000-00-00'),
(78, 124, '52.3539602187626', '5.15420203004885', '52.3540131151071', '5.15430230783725', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 33, 1363 Almere, Netherlands', '08:23:36 PM', '08:23:52 PM', '08:25:56 PM', '0', '0.00', '0.35', '0.00', '0.00', '0.00', 7, '12.35', '0.00', '12.35', '12.00', '0.00', '0.00 Km', '0.0', '2', '12.35', 1, '', 1, 1, '0000-00-00'),
(79, 126, '28.4122926', '77.0434361', '28.4123461', '77.0433922', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '08:37:38 AM', '08:37:45 AM', '08:37:51 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(80, 127, '28.4122853', '77.0434596', '28.4122853', '77.0434596', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '08:57:49 AM', '08:57:54 AM', '08:58:18 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(81, 128, '28.4123359', '77.0434011', '28.4123359', '77.0434011', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '09:01:12 AM', '09:01:19 AM', '09:01:24 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 0, '0000-00-00'),
(82, 131, '28.4123461', '77.0433922', '28.4123461', '77.0433922', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '09:09:18 AM', '09:09:21 AM', '09:09:23 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(83, 132, '28.4122853', '77.0434596', '28.4122853', '77.0434596', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:10:55 AM', '09:10:58 AM', '09:11:01 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 11, '178', '10.68', '167.32', '178.00', '0.00', '0.00 Km', '0.0', '0', '178', 1, '', 1, 1, '0000-00-00'),
(84, 133, '52.3514064033787', '5.17624790572342', '52.3514164479541', '5.17619621822598', 'Katernstraat 31, 1321 NC Almere, Netherlands', 'Katernstraat 31, 1321 NC Almere, Netherlands', '10:02:55 AM', '10:03:04 AM', '10:04:34 AM', '0', '0.00', '0.35', '0.00', '0.00', '1.235', 7, '11.115', '0.00', '11.115', '12.00', '0.00', '0.00 Km', '0.0', '2', '11.115', 1, '', 1, 1, '0000-00-00'),
(85, 134, '28.4120712779085', '77.0432407129992', '28.4120581889438', '77.0432993413757', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '10:55:14 AM', '10:55:27 AM', '10:55:39 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 13, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(86, 135, '28.4120518790849', '77.0432510599494', '28.4122507972907', '77.0433929656418', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '12:18:36 PM', '12:19:09 PM', '12:40:03 PM', '0', '0.00', '300.00', '0.00', '0.00', '0.00', 13, '400', '20.00', '380.00', '100.00', '0.00', '0.00 Km', '0.0', '21', '400', 1, '', 1, 1, '0000-00-00'),
(87, 136, '28.4123039', '77.0434254', '28.4122981', '77.0434215', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '01:19:04 PM', '01:19:14 PM', '01:19:27 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 14, '100', '5.00', '95.00', '100.00', '0.00', '0.00 Km', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(88, 137, '52.3516594634579', '5.17640300783948', '52.353956578693', '5.15420410899573', 'Katernstraat 12, 1321 NE Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '04:06:52 PM', '04:06:58 PM', '04:12:30 PM', '0', '0.00', '1.75', '0.00', '0.00', '1.375', 7, '12.375', '0.00', '12.375', '12.00', '0.00', '0.00 Km', '1542.27632656567', '6', '12.375', 1, '', 1, 1, '0000-00-00'),
(89, 138, '52.0083521226034', '5.08014735895224', '52.0086615418948', '5.08040910117156', 'Rietput, 3434 Nieuwegein, Netherlands', 'Reigersbek 110-112, 3434 Nieuwegein, Netherlands', '06:57:21 PM', '06:57:31 PM', '06:58:22 PM', '0', '0.00', '00.00', '0.00', '0.00', '1.2', 7, '10.8', '0.00', '10.8', '12.00', '0.00', '0.00 Km', '0.0', '1', '10.8', 1, '', 1, 1, '0000-00-00'),
(90, 139, '52.3539262', '5.1541664', '52.3539391', '5.1540453', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '02:45:41 PM', '02:46:05 PM', '02:48:25 PM', '0', '0.00', '0.30', '0.00', '0.00', '0.00', 2, '10.3', '1.03', '9.27', '10.00', '0.00', '0.00 Km', '0.0', '2', '10.3', 1, '', 1, 0, '0000-00-00'),
(91, 141, '52.3539071', '5.1541743', '52.3539064', '5.1541802', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '02:54:22 PM', '02:56:19 PM', '02:56:27 PM', '0', '0.00', '00.00', '0.00', '0.00', '1', 2, '9', '0.90', '8.10', '10.00', '0.00', '0.00 Km', '0.0', '0', '9', 1, '', 1, 1, '0000-00-00'),
(92, 151, '52.3539056', '5.1537029', '52.3539052', '5.1537121', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '05:14:37 PM', '05:14:55 PM', '05:14:59 PM', '0', '0.00', '00.00', '0.00', '0.00', '1', 2, '9', '0.90', '8.10', '10.00', '0.00', '0.00 Km', '0.0', '0', '9', 1, '', 1, 0, '0000-00-00'),
(93, 152, '52.3536909', '5.1538105', '52.3536869', '5.1537799', 'Achillesstraat 27, 1363 Almere, Netherlands', 'Achillesstraat 27, 1363 Almere, Netherlands', '05:16:36 PM', '05:16:47 PM', '05:16:58 PM', '0', '0.00', '00.00', '0.00', '0.00', '10', 2, '0', '0.00', '0.00', '10.00', '0.00', '0.00 Km', '0.0', '0', '0', 1, '', 1, 0, '0000-00-00'),
(94, 153, '52.353807025979', '5.15423476696014', '52.3539475422855', '5.15424641956643', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '07:05:40 PM', '07:07:55 PM', '07:08:28 PM', '2', '1.30', '00.00', '0.00', '0.00', '0.00', 12, '6.3', '0.00', '6.3', '5.00', '0.00', '0.00 Km', '0.0', '1', '6.3', 1, '', 1, 1, '0000-00-00'),
(95, 154, '52.3539630938061', '5.15415095682251', '52.3540946058933', '5.15430686998012', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 17, 1363 Almere, Netherlands', '07:35:19 PM', '07:37:05 PM', '07:37:22 PM', '1', '0.65', '00.00', '0.00', '-5.65', '0', 12, '0', '0.00', '0', '5.00', '0.00', '0.00 Km', '0.0', '0', '0', 1, '', 1, 1, '0000-00-00'),
(96, 156, '52.353809333542', '5.1541642775654', '52.3538771502701', '5.15407231161516', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '05:14:11 PM', '05:14:21 PM', '05:14:43 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '5', '0.00', '5', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(97, 157, '52.3541803219962', '5.15460615212941', '52.3522488976239', '5.13969551729615', 'Venusstraat 17, 1363 Almere, Netherlands', 'Olivier van Noortstraat, 1363 Almere, Netherlands', '06:20:34 PM', '06:21:10 PM', '06:30:51 PM', '0', '0.00', '1.25', '0.00', '0.00', '0.00', 12, '6.61', '0.00', '6.61', '5.36', '0.00', '1.33 Km', '896.354565604822', '10', '6.61', 1, '', 1, 1, '0000-00-00'),
(98, 158, '52.3539577574646', '5.15400453428367', '52.3539618839378', '5.15417527678158', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '06:59:46 PM', '06:59:57 PM', '07:04:43 PM', '0', '0.00', '1.40', '0.00', '0.00', '0.00', 7, '13.4', '0.00', '13.4', '12.00', '0.00', '0.00 Km', '0.0', '5', '13.4', 1, '', 1, 0, '0000-00-00'),
(99, 159, '52.3540131', '5.15452', '52.3540071', '5.154506', 'Venusstraat 33A, 1363 VN Almere, Netherlands', 'Venusstraat 33A, 1363 VN Almere, Netherlands', '05:32:02 PM', '05:32:24 PM', '05:32:37 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '10', '1.00', '9.00', '10.00', '0.00', '0.00 Km', '0.0', '0', '10', 1, '', 1, 0, '0000-00-00'),
(100, 161, '52.3538731261403', '5.1541898854905', '52.3538614728268', '5.15418933856142', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '06:04:58 PM', '06:05:05 PM', '06:07:56 PM', '0', '0.00', '0.70', '0.00', '0.00', '0.00', 7, '12.7', '0.00', '12.7', '12.00', '0.00', '0.00 Km', '0.0', '3', '12.7', 1, '', 0, 0, '0000-00-00'),
(101, 162, '52.3538991', '5.1540888', '52.3538991', '5.1540888', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '06:47:20 PM', '06:47:31 PM', '06:49:09 PM', '0', '0.00', '0.35', '0.00', '0.00', '12.245', 2, '110.205', '11.02', '99.19', '122.10', '0.00', '60.00 Km', '60000.0', '2', '110.205', 1, '', 1, 0, '0000-00-00'),
(102, 165, '52.3540176898999', '5.1540722906337', '52.353920477557', '5.15395482630271', 'Venusstraat 4, 1363 Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '06:37:15 AM', '06:37:29 AM', '06:37:44 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '12', '0.00', '12', '12.00', '0.00', '0.00 Km', '0.0', '0', '12', 1, '', 1, 0, '0000-00-00'),
(103, 168, '52.3533911', '5.1530706', '52.3532063', '5.15257', 'Achillesstraat 30, 1363 VK Almere, Netherlands', 'Poseidonsingel 103, 1363 Almere, Netherlands', '06:48:28 AM', '06:48:39 AM', '06:49:00 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '5', '0.00', '5', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(104, 169, '52.353766', '5.154045', '52.3535299', '5.1534041', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Jupitersingel, 1363 Almere, Netherlands', '06:49:58 AM', '06:52:39 AM', '06:53:03 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '5', '0.00', '5', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(105, 170, '52.0086836756127', '5.08075975686607', '52.0088518938107', '5.08091604302049', 'Reigersbek 130, 3434 XP Nieuwegein, Netherlands', 'Zonnebloemstraat 34, 3434 VB Nieuwegein, Netherlands', '06:34:20 PM', '06:34:34 PM', '06:35:16 PM', '0', '0.00', '00.00', '0.00', '0.00', '10', 7, '2', '0.00', '2', '12.00', '0.00', '0.00 Km', '0.0', '1', '2', 1, '', 1, 1, '0000-00-00'),
(106, 173, '52.3538929', '5.1540797', '52.3538929', '5.1540797', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '09:07:09 AM', '09:07:16 AM', '09:08:16 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '5', '0.00', '5', '5.00', '0.00', '0.00 Km', '0.0', '1', '5', 1, '', 1, 0, '0000-00-00'),
(107, 174, '52.3538584657545', '5.15415329081869', '52.3538234262384', '5.15415774648414', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '09:09:30 AM', '09:09:36 AM', '09:09:41 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '12', '0.00', '12', '12.00', '0.00', '0.00 Km', '0.0', '0', '12', 1, '', 1, 0, '0000-00-00'),
(108, 178, '52.3538438517648', '5.15422992527258', '52.3540131224418', '5.15407863672685', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '04:10:25 PM', '04:10:31 PM', '04:10:48 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '12', '0.00', '12', '12.00', '0.00', '0.00 Km', '0.0', '0', '12', 1, '', 1, 1, '0000-00-00'),
(109, 179, '52.3538816428297', '5.15428100289379', '52.3538816428297', '5.15428100289379', 'Venusstraat 33, 1363 Almere, Netherlands', 'Venusstraat 33, 1363 Almere, Netherlands', '04:23:20 PM', '04:23:37 PM', '04:23:49 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '12', '0.00', '12', '12.00', '0.00', '0.00 Km', '0.0', '0', '12', 1, '', 1, 0, '0000-00-00'),
(110, 172, '52.3537378', '5.1536733', '52.3537305', '5.1536761', 'Achillesstraat 27, 1363 Almere, Netherlands', 'Achillesstraat 27, 1363 Almere, Netherlands', '04:29:23 PM', '04:30:36 PM', '04:30:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '5', '0.00', '5', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(111, 172, '52.3537378', '5.1536733', '52.3537305', '5.1536761', 'Achillesstraat 27, 1363 Almere, Netherlands', 'Achillesstraat 27, 1363 Almere, Netherlands', '04:30:33 PM', '04:30:36 PM', '04:30:38 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '5', '0.00', '5', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(112, 180, '52.35386948420134', '5.154112726449966', '52.3538919016289', '5.15418042423778', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '04:53:53 PM', '05:12:53 PM', '05:14:51 PM', '18', '11.70', '0.35', '0.00', '0.00', '0.00', 7, '22.05', '0.00', '22.05', '10.00', '0.00', '0.00 Km', '0.0', '2', '22.05', 1, '', 0, 0, '0000-00-00'),
(113, 181, '52.353908', '5.1540945', '52.353908', '5.1540945', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '07:58:43 PM', '08:00:06 PM', '08:00:15 PM', '0', '0.00', '00.00', '0.00', '-5', '0.00', 12, '0', '0.00', '0', '5.00', '0.00', '0.00 Km', '0.0', '0', '0', 1, '', 1, 0, '0000-00-00'),
(114, 182, '52.3539307588794', '5.15416682481525', '52.3538784316727', '5.15409274658527', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '08:02:13 PM', '08:06:53 PM', '08:17:38 PM', '4', '2.60', '3.50', '0.00', '0.00', '0.00', 7, '16.1', '0.00', '16.1', '10.00', '0.00', '0.00 Km', '0.0', '11', '16.1', 1, '', 1, 0, '0000-00-00'),
(115, 185, '52.3539412', '5.1542187', '52.3539412', '5.1542187', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '05:59:55 PM', '06:00:31 PM', '06:01:12 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '75.4', '0.00', '75.4', '75.40', '0.00', '65.00 Km', '65000.0', '1', '0.00', 1, '', 1, 0, '0000-00-00'),
(116, 190, '52.3538126', '5.1541758', '52.353825', '5.154192', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '07:37:47 PM', '07:38:14 PM', '07:38:21 PM', '0', '0.00', '00.00', '0.00', '-5', '0.00', 12, '0', '0.00', '0', '5.00', '0.00', '0.00 Km', '0.0', '0', '0', 1, '', 1, 0, '0000-00-00'),
(117, 192, '52.3538754', '5.1542289', '52.3538833', '5.1542607', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '07:40:35 PM', '07:40:40 PM', '07:40:55 PM', '0', '0.00', '00.00', '0.00', '-10.5', '0.00', 12, '0', '0.00', '0', '10.50', '0.00', '6.00 Km', '6000.0', '0', '0', 1, '', 1, 0, '0000-00-00'),
(118, 194, '', '', '', '', '', '', '08:47:43 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 12, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(119, 197, '52.3539677', '5.1540839', '52.3539677', '5.1540839', 'Venusstraat 4, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '04:46:35 PM', '04:46:46 PM', '04:46:56 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '5', '0.00', '5', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(120, 204, '52.3539049', '5.154136', '52.3539049', '5.154136', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '05:18:14 PM', '05:18:20 PM', '05:18:26 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '10', '0.00', '10', '10.00', '0.00', '0.00 Km', '0.0', '0', '10', 1, 'Payment complete.', 1, 0, '0000-00-00'),
(121, 205, '52.3539049', '5.154136', '52.3539049', '5.154136', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '05:19:19 PM', '05:19:26 PM', '05:19:32 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '10', '0.00', '10', '10.00', '0.00', '0.00 Km', '0.0', '0', '10', 1, '', 1, 0, '0000-00-00'),
(122, 207, '', '', '', '', '', '', '05:21:18 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 7, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(123, 208, '52.3540181', '5.154517', '52.3540714', '5.1542924', 'Venusstraat 33, 1363 Almere, Netherlands', 'Venusstraat 17, 1363 Almere, Netherlands', '05:30:39 PM', '05:31:14 PM', '05:31:40 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '10', '0.00', '10', '10.00', '0.00', '0.00 Km', '0.0', '0', '10', 1, '', 1, 0, '0000-00-00'),
(124, 209, '52.3539821', '5.1542569', '52.3539976', '5.1542469', 'Venusstraat 33, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '05:34:11 PM', '05:34:56 PM', '05:35:26 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '10', '0.00', '10', '10.00', '0.00', '0.00 Km', '0.0', '1', '10', 1, '', 1, 0, '0000-00-00'),
(125, 210, '52.3539461', '5.154671', '52.3539281', '5.1543213', 'Venusstraat 33A, 1363 VN Almere, Netherlands', 'Venusstraat 33, 1363 Almere, Netherlands', '05:58:48 PM', '06:00:23 PM', '06:01:29 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '10', '0.00', '10', '10.00', '0.00', '0.00 Km', '0.0', '1', '10', 1, '', 1, 0, '0000-00-00'),
(126, 212, '52.3537887', '5.1537598', '52.354088', '5.1541486', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '11:40:31 AM', '11:48:14 AM', '11:51:57 AM', '6', '3.90', '1.05', '0.00', '0.00', '0.00', 7, '14.95', '0.00', '14.95', '10.00', '0.00', '0.00 Km', '0.0', '4', '14.95', 1, '', 1, 0, '0000-00-00'),
(127, 214, '52.3540327', '5.1541106', '52.3516194', '5.1762469', 'Venusstraat 4, 1363 Almere, Netherlands', 'Katernstraat 33, 1321 NC Almere, Netherlands', '11:56:09 AM', '11:56:17 AM', '12:02:31 PM', '0', '0.00', '1.75', '0.00', '0.00', '0.00', 7, '15.26', '0.00', '15.26', '13.51', '0.00', '2.67 Km', '0.0', '6', '15.26', 1, '', 1, 0, '0000-00-00'),
(128, 215, '52.3514225', '5.1761643', '52.3514225', '5.1761642', 'Katernstraat 31, 1321 NC Almere, Netherlands', 'Katernstraat 31, 1321 NC Almere, Netherlands', '02:50:07 PM', '02:50:48 PM', '02:50:55 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '10', '0.00', '10', '10.00', '0.00', '0.00 Km', '0.0', '0', '10', 1, '', 1, 0, '0000-00-00'),
(129, 218, '52.3514232', '5.1761613', '52.3514234', '5.1761612', 'Katernstraat 31, 1321 NC Almere, Netherlands', 'Katernstraat 31, 1321 NC Almere, Netherlands', '03:47:05 PM', '03:47:13 PM', '03:47:23 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '10', '0.00', '10', '10.00', '0.00', '0.00 Km', '0.0', '0', '10', 1, '', 1, 0, '0000-00-00'),
(130, 220, '52.3514251', '5.1761701', '52.3514244', '5.1761727', 'Katernstraat 31, 1321 NC Almere, Netherlands', 'Katernstraat 31, 1321 NC Almere, Netherlands', '09:13:56 AM', '09:14:17 AM', '09:14:58 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '532.9', '0.00', '532.9', '532.90', '0.00', '250.00 Km', '250000.0', '1', '532.9', 1, '', 1, 0, '0000-00-00'),
(131, 221, '52.3514254', '5.1761683', '52.3514261', '5.1761498', 'Katernstraat 31, 1321 NC Almere, Netherlands', 'Katernstraat 31, 1321 NC Almere, Netherlands', '09:25:18 AM', '09:26:08 AM', '09:26:37 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '70.9', '0.00', '70.9', '70.90', '0.00', '30.00 Km', '30000.0', '0', '70.9', 1, '', 1, 0, '0000-00-00'),
(132, 225, '52.351425', '5.1761694', '52.351425', '5.1761694', 'Katernstraat 31, 1321 NC Almere, Netherlands', 'Katernstraat 31, 1321 NC Almere, Netherlands', '09:48:47 AM', '09:49:07 AM', '09:49:13 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 16, '5', '0.00', '5', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(133, 226, '52.3514955', '5.1762545', '52.3667327', '5.1914945', 'Katernstraat 33, 1321 NC Almere, Netherlands', 'Messiaenplantsoen 9, 1323 Almere, Netherlands', '03:34:35 PM', '03:34:43 PM', '03:42:20 PM', '0', '0.00', '2.10', '0.00', '0.00', '0.00', 16, '10.33', '0.00', '10.33', '8.23', '0.00', '3.94 Km', '0.0', '8', '10.33', 1, '', 1, 0, '0000-00-00'),
(134, 227, '52.3667315', '5.1915036', '52.4034037', '5.3038412', 'Messiaenplantsoen 9, 1323 Almere, Netherlands', 'Bandastraat 74, 1335 Almere, Netherlands', '03:52:24 PM', '03:52:30 PM', '04:13:25 PM', '0', '0.00', '7.00', '0.00', '0.00', '0.00', 7, '43.9', '0.00', '43.9', '36.90', '0.00', '13.81 Km', '0.0', '21', '43.9', 1, '', 1, 0, '0000-00-00'),
(135, 228, '52.4033594', '5.3039163', '52.3540343', '5.1543565', 'Bandastraat 74, 1335 Almere, Netherlands', 'Venusstraat 33, 1363 Almere, Netherlands', '04:29:56 PM', '04:30:01 PM', '04:58:53 PM', '0', '0.00', '9.80', '0.00', '0.00', '0.00', 7, '60.92', '0.00', '60.92', '51.12', '0.00', '20.58 Km', '17850.0', '29', '60.92', 1, '', 1, 0, '0000-00-00'),
(136, 231, '52.3514268', '5.1763562', '52.3540706', '5.1537592', 'Katernstraat 33, 1321 NC Almere, Netherlands', 'Venusstraat 4, 1363 Almere, Netherlands', '12:56:51 PM', '12:57:26 PM', '01:02:44 PM', '0', '0.00', '1.40', '0.00', '0.00', '0.00', 7, '14.8', '0.00', '14.8', '13.40', '0.00', '2.62 Km', '0.0', '5', '14.8', 1, '', 1, 0, '0000-00-00'),
(137, 234, '52.3538932', '5.1540796', '52.3540363', '5.1536812', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '01:30:08 PM', '01:32:05 PM', '01:32:19 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '10', '0.00', '10', '10.00', '0.00', '0.00 Km', '0.0', '0', '0.00', 1, '', 1, 0, '0000-00-00'),
(138, 235, '', '', '', '', '', '', '03:48:22 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 7, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00'),
(139, 243, '', '', '', '', '', '', '04:32:14 PM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 7, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00');
INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(140, 246, '52.3538463', '5.1541113', '52.3538463', '5.1541113', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '05:58:53 PM', '06:00:26 PM', '06:00:46 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 7, '62.5', '0.00', '62.5', '62.50', '0.00', '26.00 Km', '26000.0', '0', '62.5', 1, '', 1, 0, '0000-00-00'),
(141, 247, '52.3539106', '5.1540613', '52.3539106', '5.1540613', 'Venusstraat 4, 1363 VN Almere, Netherlands', 'Venusstraat 4, 1363 VN Almere, Netherlands', '06:11:19 PM', '06:11:44 PM', '06:11:58 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 12, '32.5', '0.00', '32.5', '32.50', '0.00', '26.00 Km', '26000.0', '0', '32.5', 1, '', 1, 0, '0000-00-00'),
(142, 251, '52.3539019', '5.1536317', '52.3539019', '5.1536317', 'Achillesstraat 21, 1363 Almere, Netherlands', 'Achillesstraat 21, 1363 Almere, Netherlands', '07:38:13 AM', '07:38:18 AM', '07:38:24 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 16, '5', '0.00', '5', '5.00', '0.00', '0.00 Km', '0.0', '0', '5', 1, '', 1, 0, '0000-00-00'),
(143, 259, '', '', '', '', '', '', '09:53:28 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 12, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `driver_category` int(255) NOT NULL DEFAULT '0',
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `driver_category`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(1, 0, 4, 'mahi', 'mahi@gmail.com', '9865325809', '', '123456', 'x45eJkNvkglx35Fw', 0, '470.4', '0', '', 'fpv_U2ohSmo:APA91bEi5GkvUJR1ijOgtwUAtsPzcel1qodIoFVNPs4XkwIlDNuPMF5br6-IAoGL1lRCt2Nsh-e6Vb4LFu8NwkAeLTF-o_UAnE0lYdpLyI78QVs6W055CgYhuwNNjquXwRdGMra7l4-_', 2, '3', 2, 3, '123456', 56, 'Tuesday, Sep 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122853', '77.0434596', '----', '15:07', 'Wednesday, Oct 25, 2017', 5, 11, 0, 1, 0, 2, 2, 0, '', 1, '', '2017-09-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(2, 0, 10, 'TaxiUapp Driver', 'taxiuapp@gmail.com', '0645635065', '', 'Sabrine@05', 'ABxkfKwOSiEUCEYU', 0, '1218.88', '0', '6.16', 'cCT6EoJDZqg:APA91bGx4n4souDURmkZu_FJ8lO8mJPDDWA1eyeY8W7Y9A5oA5VYzpAyG2fxv8Xncrp2eIXEJVNKQq6xow7c-bywTNBjoKW5hd1ZIYgBsNasE4R8AdTrpXlvrJh_ViLRTxrcg-ISEQCI', 2, '3.11607142857', 3, 20, '66-XHG-9', 121, 'Monday, Oct 9', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '52.3537928', '5.1539236', '----', '02:04', 'Thursday, Nov 2, 2017', 47, 6, 7, 1, 0, 1, 2, 0, '', 1, '', '2017-10-09', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0),
(3, 0, 0, 'driver', 'driver@5.com', '1234512345 ', 'uploads/driver/1507973527driver_3.jpg', 'qwerty', 'HCva5Mk7saZRqCeM', 0, '501.96', '0', '', 'FAB70CA807C3D4024E04BB156860A7F3B224D64556369201B85DF4E916FB691E', 1, '3.33333333333', 4, 45, '1235', 56, 'Saturday, Oct 14', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121433664327', '77.0433514647474', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:20', 'Monday, Oct 16, 2017', 3, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-14', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(4, 0, 0, 'qwerty ', 'qwerty@gmail.com', '8874531856 ', 'uploads/driver/1508137567driver_4.jpg', 'qwerty', 'fSme47dTJvTakEL1', 0, '220.8', '0', '', 'FAB70CA807C3D4024E04BB156860A7F3B224D64556369201B85DF4E916FB691E', 1, '2.5', 2, 46, '1234', 56, 'Monday, Oct 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120975472421', '77.0432955574017', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '02:34', 'Monday, Oct 16, 2017', 2, 1, 1, 2, 0, 2, 2, 0, '2017-10-16', 1, '', '2017-10-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(5, 0, 0, 'qwertyu', 'qwertyu@gmail.com', '8582275725 ', 'uploads/driver/1508147092driver_5.jpg', 'qwertyu', 'O5xEg4gFzGjOgDis', 0, '302.4', '0', '192', 'f0rpa7CEQrk:APA91bEYWlr1QFLKBBmH4Qzdu6QMVzeed9xdeYznSw85coeQM3txt15hF8sPvh6fL-M-ANTHNU3lyoaXRt5IH4nOzSznCC_r96h9Do7_uNwP1yhflOocm-aR-7GLv1vuHRHo4HFkgicN', 2, '0', 2, 46, 'dhfu', 56, 'Monday, Oct 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123276', '77.0434099', '----', '07:15', 'Thursday, Oct 26, 2017', 3, 1, 1, 2, 0, 2, 2, 0, '', 1, '', '2017-10-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 2, 0),
(6, 0, 0, 'ahaha', 'agag@9.com', '1234567123 ', '', 'qwerty', 'ppT2qzK6LCwJ23Rx', 0, '0', '0', '', '04BD2D48DE8EE73818BC49F178B5791A5A497921412473E3A86060EE30BB8D9E', 1, '', 3, 44, '1234', 56, 'Monday, Oct 16', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121416139101', '77.043329366352', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:17', 'Tuesday, Oct 17, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-16', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(8, 0, 6, 'driver', 'driv@7.com', '1234567321', '', 'qwerty', 'QpRb97JLGdaVnQ1J', 0, '669.28', '0', '', '616D566A1954E5E82A9F273850A991A6A358DCE5A1B4C419379B083F70C5F2DD', 1, '2.71428571429', 4, 45, '12345', 56, 'Monday, Oct 23', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4124431189437', '77.0433005245013', '322B, Sohna Road, Sector 49 , Gurugram, Haryana 122018', '10:54', 'Friday, Oct 27, 2017', 4, 0, 1, 2, 0, 2, 2, 0, '', 1, '', '2017-10-23', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(7, 44, 0, 'Taxi Iphone', 'rkebdi@gmail.com', '0624372264', 'uploads/driver/1510162173driver_7.jpg', 'Sabrine@05', 'Iu6szXk1JIY9JAs1', 0, '1152.52', '0', '20', 'cCT6EoJDZqg:APA91bGx4n4souDURmkZu_FJ8lO8mJPDDWA1eyeY8W7Y9A5oA5VYzpAyG2fxv8Xncrp2eIXEJVNKQq6xow7c-bywTNBjoKW5hd1ZIYgBsNasE4R8AdTrpXlvrJh_ViLRTxrcg-ISEQCI', 2, '2.43617021277', 4, 45, 'VI- ANO-17', 121, 'Tuesday, Oct 17', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '52.3539145', '5.1540873', '----', '08:55', 'Monday, Nov 13, 2017', 41, 18, 13, 2, 0, 1, 2, 0, '2017-10-17', 1, '', '2017-10-17', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 4),
(9, 0, 4, 'qwerty', 'werty@gmail.com', '8874531851', '', 'qwerty', 'NqIIW3Tg48y23sbq', 0, '0', '0', '', '1C3044782C04AF867917FB5627574B118F5B1D8723C43AADCE512D8002DEAAF0', 1, '3.33333333333', 2, 46, '1234', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4121401939904', '77.0432488377741', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '14:48', 'Tuesday, Oct 24, 2017', 2, 0, 0, 2, 0, 1, 2, 0, '2017-10-24', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 3),
(10, 0, 5, 'sara', 'sara@gmail.com', '9898989898', '', '123456', 'Cg8L6T8eg5qWDG4j', 0, '0', '0', '', 'cg736_aRqrs:APA91bE9E_VzEU8_LGhGfr4_NBfZXV7AW840fvYji2DuIk04zT1Mw5WKVqq9NyrKseTOv9NFS-lAypj8kiL5SqIdIvv8sd36mV9BGdo9pAP94Q8xetXFjw3ynOUD8ZWpBWAgeq1FDcO0', 2, '', 3, 43, 'asdf', 56, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.412325', '77.0434072', '----', '10:49', 'Wednesday, Oct 25, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(17, 0, 0, 'info@taxiuapp.com', 'info@taxiuapp.com', '0624300000', 'uploads/driver/1511192864driver_17.jpg', 'Sabribe@05', 'yYKDsuwl19BhTxB7', 0, '0', '0', '', 'cCT6EoJDZqg:APA91bGx4n4souDURmkZu_FJ8lO8mJPDDWA1eyeY8W7Y9A5oA5VYzpAyG2fxv8Xncrp2eIXEJVNKQq6xow7c-bywTNBjoKW5hd1ZIYgBsNasE4R8AdTrpXlvrJh_ViLRTxrcg-ISEQCI', 2, '', 2, 46, '01-TEST-V7', 121, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '52.3514301', '5.176048', '----', '04:16', 'Monday, Nov 20, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0),
(11, 0, 6, 'bb', 'bb@gmail.com', '9085236580', '', '123456', '2iCIEwnAUzl96B6I', 0, '1003.92', '0', '', 'f0rpa7CEQrk:APA91bEYWlr1QFLKBBmH4Qzdu6QMVzeed9xdeYznSw85coeQM3txt15hF8sPvh6fL-M-ANTHNU3lyoaXRt5IH4nOzSznCC_r96h9Do7_uNwP1yhflOocm-aR-7GLv1vuHRHo4HFkgicN', 2, '1.0625', 4, 45, 'asdfg', 56, 'Thursday, Oct 26', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4123259', '77.0434147', '----', '10:09', 'Friday, Oct 27, 2017', 6, 0, 2, 2, 0, 2, 2, 0, '', 1, '', '2017-10-26', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(12, 0, 0, 'Rich Capdi', 'rkebdi@hotmail.com', '+310624702937', 'uploads/driver/1511209221driver_12.jpg', 'Sabrine@05', 'A0TIklhhafGy0bzf', 3, '150.81', '0', '75.4', 'cCT6EoJDZqg:APA91bGx4n4souDURmkZu_FJ8lO8mJPDDWA1eyeY8W7Y9A5oA5VYzpAyG2fxv8Xncrp2eIXEJVNKQq6xow7c-bywTNBjoKW5hd1ZIYgBsNasE4R8AdTrpXlvrJh_ViLRTxrcg-ISEQCI', 2, '2.25', 2, 46, '87-XBP-5', 121, 'Friday, Oct 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '52.3538972', '5.1540535', '----', '08:11', 'Wednesday, Nov 22, 2017', 14, 4, 4, 2, 0, 1, 2, 0, '2017-10-28', 1, '', '2017-10-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 4),
(13, 0, 5, 'ride', 'ride@9.com', '1234561234', '', 'qwerty', 'ZinNDkzIvbFXDlew', 0, '475', '0', '', '616D566A1954E5E82A9F273850A991A6A358DCE5A1B4C419379B083F70C5F2DD', 1, '2.5', 3, 44, '1234', 56, 'Friday, Oct 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120928415044', '77.0432574849822', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '02:26', 'Friday, Oct 27, 2017', 2, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 2, 0),
(14, 0, 5, 'sara', 'sara11@gmail.com', '9085214785', '', '123456', 'RMfcezw01yca6o8u', 0, '95', '0', '', 'f0rpa7CEQrk:APA91bEYWlr1QFLKBBmH4Qzdu6QMVzeed9xdeYznSw85coeQM3txt15hF8sPvh6fL-M-ANTHNU3lyoaXRt5IH4nOzSznCC_r96h9Do7_uNwP1yhflOocm-aR-7GLv1vuHRHo4HFkgicN', 2, '0', 3, 43, 'gdg', 56, 'Friday, Oct 27', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122962', '77.0434206', '----', '13:19', 'Friday, Oct 27, 2017', 1, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-10-27', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 2, 0),
(15, 0, 6, 'create', 'create@9.com', '1234567809', '', 'qwerty', 'JmdZYpiqtIlheNbE', 0, '0', '0', '', '616D566A1954E5E82A9F273850A991A6A358DCE5A1B4C419379B083F70C5F2DD', 1, '', 4, 45, '1234', 56, 'Saturday, Oct 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4120252402764', '77.04347535975', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '07:10', 'Thursday, Nov 2, 2017', 0, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-10-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(16, 0, 0, 'Rich Android', 'rkebdi@kpnmail.nl', '0624372264 ', 'uploads/driver/1510307223driver_16.jpg', 'Sabrine@05', 'X7CRpcX84bY4KkVt', 0, '20.33', '0', '', 'cCT6EoJDZqg:APA91bGx4n4souDURmkZu_FJ8lO8mJPDDWA1eyeY8W7Y9A5oA5VYzpAyG2fxv8Xncrp2eIXEJVNKQq6xow7c-bywTNBjoKW5hd1ZIYgBsNasE4R8AdTrpXlvrJh_ViLRTxrcg-ISEQCI', 2, '1.25', 2, 46, '87-XBP-5', 121, 'Friday, Nov 10', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '52.3537896', '5.1540516', '----', '08:04', 'Monday, Nov 20, 2017', 3, 2, 3, 2, 0, 1, 2, 0, '', 1, '', '2017-11-10', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0),
(18, 0, 0, 'Rich Android', 'info@admini-desk.nl', '0624123456', 'uploads/driver/1511208492driver_18.jpg', 'Sabrine@05', '3RWMhVsdrX8uzoRq', 0, '0', '0', '', 'cCT6EoJDZqg:APA91bGx4n4souDURmkZu_FJ8lO8mJPDDWA1eyeY8W7Y9A5oA5VYzpAyG2fxv8Xncrp2eIXEJVNKQq6xow7c-bywTNBjoKW5hd1ZIYgBsNasE4R8AdTrpXlvrJh_ViLRTxrcg-ISEQCI', 2, '', 4, 45, '01-VITO-XL', 121, 'Monday, Nov 20', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '52.3538107', '5.1543368', '----', '08:16', 'Monday, Nov 20, 2017', 0, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-11-20', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0),
(19, 0, 0, 'Adroid Test', 'support@taxiuapp.com', '0624372263', '', 'Sabrine@05', '8y4V189DDPENw7KW', 0, '0', '0', '', 'cCT6EoJDZqg:APA91bGx4n4souDURmkZu_FJ8lO8mJPDDWA1eyeY8W7Y9A5oA5VYzpAyG2fxv8Xncrp2eIXEJVNKQq6xow7c-bywTNBjoKW5hd1ZIYgBsNasE4R8AdTrpXlvrJh_ViLRTxrcg-ISEQCI', 2, '', 3, 44, '01-AND-ROID', 121, 'Wednesday, Nov 22', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '52.3538185', '5.1539686', '----', '08:26', 'Wednesday, Nov 22, 2017', 0, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-11-22', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 4, 1, 0),
(20, 0, 0, 'Testing Website driver sign in', 'info@taxi.nl', '+31624123370', '', 'Sabrine@05', 'tjMeT4IC9Z', 1, '0', '0', '', '', 0, '', 4, 45, '01-TEST-2', 121, 'Friday, Nov 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '', '', '', '', '', 0, 0, 0, 2, 0, 0, 1, 0, '', 0, '', '2017-11-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 1, '300', 3, '288', '12', '2017-09-26'),
(2, 2, '90', 1, '81.00', '9.00', '2017-10-09'),
(3, 2, '351.74', 12, '316.57', '28.33', '2017-10-10'),
(4, 2, '459.7', 8, '413.73', '45.97', '2017-10-11'),
(5, 2, '64', 1, '57.60', '6.40', '2017-10-12'),
(6, 3, '534', 3, '501.96', '32.04', '2017-10-14'),
(7, 2, '59.5', 3, '53.55', '5.95', '2017-10-14'),
(8, 2, '36.1', 9, '32.49', '3.61', '2017-10-15'),
(9, 1, '100', 1, '96.00', '4.00', '2017-10-16'),
(10, 5, '315', 4, '302.4', '-187.4', '2017-10-16'),
(11, 4, '230', 2, '220.8', '9.2', '2017-10-16'),
(12, 2, '57.6', 1, '51.84', '5.76', '2017-10-16'),
(13, 7, '33.6', 6, '33.6', '0', '2017-10-17'),
(14, 2, '33.107', 3, '29.8', '3.31', '2017-10-17'),
(15, 2, '16', 1, '14.40', '1.60', '2017-10-18'),
(16, 7, '5.4', 1, '5.4', '0.00', '2017-10-19'),
(17, 7, '9', 1, '9', '0.00', '2017-10-20'),
(18, 2, '28.04', 1, '25.24', '2.80', '2017-10-20'),
(19, 8, '712', 4, '669.28', '42.72', '2017-10-23'),
(20, 9, '0', 2, '0', '0', '2017-10-24'),
(21, 1, '90', 1, '86.40', '3.60', '2017-10-24'),
(22, 11, '178', 1, '167.32', '10.68', '2017-10-26'),
(23, 7, '47.85', 4, '47.85', '0', '2017-10-26'),
(24, 2, '10', 1, '9.00', '1.00', '2017-10-26'),
(25, 11, '890', 5, '836.6', '53.4', '2017-10-27'),
(26, 7, '34.29', 3, '34.29', '0', '2017-10-27'),
(27, 13, '500', 2, '475', '25', '2017-10-27'),
(28, 14, '100', 1, '95.00', '5.00', '2017-10-27'),
(29, 2, '28.3', 4, '25.47', '2.83', '2017-10-28'),
(30, 12, '6.3', 2, '6.3', '0', '2017-10-28'),
(31, 12, '5', 1, '5', '0.00', '2017-10-29'),
(32, 12, '6.61', 1, '6.61', '0.00', '2017-10-30'),
(33, 7, '13.4', 1, '13.4', '0.00', '2017-10-30'),
(34, 2, '10', 1, '9.00', '1.00', '2017-11-01'),
(35, 7, '12.7', 1, '12.7', '0.00', '2017-11-02'),
(36, 2, '110.205', 1, '99.19', '11.02', '2017-11-02'),
(37, 7, '14', 2, '14', '0', '2017-11-04'),
(38, 12, '10', 2, '10', '0', '2017-11-04'),
(39, 12, '10', 3, '10', '0', '2017-11-05'),
(40, 7, '86.15', 6, '86.15', '0', '2017-11-05'),
(41, 12, '75.4', 4, '75.4', '-75.4', '2017-11-07'),
(42, 12, '5', 1, '5', '0.00', '2017-11-08'),
(43, 7, '50', 5, '50', '-10', '2017-11-08'),
(44, 7, '50.21', 4, '50.21', '0', '2017-11-09'),
(45, 7, '708.62', 4, '708.62', '0', '2017-11-10'),
(46, 16, '15.33', 2, '15.33', '0', '2017-11-10'),
(47, 7, '24.8', 3, '24.8', '-10', '2017-11-11'),
(48, 7, '62.5', 1, '62.5', '0.00', '2017-11-12'),
(49, 12, '32.5', 1, '32.5', '0.00', '2017-11-12'),
(50, 16, '5', 1, '5', '0.00', '2017-11-13');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`) VALUES
(1, 1, 100, 1),
(2, 2, 259, 1),
(3, 3, 44, 1),
(4, 4, 69, 1),
(5, 5, 69, 1),
(6, 7, 246, 1),
(7, 8, 132, 1),
(8, 9, 115, 1),
(9, 11, 132, 1),
(10, 13, 136, 1),
(11, 14, 136, 1),
(12, 12, 250, 1),
(13, 16, 251, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extra_charges`
--

INSERT INTO `extra_charges` (`extra_charges_id`, `city_id`, `extra_charges_type`, `extra_charges_day`, `slot_one_starttime`, `slot_one_endtime`, `slot_two_starttime`, `slot_two_endtime`, `payment_type`, `slot_price`) VALUES
(1, 121, 1, 'Monday', '07:43 PM', '07:50 PM', '07:44 PM', '07:44 PM', 0, '0'),
(2, 121, 2, 'Night', '07:30 PM', '07:50 PM', '', '', 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 2, '', '52.35403597076941', '5.153858922421932', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', '', '21:50:41', '09:50:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.35403597076941,5.153858922421932&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(2, 2, '', '52.34882851459784', '5.153212174773216', 'Duitslandpad 1-15, 1363 Almere, Netherlands', '52.35523187061201', '5.165105760097504', 'Spoorbaanpad, 1362 Almere, Netherlands', '', '07:50:46', '07:50:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.34882851459784,5.153212174773216&markers=color:red|label:D|52.35523187061201,5.165105760097504&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(3, 2, '', '52.35403371818782', '5.153910554945468', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Tuesday, Oct 10', '18:47:31', '06:47:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35403371818782,5.153910554945468&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(4, 2, '', '52.353879928026615', '5.154090262949467', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Oct 11', '16:27:27', '04:27:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353879928026615,5.154090262949467&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(5, 2, '', '52.353879928026615', '5.154090262949467', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', '', '16:27:45', '04:27:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.353879928026615,5.154090262949467&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(6, 2, '', '52.353879928026615', '5.154090262949467', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', '', '16:28:05', '04:28:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.353879928026615,5.154090262949467&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 2, 0, 1),
(7, 2, '', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Wednesday, Oct 11', '16:52:20', '04:52:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(8, 2, '', '52.35386784595401', '5.1541271433234215', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', '', '19:02:09', '07:02:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.35386784595401,5.1541271433234215&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(9, 2, '', '52.35386784595401', '5.1541271433234215', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Wednesday, Oct 11', '19:02:24', '07:02:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35386784595401,5.1541271433234215&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(10, 2, '', '52.351434776878136', '5.176174901425839', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Friday, Oct 13', '13:48:21', '01:48:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351434776878136,5.176174901425839&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(11, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', '', '18:29:03', '06:29:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(12, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.52306', '5.4379417000000005', 'Batavia Stad Fashion Outlet', '', '19:00:24', '07:00:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.52306,5.4379417000000005&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(13, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.52306', '5.4379417000000005', 'Batavia Stad Fashion Outlet', '', '19:00:33', '07:00:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.52306,5.4379417000000005&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(14, 2, '', '52.35403597076941', '5.153858922421932', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', '', '10:51:54', '10:51:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.35403597076941,5.153858922421932&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(15, 2, '', '52.353986413947545', '5.1539427414536485', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', '', '11:23:24', '11:23:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.353986413947545,5.1539427414536485&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(16, 2, '', '52.35385166825839', '5.1541100442409515', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', '', '16:01:12', '04:01:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.35385166825839,5.1541100442409515&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(17, 2, '', '52.354020407475964', '5.153892785310745', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', '', '17:31:15', '05:31:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.354020407475964,5.153892785310745&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(18, 2, 'TAXIUAPP', '52.3791283', '4.900272', 'Amsterdam Centraal', '52.309456', '4.7622836', 'Schiphol Airport', '', '17:51:41', '05:51:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.3791283,4.900272&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(19, 2, 'TAXIUAPP', '52.3791283', '4.900272', 'Amsterdam Centraal', '52.309456', '4.7622836', 'Schiphol Airport', '', '17:52:50', '05:52:50 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.3791283,4.900272&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 3, 0, 1),
(20, 10, '', '52.3540767', '5.1545814', 'Venusstraat', '52.3702157', '4.8951679', 'Amsterdam', '', '20:49:34', '08:49:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.3540767,5.1545814&markers=color:red|label:D|52.3702157,4.8951679&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(21, 2, '', '52.35689235664692', '5.155521892011166', 'Castorstraat, 1363 Almere, Netherlands', '52.0704978', '4.300699899999999', 'Den Haag', 'Wednesday, Oct 18', '18:23:44', '06:23:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35689235664692,5.155521892011166&markers=color:red|label:D|52.0704978,4.300699899999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(22, 2, '', '52.35689235664692', '5.155521892011166', 'Castorstraat, 1363 Almere, Netherlands', '52.0704978', '4.300699899999999', 'Den Haag', 'Wednesday, Oct 18', '18:24:13', '06:24:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35689235664692,5.155521892011166&markers=color:red|label:D|52.0704978,4.300699899999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(23, 8, '', '28.41217307947026', '77.04319573938845', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '', '', 'Set your drop point', 'Monday, Oct 23', '10:53:29', '10:53:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41217307947026,77.04319573938845&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(24, 1, '', '28.412316396535257', '77.04342506825924', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '15:09:16', '03:09:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412316396535257,77.04342506825924&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(25, 2, '', '52.372706699999995', '4.8943658', 'NH Collection Grand Hotel Krasnapolsky', '52.3076865', '4.7674240999999995', 'Schiphol', '', '19:50:22', '07:50:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.372706699999995,4.8943658&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(26, 2, '', '52.372706699999995', '4.8943658', 'NH Collection Grand Hotel Krasnapolsky', '52.3076865', '4.7674240999999995', 'Schiphol', '', '19:51:48', '07:51:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.372706699999995,4.8943658&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(27, 2, '', '52.372706699999995', '4.8943658', 'NH Collection Grand Hotel Krasnapolsky', '52.3076865', '4.7674240999999995', 'Schiphol', '', '19:52:55', '07:52:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.372706699999995,4.8943658&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(28, 2, '', '52.372706699999995', '4.8943658', 'NH Collection Grand Hotel Krasnapolsky', '52.3076865', '4.7674240999999995', 'Schiphol', '', '19:54:57', '07:54:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.372706699999995,4.8943658&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(29, 2, '', '52.372706699999995', '4.8943658', 'NH Collection Grand Hotel Krasnapolsky', '52.3076865', '4.7674240999999995', 'Schiphol', '', '19:55:12', '07:55:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.372706699999995,4.8943658&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(30, 10, 'TAXIUAPP ', '52.3727067', '4.8943658', 'NH Collection Grand Hotel Krasnapolsky', '52.3076865', '4.7674241', 'Schiphol', '', '20:09:09', '08:09:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.3727067,4.8943658&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(31, 10, 'TAXIUAPP ', '52.3727067', '4.8943658', 'NH Collection Grand Hotel Krasnapolsky', '52.3076865', '4.7674241', 'Schiphol', '', '20:09:18', '08:09:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.3727067,4.8943658&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(32, 2, '', '52.372706699999995', '4.8943658', 'NH Collection Grand Hotel Krasnapolsky', '52.3076865', '4.7674240999999995', 'Schiphol', '', '20:18:44', '08:18:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.372706699999995,4.8943658&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(33, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.352125945125216', '5.139203332364559', 'Olivier van Noortstraat, 1363 Almere, Netherlands', 'Thursday, Oct 26', '20:20:18', '08:20:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.352125945125216,5.139203332364559&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(34, 8, '', '28.412228224125233', '77.04344853758812', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', '', '09:17:58', '09:17:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412228224125233,77.04344853758812&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(35, 8, '', '28.412228224125233', '77.04344853758812', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', '', '09:22:10', '09:22:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412228224125233,77.04344853758812&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(36, 11, '', '28.4120792696634', '77.0432521571208', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4455686722442', '77.0986142009497', 'BLC-143, Golf Course Road, DLF Phase 5, Sector 54\nGurugram, Haryana 122002', '', '10:45:50', '10:45:50 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4120792696634,77.0432521571208&markers=color:red|label:D|28.4455686722442,77.0986142009497&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(37, 8, '', '28.412297228626297', '77.04342875629663', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', '', '13:13:42', '01:13:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.412297228626297,77.04342875629663&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(38, 10, 'TAXIUAPP ', '52.0083206518317', '5.08026348161611', 'Rietput\n3434 Nieuwegein', '52.3579821126838', '5.15566874295473', 'Nikestraat\n1363 Almere', 'Friday, Oct 27', '18:56:57', '06:56:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.0083206518317,5.08026348161611&markers=color:red|label:D|52.3579821126838,5.15566874295473&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(39, 10, '', '52.3539746165072', '5.15406531185993', 'Venusstraat 4\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Monday, Oct 30', '18:18:23', '06:18:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539746165072,5.15406531185993&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(40, 10, '', '52.3539746165072', '5.15406531185993', 'Venusstraat 4\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Monday, Oct 30', '18:18:39', '06:18:39 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539746165072,5.15406531185993&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(41, 10, '', '52.3539746165072', '5.15406531185993', 'Venusstraat 4\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Monday, Oct 30', '18:18:46', '06:18:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539746165072,5.15406531185993&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(42, 10, '', '52.3539746165072', '5.15406531185993', 'Venusstraat 4\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Monday, Oct 30', '18:18:53', '06:18:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539746165072,5.15406531185993&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(43, 10, '', '52.3539746165072', '5.15406531185993', 'Venusstraat 4\n1363 Almere', '52.3667925', '5.1902615', 'Albert Heijn', 'Monday, Oct 30', '18:21:41', '06:21:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539746165072,5.15406531185993&markers=color:red|label:D|52.3667925,5.1902615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(44, 10, 'APPORIO', '52.3539746165072', '5.15406531185993', 'Venusstraat 4\n1363 Almere', '52.3667925', '5.1902615', 'Albert Heijn', 'Monday, Oct 30', '18:22:22', '06:22:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539746165072,5.15406531185993&markers=color:red|label:D|52.3667925,5.1902615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(45, 10, '', '52.3546449827123', '5.13646312057972', 'Adriaen Blockstraat\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Friday, Nov 3', '13:15:49', '01:15:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3546449827123,5.13646312057972&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 1, 1, 1, 0, 1, 0, 1),
(46, 2, '', '52.35392559413568', '5.154056064784527', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Saturday, Nov 4', '06:44:54', '06:44:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35392559413568,5.154056064784527&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(47, 2, '', '52.35392559413568', '5.154056064784527', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Saturday, Nov 4', '06:45:29', '06:45:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35392559413568,5.154056064784527&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(48, 2, '', '52.35392559413568', '5.154056064784527', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.37523136477857', '5.216244421899318', 'P.J. Oudweg 1, 1314 CH Almere, Netherlands', 'Saturday, Nov 4', '06:45:52', '06:45:52 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35392559413568,5.154056064784527&markers=color:red|label:D|52.37523136477857,5.216244421899318&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(49, 10, '', '52.0082727112317', '5.08031759576149', 'Rietput\n3434 Nieuwegein', '52.0882434', '5.1076334', 'Jaarbeurs', 'Saturday, Nov 4', '18:41:00', '06:41:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.0082727112317,5.08031759576149&markers=color:red|label:D|52.0882434,5.1076334&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(50, 10, '', '52.0082727112317', '5.08031759576149', 'Rietput\n3434 Nieuwegein', '52.0882434', '5.1076334', 'Jaarbeurs', '', '18:41:21', '06:41:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.0082727112317,5.08031759576149&markers=color:red|label:D|52.0882434,5.1076334&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 0, 1, 0, 1),
(51, 2, '', '52.35390183957362', '5.154068134725094', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Nov 5', '09:05:27', '09:05:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390183957362,5.154068134725094&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(52, 2, '', '52.35390183957362', '5.154068134725094', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Nov 5', '09:05:40', '09:05:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390183957362,5.154068134725094&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(53, 2, '', '52.35390183957362', '5.154068134725094', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Nov 5', '09:05:49', '09:05:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390183957362,5.154068134725094&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(54, 2, '', '52.35390183957362', '5.154068134725094', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Nov 5', '09:06:11', '09:06:11 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390183957362,5.154068134725094&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(55, 2, '', '52.35390183957362', '5.154068134725094', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Nov 5', '09:08:41', '09:08:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390183957362,5.154068134725094&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(56, 2, '', '52.35390183957362', '5.154068134725094', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Nov 5', '10:24:46', '10:24:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390183957362,5.154068134725094&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(57, 10, '', '52.3539400364169', '5.15407871083611', 'Venusstraat 4\n1363 Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '13:02:15', '01:02:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539400364169,5.15407871083611&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(58, 10, '', '52.3539400364169', '5.15407871083611', 'Venusstraat 4\n1363 Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '13:06:01', '01:06:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539400364169,5.15407871083611&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(59, 10, '', '52.3539400364169', '5.15407871083611', 'Venusstraat 4\n1363 Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '13:06:07', '01:06:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539400364169,5.15407871083611&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(60, 10, '', '52.3539400364169', '5.15407871083611', 'Venusstraat 4\n1363 Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '13:06:13', '01:06:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539400364169,5.15407871083611&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(61, 10, '', '52.3539400364169', '5.15407871083611', 'Venusstraat 4\n1363 Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '16:09:53', '04:09:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539400364169,5.15407871083611&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(62, 10, '', '52.3539400364169', '5.15407871083611', 'Venusstraat 4\n1363 Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '16:10:00', '04:10:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539400364169,5.15407871083611&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(63, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Sunday, Nov 5', '19:41:55', '07:41:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(64, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Sunday, Nov 5', '19:42:10', '07:42:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(65, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Sunday, Nov 5', '19:42:18', '07:42:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(66, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Sunday, Nov 5', '19:42:23', '07:42:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(67, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Tuesday, Nov 7', '17:57:40', '05:57:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(68, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Tuesday, Nov 7', '19:37:01', '07:37:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(69, 2, '', '52.35391228339129', '5.15407282859087', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Wednesday, Nov 8', '08:47:25', '08:47:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35391228339129,5.15407282859087&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(70, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '16:46:17', '04:46:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(71, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '17:18:52', '05:18:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(72, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '17:20:55', '05:20:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(73, 2, '', '52.351422284552605', '5.176163166761398', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', '', '14:51:34', '02:51:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.351422284552605,5.176163166761398&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(74, 2, '', '52.351422284552605', '5.176163166761398', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Thursday, Nov 9', '14:52:11', '02:52:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351422284552605,5.176163166761398&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(75, 2, '', '52.35142515164403', '5.176168531179428', 'Katernstraat 31, 1321 NC Almere, Netherlands', '', '', 'Set your drop point', 'Friday, Nov 10', '09:30:34', '09:30:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35142515164403,5.176168531179428&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(76, 2, '', '52.35390982602264', '5.154070481657983', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3439531', '5.220719300000001', 'Almere Haven', 'Friday, Nov 10', '17:36:56', '05:36:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390982602264,5.154070481657983&markers=color:red|label:D|52.3439531,5.220719300000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(77, 2, '', '52.35145238900321', '5.176168531179428', 'Katernstraat 31, 1321 NC Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '12:55:47', '12:55:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35145238900321,5.176168531179428&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(78, 2, '', '52.35395139649032', '5.154024548828603', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', '', '16:57:07', '04:57:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|52.35395139649032,5.154024548828603&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 0, 1, 0, 1),
(79, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.4051912', '5.28215', 'de Bosrand', 'Monday, Nov 13', '07:09:27', '07:09:27 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.4051912,5.28215&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1),
(80, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.4051912', '5.28215', 'de Bosrand', 'Monday, Nov 13', '07:09:37', '07:09:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.4051912,5.28215&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" 0px;=\"\" sans\";=\"\" open=\"\" initial;\"=\"\" initial;=\"\" 255);=\"\" 255,=\"\" rgb(255,=\"\" none;=\"\" start;=\"\" 2;=\"\" normal;=\"\" 14px;=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" !important;=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\" normal;=\"\" sans\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img width=\"232\" height=\"163\" class=\"alignnone size-full wp-image-434\" alt=\"iso_apporio\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n</h2>\r\n\r\n<a> </a>', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 1, 1, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '1'),
(2, 2, 1, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '1'),
(3, 3, 1, '1', 'Cash', 'Admin', '100', 'Tuesday, Sep 26', '1'),
(4, 4, 2, '1', 'Cash', 'Admin', '90', 'Monday, Oct 9', '1'),
(5, 5, 2, '1', 'Cash', 'Admin', '90', 'Tuesday, Oct 10', '1'),
(6, 6, 2, '1', 'Cash', 'Admin', '180.9', 'Tuesday, Oct 10', '1'),
(7, 7, 2, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 10', '1'),
(8, 8, 2, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 10', '1'),
(9, 9, 2, '3', 'Credit Card', 'Stripe', '6.84', 'Tuesday, Oct 10', 'Payment complete.'),
(10, 10, 2, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 10', '1'),
(11, 11, 2, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 10', '1'),
(12, 12, 2, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 10', '1'),
(13, 13, 2, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 10', '1'),
(14, 14, 2, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 10', '1'),
(15, 15, 2, '1', 'Cash', 'Admin', '3', 'Tuesday, Oct 10', '1'),
(16, 17, 2, '1', 'Cash', 'Admin', '50', 'Tuesday, Oct 10', '1'),
(17, 18, 2, '1', 'Cash', 'Admin', '5', 'Wednesday, Oct 11', '1'),
(18, 19, 2, '1', 'Cash', 'Admin', '136', 'Wednesday, Oct 11', '1'),
(19, 20, 2, '1', 'Cash', 'Admin', '5', 'Wednesday, Oct 11', '1'),
(20, 21, 2, '1', 'Cash', 'Admin', '233', 'Wednesday, Oct 11', '1'),
(21, 22, 2, '1', 'Cash', 'Admin', '5', 'Wednesday, Oct 11', '1'),
(22, 23, 2, '1', 'Cash', 'Admin', '5', 'Wednesday, Oct 11', '1'),
(23, 24, 2, '1', 'Cash', 'Admin', '5', 'Wednesday, Oct 11', '1'),
(24, 26, 2, '1', 'Cash', 'Admin', '65.7', 'Wednesday, Oct 11', '1'),
(25, 27, 2, '1', 'Cash', 'Admin', '64', 'Thursday, Oct 12', '1'),
(26, 28, 4, '1', 'Cash', 'Admin', '178', 'Saturday, Oct 14', '1'),
(27, 29, 4, '1', 'Cash', 'Admin', '178', 'Saturday, Oct 14', '1'),
(28, 30, 4, '1', 'Cash', 'Admin', '178', 'Saturday, Oct 14', '1'),
(29, 31, 2, '1', 'Cash', 'Admin', '5', 'Saturday, Oct 14', '1'),
(30, 32, 5, '1', 'Cash', 'Admin', '49.5', 'Saturday, Oct 14', '1'),
(31, 33, 2, '1', 'Cash', 'Admin', '5', 'Saturday, Oct 14', '1'),
(32, 34, 2, '1', 'Cash', 'Admin', '5', 'Sunday, Oct 15', '1'),
(33, 35, 2, '1', 'Cash', 'Admin', '5', 'Sunday, Oct 15', '1'),
(34, 36, 2, '1', 'Cash', 'Admin', '0.00', 'Sunday, Oct 15', '1'),
(35, 37, 2, '1', 'Cash', 'Admin', '0.00', 'Sunday, Oct 15', '1'),
(36, 38, 6, '1', 'Cash', 'Admin', '4.5', 'Sunday, Oct 15', '1'),
(37, 40, 7, '1', 'Cash', 'Admin', '0.00', 'Sunday, Oct 15', '1'),
(38, 41, 2, '1', 'Cash', 'Admin', '12.6', 'Sunday, Oct 15', '1'),
(39, 42, 2, '1', 'Cash', 'Admin', '4.5', 'Sunday, Oct 15', '1'),
(40, 43, 2, '1', 'Cash', 'Admin', '4.5', 'Sunday, Oct 15', '1'),
(41, 44, 1, '1', 'Cash', 'Admin', '100', 'Monday, Oct 16', '1'),
(42, 45, 1, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything'),
(43, 46, 8, '1', 'Cash', 'Admin', '115', 'Monday, Oct 16', '1'),
(44, 47, 8, '1', 'Cash', 'Admin', '115', 'Monday, Oct 16', '1'),
(45, 48, 8, '1', 'Cash', 'Admin', '115', 'Monday, Oct 16', '1'),
(46, 49, 8, '3', 'Credit Card', 'Stripe', '100', 'Monday, Oct 16', 'Payment complete.'),
(47, 50, 2, '1', 'Cash', 'Admin', '57.6', 'Monday, Oct 16', '1'),
(48, 51, 2, '1', 'Cash', 'Admin', '6', 'Tuesday, Oct 17', '1'),
(49, 52, 10, '1', 'Cash', 'Admin', '23.607', 'Tuesday, Oct 17', '1'),
(50, 53, 2, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 17', '1'),
(51, 54, 10, '1', 'Cash', 'Admin', '4.5', 'Tuesday, Oct 17', '1'),
(52, 55, 10, '1', 'Cash', 'Admin', '5.4', 'Tuesday, Oct 17', '1'),
(53, 56, 10, '1', 'Cash', 'Admin', '5.4', 'Tuesday, Oct 17', '1'),
(54, 57, 10, '1', 'Cash', 'Admin', '6', 'Tuesday, Oct 17', '1'),
(55, 58, 10, '1', 'Cash', 'Admin', '5.4', 'Tuesday, Oct 17', '1'),
(56, 59, 10, '1', 'Cash', 'Admin', '5.4', 'Tuesday, Oct 17', '1'),
(57, 61, 2, '1', 'Cash', 'Admin', '16', 'Wednesday, Oct 18', '1'),
(58, 62, 10, '1', 'Cash', 'Admin', '5.4', 'Thursday, Oct 19', '1'),
(59, 63, 10, '1', 'Cash', 'Admin', '9', 'Friday, Oct 20', '1'),
(60, 64, 2, '1', 'Cash', 'Admin', '28.04', 'Friday, Oct 20', '1'),
(61, 65, 11, '1', 'Cash', 'Admin', '178', 'Monday, Oct 23', '1'),
(62, 66, 11, '1', 'Cash', 'Admin', '178', 'Monday, Oct 23', '1'),
(63, 67, 11, '1', 'Cash', 'Admin', '178', 'Monday, Oct 23', '1'),
(64, 68, 11, '1', 'Cash', 'Admin', '178', 'Monday, Oct 23', '1'),
(65, 69, 12, '1', 'Cash', 'Admin', '0', 'Tuesday, Oct 24', '1'),
(66, 70, 12, '1', 'Cash', 'Admin', '0.00', 'Tuesday, Oct 24', '1'),
(67, 71, 13, '1', 'Cash', 'Admin', '90', 'Tuesday, Oct 24', '1'),
(68, 73, 8, '1', 'Cash', 'Admin', '178', 'Thursday, Oct 26', '1'),
(69, 75, 10, '1', 'Cash', 'Admin', '24.7', 'Thursday, Oct 26', '1'),
(70, 76, 10, '1', 'Cash', 'Admin', '10.8', 'Thursday, Oct 26', '1'),
(71, 77, 2, '1', 'Cash', 'Admin', '10', 'Thursday, Oct 26', '1'),
(72, 0, 2, '1', 'Cash', 'Admin', '0', 'Thursday, Oct 26', '1'),
(73, 78, 2, '1', 'Cash', 'Admin', '12.35', 'Thursday, Oct 26', '1'),
(74, 79, 8, '1', 'Cash', 'Admin', '178', 'Friday, Oct 27', '1'),
(75, 80, 8, '1', 'Cash', 'Admin', '178', 'Friday, Oct 27', '1'),
(76, 81, 8, '1', 'Cash', 'Admin', '178', 'Friday, Oct 27', '1'),
(77, 82, 8, '1', 'Cash', 'Admin', '178', 'Friday, Oct 27', '1'),
(78, 83, 8, '1', 'Cash', 'Admin', '178', 'Friday, Oct 27', '1'),
(79, 84, 10, '1', 'Cash', 'Admin', '11.115', 'Friday, Oct 27', '1'),
(80, 85, 11, '1', 'Cash', 'Admin', '100', 'Friday, Oct 27', '1'),
(81, 86, 11, '1', 'Cash', 'Admin', '400', 'Friday, Oct 27', '1'),
(82, 87, 8, '1', 'Cash', 'Admin', '100', 'Friday, Oct 27', '1'),
(83, 88, 10, '1', 'Cash', 'Admin', '12.375', 'Friday, Oct 27', '1'),
(84, 89, 10, '1', 'Cash', 'Admin', '10.8', 'Friday, Oct 27', '1'),
(85, 90, 2, '1', 'Cash', 'Admin', '10.3', 'Saturday, Oct 28', '1'),
(86, 91, 2, '1', 'Cash', 'Admin', '9', 'Saturday, Oct 28', '1'),
(87, 92, 5, '1', 'Cash', 'Admin', '9', 'Saturday, Oct 28', '1'),
(88, 93, 10, '1', 'Cash', 'Admin', '0', 'Saturday, Oct 28', '1'),
(89, 94, 10, '1', 'Cash', 'Admin', '6.3', 'Saturday, Oct 28', '1'),
(90, 95, 10, '1', 'Cash', 'Admin', '0', 'Saturday, Oct 28', '1'),
(91, 96, 10, '1', 'Cash', 'Admin', '5', 'Sunday, Oct 29', '1'),
(92, 97, 10, '1', 'Cash', 'Admin', '6.61', 'Monday, Oct 30', '1'),
(93, 98, 10, '1', 'Cash', 'Admin', '13.4', 'Monday, Oct 30', '1'),
(94, 99, 2, '1', 'Cash', 'Admin', '10', 'Wednesday, Nov 1', '1'),
(95, 100, 10, '1', 'Cash', 'Admin', '12.7', 'Thursday, Nov 2', '1'),
(96, 101, 2, '1', 'Cash', 'Admin', '110.205', 'Thursday, Nov 2', '1'),
(97, 102, 2, '1', 'Cash', 'Admin', '12', 'Saturday, Nov 4', '1'),
(98, 103, 2, '1', 'Cash', 'Admin', '5', 'Saturday, Nov 4', '1'),
(99, 104, 2, '1', 'Cash', 'Admin', '5', 'Saturday, Nov 4', '1'),
(100, 105, 10, '1', 'Cash', 'Admin', '2', 'Saturday, Nov 4', '1'),
(101, 106, 2, '1', 'Cash', 'Admin', '5', 'Sunday, Nov 5', '1'),
(102, 107, 2, '1', 'Cash', 'Admin', '12', 'Sunday, Nov 5', '1'),
(103, 108, 10, '1', 'Cash', 'Admin', '12', 'Sunday, Nov 5', '1'),
(104, 109, 2, '1', 'Cash', 'Admin', '12', 'Sunday, Nov 5', '1'),
(105, 110, 10, '1', 'Cash', 'Admin', '12', 'Sunday, Nov 5', '1'),
(106, 110, 10, '1', 'Cash', 'Admin', '5', 'Sunday, Nov 5', '1'),
(107, 112, 2, '1', 'Cash', 'Admin', '22.05', 'Sunday, Nov 5', '1'),
(108, 113, 2, '1', 'Cash', 'Admin', '0', 'Sunday, Nov 5', '1'),
(109, 114, 2, '1', 'Cash', 'Admin', '16.1', 'Sunday, Nov 5', '1'),
(110, 115, 2, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything'),
(111, 116, 2, '1', 'Cash', 'Admin', '0', 'Tuesday, Nov 7', '1'),
(112, 117, 2, '1', 'Cash', 'Admin', '0', 'Tuesday, Nov 7', '1'),
(113, 119, 2, '1', 'Cash', 'Admin', '5', 'Wednesday, Nov 8', '1'),
(114, 120, 2, '3', 'Credit Card', 'Stripe', '10', 'Wednesday, Nov 8', 'Payment complete.'),
(115, 121, 2, '1', 'Cash', 'Admin', '10', 'Wednesday, Nov 8', '1'),
(116, 123, 2, '1', 'Cash', 'Admin', '10', 'Wednesday, Nov 8', '1'),
(117, 124, 2, '1', 'Cash', 'Admin', '10', 'Wednesday, Nov 8', '1'),
(118, 125, 2, '1', 'Cash', 'Admin', '10', 'Wednesday, Nov 8', '1'),
(119, 126, 2, '1', 'Cash', 'Admin', '14.95', 'Thursday, Nov 9', '1'),
(120, 127, 2, '1', 'Cash', 'Admin', '15.26', 'Thursday, Nov 9', '1'),
(121, 128, 2, '1', 'Cash', 'Admin', '10', 'Thursday, Nov 9', '1'),
(122, 129, 2, '1', 'Cash', 'Admin', '10', 'Thursday, Nov 9', '1'),
(123, 130, 2, '1', 'Cash', 'Admin', '532.9', 'Friday, Nov 10', '1'),
(124, 131, 2, '1', 'Cash', 'Admin', '70.9', 'Friday, Nov 10', '1'),
(125, 132, 2, '1', 'Cash', 'Admin', '5', 'Friday, Nov 10', '1'),
(126, 133, 2, '1', 'Cash', 'Admin', '10.33', 'Friday, Nov 10', '1'),
(127, 134, 2, '1', 'Cash', 'Admin', '43.9', 'Friday, Nov 10', '1'),
(128, 135, 2, '1', 'Cash', 'Admin', '60.92', 'Friday, Nov 10', '1'),
(129, 136, 2, '1', 'Cash', 'Admin', '14.8', 'Saturday, Nov 11', '1'),
(130, 137, 2, '3', 'Cash', 'Android', '0.00', 'Anything', 'Anything'),
(131, 140, 2, '1', 'Cash', 'Admin', '62.5', 'Sunday, Nov 12', '1'),
(132, 141, 2, '1', 'Cash', 'Admin', '32.5', 'Sunday, Nov 12', '1'),
(133, 142, 2, '1', 'Cash', 'Admin', '5', 'Monday, Nov 13', '1');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 0),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(3, 56, 'Miles', '', 1, 5, 0, 0, 0, 0, 0, 0, '3', '50', '25', '3', '10', '2', '15'),
(12, 56, 'Km', '', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Km', '', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Km', '', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(15, 56, 'Miles', 'â‚¬', 5, 7, 0, 0, 0, 0, 0, 0, '4', '100', '20', '1', '10', '1', '12'),
(46, 121, 'Km', 'â‚¬', 2, 0, 0, 0, 0, 0, 0, 0, '1', '5', '1.10', '20', '0.55', '1', '0.35'),
(47, 121, 'Km', 'â‚¬', 3, 0, 0, 0, 0, 0, 0, 0, '1', '8', '1.90', '20', '0.55', '1', '0.35'),
(48, 121, 'Km', 'â‚¬', 4, 0, 0, 0, 0, 0, 0, 0, '1', '10', '2.10', '20', '0.55', '1', '0.35'),
(49, 122, 'Km', 'MAD', 2, 0, 0, 0, 0, 0, 0, 0, '1', '1', '1', '5', '0.65', '1', '0.25');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'Test', 'Test Push notification', 'uploads/notification/push_image_1.png', '', 0, 0, '2017-10-10', 2),
(2, 'Test', 'Test notification', 'uploads/notification/push_image_2.jpg', '', 0, 0, '2017-10-10', 1),
(3, 'Test', 'Testing E mail', 'uploads/notification/push_image_3.png', '', 0, 0, '2017-10-10', 1),
(4, 'Test', 'Testing E mail', 'uploads/notification/push_image_4.png', '', 0, 0, '2017-10-10', 2),
(5, 'test', 'test', 'uploads/notification/1500380956793.jpg', '', 2, 0, '2017-10-13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_booking`
--

INSERT INTO `rental_booking` (`rental_booking_id`, `user_id`, `rentcard_id`, `payment_option_id`, `coupan_code`, `car_type_id`, `booking_type`, `driver_id`, `pickup_lat`, `pickup_long`, `pickup_location`, `start_meter_reading`, `start_meter_reading_image`, `end_meter_reading`, `end_meter_reading_image`, `booking_date`, `booking_time`, `user_booking_date_time`, `last_update_time`, `booking_status`, `payment_status`, `pem_file`, `booking_admin_status`) VALUES
(1, 2, 3, 1, 'TAXIUAPP', 4, 1, 0, '52.35396839327128', '5.153976604342461', 'Venusstraat 4, 1363 Almere, Netherlands', '0', '', '0', '', 'Friday, Nov 3', '07:35 PM', 'Friday, Nov 3, 07:35 PM', '07:35:46 PM', 10, 0, 1, 1),
(2, 2, 3, 1, 'TAXIUAPP', 4, 1, 0, '52.35396839327128', '5.153976604342461', 'Venusstraat 4, 1363 Almere, Netherlands', '0', '', '0', '', 'Friday, Nov 3', '07:43 PM', 'Friday, Nov 3, 07:43 PM', '07:43:37 PM', 10, 0, 1, 1),
(3, 2, 2, 1, 'TAXIUAPP', 2, 1, 0, '52.35396839327128', '5.153976604342461', 'Venusstraat 4, 1363 Almere, Netherlands', '0', '', '0', '', 'Friday, Nov 3', '07:43 PM', 'Friday, Nov 3, 07:43 PM', '07:45:04 PM', 15, 0, 1, 1),
(4, 2, 2, 1, '', 2, 1, 0, '52.35396839327128', '5.153976604342461', 'Venusstraat 4, 1363 Almere, Netherlands', '0', '', '0', '', 'Friday, Nov 3', '07:45 PM', 'Friday, Nov 3, 07:45 PM', '07:46:06 PM', 15, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_category`
--

INSERT INTO `rental_category` (`rental_category_id`, `rental_category`, `rental_category_hours`, `rental_category_kilometer`, `rental_category_distance_unit`, `rental_category_description`, `rental_category_admin_status`) VALUES
(1, '2 Hours Luxury Taxi rental', '2', '150', 'Km', '<br>', 1),
(2, '2 Hours Standard Taxi rental', '2', '150', 'Km', '<br>', 1),
(3, '2 Hours Minivan Taxi rental', '2', '150', 'Km', '<br>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentcard`
--

INSERT INTO `rentcard` (`rentcard_id`, `city_id`, `car_type_id`, `rental_category_id`, `price`, `price_per_hrs`, `price_per_kms`, `rentcard_admin_status`) VALUES
(1, 121, 3, 1, '200', 100, 2, 1),
(2, 121, 2, 2, '150', 75, 1, 1),
(3, 121, 4, 3, '250', 125, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 102, 1, 0, '2017-09-26'),
(2, 102, 1, 0, '2017-09-26'),
(3, 102, 1, 0, '2017-09-26'),
(4, 4, 2, 1, '2017-10-09'),
(5, 7, 2, 1, '2017-10-10'),
(6, 8, 2, 1, '2017-10-10'),
(7, 9, 2, 0, '2017-10-10'),
(8, 10, 2, 1, '2017-10-10'),
(9, 11, 2, 1, '2017-10-10'),
(10, 12, 2, 1, '2017-10-10'),
(11, 13, 2, 0, '2017-10-10'),
(12, 14, 2, 0, '2017-10-10'),
(13, 15, 2, 1, '2017-10-10'),
(14, 16, 2, 1, '2017-10-10'),
(15, 18, 2, 1, '2017-10-10'),
(16, 19, 2, 1, '2017-10-10'),
(17, 20, 2, 1, '2017-10-10'),
(18, 21, 2, 1, '2017-10-10'),
(19, 102, 1, 0, '0000-00-00'),
(20, 5, 2, 1, '0000-00-00'),
(21, 5, 2, 8, '0000-00-00'),
(22, 22, 2, 1, '2017-10-10'),
(23, 23, 2, 1, '2017-10-11'),
(24, 24, 2, 0, '2017-10-11'),
(25, 25, 2, 0, '2017-10-11'),
(26, 26, 2, 1, '2017-10-11'),
(27, 27, 2, 1, '2017-10-11'),
(28, 28, 2, 1, '2017-10-11'),
(29, 29, 2, 1, '2017-10-11'),
(30, 30, 2, 1, '2017-10-11'),
(31, 31, 2, 1, '2017-10-11'),
(32, 32, 2, 1, '2017-10-11'),
(33, 33, 2, 1, '2017-10-11'),
(34, 34, 2, 1, '2017-10-11'),
(35, 35, 2, 1, '2017-10-11'),
(36, 36, 2, 1, '2017-10-11'),
(37, 37, 2, 1, '2017-10-12'),
(38, 38, 2, 0, '2017-10-13'),
(39, 39, 2, 1, '2017-10-13'),
(40, 40, 2, 1, '0000-00-00'),
(41, 41, 3, 0, '2017-10-14'),
(42, 42, 3, 1, '2017-10-14'),
(43, 43, 3, 1, '2017-10-14'),
(44, 44, 3, 1, '2017-10-14'),
(45, 45, 2, 1, '2017-10-14'),
(46, 46, 2, 0, '2017-10-14'),
(47, 48, 2, 1, '0000-00-00'),
(48, 49, 2, 1, '2017-10-14'),
(49, 50, 2, 1, '0000-00-00'),
(50, 51, 2, 0, '2017-10-15'),
(51, 52, 2, 1, '2017-10-15'),
(52, 53, 2, 1, '2017-10-15'),
(53, 54, 2, 1, '2017-10-15'),
(54, 55, 2, 1, '0000-00-00'),
(55, 55, 2, 1, '0000-00-00'),
(56, 56, 2, 1, '0000-00-00'),
(57, 57, 2, 1, '2017-10-15'),
(58, 58, 2, 1, '2017-10-15'),
(59, 59, 2, 1, '2017-10-15'),
(60, 102, 1, 0, '2017-10-16'),
(61, 102, 1, 0, '2017-10-16'),
(62, 61, 4, 1, '2017-10-16'),
(63, 62, 4, 0, '2017-10-16'),
(64, 63, 4, 0, '2017-10-16'),
(65, 63, 5, 1, '2017-10-16'),
(66, 64, 4, 1, '2017-10-16'),
(67, 64, 5, 0, '2017-10-16'),
(68, 64, 4, 1, '0000-00-00'),
(69, 65, 4, 0, '2017-10-16'),
(70, 65, 5, 1, '2017-10-16'),
(71, 66, 4, 1, '2017-10-16'),
(72, 66, 5, 0, '2017-10-16'),
(73, 67, 4, 0, '2017-10-16'),
(74, 67, 5, 1, '2017-10-16'),
(75, 68, 4, 0, '2017-10-16'),
(76, 68, 5, 0, '2017-10-16'),
(77, 69, 4, 0, '2017-10-16'),
(78, 69, 5, 1, '2017-10-16'),
(79, 70, 2, 1, '2017-10-16'),
(80, 71, 2, 0, '2017-10-17'),
(81, 73, 2, 0, '2017-10-17'),
(82, 74, 7, 1, '2017-10-17'),
(83, 75, 2, 0, '2017-10-17'),
(84, 76, 2, 1, '2017-10-17'),
(85, 77, 2, 1, '2017-10-17'),
(86, 78, 2, 1, '2017-10-17'),
(87, 79, 7, 1, '2017-10-17'),
(88, 80, 7, 1, '2017-10-17'),
(89, 72, 7, 1, '0000-00-00'),
(90, 81, 7, 0, '2017-10-17'),
(91, 82, 7, 1, '2017-10-17'),
(92, 83, 7, 1, '2017-10-17'),
(93, 84, 7, 1, '2017-10-17'),
(94, 85, 2, 0, '2017-10-17'),
(95, 86, 7, 0, '2017-10-18'),
(96, 87, 2, 1, '2017-10-18'),
(97, 88, 7, 1, '2017-10-19'),
(98, 89, 7, 0, '2017-10-20'),
(99, 90, 7, 1, '2017-10-20'),
(100, 91, 2, 1, '2017-10-20'),
(101, 110, 8, 1, '2017-10-23'),
(102, 110, 8, 1, '2017-10-23'),
(103, 110, 8, 1, '2017-10-23'),
(104, 110, 8, 1, '2017-10-23'),
(105, 110, 8, 1, '2017-10-23'),
(106, 110, 8, 1, '2017-10-23'),
(107, 111, 9, 0, '2017-10-24'),
(108, 111, 9, 0, '2017-10-24'),
(109, 102, 1, 0, '2017-10-24'),
(110, 111, 9, 0, '2017-10-24'),
(111, 102, 1, 0, '2017-10-25'),
(112, 111, 9, 0, '2017-10-25'),
(113, 102, 1, 0, '0000-00-00'),
(114, 102, 1, 0, '2017-10-25'),
(115, 111, 9, 0, '2017-10-25'),
(116, 102, 1, 0, '0000-00-00'),
(117, 111, 9, 0, '2017-10-25'),
(118, 111, 9, 0, '2017-10-25'),
(119, 111, 9, 0, '2017-10-25'),
(120, 111, 9, 0, '2017-10-25'),
(121, 111, 9, 0, '2017-10-25'),
(122, 111, 9, 0, '2017-10-25'),
(123, 110, 8, 1, '2017-10-25'),
(124, 110, 8, 1, '2017-10-25'),
(125, 111, 9, 0, '2017-10-25'),
(126, 112, 9, 0, '2017-10-25'),
(127, 113, 7, 0, '2017-10-25'),
(128, 114, 9, 0, '2017-10-26'),
(129, 115, 9, 0, '2017-10-26'),
(130, 116, 8, 0, '2017-10-26'),
(131, 116, 11, 1, '2017-10-26'),
(132, 117, 8, 0, '2017-10-26'),
(133, 117, 11, 1, '2017-10-26'),
(134, 118, 8, 0, '2017-10-26'),
(135, 118, 11, 1, '2017-10-26'),
(136, 119, 7, 0, '2017-10-26'),
(137, 120, 7, 1, '2017-10-26'),
(138, 121, 7, 1, '0000-00-00'),
(139, 122, 7, 1, '2017-10-26'),
(140, 123, 2, 1, '2017-10-26'),
(141, 124, 7, 1, '2017-10-26'),
(142, 125, 7, 0, '2017-10-26'),
(143, 126, 8, 0, '2017-10-27'),
(144, 126, 11, 1, '2017-10-27'),
(145, 127, 8, 0, '2017-10-27'),
(146, 127, 11, 1, '2017-10-27'),
(147, 128, 8, 0, '2017-10-27'),
(148, 128, 11, 1, '2017-10-27'),
(149, 129, 8, 0, '2017-10-27'),
(150, 130, 8, 0, '2017-10-27'),
(151, 131, 8, 0, '2017-10-27'),
(152, 131, 11, 1, '2017-10-27'),
(153, 132, 8, 0, '2017-10-27'),
(154, 132, 11, 1, '2017-10-27'),
(155, 133, 7, 1, '2017-10-27'),
(156, 134, 13, 1, '2017-10-27'),
(157, 135, 13, 1, '2017-10-27'),
(158, 136, 13, 0, '2017-10-27'),
(159, 136, 14, 1, '2017-10-27'),
(160, 137, 7, 1, '2017-10-27'),
(161, 138, 7, 1, '2017-10-27'),
(162, 139, 2, 1, '2017-10-28'),
(163, 140, 2, 0, '2017-10-28'),
(164, 141, 2, 1, '2017-10-28'),
(165, 142, 7, 0, '2017-10-28'),
(166, 143, 7, 0, '2017-10-28'),
(167, 144, 7, 0, '2017-10-28'),
(168, 145, 7, 0, '2017-10-28'),
(169, 146, 7, 0, '2017-10-28'),
(170, 147, 7, 0, '2017-10-28'),
(171, 148, 7, 0, '2017-10-28'),
(172, 149, 7, 0, '2017-10-28'),
(173, 150, 7, 0, '2017-10-28'),
(174, 151, 2, 1, '0000-00-00'),
(175, 152, 2, 1, '0000-00-00'),
(176, 153, 12, 1, '2017-10-28'),
(177, 154, 12, 1, '2017-10-28'),
(178, 155, 12, 0, '2017-10-29'),
(179, 156, 12, 1, '2017-10-29'),
(180, 157, 12, 1, '2017-10-30'),
(181, 158, 7, 1, '2017-10-30'),
(182, 159, 2, 1, '2017-11-01'),
(183, 160, 7, 0, '2017-11-02'),
(184, 161, 7, 1, '2017-11-02'),
(185, 162, 2, 1, '2017-11-02'),
(186, 163, 7, 0, '2017-11-03'),
(187, 164, 2, 0, '2017-11-03'),
(188, 165, 7, 1, '2017-11-04'),
(189, 166, 7, 0, '2017-11-04'),
(190, 167, 7, 1, '2017-11-04'),
(191, 168, 2, 0, '2017-11-04'),
(192, 169, 2, 0, '2017-11-04'),
(193, 170, 7, 1, '2017-11-04'),
(194, 171, 7, 0, '2017-11-04'),
(195, 173, 2, 0, '2017-11-05'),
(196, 174, 7, 1, '2017-11-05'),
(197, 175, 7, 0, '2017-11-05'),
(198, 176, 2, 0, '2017-11-05'),
(199, 177, 7, 0, '2017-11-05'),
(200, 178, 7, 1, '2017-11-05'),
(201, 179, 7, 1, '2017-11-05'),
(202, 172, 7, 1, '0000-00-00'),
(203, 172, 2, 8, '0000-00-00'),
(204, 180, 7, 1, '2017-11-05'),
(205, 181, 2, 0, '2017-11-05'),
(206, 182, 7, 1, '2017-11-05'),
(207, 183, 2, 0, '2017-11-07'),
(208, 184, 2, 0, '2017-11-07'),
(209, 185, 2, 0, '2017-11-07'),
(210, 186, 7, 0, '2017-11-07'),
(211, 187, 7, 0, '2017-11-07'),
(212, 188, 7, 0, '2017-11-07'),
(213, 189, 7, 0, '2017-11-07'),
(214, 190, 2, 0, '2017-11-07'),
(215, 191, 2, 0, '2017-11-07'),
(216, 192, 2, 0, '2017-11-07'),
(217, 193, 7, 0, '2017-11-07'),
(218, 194, 2, 0, '2017-11-08'),
(219, 195, 2, 0, '2017-11-08'),
(220, 196, 2, 0, '2017-11-08'),
(221, 197, 2, 0, '2017-11-08'),
(222, 198, 7, 0, '2017-11-08'),
(223, 199, 7, 0, '2017-11-08'),
(224, 200, 7, 0, '2017-11-08'),
(225, 201, 7, 0, '2017-11-08'),
(226, 202, 7, 0, '2017-11-08'),
(227, 203, 7, 0, '2017-11-08'),
(228, 204, 7, 1, '2017-11-08'),
(229, 205, 2, 0, '2017-11-08'),
(230, 206, 7, 1, '2017-11-08'),
(231, 207, 2, 0, '2017-11-08'),
(232, 208, 7, 1, '2017-11-08'),
(233, 209, 2, 0, '2017-11-08'),
(234, 210, 2, 0, '2017-11-08'),
(235, 211, 2, 0, '2017-11-08'),
(236, 212, 7, 1, '2017-11-09'),
(237, 213, 7, 1, '2017-11-09'),
(238, 214, 7, 1, '2017-11-09'),
(239, 215, 2, 8, '0000-00-00'),
(240, 216, 7, 1, '2017-11-09'),
(241, 217, 2, 0, '2017-11-09'),
(242, 218, 7, 1, '2017-11-09'),
(243, 220, 7, 1, '2017-11-10'),
(244, 221, 2, 0, '2017-11-10'),
(245, 222, 2, 0, '2017-11-10'),
(246, 222, 7, 0, '0000-00-00'),
(247, 223, 2, 0, '2017-11-10'),
(248, 223, 7, 0, '0000-00-00'),
(249, 224, 2, 0, '2017-11-10'),
(250, 224, 16, 0, '0000-00-00'),
(251, 225, 2, 0, '2017-11-10'),
(252, 226, 16, 1, '2017-11-10'),
(253, 227, 7, 1, '2017-11-10'),
(254, 228, 2, 0, '2017-11-10'),
(255, 229, 2, 0, '2017-11-10'),
(256, 230, 2, 0, '2017-11-10'),
(257, 230, 7, 0, '0000-00-00'),
(258, 231, 7, 1, '2017-11-11'),
(259, 232, 2, 0, '2017-11-11'),
(260, 232, 7, 0, '0000-00-00'),
(261, 233, 2, 0, '2017-11-11'),
(262, 233, 7, 0, '0000-00-00'),
(263, 234, 2, 0, '2017-11-11'),
(264, 235, 2, 0, '2017-11-11'),
(265, 236, 2, 0, '2017-11-11'),
(266, 236, 7, 0, '0000-00-00'),
(267, 237, 2, 0, '2017-11-11'),
(268, 238, 2, 0, '2017-11-11'),
(269, 238, 7, 0, '0000-00-00'),
(270, 239, 2, 0, '2017-11-11'),
(271, 240, 2, 0, '2017-11-11'),
(272, 240, 7, 0, '0000-00-00'),
(273, 241, 2, 0, '2017-11-11'),
(274, 242, 2, 0, '2017-11-11'),
(275, 243, 2, 0, '2017-11-11'),
(276, 244, 2, 0, '2017-11-11'),
(277, 245, 7, 1, '2017-11-12'),
(278, 246, 7, 1, '2017-11-12'),
(279, 247, 12, 1, '2017-11-12'),
(280, 248, 12, 0, '2017-11-12'),
(281, 249, 12, 0, '2017-11-13'),
(282, 250, 12, 0, '2017-11-13'),
(283, 251, 16, 1, '2017-11-13'),
(284, 252, 2, 0, '2017-11-13'),
(285, 253, 2, 0, '2017-11-13'),
(286, 254, 2, 0, '2017-11-13'),
(287, 255, 2, 0, '2017-11-13'),
(288, 256, 2, 0, '2017-11-13'),
(289, 257, 2, 0, '2017-11-13'),
(290, 258, 2, 0, '2017-11-13'),
(291, 258, 12, 0, '0000-00-00'),
(292, 259, 2, 0, '2017-11-13');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 7, 2),
(2, 24, 2),
(3, 24, 2),
(4, 25, 2),
(5, 25, 2),
(6, 51, 2),
(7, 62, 4),
(8, 64, 5),
(9, 81, 7),
(10, 101, 1),
(11, 101, 1),
(12, 101, 1),
(13, 101, 1),
(14, 101, 1),
(15, 101, 1),
(16, 101, 1),
(17, 101, 1),
(18, 101, 1),
(19, 102, 1),
(20, 102, 1),
(21, 160, 7),
(22, 171, 7),
(23, 222, 7),
(24, 222, 7),
(25, 223, 7),
(26, 223, 7),
(27, 224, 16),
(28, 224, 16),
(29, 230, 7),
(30, 230, 7),
(31, 232, 7),
(32, 233, 7),
(33, 233, 7),
(34, 236, 7),
(35, 236, 7),
(36, 238, 7),
(37, 238, 7),
(38, 240, 7),
(39, 240, 7),
(40, 249, 12),
(41, 250, 12),
(42, 258, 12),
(43, 258, 12);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 1, '', '28.41221878760878', '77.04323463141918', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 26', '08:05:34', '08:06:17 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41221878760878,77.04323463141918&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(2, 1, '', '28.412214069350203', '77.04323999583721', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 26', '08:13:03', '08:13:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412214069350203,77.04323999583721&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(3, 1, '', '28.412213774459047', '77.04323932528496', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Sep 26', '08:31:20', '08:31:28 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412213774459047,77.04323932528496&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-09-26'),
(4, 2, '', '52.35389323878078', '5.154079869389535', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Monday, Oct 9', '21:51:07', '09:54:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35389323878078,5.154079869389535&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-09'),
(5, 2, '', '52.35403371818782', '5.153910554945468', 'Venusstraat 4, 1363 Almere, Netherlands', '52.090737399999995', '5.121420099999999', 'Utrecht', 'Tuesday, Oct 10', '06:18:39', '07:20:49 PM', '', '11/10/2017', '07:18', 2, 3, 2, 1, 17, 0, 12, 1, 0, 1, 1, '2017-10-10'),
(6, 2, '', '52.35403371818782', '5.153910554945468', 'Venusstraat 4, 1363 Almere, Netherlands', '52.090737399999995', '5.121420099999999', 'Utrecht', 'Tuesday, Oct 10', '06:19:51', '08:00:09 AM', '', '10/10/2017', '07:30', 0, 3, 2, 1, 17, 0, 12, 1, 0, 1, 1, '2017-10-10'),
(7, 2, '', '52.35403371818782', '5.153910554945468', 'Venusstraat 4, 1363 Almere, Netherlands', '52.090737399999995', '5.121420099999999', 'Utrecht', 'Tuesday, Oct 10', '06:22:03', '06:26:08 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35403371818782,5.153910554945468&markers=color:red|label:D|52.090737399999995,5.121420099999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(8, 2, '', '52.3540767', '5.1545814000000005', 'Venusstraat', '52.35523187061201', '5.165105760097504', 'Spoorbaanpad, 1362 Almere, Netherlands', 'Tuesday, Oct 10', '07:22:21', '07:45:53 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3540767,5.1545814000000005&markers=color:red|label:D|52.35523187061201,5.165105760097504&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(9, 2, '', '52.3540767', '5.1545814000000005', 'Venusstraat', '52.35523187061201', '5.165105760097504', 'Spoorbaanpad, 1362 Almere, Netherlands', 'Tuesday, Oct 10', '07:35:15', '07:35:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3540767,5.1545814000000005&markers=color:red|label:D|52.35523187061201,5.165105760097504&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-10'),
(10, 2, '', '52.35401426406916', '5.153880715370178', 'Venusstraat 4, 1363 Almere, Netherlands', '52.346205999999995', '5.1547472', 'Europalaan', 'Tuesday, Oct 10', '08:34:08', '08:39:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35401426406916,5.153880715370178&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(11, 2, '', '52.354027370002676', '5.153818018734456', 'Venusstraat 4, 1363 Almere, Netherlands', '52.346205999999995', '5.1547472', 'Europalaan', 'Tuesday, Oct 10', '08:43:17', '08:44:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.354027370002676,5.153818018734456&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(12, 2, '', '52.34690945695287', '5.151435881853104', 'Duitslandstraat 1, 1363 BG Almere, Netherlands', '52.3540767', '5.1545814000000005', 'Venusstraat', 'Tuesday, Oct 10', '09:01:30', '09:04:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.34690945695287,5.151435881853104&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 3, 1, 1, 1, '2017-10-10'),
(13, 2, '', '52.353194521160766', '5.160912461578846', 'Spoorbaanpad, 1362 Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Tuesday, Oct 10', '11:51:01', '11:51:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353194521160766,5.160912461578846&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-10'),
(14, 2, '', '52.353194521160766', '5.160912461578846', 'Spoorbaanpad, 1362 Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Tuesday, Oct 10', '11:51:20', '11:51:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353194521160766,5.160912461578846&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-10'),
(15, 2, '', '52.353194521160766', '5.160912461578846', 'Spoorbaanpad, 1362 Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Tuesday, Oct 10', '11:57:37', '11:58:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353194521160766,5.160912461578846&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(16, 2, '', '52.353194521160766', '5.160912461578846', 'Spoorbaanpad, 1362 Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Tuesday, Oct 10', '16:17:29', '04:19:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353194521160766,5.160912461578846&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(17, 2, '', '52.35143272895615', '5.176220163702965', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Tuesday, Oct 10', '16:37:02', '06:01:08 PM', '', '12/10/2017', '20:30', 0, 3, 2, 1, 2, 0, 3, 1, 0, 1, 1, '2017-10-10'),
(18, 2, '', '52.35403699467011', '5.153833106160164', 'Venusstraat 4, 1363 Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Tuesday, Oct 10', '17:54:15', '05:57:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35403699467011,5.153833106160164&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(19, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '35.7594651', '-5.8339542999999985', 'Tanger', 'Tuesday, Oct 10', '18:05:05', '06:05:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|35.7594651,-5.8339542999999985&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(20, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '50.8503396', '4.3517103', 'Brussel', 'Tuesday, Oct 10', '18:08:47', '06:10:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|50.8503396,4.3517103&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(21, 2, '', '52.35401303538768', '5.153906866908073', 'Venusstraat 4, 1363 Almere, Netherlands', '52.518536999999995', '5.471422', 'Lelystad', 'Tuesday, Oct 10', '18:47:54', '06:49:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35401303538768,5.153906866908073&markers=color:red|label:D|52.518536999999995,5.471422&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(22, 2, '', '52.353901430012094', '5.15404399484396', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3079989', '4.971545100000001', 'Amsterdam-Zuidoost', 'Tuesday, Oct 10', '19:50:05', '07:50:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353901430012094,5.15404399484396&markers=color:red|label:D|52.3079989,4.971545100000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-10'),
(23, 2, '', '52.35397085063665', '5.153996720910072', 'Venusstraat 4, 1363 Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Oct 11', '16:29:46', '04:36:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35397085063665,5.153996720910072&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(24, 2, '', '52.35397085063665', '5.153996720910072', 'Venusstraat 4, 1363 Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Oct 11', '16:43:41', '06:34:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35397085063665,5.153996720910072&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 17, 0, 12, 1, 0, 1, 1, '2017-10-11'),
(25, 2, '', '52.35397085063665', '5.153996720910072', 'Venusstraat 4, 1363 Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Oct 11', '16:44:16', '04:44:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35397085063665,5.153996720910072&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-11'),
(26, 2, '', '52.35397085063665', '5.153996720910072', 'Venusstraat 4, 1363 Almere, Netherlands', '51.4498405', '5.375346200000001', 'Eindhoven Airport', 'Wednesday, Oct 11', '16:46:10', '04:49:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35397085063665,5.153996720910072&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(27, 2, '', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Oct 11', '16:54:21', '05:02:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|50.851368199999996,5.690972499999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(28, 2, '', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '50.851368199999996', '5.690972499999999', 'Maastricht', 'Wednesday, Oct 11', '17:03:55', '05:06:02 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|50.851368199999996,5.690972499999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(29, 2, '', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '50.851368199999996', '5.690972499999999', 'Maastricht', 'Wednesday, Oct 11', '17:36:19', '05:36:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|50.851368199999996,5.690972499999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 9, 0, 8, 4, 0, 1, 1, '2017-10-11'),
(30, 2, '', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '50.851368199999996', '5.690972499999999', 'Maastricht', 'Wednesday, Oct 11', '17:41:15', '05:42:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|50.851368199999996,5.690972499999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(31, 2, '', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '53.123257900000006', '6.581490199999998', 'Groningen Airport Eelde', 'Wednesday, Oct 11', '17:45:16', '05:46:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|53.123257900000006,6.581490199999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-10-11'),
(32, 2, '', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '53.123257900000006', '6.581490199999998', 'Groningen Airport Eelde', 'Wednesday, Oct 11', '17:46:34', '05:47:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|53.123257900000006,6.581490199999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(33, 2, '', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '53.123257900000006', '6.581490199999998', 'Groningen Airport Eelde', 'Wednesday, Oct 11', '17:48:09', '05:48:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|53.123257900000006,6.581490199999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(34, 2, '', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '53.123257900000006', '6.581490199999998', 'Groningen Airport Eelde', 'Wednesday, Oct 11', '17:49:07', '05:49:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|53.123257900000006,6.581490199999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 2, 0, 8, 1, 0, 1, 1, '2017-10-11'),
(35, 2, 'FREERIDE', '52.353994195600926', '5.153944082558156', 'Venusstraat 4, 1363 Almere, Netherlands', '52.35347486962181', '5.153805948793889', 'Jupitersingel, 1363 Almere, Netherlands', 'Wednesday, Oct 11', '18:58:36', '06:59:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353994195600926,5.153944082558156&markers=color:red|label:D|52.35347486962181,5.153805948793889&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-11'),
(36, 2, 'FREERIDE', '52.35386784595401', '5.1541271433234215', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.371148700000006', '4.533355', 'Zandvoort', 'Wednesday, Oct 11', '19:03:44', '07:05:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35386784595401,5.1541271433234215&markers=color:red|label:D|52.371148700000006,4.533355&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-11'),
(37, 2, '', '52.35143231937175', '5.176226869225502', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Thursday, Oct 12', '15:53:02', '03:54:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35143231937175,5.176226869225502&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-12'),
(38, 2, '', '52.351434776878136', '5.176174901425839', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Friday, Oct 13', '13:35:55', '01:35:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351434776878136,5.176174901425839&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-13'),
(39, 2, '', '52.351434776878136', '5.176174901425839', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Friday, Oct 13', '13:47:08', '01:47:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351434776878136,5.176174901425839&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-13'),
(40, 2, '', '52.351434776878136', '5.176174901425839', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Friday, Oct 13', '13:51:13', '05:52:51 PM', '', '14/10/2017', '17:30', 2, 3, 2, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-13'),
(41, 3, '', '28.4121211785923', '77.0432701706886', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4228449520235', '77.0648089796305', 'Block J, Mayfield Garden, Sector 51\nGurugram, Haryana 122003', 'Saturday, Oct 14', '09:06:09', '06:09:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121211785923,77.0432701706886&markers=color:red|label:D|28.4228449520235,77.0648089796305&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 17, 0, 12, 1, 0, 1, 1, '2017-10-14'),
(42, 4, '', '28.4122214416291', '77.043376788497', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4352419752103', '77.0728934928775', 'Wazirabad Village Road, Block D, Indira Colony 2, Sector 52\nGurugram, Haryana 122003', 'Saturday, Oct 14', '10:23:42', '10:24:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122214416291,77.043376788497&markers=color:red|label:D|28.4352419752103,77.0728934928775&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(43, 4, '', '28.412151847296', '77.0432845875621', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4338633550588', '77.0817380771041', 'Wazirabad Mata Chowk Road, Wazirabad, Sector 52\nGurugram, Haryana 122413', 'Saturday, Oct 14', '10:31:00', '10:31:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412151847296,77.0432845875621&markers=color:red|label:D|28.4338633550588,77.0817380771041&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(44, 4, '', '28.4122144305137', '77.0433421305194', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.438888922661', '77.0857560262084', 'Sarswati Kunj II, Wazirabad, Sector 52\nGurugram, Haryana 122022', 'Saturday, Oct 14', '10:32:21', '10:32:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122144305137,77.0433421305194&markers=color:red|label:D|28.438888922661,77.0857560262084&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(45, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Saturday, Oct 14', '18:04:52', '06:06:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(46, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Saturday, Oct 14', '18:09:47', '06:09:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-14'),
(47, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.0894444', '5.1099868', 'Utrecht Centraal', 'Saturday, Oct 14', '18:29:58', '06:31:05 PM', '', '15/10/2017', '19:30', 0, 3, 2, 1, 2, 0, 3, 1, 0, 1, 1, '2017-10-14'),
(48, 5, 'FREERIDE', '52.3540767', '5.154581399999984', 'Venusstraat, Almere, Nederland', '52.309456', '4.762283600000046', 'Schiphol Airport, Schiphol, Nederland', 'Saturday, Oct 14', '18:41:00', '06:41:56 PM', '', '2017-10-14', '19 : 45', 2, 3, 2, 1, 7, 1, 0, 1, 0, 2, 1, '2017-10-14'),
(49, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.52306', '5.4379417000000005', 'Batavia Stad Fashion Outlet', 'Saturday, Oct 14', '19:00:50', '07:01:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.52306,5.4379417000000005&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-10-14'),
(50, 2, '', '52.353878904122304', '5.154116079211234', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3702157', '4.895167900000001', 'Amsterdam', 'Sunday, Oct 15', '08:26:24', '08:31:12 AM', '', '15/10/2017', '09:45', 2, 3, 2, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-15'),
(51, 2, '', '52.353878904122304', '5.154116079211234', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3702157', '4.895167900000001', 'Amsterdam', 'Sunday, Oct 15', '08:33:45', '08:33:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353878904122304,5.154116079211234&markers=color:red|label:D|52.3702157,4.895167900000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-15'),
(52, 2, '', '52.353878904122304', '5.154116079211234', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3702157', '4.895167900000001', 'Amsterdam', 'Sunday, Oct 15', '08:37:01', '08:37:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353878904122304,5.154116079211234&markers=color:red|label:D|52.3702157,4.895167900000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-15'),
(53, 2, 'TAXIUFREE', '52.35394402439056', '5.154002085328102', 'Venusstraat 4, 1363 Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Oct 15', '10:52:37', '10:53:25 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35394402439056,5.154002085328102&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-15'),
(54, 2, 'TAXIUFREE', '52.35399112389582', '5.153936706483364', 'Venusstraat 4, 1363 Almere, Netherlands', '52.5080182', '5.4728518', 'Lelystad Centrum', 'Sunday, Oct 15', '10:55:02', '10:55:46 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35399112389582,5.153936706483364&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-15'),
(55, 6, 'FREERIDE', '35.72659549999999', '-5.91506830000003', 'Airport Tangier Ibn Batouta, Tanger, Marokko', '', '', 'TANGER VILLE, Rue Al Kindy, Tanger, Marokko', 'Sunday, Oct 15', '11:27:14', '11:29:00 AM', '', '2017-10-15', '12 : 25', 2, 3, 2, 1, 9, 1, 8, 1, 0, 2, 1, '2017-10-15'),
(56, 7, 'TAXIUFREE', '', '', 'airport tang', '', '', 'centre ville tange', 'Sunday, Oct 15', '14:49:45', '03:13:30 PM', '', '2017-10-16', '15 : 46', 2, 3, 2, 1, 7, 1, 0, 1, 0, 2, 1, '2017-10-15'),
(57, 2, 'TAXIUAPP', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Oct 15', '16:21:21', '04:22:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-15'),
(58, 2, 'TAXIUAPP', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.36010564687326', '5.2176277711987495', 'Lesplaats, 1324 Almere, Netherlands', 'Sunday, Oct 15', '16:40:44', '04:41:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.36010564687326,5.2176277711987495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-15'),
(59, 2, 'TAXIUAPP', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.36010564687326', '5.2176277711987495', 'Lesplaats, 1324 Almere, Netherlands', 'Sunday, Oct 15', '16:51:26', '04:52:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.36010564687326,5.2176277711987495&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-15'),
(60, 1, '', '28.41214565457769', '77.04321853816509', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 16', '07:54:05', '07:55:14 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41214565457769,77.04321853816509&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(61, 4, '', '28.412234348875', '77.043457228196', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4632114649548', '77.0955350250006', 'Sunset Boulevard, Block F, DLF Phase 1, Sector 26A\nGurugram, Haryana 122002', 'Monday, Oct 16', '08:06:39', '08:07:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412234348875,77.043457228196&markers=color:red|label:D|28.4632114649548,77.0955350250006&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-16'),
(62, 4, '', '28.4122175457124', '77.0432546300064', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4469916538081', '77.0797109976411', '1707, Saint Thomas Marg, Sector 43\nGurugram, Haryana 122022', 'Monday, Oct 16', '08:26:37', '08:26:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4122175457124,77.0432546300064&markers=color:red|label:D|28.4469916538081,77.0797109976411&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-16'),
(63, 1, '', '28.412147423925834', '77.04323060810566', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.424843799675212', '77.06950955092907', '601, Orchid Island, Sector 51, Gurugram, Haryana 122003, India', 'Monday, Oct 16', '09:02:39', '09:04:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412147423925834,77.04323060810566&markers=color:red|label:D|28.424843799675212,77.06950955092907&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 2, 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-10-16'),
(64, 8, '', '28.412144180120908', '77.04322658479214', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.449781844863193', '77.0867782831192', '129, St Thomas Marg, Sector 52A, Gurugram, Haryana 122003, India', 'Monday, Oct 16', '10:09:41', '10:13:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412144180120908,77.04322658479214&markers=color:red|label:D|28.449781844863193,77.0867782831192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(65, 8, '', '28.412144180120908', '77.04322658479214', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.449781844863193', '77.0867782831192', '129, St Thomas Marg, Sector 52A, Gurugram, Haryana 122003, India', 'Monday, Oct 16', '10:14:20', '10:14:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412144180120908,77.04322658479214&markers=color:red|label:D|28.449781844863193,77.0867782831192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 2, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-16'),
(66, 8, '', '28.412144180120908', '77.04322658479214', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.449781844863193', '77.0867782831192', '129, St Thomas Marg, Sector 52A, Gurugram, Haryana 122003, India', 'Monday, Oct 16', '10:15:01', '10:17:59 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412144180120908,77.04322658479214&markers=color:red|label:D|28.449781844863193,77.0867782831192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(67, 8, '', '28.412144180120908', '77.04322658479214', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.449781844863193', '77.0867782831192', '129, St Thomas Marg, Sector 52A, Gurugram, Haryana 122003, India', 'Monday, Oct 16', '10:36:59', '10:39:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412144180120908,77.04322658479214&markers=color:red|label:D|28.449781844863193,77.0867782831192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(68, 8, '', '28.412144180120908', '77.04322658479214', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.45715891452432', '77.05361545085907', '109, Uniworld City, Sector 30, Gurugram, Haryana 122022, India', 'Monday, Oct 16', '10:52:13', '10:52:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412144180120908,77.04322658479214&markers=color:red|label:D|28.45715891452432,77.05361545085907&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 3, 2, 1, 1, '2017-10-16'),
(69, 8, '', '28.412144180120908', '77.04322658479214', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.45715891452432', '77.05361545085907', '109, Uniworld City, Sector 30, Gurugram, Haryana 122022, India', 'Monday, Oct 16', '10:53:56', '10:55:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412144180120908,77.04322658479214&markers=color:red|label:D|28.45715891452432,77.05361545085907&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 5, 2, 1, 1, 7, 1, 0, 3, 2, 1, 1, '2017-10-16'),
(70, 2, 'TAXIUAPP', '52.353989280872646', '5.153946429491043', 'Venusstraat 4, 1363 Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Monday, Oct 16', '17:59:02', '06:01:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353989280872646,5.153946429491043&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-16'),
(71, 10, 'TAXIUAPP', '52.351407460409', '5.17615905245112', 'Katernstraat 33\n1321 NC Almere', '52.3699594672719', '4.87463191151619', 'Nassaukade 177II\n1053 LM Amsterdam', 'Tuesday, Oct 17', '10:24:13', '10:24:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351407460409,5.17615905245112&markers=color:red|label:D|52.3699594672719,4.87463191151619&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(72, 10, '', '52.351407460409', '5.17615905245112', 'Katernstraat 33\n1321 NC Almere', '52.3699594672719', '4.87463191151619', 'Nassaukade 177II\n1053 LM Amsterdam', 'Tuesday, Oct 17', '10:26:09', '06:09:45 PM', '', '2017-10-17', '12:26:02', 7, 4, 2, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(73, 10, 'TAXIUAPP', '52.351407460409', '5.17615905245112', 'Katernstraat 33\n1321 NC Almere', '52.3699594672719', '4.87463191151619', 'Nassaukade 177II\n1053 LM Amsterdam', 'Tuesday, Oct 17', '10:26:19', '10:26:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351407460409,5.17615905245112&markers=color:red|label:D|52.3699594672719,4.87463191151619&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(74, 2, '', '52.35386784595401', '5.1541271433234215', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3079989', '4.971545100000001', 'Amsterdam-Zuidoost', 'Tuesday, Oct 17', '16:44:27', '04:46:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35386784595401,5.1541271433234215&markers=color:red|label:D|52.3079989,4.971545100000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(75, 2, '', '52.35386784595401', '5.1541271433234215', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.29320537835108', '4.971181713044643', 'Tefelenstraat 53, 1107 SJ Amsterdam-Zuidoost, Netherlands', 'Tuesday, Oct 17', '16:50:26', '04:50:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35386784595401,5.1541271433234215&markers=color:red|label:D|52.29320537835108,4.971181713044643&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(76, 10, 'TAXIUAPP', '52.3538948770271', '5.15422772616148', 'Venusstraat 4\n1363 Almere', '52.3791283', '4.900272', 'Amsterdam Centraal', 'Tuesday, Oct 17', '16:50:26', '04:52:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3538948770271,5.15422772616148&markers=color:red|label:D|52.3791283,4.900272&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(77, 2, '', '52.35386784595401', '5.1541271433234215', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.29320537835108', '4.971181713044643', 'Tefelenstraat 53, 1107 SJ Amsterdam-Zuidoost, Netherlands', 'Tuesday, Oct 17', '16:53:55', '04:54:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35386784595401,5.1541271433234215&markers=color:red|label:D|52.29320537835108,4.971181713044643&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(78, 10, 'TAXIUAPP', '52.3538811567118', '5.15435513108969', 'Venusstraat 33A\n1363 VN Almere', '52.373866', '5.2111245', 'Almere Stad', 'Tuesday, Oct 17', '16:57:03', '04:57:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3538811567118,5.15435513108969&markers=color:red|label:D|52.373866,5.2111245&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(79, 10, 'TAXIUAPP', '52.3538750132856', '5.15420023351908', 'Venusstraat 4\n1363 Almere', '52.5080182', '5.4728518', 'Lelystad Centrum', 'Tuesday, Oct 17', '17:00:03', '05:02:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3538750132856,5.15420023351908&markers=color:red|label:D|52.5080182,5.4728518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(80, 10, 'TAXIUAPP', '52.3539176076895', '5.15437692403793', 'Venusstraat 33A\n1363 VN Almere', '52.3791283', '4.900272', 'Amsterdam Centraal', 'Tuesday, Oct 17', '17:26:57', '05:28:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539176076895,5.15437692403793&markers=color:red|label:D|52.3791283,4.900272&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(81, 10, 'TAXIUAPP', '52.3539416401243', '5.15413130985773', 'Venusstraat 4\n1363 Almere', '52.3691448', '5.2235055', 'Flevoziekenhuis', 'Tuesday, Oct 17', '19:57:03', '07:57:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539416401243,5.15413130985773&markers=color:red|label:D|52.3691448,5.2235055&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(82, 10, 'TAXIUAPP', '52.3539416401243', '5.15413130985773', 'Venusstraat 4\n1363 Almere', '52.3626429', '4.8821979', 'Holland Casino Amsterdam Centrum', 'Tuesday, Oct 17', '20:01:20', '08:08:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539416401243,5.15413130985773&markers=color:red|label:D|52.3626429,4.8821979&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(83, 10, 'TAXIUAPP', '52.3540767', '5.1545814', 'Venusstraat', '52.3510796', '5.1768235', 'Katernstraat', 'Tuesday, Oct 17', '20:17:41', '08:25:33 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3540767,5.1545814&markers=color:red|label:D|52.3510796,5.1768235&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-17'),
(84, 10, '', '52.3539852881005', '5.15437129272257', 'Venusstraat 33\n1363 Almere', '52.3702157', '4.8951679', 'Amsterdam', 'Tuesday, Oct 17', '20:34:40', '08:40:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539852881005,5.15437129272257&markers=color:red|label:D|52.3702157,4.8951679&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-17'),
(85, 10, '', '52.3540767', '5.1545814', 'Venusstraat', '52.3702157', '4.8951679', 'Amsterdam', 'Tuesday, Oct 17', '20:49:25', '08:49:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3540767,5.1545814&markers=color:red|label:D|52.3702157,4.8951679&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-17'),
(86, 10, '', '52.3540767', '5.1545814', 'Venusstraat', '36.721261', '-4.4212655', 'MÃ¡laga', 'Wednesday, Oct 18', '14:52:35', '02:52:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3540767,5.1545814&markers=color:red|label:D|36.721261,-4.4212655&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-18'),
(87, 2, '', '52.35689235664692', '5.155521892011166', 'Castorstraat, 1363 Almere, Netherlands', '52.0704978', '4.300699899999999', 'Den Haag', 'Wednesday, Oct 18', '18:17:50', '06:21:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35689235664692,5.155521892011166&markers=color:red|label:D|52.0704978,4.300699899999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-18'),
(88, 10, 'TAXIUAPP', '52.3539374630002', '5.15416883386133', 'Venusstraat 4\n1363 Almere', '52.3510796', '5.1768235', 'Katernstraat', 'Thursday, Oct 19', '18:48:47', '07:00:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539374630002,5.15416883386133&markers=color:red|label:D|52.3510796,5.1768235&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-19'),
(89, 10, 'TAXIUAPP', '52.3539786322928', '5.15409026294947', 'Venusstraat 4\n1363 Almere', '52.2770013', '5.1855548', '1402 GP', 'Friday, Oct 20', '18:05:06', '06:05:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539786322928,5.15409026294947&markers=color:red|label:D|52.2770013,5.1855548&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-20'),
(90, 10, 'TAXIUAPP', '52.3539786322928', '5.15409026294947', 'Venusstraat 4\n1363 Almere', '52.2770013', '5.1855548', '1402 GP', 'Friday, Oct 20', '18:05:50', '06:31:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539786322928,5.15409026294947&markers=color:red|label:D|52.2770013,5.1855548&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-20'),
(91, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.28939079999999', '5.172983499999999', 'Amersfoortsestraatweg 43', 'Friday, Oct 20', '18:11:32', '06:31:41 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.28939079999999,5.172983499999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-20'),
(92, 11, '', '28.4121464845058', '77.0432839170816', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 23', '11:34:23', '11:34:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121464845058,77.0432839170816&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(93, 11, '', '28.4121464845058', '77.0432839170816', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 23', '11:34:49', '11:35:30 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121464845058,77.0432839170816&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(94, 11, '', '28.412136923741', '77.043255496957', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4532596788264', '77.0899660885334', '1143, Block C, Sushant Lok Phase I, Sector 43\nGurugram, Haryana 122022', 'Monday, Oct 23', '11:39:39', '11:40:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412136923741,77.043255496957&markers=color:red|label:D|28.4532596788264,77.0899660885334&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(95, 11, '', '28.4121613608792', '77.0432187251581', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4388175769561', '77.0737779513001', '14, Road Number D 16, Block D, Ardee City, Sector 52\nGurugram, Haryana 122003', 'Monday, Oct 23', '12:02:36', '12:03:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121613608792,77.0432187251581&markers=color:red|label:D|28.4388175769561,77.0737779513001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(96, 11, '', '28.412113511415', '77.043220885098', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4373782702696', '77.0735063776374', '14, Road Number D 15, Block D, Indira Colony 2, Sector 52\nGurugram, Haryana 122003', 'Monday, Oct 23', '13:25:30', '01:25:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412113511415,77.043220885098&markers=color:red|label:D|28.4373782702696,77.0735063776374&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-23'),
(97, 11, '', '28.412113511415', '77.043220885098', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4373782702696', '77.0735063776374', '14, Road Number D 15, Block D, Indira Colony 2, Sector 52\nGurugram, Haryana 122003', 'Monday, Oct 23', '13:27:15', '01:27:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412113511415,77.043220885098&markers=color:red|label:D|28.4373782702696,77.0735063776374&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-23'),
(98, 12, 'BABA', '28.4120986706545', '77.0432950436454', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.5245787', '77.206615', 'Saket', 'Tuesday, Oct 24', '14:41:00', '02:42:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120986706545,77.0432950436454&markers=color:red|label:D|28.5245787,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(99, 12, 'BABA', '28.412090416659', '77.0432860915216', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.5245787', '77.206615', 'Saket', 'Tuesday, Oct 24', '14:46:03', '02:46:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412090416659,77.0432860915216&markers=color:red|label:D|28.5245787,77.206615&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 9, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(100, 13, 'APPORIO', '28.41229221548028', '77.0434596017003', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 24', '15:41:05', '03:41:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41229221548028,77.0434596017003&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-24'),
(101, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:03:54', '11:03:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(102, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:05:55', '11:05:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(103, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:06:33', '11:06:33 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(104, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:06:45', '11:06:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(105, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:07:37', '11:07:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(106, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:09:06', '11:09:06 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(107, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:10:49', '11:10:49 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(108, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:11:04', '11:11:04 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(109, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:20:15', '11:20:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-25'),
(110, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:21:16', '11:24:50 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 8, 4, 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-10-25'),
(111, 1, '', '28.4138566', '77.0421785', 'Spaze i-Tech Park, Sector 49, Gurugram, Haryana, India', '28.45908', '77.070518', 'Huda City Metro Station, Sector 29, Gurugram, Haryana, India', 'Wednesday, Oct 25', '11:59:48', '12:00:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|28.4138566,77.0421785&markers=color:red|label:D|28.45908,77.070518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-10-25'),
(112, 8, '', '28.41230076731744', '77.04343445599079', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Wednesday, Oct 25', '14:14:36', '02:14:36 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41230076731744,77.04343445599079&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(113, 10, '', '52.3514028292845', '5.17620172351599', 'Katernstraat 33\n1321 NC Almere', '0.0', '0.0', 'No drop off point', 'Wednesday, Oct 25', '15:56:07', '03:56:07 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3514028292845,5.17620172351599&markers=color:red|label:D|0.0,0.0&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-25'),
(114, 8, '', '28.41232583304304', '77.04341165721416', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 26', '07:14:02', '07:14:02 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232583304304,77.04341165721416&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-26'),
(115, 8, '', '28.41232583304304', '77.04341165721416', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 26', '07:14:35', '07:14:35 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232583304304,77.04341165721416&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-26'),
(116, 8, '', '28.412327602388157', '77.04340998083353', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 26', '07:18:06', '07:18:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412327602388157,77.04340998083353&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-26'),
(117, 8, '', '28.412288676788858', '77.04340964555739', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Thursday, Oct 26', '11:06:33', '11:06:56 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412288676788858,77.04340964555739&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-26'),
(118, 8, '', '28.412289266570767', '77.04341232776642', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '', '', 'Set your drop point', 'Thursday, Oct 26', '11:08:26', '08:34:40 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412289266570767,77.04341232776642&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-10-26'),
(119, 10, '', '52.3539360379477', '5.15422202646732', 'Venusstraat 4\n1363 Almere', '52.3514028292845', '5.17620172351599', 'Katernstraat 33\n1321 NC Almere', 'Thursday, Oct 26', '12:33:19', '12:33:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539360379477,5.15422202646732&markers=color:red|label:D|52.3514028292845,5.17620172351599&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-26'),
(120, 10, '', '52.3539360379477', '5.15422202646732', 'Venusstraat 4\n1363 Almere', '52.3514028292845', '5.17620172351599', 'Katernstraat 33\n1321 NC Almere', 'Thursday, Oct 26', '12:33:51', '01:17:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539360379477,5.15422202646732&markers=color:red|label:D|52.3514028292845,5.17620172351599&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-26'),
(121, 5, 'TAXIUAPP', '', '', 'venusstraat ', '', '', 'amsterdam', 'Thursday, Oct 26', '18:49:10', '06:50:05 PM', '', '2017-10-27', '19 : 47', 7, 4, 2, 1, 17, 0, 12, 1, 0, 2, 1, '2017-10-26'),
(122, 10, 'TAXIUAPP ', '52.3539354236059', '5.15418078750372', 'Venusstraat 4\n1363 Almere', '52.3697554', '4.8887815', 'Hotel NH City Centre Amsterdam', 'Thursday, Oct 26', '20:10:22', '08:11:01 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539354236059,5.15418078750372&markers=color:red|label:D|52.3697554,4.8887815&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-26'),
(123, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.352125945125216', '5.139203332364559', 'Olivier van Noortstraat, 1363 Almere, Netherlands', 'Thursday, Oct 26', '20:19:43', '08:21:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.352125945125216,5.139203332364559&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-26'),
(124, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.352125945125216', '5.139203332364559', 'Olivier van Noortstraat, 1363 Almere, Netherlands', 'Thursday, Oct 26', '20:23:09', '08:25:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.352125945125216,5.139203332364559&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-26'),
(125, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.352125945125216', '5.139203332364559', 'Olivier van Noortstraat, 1363 Almere, Netherlands', 'Thursday, Oct 26', '20:23:59', '08:24:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|52.352125945125216,5.139203332364559&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-10-26'),
(126, 8, '', '28.412284253424396', '77.04343177378178', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 27', '08:37:07', '08:37:51 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412284253424396,77.04343177378178&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(127, 8, '', '28.412285432988263', '77.0434596017003', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 27', '08:57:39', '08:58:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412285432988263,77.0434596017003&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(128, 8, '', '28.412335859331613', '77.04340126365423', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 27', '09:00:59', '09:01:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412335859331613,77.04340126365423&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(129, 8, '', '28.412228224125233', '77.04344853758812', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 27', '09:08:16', '09:08:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412228224125233,77.04344853758812&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-27'),
(130, 8, '', '28.412228224125233', '77.04344853758812', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 27', '09:08:43', '09:08:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412228224125233,77.04344853758812&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-10-27'),
(131, 8, '', '28.412228224125233', '77.04344853758812', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 27', '09:09:11', '09:09:23 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412228224125233,77.04344853758812&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(132, 8, '', '28.412228224125233', '77.04344853758812', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 27', '09:10:50', '09:11:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412228224125233,77.04344853758812&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 11, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(133, 10, 'TAXIUAPP ', '52.351419417461', '5.17619166523218', 'Katernstraat 33\n1321 NC Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Friday, Oct 27', '10:02:34', '10:04:34 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351419417461,5.17619166523218&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(134, 11, '', '28.4120793040018', '77.0432520657778', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4455686722442', '77.0986142009497', 'BLC-143, Golf Course Road, DLF Phase 5, Sector 54\nGurugram, Haryana 122002', 'Friday, Oct 27', '10:54:38', '10:55:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120793040018,77.0432520657778&markers=color:red|label:D|28.4455686722442,77.0986142009497&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(135, 11, '', '28.4120518790849', '77.0432510599494', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4903325602215', '77.1641358733177', '10, Sultanpur Mandi Road, Chhatarpur Farms, Sainik Farm\nNew Delhi, Delhi 110030', 'Friday, Oct 27', '12:18:19', '12:40:03 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4120518790849,77.0432510599494&markers=color:red|label:D|28.4903325602215,77.1641358733177&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 13, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(136, 8, '', '28.412297228626297', '77.04342875629663', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 27', '13:17:06', '01:19:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412297228626297,77.04342875629663&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 14, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(137, 10, 'TAXIUAPP ', '52.3515461600103', '5.17644736373946', 'Katernstraat 12\n1321 NE Almere', '52.3540767', '5.1545814', 'Venusstraat', 'Friday, Oct 27', '16:06:35', '04:12:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3515461600103,5.17644736373946&markers=color:red|label:D|52.3540767,5.1545814&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(138, 10, 'TAXIUAPP ', '52.0083206518317', '5.08026348161611', 'Rietput\n3434 Nieuwegein', '52.3579821126838', '5.15566874295473', 'Nikestraat\n1363 Almere', 'Friday, Oct 27', '18:57:03', '06:58:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.0083206518317,5.08026348161611&markers=color:red|label:D|52.3579821126838,5.15566874295473&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-27'),
(139, 2, '', '52.353966140686225', '5.154028236865997', 'Venusstraat 4, 1363 Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Saturday, Oct 28', '14:45:23', '02:48:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353966140686225,5.154028236865997&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(140, 2, '', '52.353952829954046', '5.154018849134445', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '14:51:39', '02:51:39 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353952829954046,5.154018849134445&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(141, 2, 'TAXIUAPP', '52.353952829954046', '5.154018849134445', 'Venusstraat 4, 1363 Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Saturday, Oct 28', '14:53:59', '02:56:27 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353952829954046,5.154018849134445&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(142, 2, '', '52.35395692556438', '5.154033936560154', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '15:13:29', '03:13:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35395692556438,5.154033936560154&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(143, 2, '', '52.35395692556438', '5.154033936560154', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '15:13:43', '03:13:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35395692556438,5.154033936560154&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(144, 2, '', '52.35395692556438', '5.154033936560154', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '16:46:42', '04:46:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35395692556438,5.154033936560154&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(145, 2, '', '52.35395692556438', '5.154033936560154', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '16:46:42', '04:46:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35395692556438,5.154033936560154&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(146, 2, '', '52.35395692556438', '5.154033936560154', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '16:46:42', '04:46:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35395692556438,5.154033936560154&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(147, 2, '', '52.35395692556438', '5.154033936560154', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '16:46:42', '04:46:42 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35395692556438,5.154033936560154&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(148, 2, '', '52.35395692556438', '5.154033936560154', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '16:46:43', '04:46:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35395692556438,5.154033936560154&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(149, 2, 'TAXIUAPP', '52.35395692556438', '5.154033936560154', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '16:48:09', '04:48:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35395692556438,5.154033936560154&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(150, 2, 'TAXIUAPP', '52.35395692556438', '5.154033936560154', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Oct 28', '16:48:49', '04:48:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35395692556438,5.154033936560154&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-28'),
(151, 5, 'TAXIUAPP', '52.3717084', '4.914879700000029', 'Het Scheepvaartmuseum, Kattenburgerplein, Amsterdam, Nederland', '52.309456', '4.762283600000046', 'Schiphol Airport, Schiphol, Nederland', 'Saturday, Oct 28', '17:14:08', '05:14:59 PM', '', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 2, 1, '2017-10-28'),
(152, 10, 'APPORIO', '52.3540767', '5.154581399999984', 'Venusstraat, Almere, Nederland', '52.309456', '4.762283600000046', 'Schiphol Airport, Schiphol, Nederland', 'Saturday, Oct 28', '17:16:28', '05:16:58 PM', '', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 2, 1, '2017-10-28'),
(153, 10, '', '52.353807025979', '5.15423476696014', 'Venusstraat 4\n1363 Almere', '52.3510796', '5.1768235', 'Katernstraat', 'Saturday, Oct 28', '19:04:51', '07:08:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353807025979,5.15423476696014&markers=color:red|label:D|52.3510796,5.1768235&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(154, 10, 'TAXIUAPP', '52.353807025979', '5.15423476696014', 'Venusstraat 4\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Saturday, Oct 28', '19:35:03', '07:37:22 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353807025979,5.15423476696014&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(155, 10, '', '52.3540327869001', '5.15415568059037', 'Venusstraat 4\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Sunday, Oct 29', '17:12:43', '05:12:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3540327869001,5.15415568059037&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-29'),
(156, 10, '', '52.3540327869001', '5.15415568059037', 'Venusstraat 4\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Sunday, Oct 29', '17:13:56', '05:14:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3540327869001,5.15415568059037&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-29'),
(157, 10, '', '52.3539746165072', '5.15406531185993', 'Venusstraat 4\n1363 Almere', '52.3667925', '5.1902615', 'Albert Heijn', 'Monday, Oct 30', '18:20:01', '06:30:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539746165072,5.15406531185993&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(158, 10, '', '52.3539868753951', '5.1541204175613', 'Venusstraat 4\n1363 Almere', '52.3510796', '5.1768235', 'Katernstraat', 'Monday, Oct 30', '18:59:35', '07:04:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539868753951,5.1541204175613&markers=color:red|label:D|52.3510796,5.1768235&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(159, 2, '', '52.356886827940116', '5.155525244772434', 'Castorstraat, 1363 Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 1', '17:31:05', '05:32:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.356886827940116,5.155525244772434&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-01'),
(160, 10, '', '52.353910439874', '5.15413833738987', 'Venusstraat 4\n1363 Almere', '52.0883819', '5.1074518', 'Beatrix Theater', 'Thursday, Nov 2', '18:03:18', '06:03:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353910439874,5.15413833738987&markers=color:red|label:D|52.0883819,5.1074518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-02'),
(161, 10, '', '52.353910439874', '5.15413833738987', 'Venusstraat 4\n1363 Almere', '52.0883819', '5.1074518', 'Beatrix Theater', 'Thursday, Nov 2', '18:03:39', '06:07:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353910439874,5.15413833738987&markers=color:red|label:D|52.0883819,5.1074518&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(162, 2, 'TAXIUAPP', '52.35396839327128', '5.153976604342461', 'Venusstraat 4, 1363 Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Thursday, Nov 2', '18:46:54', '06:49:09 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35396839327128,5.153976604342461&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(163, 10, '', '52.3546449827123', '5.13646312057972', 'Adriaen Blockstraat\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Friday, Nov 3', '13:15:57', '01:15:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3546449827123,5.13646312057972&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-03'),
(164, 10, '', '52.3546449827123', '5.13646312057972', 'Adriaen Blockstraat\n1363 Almere', '52.3076865', '4.7674241', 'Schiphol', 'Friday, Nov 3', '13:17:06', '01:17:06 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3546449827123,5.13646312057972&markers=color:red|label:D|52.3076865,4.7674241&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-03'),
(165, 2, '', '52.35392559413568', '5.154056064784527', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Saturday, Nov 4', '06:36:21', '06:37:44 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35392559413568,5.154056064784527&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-04'),
(166, 2, '', '52.35392559413568', '5.154056064784527', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.37523136477857', '5.216244421899318', 'P.J. Oudweg 1, 1314 CH Almere, Netherlands', 'Saturday, Nov 4', '06:46:10', '06:46:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35392559413568,5.154056064784527&markers=color:red|label:D|52.37523136477857,5.216244421899318&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-04'),
(167, 2, '', '52.35392559413568', '5.154056064784527', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.37523136477857', '5.216244421899318', 'P.J. Oudweg 1, 1314 CH Almere, Netherlands', 'Saturday, Nov 4', '06:47:11', '06:47:51 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35392559413568,5.154056064784527&markers=color:red|label:D|52.37523136477857,5.216244421899318&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-11-04'),
(168, 2, '', '52.35392559413568', '5.154056064784527', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.37523136477857', '5.216244421899318', 'P.J. Oudweg 1, 1314 CH Almere, Netherlands', 'Saturday, Nov 4', '06:48:07', '06:49:00 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35392559413568,5.154056064784527&markers=color:red|label:D|52.37523136477857,5.216244421899318&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-04'),
(169, 2, '', '52.35392559413568', '5.154056064784527', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Saturday, Nov 4', '06:49:48', '06:53:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35392559413568,5.154056064784527&markers=color:red|label:D|52.37523136477857,5.216244421899318&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-04'),
(170, 10, 'APPORIO', '52.0082407411243', '5.08037217915935', 'Rietput\n3434 Nieuwegein', '52.309456', '4.7622836', 'Schiphol Airport', 'Saturday, Nov 4', '18:33:54', '06:35:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.0082407411243,5.08037217915935&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-04'),
(171, 10, '', '52.0082727112317', '5.08031759576149', 'Rietput\n3434 Nieuwegein', '52.0882434', '5.1076334', 'Jaarbeurs', 'Saturday, Nov 4', '18:41:35', '06:41:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.0082727112317,5.08031759576149&markers=color:red|label:D|52.0882434,5.1076334&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-04'),
(172, 10, '', '52.0082727112317', '5.08031759576149', 'Rietput\n3434 Nieuwegein', '52.0882434', '5.1076334', 'Jaarbeurs', 'Saturday, Nov 4', '18:43:28', '04:30:38 PM', '', '2017-11-04', '20:42:46', 12, 4, 2, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-04'),
(173, 2, '', '52.35390183957362', '5.154068134725094', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Nov 5', '09:06:16', '09:08:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390183957362,5.154068134725094&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(174, 2, '', '52.35390183957362', '5.154068134725094', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Nov 5', '09:08:50', '09:09:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390183957362,5.154068134725094&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(175, 2, '', '52.35390183957362', '5.154068134725094', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Sunday, Nov 5', '10:24:55', '10:24:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390183957362,5.154068134725094&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-05'),
(176, 10, '', '52.3539400364169', '5.15407871083611', 'Venusstraat 4\n1363 Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '13:02:25', '01:02:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539400364169,5.15407871083611&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-05'),
(177, 10, '', '52.3539400364169', '5.15407871083611', 'Venusstraat 4\n1363 Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '13:03:51', '01:03:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539400364169,5.15407871083611&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-05'),
(178, 10, '', '52.3539400364169', '5.15407871083611', 'Venusstraat 4\n1363 Almere', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '16:10:06', '04:10:48 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539400364169,5.15407871083611&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(179, 2, '', '52.353891395753514', '5.15407282859087', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '16:23:02', '04:23:49 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353891395753514,5.15407282859087&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(180, 2, '', '52.35386948420134', '5.154112726449966', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.1140447', '4.279497', 'Scheveningen Strand', 'Sunday, Nov 5', '16:52:46', '05:14:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35386948420134,5.154112726449966&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(181, 2, '', '52.35385822124972', '5.15411239117384', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 5', '19:42:30', '08:00:15 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35385822124972,5.15411239117384&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(182, 2, '', '52.35388095193093', '5.15409629791975', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Sunday, Nov 5', '20:01:44', '08:17:38 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35388095193093,5.15409629791975&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-05'),
(183, 2, '', '52.35394300048768', '5.154025219380856', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Tuesday, Nov 7', '17:57:55', '05:58:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35394300048768,5.154025219380856&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-07'),
(184, 2, '', '52.35394300048768', '5.154025219380856', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Tuesday, Nov 7', '17:58:20', '05:58:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35394300048768,5.154025219380856&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(185, 2, '', '52.35394300048768', '5.154025219380856', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Tuesday, Nov 7', '17:59:39', '06:01:12 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35394300048768,5.154025219380856&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-11-07'),
(186, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Tuesday, Nov 7', '18:02:45', '06:02:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(187, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Tuesday, Nov 7', '18:03:00', '06:03:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(188, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Tuesday, Nov 7', '19:30:04', '07:30:04 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 3, 0, 1, 1, '2017-11-07'),
(189, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '', '', 'Set your drop point', 'Tuesday, Nov 7', '19:32:11', '07:32:11 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(190, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Tuesday, Nov 7', '19:37:20', '07:38:21 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-07'),
(191, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Tuesday, Nov 7', '19:39:50', '07:40:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 2, 0, 4, 3, 0, 1, 1, '2017-11-07'),
(192, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Tuesday, Nov 7', '19:40:27', '07:40:55 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-07'),
(193, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Tuesday, Nov 7', '19:41:40', '07:41:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|52.373866,5.2111244999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-07'),
(194, 2, '', '52.35391228339129', '5.15407282859087', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '08:47:35', '08:48:47 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35391228339129,5.15407282859087&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-08'),
(195, 2, '', '52.35391228339129', '5.15407282859087', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Wednesday, Nov 8', '08:48:22', '08:48:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35391228339129,5.15407282859087&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-08'),
(196, 2, '', '52.35391228339129', '5.15407282859087', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Wednesday, Nov 8', '08:48:29', '08:48:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35391228339129,5.15407282859087&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-08'),
(197, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '16:46:27', '04:46:56 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(198, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '16:56:05', '04:56:05 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-08'),
(199, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '16:57:37', '04:57:37 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-08'),
(200, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '16:57:45', '04:57:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-08');
INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(201, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '16:57:52', '04:57:52 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-08'),
(202, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '16:58:08', '04:58:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 3, 14, 1, 1, '2017-11-08'),
(203, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '17:05:30', '05:05:30 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 4, 1, 1, 2, 0, 0, 3, 14, 1, 1, '2017-11-08'),
(204, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '17:18:02', '05:18:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 3, 14, 1, 1, '2017-11-08'),
(205, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '17:19:00', '05:19:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(206, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '17:19:59', '05:20:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-11-08'),
(207, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Wednesday, Nov 8', '17:21:05', '05:21:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-11-08'),
(208, 2, '', '52.353848391762355', '5.15409730374813', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3107771298611', '4.762283563613892', 'Vertrekpassage 106, 1118 AS Schiphol, Netherlands', 'Wednesday, Nov 8', '17:30:27', '05:31:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353848391762355,5.15409730374813&markers=color:red|label:D|52.3107771298611,4.762283563613892&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(209, 2, '', '52.35394300048768', '5.154009126126766', 'Venusstraat 4, 1363 VN Almere, Netherlands', '50.851368199999996', '5.690972499999999', 'Maastricht', 'Wednesday, Nov 8', '17:33:49', '05:35:26 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35394300048768,5.154009126126766&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(210, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '52.3702157', '4.895167900000001', 'Amsterdam', 'Wednesday, Nov 8', '17:58:26', '06:01:29 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-08'),
(211, 2, '', '52.353879928026615', '5.154090262949467', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Wednesday, Nov 8', '18:17:56', '06:18:18 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353879928026615,5.154090262949467&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 2, 0, 8, 1, 0, 1, 1, '2017-11-08'),
(212, 2, '', '52.35391228339129', '5.154045671224594', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.35107959999999', '5.1768234999999985', 'Katernstraat', 'Thursday, Nov 9', '11:39:56', '11:51:57 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35391228339129,5.154045671224594&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-09'),
(213, 2, '', '52.353967164588525', '5.154002420604229', 'Venusstraat 4, 1363 Almere, Netherlands', '52.35107959999999', '5.1768234999999985', 'Katernstraat', 'Thursday, Nov 9', '11:54:40', '11:55:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353967164588525,5.154002420604229&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-09'),
(214, 2, '', '52.35397699404949', '5.154017172753811', 'Venusstraat 4, 1363 Almere, Netherlands', '52.35107959999999', '5.1768234999999985', 'Katernstraat', 'Thursday, Nov 9', '11:55:42', '12:02:31 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35397699404949,5.154017172753811&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-09'),
(215, 2, '', '52.351422284552605', '5.176163166761398', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Thursday, Nov 9', '14:49:29', '02:50:55 PM', '', '10/11/2017', '18:30', 7, 4, 2, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-09'),
(216, 2, '', '52.351422284552605', '5.176163166761398', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Thursday, Nov 9', '14:51:14', '02:51:24 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351422284552605,5.176163166761398&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-09'),
(217, 2, '', '52.351422284552605', '5.176163166761398', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Thursday, Nov 9', '14:51:43', '02:52:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351422284552605,5.176163166761398&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 2, 0, 2, 1, 0, 1, 1, '2017-11-09'),
(218, 2, '', '52.351422284552605', '5.176163166761398', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.3076865', '4.7674240999999995', 'Schiphol', 'Thursday, Nov 9', '15:46:47', '03:47:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.351422284552605,5.176163166761398&markers=color:red|label:D|52.3076865,4.7674240999999995&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-09'),
(219, 2, '', '52.35142494685177', '5.176160149276256', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Thursday, Nov 9', '15:51:46', '03:51:46 PM', '', '11/11/2017', '16:51', 0, 3, 2, 1, 1, 0, 0, 3, 0, 1, 1, '2017-11-09'),
(220, 2, '', '52.35142515164403', '5.176168195903302', 'Katernstraat 31, 1321 NC Almere, Netherlands', '50.9140244', '5.776135', 'Maastricht-Airport', 'Friday, Nov 10', '09:13:15', '09:14:58 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35142515164403,5.176168195903302&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-10'),
(221, 2, '', '52.35142310372161', '5.176193676888944', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.5226476', '5.438944300000001', 'Bataviaplein', 'Friday, Nov 10', '09:23:53', '09:26:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35142310372161,5.176193676888944&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-10'),
(222, 2, '', '52.35142515164403', '5.176168531179428', 'Katernstraat 31, 1321 NC Almere, Netherlands', '', '', 'Set your drop point', 'Friday, Nov 10', '09:30:42', '09:30:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35142515164403,5.176168531179428&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-10'),
(223, 2, '', '52.35142515164403', '5.176168531179428', 'Katernstraat 31, 1321 NC Almere, Netherlands', '', '', 'Set your drop point', 'Friday, Nov 10', '09:31:01', '09:31:01 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35142515164403,5.176168531179428&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-10'),
(224, 2, '', '52.35142515164403', '5.176168531179428', 'Katernstraat 31, 1321 NC Almere, Netherlands', '', '', 'Set your drop point', 'Friday, Nov 10', '09:47:43', '09:47:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35142515164403,5.176168531179428&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-10'),
(225, 2, '', '52.35142515164403', '5.176168531179428', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.373866', '5.2111244999999995', 'Almere Stad', 'Friday, Nov 10', '09:48:23', '09:49:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35142515164403,5.176168531179428&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-10'),
(226, 2, '', '52.35142515164403', '5.176168531179428', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.3667752', '5.1907975', 'Bruna', 'Friday, Nov 10', '15:34:20', '03:42:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35142515164403,5.176168531179428&markers=color:red|label:D|52.3667752,5.1907975&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-10'),
(227, 2, '', '52.36672662623792', '5.191483274102211', 'Messiaenplantsoen 9, 1323 Almere, Netherlands', '52.4032911', '5.3040335', 'Minisupermarkt', 'Friday, Nov 10', '15:52:09', '04:13:25 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.36672662623792,5.191483274102211&markers=color:red|label:D|52.4032911,5.3040335&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-10'),
(228, 2, '', '52.36672662623792', '5.191483274102211', 'Messiaenplantsoen 9, 1323 Almere, Netherlands', '52.34619409999999', '5.1528857', 'Albert Heijn', 'Friday, Nov 10', '16:29:46', '04:58:53 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.36672662623792,5.191483274102211&markers=color:red|label:D|52.34619409999999,5.1528857&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-10'),
(229, 2, '', '52.35390982602264', '5.154070481657983', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3439531', '5.220719300000001', 'Almere Haven', 'Friday, Nov 10', '17:37:11', '05:37:23 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390982602264,5.154070481657983&markers=color:red|label:D|52.3439531,5.220719300000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 2, 0, 8, 1, 0, 1, 1, '2017-11-10'),
(230, 2, '', '52.35390982602264', '5.154070481657983', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3439531', '5.220719300000001', 'Almere Haven', 'Friday, Nov 10', '17:37:35', '05:37:35 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390982602264,5.154070481657983&markers=color:red|label:D|52.3439531,5.220719300000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-10'),
(231, 2, '', '52.35146160464726', '5.176196023821831', 'Katernstraat 31, 1321 NC Almere, Netherlands', '52.3539685', '5.154308', '1363 VN', 'Saturday, Nov 11', '12:56:25', '01:02:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35146160464726,5.176196023821831&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-11'),
(232, 2, '', '52.35390900689973', '5.154084898531438', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '13:23:10', '01:23:10 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390900689973,5.154084898531438&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(233, 2, '', '52.35390900689973', '5.154084898531438', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '13:23:43', '01:23:43 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390900689973,5.154084898531438&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(234, 2, '', '52.35390900689973', '5.154084898531438', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.4081006', '5.2624778', 'Almere Buiten', 'Saturday, Nov 11', '13:29:23', '01:32:19 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35390900689973,5.154084898531438&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 7, 1, 0, 3, 0, 1, 1, '2017-11-11'),
(235, 2, '', '52.353914535979065', '5.154104344546795', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '15:47:55', '03:48:40 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353914535979065,5.154104344546795&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-11'),
(236, 2, '', '52.353914535979065', '5.154104344546795', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '15:50:59', '03:50:59 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353914535979065,5.154104344546795&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(237, 2, '', '52.353914535979065', '5.154104344546795', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '15:51:51', '03:51:51 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353914535979065,5.154104344546795&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(238, 2, '', '52.353914535979065', '5.154104344546795', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '15:52:45', '03:52:45 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353914535979065,5.154104344546795&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(239, 2, '', '52.353914535979065', '5.154104344546795', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '15:53:14', '03:53:14 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353914535979065,5.154104344546795&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(240, 2, '', '52.353914535979065', '5.154104344546795', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '15:59:28', '03:59:28 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353914535979065,5.154104344546795&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(241, 2, '', '52.353914535979065', '5.154104344546795', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '16:28:32', '04:28:32 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353914535979065,5.154104344546795&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(242, 2, '', '52.353914535979065', '5.154104344546795', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '16:30:08', '04:30:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353914535979065,5.154104344546795&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 2, 0, 8, 1, 0, 1, 1, '2017-11-11'),
(243, 2, '', '52.353914535979065', '5.154104344546795', 'Venusstraat 4, 1363 VN Almere, Netherlands', '29.760426700000004', '-95.3698028', 'Houston', 'Saturday, Nov 11', '16:31:49', '04:32:44 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353914535979065,5.154104344546795&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-11'),
(244, 2, '', '52.35394750566004', '5.154027231037618', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Saturday, Nov 11', '16:57:17', '04:57:17 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35394750566004,5.154027231037618&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-11'),
(245, 2, '', '52.353920065057714', '5.154062770307064', 'Venusstraat 4, 1363 VN Almere, Netherlands', '', '', 'Set your drop point', 'Sunday, Nov 12', '17:57:33', '05:57:47 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353920065057714,5.154062770307064&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 2, 0, 4, 3, 14, 1, 1, '2017-11-12'),
(246, 2, '', '52.353913512075536', '5.1540761813521385', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.2291696', '5.166897400000001', 'Hilversum', 'Sunday, Nov 12', '17:57:58', '06:00:46 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.353913512075536,5.1540761813521385&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 7, 4, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-12'),
(247, 2, '', '52.35387214635311', '5.154085233807565', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.2291696', '5.166897400000001', 'Hilversum', 'Sunday, Nov 12', '18:10:59', '06:11:58 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35387214635311,5.154085233807565&markers=color:red|label:D|52.2291696,5.166897400000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-12'),
(248, 2, '', '52.35387214635311', '5.154085233807565', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.309456', '4.7622836', 'Schiphol Airport', 'Sunday, Nov 12', '18:14:34', '06:14:34 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.35387214635311,5.154085233807565&markers=color:red|label:D|52.309456,4.7622836&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-12'),
(249, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.4051912', '5.28215', 'de Bosrand', 'Monday, Nov 13', '07:08:36', '07:08:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.4051912,5.28215&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(250, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.4051912', '5.28215', 'de Bosrand', 'Monday, Nov 13', '07:09:13', '07:09:13 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.4051912,5.28215&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(251, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3533994', '5.1370549', 'Adriaen Blockstraat', 'Monday, Nov 13', '07:38:06', '07:38:24 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.3533994,5.1370549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-13'),
(252, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3533994', '5.1370549', 'Adriaen Blockstraat', 'Monday, Nov 13', '08:35:25', '08:43:52 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.3533994,5.1370549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-13'),
(253, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3533994', '5.1370549', 'Adriaen Blockstraat', 'Monday, Nov 13', '08:35:54', '08:35:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.3533994,5.1370549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(254, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3533994', '5.1370549', 'Adriaen Blockstraat', 'Monday, Nov 13', '08:44:13', '08:44:21 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.3533994,5.1370549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-13'),
(255, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3533994', '5.1370549', 'Adriaen Blockstraat', 'Monday, Nov 13', '08:45:04', '08:45:12 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.3533994,5.1370549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 16, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-13'),
(256, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3533994', '5.1370549', 'Adriaen Blockstraat', 'Monday, Nov 13', '08:45:36', '08:45:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.3533994,5.1370549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(257, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3533994', '5.1370549', 'Adriaen Blockstraat', 'Monday, Nov 13', '08:45:54', '08:45:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.3533994,5.1370549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(258, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3533994', '5.1370549', 'Adriaen Blockstraat', 'Monday, Nov 13', '08:58:19', '08:58:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.3533994,5.1370549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 3, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-13'),
(259, 2, '', '52.3539024539159', '5.154087245464325', 'Venusstraat 4, 1363 VN Almere, Netherlands', '52.3533994', '5.1370549', 'Adriaen Blockstraat', 'Monday, Nov 13', '09:53:10', '10:19:38 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|52.3539024539159,5.154087245464325&markers=color:red|label:D|52.3533994,5.1370549&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 12, 3, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-13');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '112', 1),
(4, 'Ambulance', '112', 1),
(6, 'Breakdown ANWB', '09009000888', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppourt`
--

INSERT INTO `suppourt` (`sup_id`, `driver_id`, `name`, `email`, `phone`, `query`) VALUES
(1, 212, 'rohit', 'rohit', '8950200340', 'np'),
(2, 212, 'shilpa', 'shilpa', 'fgffchchch', 'yffhjkjhk'),
(3, 282, 'zak', 'zak', '0628926431', 'Hi the app driver is always crashing on android devices.also the gps  position is not taken by the application.regards'),
(4, 476, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SÓ CHAMAR NO WHATS ', 'SÓ CHAMAR NO WHATS '),
(5, 477, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SEJA BEM VINDO', 'SEJA BEM VINDO');

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Polution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive'),
(10, 'Taxi Permit');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(20, 3, 2, 1),
(19, 3, 1, 1),
(3, 56, 3, 1),
(4, 56, 2, 1),
(5, 56, 4, 1),
(23, 84, 8, 1),
(22, 84, 6, 1),
(21, 3, 4, 1),
(42, 121, 10, 1),
(41, 121, 4, 1),
(40, 121, 2, 1),
(39, 121, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_bill`
--

INSERT INTO `table_driver_bill` (`bill_id`, `bill_from_date`, `bill_to_date`, `driver_id`, `outstanding_amount`, `bill_settle_date`, `bill_status`) VALUES
(1, '2017-10-09 00.00.01 AM', '2017-10-10 05:38:47 AM', 2, '9', '2017-10-10', 1),
(2, '2017-09-26 00.00.01 AM', '2017-10-10 05:38:47 AM', 1, '12', '2017-10-15', 1),
(3, '2017-10-10 05:39:47 AM', '2017-10-11 07:41:05 PM', 2, '74.3', '2017-10-11', 1),
(4, '2017-10-10 05:39:47 AM', '2017-10-11 07:41:05 PM', 1, '0', '2017-10-19', 1),
(5, '2017-10-11 07:42:05 PM', '2017-10-15 02:35:11 PM', 3, '32.04', '2017-10-19', 1),
(6, '2017-10-11 07:42:05 PM', '2017-10-15 02:35:11 PM', 2, '13.8', '2017-10-15', 1),
(7, '2017-10-11 07:42:05 PM', '2017-10-15 02:35:11 PM', 1, '0', '2017-10-19', 1),
(8, '2017-10-15 02:36:11 PM', '2017-10-16 08:31:49 AM', 4, '0', '0000-00-00', 0),
(9, '2017-10-15 02:36:11 PM', '2017-10-16 08:31:49 AM', 3, '0', '2017-10-19', 1),
(10, '2017-10-15 02:36:11 PM', '2017-10-16 08:31:49 AM', 2, '2.16', '2017-10-16', 1),
(11, '2017-10-15 02:36:11 PM', '2017-10-16 08:31:49 AM', 1, '4', '2017-10-19', 1),
(12, '2017-10-16 08:32:49 AM', '2017-10-17 06:12:29 PM', 7, '0', '2017-11-10', 1),
(13, '2017-10-16 08:32:49 AM', '2017-10-17 06:12:29 PM', 6, '0', '0000-00-00', 0),
(14, '2017-10-16 08:32:49 AM', '2017-10-17 06:12:29 PM', 5, '-187.4', '2017-10-17', 1),
(15, '2017-10-16 08:32:49 AM', '2017-10-17 06:12:29 PM', 4, '9.2', '2017-10-19', 1),
(16, '2017-10-16 08:32:49 AM', '2017-10-17 06:12:29 PM', 3, '0', '0000-00-00', 0),
(17, '2017-10-16 08:32:49 AM', '2017-10-17 06:12:29 PM', 2, '9.07', '2017-10-17', 1),
(18, '2017-10-16 08:32:49 AM', '2017-10-17 06:12:29 PM', 1, '0', '2017-10-19', 1),
(19, '2017-10-17 06:13:29 PM', '2017-10-18 05:48:37 PM', 7, '0', '2017-11-10', 1),
(20, '2017-10-17 06:13:29 PM', '2017-10-18 05:48:37 PM', 6, '0', '0000-00-00', 0),
(21, '2017-10-17 06:13:29 PM', '2017-10-18 05:48:37 PM', 5, '0', '0000-00-00', 0),
(22, '2017-10-17 06:13:29 PM', '2017-10-18 05:48:37 PM', 4, '0', '0000-00-00', 0),
(23, '2017-10-17 06:13:29 PM', '2017-10-18 05:48:37 PM', 3, '0', '0000-00-00', 0),
(24, '2017-10-17 06:13:29 PM', '2017-10-18 05:48:37 PM', 2, '0', '2017-10-19', 1),
(25, '2017-10-17 06:13:29 PM', '2017-10-18 05:48:37 PM', 1, '0', '2017-10-19', 1),
(26, '2017-10-18 05:49:37 PM', '2017-10-19 05:04:16 PM', 7, '0', '2017-11-10', 1),
(27, '2017-10-18 05:49:37 PM', '2017-10-19 05:04:16 PM', 6, '0', '0000-00-00', 0),
(28, '2017-10-18 05:49:37 PM', '2017-10-19 05:04:16 PM', 5, '0', '0000-00-00', 0),
(29, '2017-10-18 05:49:37 PM', '2017-10-19 05:04:16 PM', 4, '0', '0000-00-00', 0),
(30, '2017-10-18 05:49:37 PM', '2017-10-19 05:04:16 PM', 3, '0', '0000-00-00', 0),
(31, '2017-10-18 05:49:37 PM', '2017-10-19 05:04:16 PM', 2, '1.6', '2017-10-28', 1),
(32, '2017-10-18 05:49:37 PM', '2017-10-19 05:04:16 PM', 1, '0', '0000-00-00', 0),
(33, '2017-10-19 05:05:16 PM', '2017-10-20 05:09:57 PM', 7, '0', '2017-11-10', 1),
(34, '2017-10-19 05:05:16 PM', '2017-10-20 05:09:57 PM', 6, '0', '0000-00-00', 0),
(35, '2017-10-19 05:05:16 PM', '2017-10-20 05:09:57 PM', 5, '0', '0000-00-00', 0),
(36, '2017-10-19 05:05:16 PM', '2017-10-20 05:09:57 PM', 4, '0', '0000-00-00', 0),
(37, '2017-10-19 05:05:16 PM', '2017-10-20 05:09:57 PM', 3, '0', '0000-00-00', 0),
(38, '2017-10-19 05:05:16 PM', '2017-10-20 05:09:57 PM', 2, '0', '2017-10-28', 1),
(39, '2017-10-19 05:05:16 PM', '2017-10-20 05:09:57 PM', 1, '0', '0000-00-00', 0),
(40, '2017-10-20 05:10:57 PM', '2017-10-21 04:24:40 PM', 7, '0', '2017-11-10', 1),
(41, '2017-10-20 05:10:57 PM', '2017-10-21 04:24:40 PM', 6, '0', '0000-00-00', 0),
(42, '2017-10-20 05:10:57 PM', '2017-10-21 04:24:40 PM', 5, '0', '0000-00-00', 0),
(43, '2017-10-20 05:10:57 PM', '2017-10-21 04:24:40 PM', 4, '0', '0000-00-00', 0),
(44, '2017-10-20 05:10:57 PM', '2017-10-21 04:24:40 PM', 3, '0', '0000-00-00', 0),
(45, '2017-10-20 05:10:57 PM', '2017-10-21 04:24:40 PM', 2, '2.8', '2017-10-28', 1),
(46, '2017-10-20 05:10:57 PM', '2017-10-21 04:24:40 PM', 1, '0', '0000-00-00', 0),
(47, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 14, '5', '0000-00-00', 0),
(48, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 13, '25', '0000-00-00', 0),
(49, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 12, '0', '2017-11-08', 1),
(50, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 11, '64.08', '0000-00-00', 0),
(51, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 10, '0', '0000-00-00', 0),
(52, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 9, '0', '0000-00-00', 0),
(53, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 8, '42.72', '0000-00-00', 0),
(54, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 7, '0', '2017-11-10', 1),
(55, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 6, '0', '0000-00-00', 0),
(56, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 5, '0', '0000-00-00', 0),
(57, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 4, '0', '0000-00-00', 0),
(58, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 3, '0', '0000-00-00', 0),
(59, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 2, '1', '2017-10-28', 1),
(60, '2017-10-21 04:25:40 PM', '2017-10-28 05:52:45 AM', 1, '3.6', '0000-00-00', 0),
(61, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 16, '0', '0000-00-00', 0),
(62, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 15, '0', '0000-00-00', 0),
(63, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 14, '0', '0000-00-00', 0),
(64, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 13, '0', '0000-00-00', 0),
(65, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 12, '-75.4', '2017-11-11', 1),
(66, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 11, '0', '0000-00-00', 0),
(67, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 10, '0', '0000-00-00', 0),
(68, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 9, '0', '0000-00-00', 0),
(69, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 8, '0', '0000-00-00', 0),
(70, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 7, '-20', '2017-11-11', 1),
(71, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 6, '0', '0000-00-00', 0),
(72, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 5, '0', '0000-00-00', 0),
(73, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 4, '0', '0000-00-00', 0),
(74, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 3, '0', '0000-00-00', 0),
(75, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 2, '14.85', '2017-11-11', 1),
(76, '2017-10-28 05:53:45 AM', '2017-11-11 01:47:49 PM', 1, '0', '0000-00-00', 0),
(77, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 16, '0', '0000-00-00', 0),
(78, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 15, '0', '0000-00-00', 0),
(79, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 14, '0', '0000-00-00', 0),
(80, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 13, '0', '0000-00-00', 0),
(81, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 12, '0', '2017-11-11', 1),
(82, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 11, '0', '0000-00-00', 0),
(83, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 10, '0', '0000-00-00', 0),
(84, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 9, '0', '0000-00-00', 0),
(85, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 8, '0', '0000-00-00', 0),
(86, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 7, '0', '2017-11-11', 1),
(87, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 6, '0', '0000-00-00', 0),
(88, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 5, '0', '0000-00-00', 0),
(89, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 4, '0', '0000-00-00', 0),
(90, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 3, '0', '0000-00-00', 0),
(91, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 2, '0', '2017-11-11', 1),
(92, '2017-11-11 01:48:49 PM', '2017-11-11 01:47:57 PM', 1, '0', '0000-00-00', 0),
(93, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 16, '0', '0000-00-00', 0),
(94, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 15, '0', '0000-00-00', 0),
(95, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 14, '0', '0000-00-00', 0),
(96, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 13, '0', '0000-00-00', 0),
(97, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 12, '0', '2017-11-11', 1),
(98, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 11, '0', '0000-00-00', 0),
(99, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 10, '0', '0000-00-00', 0),
(100, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 9, '0', '0000-00-00', 0),
(101, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 8, '0', '0000-00-00', 0),
(102, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 7, '0', '2017-11-11', 1),
(103, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 6, '0', '0000-00-00', 0),
(104, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 5, '0', '0000-00-00', 0),
(105, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 4, '0', '0000-00-00', 0),
(106, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 3, '0', '0000-00-00', 0),
(107, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 2, '0', '0000-00-00', 0),
(108, '2017-11-11 01:48:57 PM', '2017-11-11 01:48:29 PM', 1, '0', '0000-00-00', 0),
(109, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 16, '0', '0000-00-00', 0),
(110, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 15, '0', '0000-00-00', 0),
(111, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 14, '0', '0000-00-00', 0),
(112, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 13, '0', '0000-00-00', 0),
(113, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 12, '0', '0000-00-00', 0),
(114, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 11, '0', '0000-00-00', 0),
(115, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 10, '0', '0000-00-00', 0),
(116, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 9, '0', '0000-00-00', 0),
(117, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 8, '0', '0000-00-00', 0),
(118, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 7, '0', '0000-00-00', 0),
(119, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 6, '0', '0000-00-00', 0),
(120, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 5, '0', '0000-00-00', 0),
(121, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 4, '0', '0000-00-00', 0),
(122, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 3, '0', '0000-00-00', 0),
(123, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 2, '0', '0000-00-00', 0),
(124, '2017-11-11 01:49:29 PM', '2017-11-11 01:49:29 PM', 1, '0', '0000-00-00', 0),
(125, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 16, '0', '0000-00-00', 0),
(126, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 15, '0', '0000-00-00', 0),
(127, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 14, '0', '0000-00-00', 0),
(128, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 13, '0', '0000-00-00', 0),
(129, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 12, '0', '0000-00-00', 0),
(130, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 11, '0', '0000-00-00', 0),
(131, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 10, '0', '0000-00-00', 0),
(132, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 9, '0', '0000-00-00', 0),
(133, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 8, '0', '0000-00-00', 0),
(134, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 7, '0', '0000-00-00', 0),
(135, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 6, '0', '0000-00-00', 0),
(136, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 5, '0', '0000-00-00', 0),
(137, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 4, '0', '0000-00-00', 0),
(138, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 3, '0', '0000-00-00', 0),
(139, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 2, '0', '0000-00-00', 0),
(140, '2017-11-11 01:50:29 PM', '2017-11-13 07:47:37 AM', 1, '0', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1506409473document_image_13.jpg', '15-2-2018', 1),
(2, 1, 2, 'uploads/driver/1506409489document_image_12.jpg', '26-5-2018', 1),
(3, 1, 4, 'uploads/driver/1506409502document_image_14.jpg', '16-3-2018', 1),
(4, 2, 1, 'uploads/driver/1507581872document_image_21.jpg', '9-10-2018', 1),
(5, 2, 2, 'uploads/driver/1507581905document_image_22.jpg', '9-10-2018', 1),
(6, 2, 3, 'uploads/driver/1507581931document_image_23.jpg', '9-10-2019', 1),
(7, 2, 4, 'uploads/driver/1507581963document_image_24.jpg', '9-10-2020', 1),
(8, 3, 3, 'uploads/driver/1507968166document_image_33.jpg', '2017-10-28', 1),
(9, 3, 2, 'uploads/driver/1507968174document_image_32.jpg', '2017-10-28', 1),
(10, 3, 4, 'uploads/driver/1507968184document_image_34.jpg', '2017-10-28', 1),
(11, 4, 3, 'uploads/driver/1508137339document_image_43.jpg', '2017-10-16', 2),
(12, 4, 2, 'uploads/driver/1508137432document_image_42.jpg', '2017-10-16', 2),
(13, 4, 4, 'uploads/driver/1508137442document_image_44.jpg', '2017-10-16', 2),
(14, 5, 3, 'uploads/driver/1508140729document_image_53.jpg', '27-10-2017', 1),
(15, 5, 4, 'uploads/driver/1508140761document_image_54.jpg', '26-10-2017', 1),
(16, 5, 2, 'uploads/driver/1508140818document_image_52.jpg', '28-10-2017', 1),
(17, 6, 3, 'uploads/driver/1508161280document_image_63.jpg', '2017-10-16', 1),
(18, 6, 2, 'uploads/driver/1508161289document_image_62.jpg', '2017-10-28', 1),
(19, 6, 4, 'uploads/driver/1508161298document_image_64.jpg', '2017-10-28', 1),
(20, 7, 10, 'uploads/driver/15082545400GIFlogoColorLarge.gifdocument_image_7.gif', '2020-10-17', 2),
(21, 7, 4, 'uploads/driver/15082545401GIFlogoColorLarge.gifdocument_image_7.gif', '2020-10-17', 2),
(22, 7, 2, 'uploads/driver/15082545402GIFlogoColorLarge.gifdocument_image_7.gif', '2018-11-17', 2),
(23, 7, 1, 'uploads/driver/15082545403GIFlogoColorLarge.gifdocument_image_7.gif', '2019-10-17', 2),
(24, 8, 3, 'uploads/driver/1508754574document_image_83.jpg', '2017-10-28', 1),
(25, 8, 2, 'uploads/driver/1508754582document_image_82.jpg', '2017-10-28', 1),
(26, 8, 4, 'uploads/driver/1508754590document_image_84.jpg', '2017-10-28', 1),
(27, 9, 3, 'uploads/driver/1508852158document_image_93.jpg', '2017-10-24', 2),
(28, 9, 2, 'uploads/driver/1508852175document_image_92.jpg', '2017-10-26', 2),
(29, 9, 4, 'uploads/driver/1508852190document_image_94.jpg', '2017-10-26', 2),
(30, 10, 3, 'uploads/driver/1508854280document_image_103.jpg', '8-12-2017', 1),
(31, 10, 2, 'uploads/driver/1508854293document_image_102.jpg', '6-4-2018', 1),
(32, 10, 4, 'uploads/driver/1508854305document_image_104.jpg', '23-3-2018', 1),
(33, 11, 3, 'uploads/driver/1508998619document_image_113.jpg', '16-11-2018', 1),
(34, 11, 2, 'uploads/driver/1508998632document_image_112.jpg', '27-4-2018', 1),
(35, 11, 4, 'uploads/driver/1508998655document_image_114.jpg', '4-5-2018', 1),
(36, 12, 10, 'uploads/driver/1509091048document_image_120editcar_2.jpg', '27/10/2018', 2),
(37, 12, 4, 'uploads/driver/1509091048document_image_121editcar_2.jpg', '27/10/2018', 2),
(38, 12, 2, 'uploads/driver/1509091048document_image_122editcar_2.jpg', '27/10/2018', 2),
(39, 12, 1, 'uploads/driver/1509091048document_image_123Standard.jpg', '27/10/2017', 2),
(40, 13, 3, 'uploads/driver/1509097923document_image_133.jpg', '2017-10-31', 1),
(41, 13, 2, 'uploads/driver/1509097930document_image_132.jpg', '2017-10-31', 1),
(42, 13, 4, 'uploads/driver/1509097937document_image_134.jpg', '2017-10-31', 1),
(43, 14, 3, 'uploads/driver/1509106504document_image_143.jpg', '30-12-2017', 1),
(44, 14, 2, 'uploads/driver/1509106529document_image_142.jpg', '29-6-2018', 1),
(45, 14, 4, 'uploads/driver/1509106549document_image_144.jpg', '27-4-2019', 1),
(46, 15, 3, 'uploads/driver/1509172817document_image_153.jpg', '2017-10-31', 1),
(47, 15, 2, 'uploads/driver/1509172826document_image_152.jpg', '2017-10-31', 1),
(48, 15, 4, 'uploads/driver/1509172834document_image_154.jpg', '2017-10-31', 1),
(49, 16, 10, 'uploads/driver/1510306902document_image_1610.jpg', '8-8-2020', 1),
(50, 16, 4, 'uploads/driver/1510306967document_image_164.jpg', '31-12-2018', 1),
(51, 16, 2, 'uploads/driver/1510307094document_image_162.jpg', '10-11-2020', 1),
(52, 16, 1, 'uploads/driver/1510307164document_image_161.jpg', '10-11-2018', 1),
(53, 17, 10, 'uploads/driver/1511192782document_image_1710.jpg', '7-5-2022', 1),
(54, 17, 4, 'uploads/driver/1511192801document_image_174.jpg', '21-5-2022', 1),
(55, 17, 2, 'uploads/driver/1511192819document_image_172.jpg', '30-11-2019', 1),
(56, 17, 1, 'uploads/driver/1511192838document_image_171.jpg', '30-3-2019', 1),
(57, 18, 10, 'uploads/driver/1511208421document_image_1810.jpg', '9-1-2021', 1),
(58, 18, 2, 'uploads/driver/1511208435document_image_182.jpg', '23-12-2017', 1),
(59, 18, 4, 'uploads/driver/1511208448document_image_184.jpg', '27-1-2018', 1),
(60, 18, 1, 'uploads/driver/1511208465document_image_181.jpg', '30-6-2019', 1),
(61, 19, 10, 'uploads/driver/1511338577document_image_1910.jpg', '7-3-2020', 1),
(62, 19, 4, 'uploads/driver/1511338595document_image_194.jpg', '7-8-2020', 1),
(63, 19, 2, 'uploads/driver/1511338609document_image_192.jpg', '23-10-2021', 1),
(64, 19, 1, 'uploads/driver/1511338629document_image_191.jpg', '21-7-2018', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-09-26 08:05:12', '2017-10-16 08:07:25', '0 Hours 2 Minutes', 0, 2, '2017-09-26'),
(2, 2, '2017-10-09 21:47:48', '2017-10-10 06:15:34', '8 Hours 27 Minutes', 8, 27, '2017-10-09'),
(3, 2, '2017-10-10 07:17:12', '2017-10-11 19:00:06', '11 Hours 54 Minutes', 11, 54, '2017-10-10'),
(4, 2, '2017-10-11 19:02:30', '2017-10-14 17:56:16', '22 Hours 53 Minutes', 22, 53, '2017-10-11'),
(5, 3, '2017-10-14 09:03:13', '2017-10-16 07:20:29', '22 Hours 17 Minutes', 22, 17, '2017-10-14'),
(6, 2, '2017-10-14 18:03:19', '2017-10-15 08:33:56', '14 Hours 30 Minutes', 14, 30, '2017-10-14'),
(7, 2, '2017-10-15 18:32:40', '2017-10-16 16:55:43', '31 Hours 81 Minutes', 31, 81, '2017-10-15'),
(8, 4, '2017-10-16 08:04:30', '2017-10-16 14:34:13', '6 Hours 29 Minutes', 6, 29, '2017-10-16'),
(9, 1, '2017-10-16 08:55:28', '2017-10-16 08:55:32', '0 Hours 0 Minutes', 0, 0, '2017-10-16'),
(10, 5, '2017-10-16 09:00:26', '2017-10-26 07:15:01', '22 Hours 14 Minutes', 22, 14, '2017-10-16'),
(11, 6, '2017-10-16 14:41:47', '2017-10-17 07:17:04', '32 Hours 69 Minutes', 32, 69, '2017-10-16'),
(12, 2, '2017-10-16 17:30:59', '2017-10-20 18:11:12', '0 Hours 40 Minutes', 0, 40, '2017-10-16'),
(13, 7, '2017-10-17 16:37:31', '2017-10-18 18:23:58', '1 Hours 46 Minutes', 1, 46, '2017-10-17'),
(14, 7, '2017-10-18 18:24:02', '2017-10-18 18:24:28', '0 Hours 0 Minutes', 0, 0, '2017-10-18'),
(15, 7, '2017-10-19 17:46:54', '2017-10-20 10:16:44', '16 Hours 29 Minutes', 16, 29, '2017-10-19'),
(16, 7, '2017-10-20 18:05:44', '2017-10-21 10:03:01', '22 Hours 104 Minutes', 22, 104, '2017-10-20'),
(17, 2, '2017-10-20 18:11:16', '2017-10-20 18:12:07', '0 Hours 0 Minutes', 0, 0, '2017-10-20'),
(18, 7, '2017-10-21 11:35:19', '', '', 0, 0, '2017-10-21'),
(19, 8, '2017-10-23 11:30:28', '2017-10-27 10:46:23', '23 Hours 15 Minutes', 23, 15, '2017-10-23'),
(20, 9, '2017-10-24 14:38:05', '', '', 0, 0, '2017-10-24'),
(21, 10, '2017-10-24 15:11:53', '2017-10-25 10:49:06', '19 Hours 37 Minutes', 19, 37, '2017-10-24'),
(22, 1, '2017-10-24 15:38:34', '2017-10-25 11:06:12', '19 Hours 27 Minutes', 19, 27, '2017-10-24'),
(23, 11, '2017-10-26 07:17:45', '2017-10-27 09:18:34', '2 Hours 0 Minutes', 2, 0, '2017-10-26'),
(24, 7, '2017-10-26 16:05:42', '2017-10-27 10:00:17', '20 Hours 76 Minutes', 20, 76, '2017-10-26'),
(25, 2, '2017-10-26 20:18:13', '2017-10-28 14:51:13', '18 Hours 63 Minutes', 18, 63, '2017-10-26'),
(26, 11, '2017-10-27 10:08:18', '2017-10-27 10:09:46', '0 Hours 4 Minutes', 0, 4, '2017-10-27'),
(27, 7, '2017-10-27 18:57:00', '2017-10-27 18:59:41', '8 Hours 58 Minutes', 8, 58, '2017-10-27'),
(28, 8, '2017-10-27 10:46:26', '2017-10-27 10:47:34', '0 Hours 1 Minutes', 0, 1, '2017-10-27'),
(29, 13, '2017-10-27 10:52:25', '2017-10-27 14:26:53', '3 Hours 34 Minutes', 3, 34, '2017-10-27'),
(30, 14, '2017-10-27 13:15:59', '', '', 0, 0, '2017-10-27'),
(31, 15, '2017-10-28 07:40:42', '2017-11-02 07:08:18', '-1 Hours 27 Minutes', -1, 27, '2017-10-28'),
(32, 7, '2017-10-28 08:56:49', '2017-11-03 14:14:12', '5 Hours 17 Minutes', 5, 17, '2017-10-28'),
(33, 2, '2017-10-28 15:07:29', '2017-11-02 18:46:17', '3 Hours 54 Minutes', 3, 54, '2017-10-28'),
(34, 12, '2017-10-28 18:54:38', '2017-10-30 18:18:39', '-1 Hours 24 Minutes', -1, 24, '2017-10-28'),
(35, 12, '2017-10-30 18:19:50', '2017-11-03 14:14:07', '19 Hours 54 Minutes', 19, 54, '2017-10-30'),
(36, 15, '2017-11-02 07:08:25', '', '', 0, 0, '2017-11-02'),
(37, 2, '2017-11-02 18:46:20', '', '', 0, 0, '2017-11-02'),
(38, 7, '2017-11-03 14:14:15', '2017-11-04 06:46:45', '16 Hours 32 Minutes', 16, 32, '2017-11-03'),
(39, 12, '2017-11-03 14:14:17', '2017-11-04 06:45:07', '16 Hours 30 Minutes', 16, 30, '2017-11-03'),
(40, 12, '2017-11-04 06:45:10', '2017-11-04 06:49:47', '0 Hours 4 Minutes', 0, 4, '2017-11-04'),
(41, 7, '2017-11-04 18:29:02', '2017-11-09 14:51:31', '20 Hours 89 Minutes', 20, 89, '2017-11-04'),
(42, 12, '2017-11-05 16:32:56', '2017-11-05 20:02:37', '10 Hours 55 Minutes', 10, 55, '2017-11-05'),
(43, 12, '2017-11-07 17:57:05', '2017-11-13 07:09:22', '13 Hours 12 Minutes', 13, 12, '2017-11-07'),
(44, 7, '2017-11-09 15:46:28', '2017-11-10 09:03:32', '17 Hours 17 Minutes', 17, 17, '2017-11-09'),
(45, 7, '2017-11-10 09:09:53', '2017-11-11 15:50:56', '6 Hours 41 Minutes', 6, 41, '2017-11-10'),
(46, 16, '2017-11-10 09:46:37', '2017-11-20 19:56:24', '10 Hours 9 Minutes', 10, 9, '2017-11-10'),
(47, 7, '2017-11-11 15:59:01', '2017-11-11 16:56:52', '0 Hours 57 Minutes', 0, 57, '2017-11-11'),
(48, 7, '2017-11-12 17:55:57', '', '', 0, 0, '2017-11-12'),
(49, 12, '2017-11-13 08:56:11', '2017-11-13 08:58:33', '0 Hours 2 Minutes', 0, 2, '2017-11-13'),
(50, 17, '2017-11-20 16:08:45', '2017-11-20 16:08:42', '0 Hours 20 Minutes', 0, 20, '2017-11-20'),
(51, 16, '2017-11-20 19:57:07', '', '', 0, 0, '2017-11-20'),
(52, 18, '2017-11-20 20:08:23', '', '', 0, 0, '2017-11-20'),
(53, 12, '2017-11-20 20:20:30', '', '', 0, 0, '2017-11-20'),
(54, 19, '2017-11-22 08:22:38', '', '', 0, 0, '2017-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(39, 'French', 1),
(38, 'Arabic', 1),
(37, 'Arabic', 1),
(36, 'Aymara', 1),
(35, 'Portuguese', 2),
(34, 'Russian', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login SUCCESSFUL'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Required fields Missing'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'Logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Send Successfully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has been arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Book Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancel By Driver'),
(108, 1, 'nl', 'Ingelogd'),
(109, 2, 'nl', 'Gebruikersnaam inactief'),
(110, 3, 'nl', 'Vereiste velden ontbreken'),
(111, 4, 'nl', 'E-mailadres of wachtwoord ongeldig'),
(112, 5, 'nl', 'Uitgelogd'),
(113, 6, 'nl', 'Gegevens onbekend'),
(114, 7, 'nl', 'Aangemeld'),
(115, 8, 'nl', 'Telefoonnummer is al in gebruik'),
(116, 9, 'nl', 'Emailadres is al in gebruik'),
(117, 10, 'nl', 'rc kopie ontbreekt'),
(118, 11, 'nl', 'Kopie rijbewijs ontbreekt'),
(119, 12, 'nl', 'Kopie verzekeringsbewijs ontbreekt'),
(120, 13, 'nl', 'Wachtwoord gewijzigd'),
(121, 14, 'nl', 'Oude wachtwoord komt niet overeen'),
(122, 15, 'nl', 'Ongeldige kortingscode'),
(123, 16, 'nl', 'Kortingscode is toegepast'),
(124, 17, 'nl', 'Gebruikersnaam onbekend'),
(125, 18, 'nl', 'Update voltooid'),
(126, 19, 'nl', 'Telefoonnummer is al in gebruik'),
(127, 20, 'nl', 'Online'),
(128, 21, 'nl', 'Offline'),
(129, 22, 'nl', 'Verificatiecode per SMS verstuurd'),
(130, 23, 'nl', 'Beoordeling voltooid'),
(131, 24, 'nl', 'Email verzonden'),
(132, 25, 'nl', 'Boeking geaccepteerd'),
(133, 26, 'nl', 'Chauffeur is gearriveerd '),
(134, 27, 'nl', 'Rit geannuleerd'),
(135, 28, 'nl', 'De rit is beÃ«indigd'),
(136, 29, 'nl', 'Rit geboekt'),
(137, 30, 'nl', 'Rit is geannuleerd'),
(138, 31, 'nl', 'Rit is gestart'),
(139, 32, 'nl', 'Nieuwe rit toegewezen'),
(140, 33, 'nl', 'Rit geannuleerd door klant'),
(141, 34, 'nl', 'Boeking geaccepteerd '),
(142, 35, 'nl', 'Boeking afgewezen'),
(143, 36, 'nl', 'Boeking geannuleerd door chauffeur'),
(144, 1, 'fr', 'ConnectÃ©'),
(145, 3, 'fr', 'Les champs obligatoires sont manquants'),
(146, 5, 'fr', 'DÃ©connectÃ©'),
(147, 6, 'fr', 'DonnÃ©es inconnues'),
(148, 7, 'fr', 'ConnectÃ©'),
(149, 8, 'fr', 'Le numÃ©ro de tÃ©lÃ©phone est dÃ©jÃ  utilisÃ©'),
(150, 10, 'fr', 'La copie rc est manquante'),
(151, 11, 'fr', 'Une copie du permis de conduire est manquante'),
(152, 13, 'fr', 'Mot de passe changÃ©'),
(153, 15, 'fr', 'Code de rÃ©duction incorrect'),
(154, 16, 'fr', 'Le code de rÃ©duction a Ã©tÃ© appliquÃ©'),
(155, 18, 'fr', 'Mise Ã  jour terminÃ©e'),
(156, 19, 'fr', 'Le numÃ©ro de tÃ©lÃ©phone est dÃ©jÃ  utilisÃ©'),
(157, 20, 'fr', 'En ligne'),
(158, 21, 'fr', 'Hors ligne'),
(159, 22, 'fr', 'Code de vÃ©rification envoyÃ© par SMS'),
(160, 23, 'fr', 'Ã‰valuation terminÃ©e'),
(161, 24, 'fr', 'Email envoyÃ©'),
(162, 25, 'fr', 'RÃ©servation acceptÃ©e'),
(163, 26, 'fr', 'Le conducteur est arrivÃ©'),
(164, 27, 'fr', 'Voyage annulÃ©'),
(165, 28, 'fr', 'Le trajet est terminÃ©'),
(166, 29, 'fr', 'Voyage rÃ©servÃ©'),
(167, 30, 'fr', 'Le tour a Ã©tÃ© annulÃ©'),
(168, 31, 'fr', 'Le tour a commencÃ©'),
(169, 32, 'fr', 'Nouvelle attraction assignÃ©e'),
(170, 33, 'fr', 'Voyage annulÃ© par le client'),
(171, 34, 'fr', 'RÃ©servation acceptÃ©e'),
(172, 35, 'fr', 'RÃ©servation refusÃ©e'),
(173, 36, 'fr', 'RÃ©servation annulÃ©e par le conducteur');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(5, 1313, 7, 4, '', 232, 4.5, '', '0000-00-00'),
(6, 1312, 323, 4.5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(7, 1320, 7, 3, '', 232, 4, '', '0000-00-00'),
(8, 1321, 7, 4.5, '', 232, 4, '', '0000-00-00'),
(9, 1322, 7, 4, '', 232, 4.5, '', '0000-00-00'),
(10, 1319, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(11, 1327, 7, 4, '', 232, 0, '', '0000-00-00'),
(12, 1328, 7, 4.5, '', 232, 0, '', '0000-00-00'),
(13, 1332, 7, 4.5, '', 212, 5, 'Static comment', '0000-00-00'),
(14, 1334, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(15, 1338, 323, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(16, 1343, 7, 4, '', 232, 4.5, '', '0000-00-00'),
(17, 1341, 323, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(18, 1346, 323, 4.5, '', 212, 5, 'Static comment', '0000-00-00'),
(19, 1349, 7, 5, '', 232, 4.5, '', '0000-00-00'),
(20, 1350, 7, 4.5, '', 232, 4, '', '0000-00-00'),
(21, 1351, 7, 4, '', 232, 4.5, '', '0000-00-00'),
(22, 1348, 323, 4.5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(23, 1353, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(24, 1356, 323, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(25, 1357, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(26, 1358, 419, 0, '', 282, 5, 'Static comment', '0000-00-00'),
(27, 1361, 323, 0, '', 212, 5, 'Static comment', '0000-00-00'),
(28, 1362, 61, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(29, 1363, 61, 5, '', 258, 4.5, 'Static comment', '0000-00-00'),
(30, 1366, 61, 4.5, '', 258, 4.5, 'Static comment', '0000-00-00'),
(31, 1368, 420, 4.5, '', 282, 5, 'Static comment', '0000-00-00'),
(32, 1367, 323, 5, '', 212, 5, 'Static comment', '0000-00-00'),
(33, 1380, 421, 4.5, '', 282, 5, 'Static comment', '0000-00-00'),
(34, 1386, 292, 5, '', 238, 0, '', '0000-00-00'),
(35, 1387, 61, 5, '', 258, 5, 'Static comment', '0000-00-00'),
(36, 1391, 424, 5, '', 254, 4, 'Static comment', '0000-00-00'),
(37, 1392, 424, 0, '', 254, 4, 'Static comment', '0000-00-00'),
(38, 1393, 424, 5, '', 254, 4.5, 'Static comment', '0000-00-00'),
(39, 1395, 424, 5, '', 254, 4.5, 'Static comment', '0000-00-00'),
(40, 1396, 424, 5, '', 254, 5, 'Static comment', '0000-00-00'),
(41, 1397, 424, 4.5, '', 254, 4.5, 'Static comment', '0000-00-00'),
(42, 1390, 7, 5, '', 232, 5, '', '0000-00-00'),
(43, 1403, 7, 4.5, '', 232, 4.5, '', '0000-00-00'),
(44, 1405, 7, 4.5, '', 232, 5, '', '0000-00-00'),
(45, 1404, 323, 5, '', 212, 4.5, 'Static comment', '0000-00-00'),
(46, 1410, 7, 4.5, '', 232, 4, '', '0000-00-00'),
(47, 1411, 7, 5, '', 232, 5, '', '0000-00-00'),
(48, 1415, 7, 3.5, '', 232, 4.5, '', '2017-08-26'),
(49, 1416, 7, 4.5, '', 232, 5, '', '2017-08-26'),
(50, 1417, 421, 5, '', 282, 5, 'Static comment', '2017-08-26'),
(51, 1419, 7, 5, '', 232, 4.5, '', '2017-08-26'),
(52, 1421, 7, 0, '', 232, 4.5, '', '2017-08-26'),
(53, 1422, 7, 0, '', 232, 4.5, '', '2017-08-26'),
(54, 1423, 323, 5, 'hshdhisjdidj', 212, 4.5, 'Static comment', '2017-08-26'),
(55, 1425, 323, 4.5, 'hshshhdbbx', 212, 5, 'Static comment', '2017-08-26'),
(56, 1427, 323, 4, 'yyujjj', 212, 5, 'Static commentffgvvc', '2017-08-26'),
(57, 1428, 7, 0, '', 232, 4.5, '', '2017-08-26'),
(58, 1429, 7, 0, '', 232, 4.5, 'dhxhgsfahxcnxgfuvjbk k ', '2017-08-26'),
(59, 1430, 7, 5, 'good work', 232, 4, '', '2017-08-26'),
(60, 1431, 7, 4, 'geerr', 232, 5, 'dhhchccj', '2017-08-26'),
(61, 1433, 425, 5, '', 287, 5, 'Static comment', '2017-08-26'),
(62, 1437, 61, 5, '', 212, 5, 'dhdhd', '2017-08-26'),
(63, 1440, 61, 1.5, '', 212, 5, '', '2017-08-26'),
(64, 1441, 426, 2, '', 286, 4, 'Static comment', '2017-08-26'),
(65, 1451, 427, 5, '', 292, 5, 'Static comment', '2017-08-27'),
(66, 1454, 427, 5, 'hjiiiioi', 292, 5, 'Static comment', '2017-08-27'),
(67, 1463, 429, 4, '', 294, 5, '', '2017-08-27'),
(68, 1464, 429, 5, 'nice ride', 294, 5, 'good customer', '2017-08-27'),
(69, 1465, 429, 5, '', 294, 5, '', '2017-08-27'),
(70, 1466, 429, 3.5, '', 294, 5, '', '2017-08-27'),
(71, 1468, 429, 4.5, '', 294, 5, '', '2017-08-27'),
(72, 1469, 7, 5, '', 294, 5, '', '2017-08-27'),
(73, 1473, 430, 3.5, '', 286, 4.5, 'Static comment', '2017-08-27'),
(74, 1474, 432, 5, 'hdjr', 286, 4, 'Static comment', '2017-08-27'),
(75, 1487, 191, 5, '', 296, 4.5, 'Static comment', '2017-08-28'),
(76, 1488, 429, 5, '', 294, 5, '', '2017-08-28'),
(77, 1489, 429, 5, '', 294, 5, '', '2017-08-28'),
(78, 1490, 7, 5, '', 232, 5, 'Stdydyuf', '2017-08-28'),
(79, 1491, 7, 4, '', 232, 4, '', '2017-08-28'),
(80, 1492, 7, 5, '', 302, 4, '', '2017-08-28'),
(81, 1493, 435, 4.5, '', 303, 4, 'Hi', '2017-08-28'),
(82, 1494, 435, 4.5, '', 303, 4, 'H', '2017-08-28'),
(83, 1501, 435, 4, '', 303, 3.5, '', '2017-08-28'),
(84, 1495, 323, 5, '', 212, 5, '', '2017-08-28'),
(85, 1502, 323, 5, '', 212, 5, '', '2017-08-28'),
(86, 1506, 435, 3.5, '', 303, 4, '', '2017-08-28'),
(87, 1508, 435, 4, '', 303, 4, '', '2017-08-28'),
(88, 1509, 435, 4, '', 303, 4.5, '', '2017-08-28'),
(89, 1510, 435, 5, '', 303, 4, '', '2017-08-28'),
(90, 1507, 323, 4.5, '', 212, 5, 'xhchxhxh', '2017-08-28'),
(91, 1512, 323, 5, '', 212, 5, '', '2017-08-28'),
(92, 1513, 323, 0, '', 212, 5, '', '2017-08-28'),
(93, 1514, 323, 0, '', 212, 4.5, '', '2017-08-28'),
(94, 1515, 323, 5, '', 212, 0, '', '2017-08-28'),
(95, 1516, 323, 5, '', 212, 0, '', '2017-08-28'),
(96, 1517, 323, 5, '', 212, 5, '', '2017-08-28'),
(97, 1518, 323, 5, '', 212, 5, '', '2017-08-28'),
(98, 1519, 323, 4.5, '', 212, 5, '', '2017-08-28'),
(99, 1520, 323, 5, '', 212, 5, '', '2017-08-28'),
(100, 1521, 323, 5, '', 212, 5, '', '2017-08-28'),
(101, 1522, 323, 4.5, '', 212, 4.5, '', '2017-08-28'),
(102, 1523, 323, 4.5, '', 212, 4.5, '', '2017-08-28'),
(103, 1524, 435, 4, '', 303, 4, '', '2017-08-28'),
(104, 1525, 323, 5, '', 212, 4.5, '', '2017-08-28'),
(105, 1526, 323, 4.5, '', 212, 5, '', '2017-08-28'),
(106, 1527, 435, 4, '', 232, 5, '', '2017-08-28'),
(107, 1529, 435, 4, '', 232, 4, '', '2017-08-28'),
(108, 1528, 323, 5, '', 212, 4.5, '', '2017-08-28'),
(109, 1530, 409, 5, '', 154, 5, 'Static comment', '2017-08-28'),
(110, 1531, 61, 2.5, '', 306, 3.5, '', '2017-08-28'),
(111, 1532, 61, 2.5, '', 306, 2.5, '', '2017-08-28'),
(112, 1536, 61, 5, '', 306, 4.5, '', '2017-08-28'),
(113, 1470, 359, 3.5, '', 241, 0, '', '2017-08-28'),
(114, 1538, 441, 4, '', 286, 0, '', '2017-08-28'),
(115, 1539, 442, 3.5, '', 247, 3, 'Static comment', '2017-08-29'),
(116, 1540, 442, 0, '', 247, 2.5, 'Static comment', '2017-08-29'),
(117, 1541, 442, 0, '', 247, 2.5, 'Static comment', '2017-08-29'),
(118, 1535, 244, 5, 'great!', 169, 5, 'ok', '2017-08-29'),
(119, 1547, 244, 5, '', 169, 1.5, '', '2017-08-29'),
(120, 1548, 244, 3.5, '', 169, 5, '', '2017-08-29'),
(121, 1549, 443, 5, '', 306, 5, '', '2017-08-29'),
(122, 1551, 61, 5, '', 306, 4.5, '', '2017-08-29'),
(123, 1553, 61, 5, '', 306, 5, '', '2017-08-29'),
(124, 1554, 61, 4.5, '', 306, 5, '', '2017-08-29'),
(125, 1197, 360, 0, '', 140, 4, '', '2017-08-29'),
(126, 1555, 360, 5, '', 140, 5, '', '2017-08-29'),
(127, 1559, 447, 1, '', 311, 0, '', '2017-08-29'),
(128, 1560, 447, 0, '', 311, 3, '', '2017-08-29'),
(129, 1569, 450, 5, '', 282, 5, 'nice', '2017-08-29'),
(130, 1572, 411, 0, '', 315, 4, '', '2017-08-29'),
(131, 1574, 411, 0, '', 315, 4, '', '2017-08-29'),
(132, 1579, 454, 0, '', 316, 5, '', '2017-08-29'),
(133, 1584, 150, 5, '', 318, 0, '', '2017-08-30'),
(134, 1596, 61, 1.5, '', 95, 5, '', '2017-08-30'),
(135, 1597, 244, 5, '', 169, 5, '', '2017-08-30'),
(136, 1599, 150, 0, '', 318, 5, '', '2017-08-30'),
(137, 1600, 150, 4.5, '', 318, 5, '', '2017-08-30'),
(138, 1601, 409, 5, '', 154, 5, '', '2017-08-30'),
(139, 1602, 411, 5, '', 321, 4, '', '2017-08-30'),
(140, 1615, 411, 4, '', 321, 0, '', '2017-08-30'),
(141, 1635, 435, 4.5, '', 232, 4.5, '', '2017-08-30'),
(142, 1639, 435, 4, '', 232, 4.5, '', '2017-08-30'),
(143, 1640, 435, 4, '', 232, 5, '', '2017-08-30'),
(144, 1645, 435, 4, '', 232, 5, '', '2017-08-30'),
(145, 1647, 435, 4, '', 232, 4.5, '', '2017-08-30'),
(146, 1649, 462, 3.5, '', 325, 0, '', '2017-08-30'),
(147, 1651, 435, 5, '', 232, 5, '', '2017-08-30'),
(148, 1656, 411, 0, '', 325, 4, '', '2017-08-30'),
(149, 1660, 411, 0, '', 325, 4, '', '2017-08-30'),
(150, 1671, 435, 4.5, '', 232, 5, '', '2017-08-31'),
(151, 1672, 435, 5, '', 232, 5, '', '2017-08-31'),
(152, 1675, 323, 5, '', 212, 5, '', '2017-08-31'),
(153, 1677, 435, 4, '', 232, 3.5, '', '2017-08-31'),
(154, 1678, 435, 5, '', 232, 5, '', '2017-08-31'),
(155, 1676, 465, 4.5, '', 212, 4.5, '', '2017-08-31'),
(156, 1681, 465, 5, '', 212, 5, '', '2017-08-31'),
(157, 1683, 465, 4, '', 212, 5, '', '2017-08-31'),
(158, 1682, 435, 5, '', 232, 4, '', '2017-08-31'),
(159, 1684, 465, 4, '', 212, 5, '', '2017-08-31'),
(160, 1685, 465, 5, '', 212, 5, '', '2017-08-31'),
(161, 1686, 435, 5, '', 232, 4.5, '', '2017-08-31'),
(162, 1687, 435, 4, '', 232, 5, '', '2017-08-31'),
(163, 1689, 465, 4.5, '', 212, 5, '', '2017-08-31'),
(164, 1691, 465, 5, '', 212, 5, '', '2017-08-31'),
(165, 1688, 435, 4.5, '', 232, 5, '', '2017-08-31'),
(166, 1693, 435, 5, '', 232, 5, '', '2017-08-31'),
(167, 1694, 435, 4.5, '', 232, 5, '', '2017-08-31'),
(168, 1695, 435, 3, '', 232, 5, '', '2017-08-31'),
(169, 1697, 465, 5, '', 212, 4.5, '', '2017-08-31'),
(170, 1696, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(171, 1700, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(172, 1699, 323, 5, '', 212, 5, '', '2017-08-31'),
(173, 1701, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(174, 1702, 323, 5, 'n7b7hh', 212, 5, '', '2017-08-31'),
(175, 1703, 435, 5, '', 232, 4.5, '', '2017-08-31'),
(176, 1704, 323, 5, '', 212, 4.5, '', '2017-08-31'),
(177, 1706, 323, 5, '', 212, 4.5, '', '2017-08-31'),
(178, 1705, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(179, 1707, 435, 4.5, '', 232, 4, '', '2017-08-31'),
(180, 1708, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(181, 1709, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(182, 1710, 435, 5, '', 232, 5, '', '2017-08-31'),
(183, 1711, 435, 4, '', 232, 3.5, '', '2017-08-31'),
(184, 1712, 435, 4, '', 232, 4, '', '2017-08-31'),
(185, 1713, 435, 0, '', 232, 5, '', '2017-08-31'),
(186, 1714, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(187, 1715, 435, 4, '', 232, 4, '', '2017-08-31'),
(188, 1716, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(189, 1718, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(190, 1719, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(191, 1723, 435, 4.5, '', 232, 4, '', '2017-08-31'),
(192, 1724, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(193, 1722, 411, 0, '', 325, 4, 'buhvug', '2017-08-31'),
(194, 1725, 435, 4, '', 232, 4.5, '', '2017-08-31'),
(195, 1726, 435, 3.5, '', 232, 4.5, '', '2017-08-31'),
(196, 1727, 323, 5, '', 212, 5, '', '2017-08-31'),
(197, 1728, 435, 4, '', 232, 5, '', '2017-08-31'),
(198, 1729, 435, 4.5, '', 232, 5, '', '2017-08-31'),
(199, 1730, 411, 0, '', 329, 4, '', '2017-08-31'),
(200, 1731, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(201, 1738, 435, 4.5, '', 232, 4.5, '', '2017-08-31'),
(202, 1740, 413, 0, '', 250, 5, '', '2017-08-31'),
(203, 1741, 468, 5, '', 212, 3.5, '', '2017-08-31'),
(204, 1742, 468, 5, '', 212, 4, '', '2017-08-31'),
(205, 1743, 413, 5, '', 250, 5, '', '2017-09-01'),
(206, 1747, 471, 4.5, '', 331, 2, '', '2017-09-01'),
(207, 1748, 471, 0, '', 331, 4.5, '', '2017-09-01'),
(208, 1750, 464, 5, 'great', 169, 0, '', '2017-09-01'),
(209, 1751, 435, 4, '', 232, 5, '', '2017-09-01'),
(210, 1754, 323, 5, '', 212, 5, '', '2017-09-01'),
(211, 1756, 323, 5, '', 212, 5, '', '2017-09-01'),
(212, 1758, 468, 5, '', 212, 5, '', '2017-09-01'),
(213, 1759, 468, 5, '', 212, 4.5, '', '2017-09-01'),
(214, 1760, 468, 5, '', 212, 5, '', '2017-09-01'),
(215, 1761, 61, 5, '', 294, 5, '', '2017-09-01'),
(216, 1762, 468, 4.5, '', 212, 4.5, '', '2017-09-01'),
(217, 1763, 468, 5, '', 212, 5, '', '2017-09-01'),
(218, 1765, 468, 1, '', 212, 5, '', '2017-09-01'),
(219, 1766, 468, 0, '', 212, 5, '', '2017-09-01'),
(220, 1767, 468, 3, '', 212, 5, '', '2017-09-01'),
(221, 1768, 468, 1.5, '', 212, 5, '', '2017-09-01'),
(222, 1769, 468, 0, '', 212, 5, '', '2017-09-01'),
(223, 1772, 435, 4.5, '', 232, 5, '', '2017-09-01'),
(224, 1773, 468, 5, 'hsdhdjdjdjdudj', 212, 5, 'nbh', '2017-09-01'),
(225, 1774, 254, 0, '', 316, 5, '', '2017-09-01'),
(226, 1775, 468, 5, '', 212, 5, '', '2017-09-01'),
(227, 1779, 254, 5, '', 316, 5, '', '2017-09-01'),
(228, 1778, 468, 5, '', 212, 5, '', '2017-09-01'),
(229, 1783, 474, 5, '', 333, 5, '', '2017-09-01'),
(230, 1784, 476, 0, '', 333, 4.5, '', '2017-09-01'),
(231, 1786, 361, 5, '', 235, 0, '', '2017-09-01'),
(232, 1787, 474, 4.5, '', 333, 0, '', '2017-09-01'),
(233, 1789, 360, 4, '', 140, 4, '', '2017-09-02'),
(234, 1794, 359, 3, '', 336, 2.5, '', '2017-09-02'),
(235, 1795, 474, 5, '', 333, 5, '', '2017-09-02'),
(236, 1796, 468, 5, '', 212, 5, '', '2017-09-02'),
(237, 1800, 480, 5, '', 337, 5, '', '2017-09-02'),
(238, 1801, 479, 5, '', 338, 5, '', '2017-09-02'),
(239, 1803, 479, 4, '', 338, 5, '', '2017-09-02'),
(240, 1804, 479, 5, '', 338, 5, '', '2017-09-02'),
(241, 1805, 479, 4, '', 338, 5, '', '2017-09-02'),
(242, 1806, 479, 4.5, '', 338, 4.5, '', '2017-09-02'),
(243, 1807, 479, 4.5, '', 338, 5, '', '2017-09-02'),
(244, 1808, 480, 5, '', 337, 5, '', '2017-09-02'),
(245, 1809, 479, 5, 'Chufgigijvjv', 338, 5, 'jcgjkg', '2017-09-02'),
(246, 1812, 479, 5, '', 338, 5, '', '2017-09-02'),
(247, 1813, 479, 4.5, '', 338, 4.5, '', '2017-09-02'),
(248, 1814, 479, 4.5, 'NBC', 338, 5, '', '2017-09-02'),
(249, 1815, 479, 4.5, '', 338, 5, '', '2017-09-02'),
(250, 1816, 479, 4.5, '', 338, 4.5, '', '2017-09-02'),
(251, 1821, 468, 5, '', 212, 5, '', '2017-09-02'),
(252, 1822, 468, 4.5, '', 212, 5, '', '2017-09-02'),
(253, 1820, 413, 5, '', 250, 5, '', '2017-09-02'),
(254, 1824, 360, 5, '', 140, 5, '', '2017-09-02'),
(255, 1835, 7, 4.5, '', 338, 4.5, '', '2017-09-02'),
(256, 1836, 468, 4.5, 'dffcf', 212, 4.5, '', '2017-09-02'),
(257, 1837, 7, 5, '', 338, 4.5, '', '2017-09-02'),
(258, 1839, 468, 0, '', 212, 4, '', '2017-09-02'),
(259, 1840, 7, 4.5, '', 338, 4.5, '', '2017-09-02'),
(260, 1842, 491, 5, '', 342, 5, '', '2017-09-02'),
(261, 1843, 493, 5, '', 212, 5, '', '2017-09-02'),
(262, 1845, 495, 4.5, '', 344, 4.5, '', '2017-09-02'),
(263, 1849, 495, 4.5, '', 344, 4, '', '2017-09-02'),
(264, 1862, 496, 4.5, '', 345, 5, '', '2017-09-02'),
(265, 1863, 360, 4, '', 140, 5, '', '2017-09-03'),
(266, 1869, 487, 0, '', 146, 5, '', '2017-09-03'),
(267, 1870, 487, 0, '', 146, 5, '', '2017-09-03'),
(268, 1872, 333, 5, '', 220, 3.5, '', '2017-09-03'),
(269, 1871, 487, 5, '', 146, 5, 'in night', '2017-09-03'),
(270, 1877, 487, 0, '', 212, 5, '', '2017-09-03'),
(271, 1878, 487, 5, '', 212, 5, '', '2017-09-03'),
(272, 1879, 468, 4.5, '', 212, 4.5, '', '2017-09-03'),
(273, 1880, 468, 3.5, '', 212, 5, '', '2017-09-03'),
(274, 1881, 468, 4.5, '', 212, 4.5, '', '2017-09-03'),
(275, 1882, 468, 5, '', 212, 5, '', '2017-09-03'),
(276, 1883, 468, 5, '', 212, 4.5, '', '2017-09-03'),
(277, 1887, 468, 5, '', 212, 5, '', '2017-09-03'),
(278, 1888, 360, 4, '', 140, 5, '', '2017-09-03'),
(279, 1895, 464, 2, 'fddvggcff', 169, 2.5, 'f', '2017-09-04'),
(280, 1899, 504, 5, '', 348, 5, '', '2017-09-04'),
(281, 1901, 504, 5, '', 350, 5, '', '2017-09-04'),
(282, 1906, 323, 5, '', 212, 5, '', '2017-09-04'),
(283, 1907, 323, 4.5, '', 212, 4.5, '', '2017-09-04'),
(284, 141, 46, 5, '', 31, 5, '', '2017-09-04'),
(285, 1909, 46, 5, '', 358, 0, '', '2017-09-04'),
(286, 1911, 436, 5, '', 359, 0, '', '2017-09-04'),
(287, 1916, 46, 4.5, '', 358, 5, '', '2017-09-04'),
(288, 1918, 46, 5, '', 360, 3.5, '', '2017-09-04'),
(289, 1919, 46, 5, '', 362, 0, '', '2017-09-04'),
(290, 1912, 46, 0, '', 359, 5, '', '2017-09-04'),
(291, 1921, 413, 5, '', 235, 5, '', '2017-09-04'),
(292, 1923, 7, 4.5, '', 359, 5, '', '2017-09-04'),
(293, 1924, 323, 5, '', 212, 5, '', '2017-09-04'),
(294, 1926, 510, 0, '', 365, 5, '', '2017-09-04'),
(295, 1934, 510, 3.5, '', 367, 3, '', '2017-09-04'),
(296, 1935, 510, 4, '', 367, 4, '', '2017-09-04'),
(297, 1937, 510, 0, '', 367, 5, '', '2017-09-04'),
(298, 1936, 510, 0, '', 365, 3, '', '2017-09-05'),
(299, 1941, 240, 5, '', 96, 5, '', '2017-09-05'),
(300, 1942, 240, 5, '', 96, 5, '', '2017-09-05'),
(301, 1943, 514, 5, '', 368, 0, '', '2017-09-05'),
(302, 1944, 514, 5, '', 368, 5, '', '2017-09-05'),
(303, 1945, 515, 0, '', 294, 4, '', '2017-09-05'),
(304, 1947, 7, 4.5, '', 338, 4.5, '', '2017-09-05'),
(305, 1948, 7, 0, '', 338, 5, '', '2017-09-05'),
(306, 1949, 7, 4.5, '', 338, 4.5, '', '2017-09-05'),
(307, 1950, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(308, 1951, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(309, 1952, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(310, 1953, 7, 4.5, '', 338, 4.5, '', '2017-09-05'),
(311, 1955, 7, 4, '', 338, 5, '', '2017-09-05'),
(312, 1956, 7, 5, '', 338, 5, '', '2017-09-05'),
(313, 1961, 7, 5, '', 338, 5, '', '2017-09-05'),
(314, 1962, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(315, 1963, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(316, 1964, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(317, 1966, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(318, 1967, 7, 5, '', 338, 5, '', '2017-09-05'),
(319, 1968, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(320, 1969, 7, 4.5, '', 338, 4, '', '2017-09-05'),
(321, 1965, 520, 0, '', 212, 5, '', '2017-09-05'),
(322, 1973, 7, 4.5, '', 338, 4, '', '2017-09-05'),
(323, 1974, 7, 3.5, '', 338, 5, '', '2017-09-05'),
(324, 1976, 520, 4.5, '', 212, 5, '', '2017-09-05'),
(325, 1975, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(326, 1978, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(327, 1977, 520, 4.5, '', 212, 5, '', '2017-09-05'),
(328, 1979, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(329, 1980, 7, 4, '', 338, 5, '', '2017-09-05'),
(330, 1981, 7, 5, '', 338, 5, '', '2017-09-05'),
(331, 1982, 7, 5, '', 338, 4.5, '', '2017-09-05'),
(332, 1984, 7, 5, '', 338, 5, '', '2017-09-05'),
(333, 1986, 520, 5, '', 212, 4.5, '', '2017-09-05'),
(334, 1987, 7, 4.5, '', 338, 5, '', '2017-09-05'),
(335, 1989, 524, 5, '', 372, 5, 'Ho', '2017-09-05'),
(336, 1990, 524, 5, '', 372, 4, '', '2017-09-05'),
(337, 1988, 520, 4.5, '', 212, 5, '', '2017-09-05'),
(338, 1992, 524, 5, 'NBC', 372, 4.5, 'NBC', '2017-09-05'),
(339, 1994, 524, 4, '', 372, 4.5, '', '2017-09-05'),
(340, 1995, 524, 4.5, '', 372, 5, '', '2017-09-05'),
(341, 1996, 524, 5, '', 372, 4.5, '', '2017-09-05'),
(342, 1998, 524, 4.5, '', 372, 5, '', '2017-09-05'),
(343, 1999, 524, 5, '', 372, 4.5, '', '2017-09-05'),
(344, 2000, 524, 4.5, '', 372, 5, '', '2017-09-05'),
(345, 2005, 525, 4.5, '', 310, 4.5, '', '2017-09-05'),
(346, 2010, 525, 5, '', 310, 4.5, '', '2017-09-05'),
(347, 2011, 525, 5, '', 310, 5, '', '2017-09-05'),
(348, 2024, 525, 4.5, '', 310, 0, '', '2017-09-05'),
(349, 2025, 525, 0, '', 310, 4.5, '', '2017-09-05'),
(350, 2027, 525, 4, '', 310, 4.5, '', '2017-09-05'),
(351, 2028, 525, 4.5, '', 310, 5, '', '2017-09-05'),
(352, 2029, 520, 4.5, '', 212, 4.5, '', '2017-09-05'),
(353, 2032, 528, 5, '', 377, 5, '', '2017-09-05'),
(354, 2034, 528, 5, '', 377, 5, '', '2017-09-05'),
(355, 2039, 530, 5, '', 379, 5, '', '2017-09-06'),
(356, 2040, 532, 5, '', 379, 5, '', '2017-09-06'),
(357, 2041, 532, 4.5, '', 381, 5, '', '2017-09-06'),
(358, 2042, 532, 5, '', 381, 5, '', '2017-09-06'),
(359, 2043, 532, 0.5, '', 384, 5, '', '2017-09-06'),
(360, 2050, 520, 5, '', 212, 4.5, '', '2017-09-06'),
(361, 2051, 520, 5, '', 212, 5, '', '2017-09-06'),
(362, 2061, 525, 4.5, '', 310, 4, '', '2017-09-06'),
(363, 2062, 525, 4.5, '', 310, 4, '', '2017-09-06'),
(364, 2070, 546, 4.5, '', 338, 5, '', '2017-09-06'),
(365, 2069, 545, 4.5, '', 212, 5, '', '2017-09-06'),
(366, 2071, 546, 4.5, '', 338, 5, '', '2017-09-06'),
(367, 2072, 546, 5, '', 338, 4.5, '', '2017-09-06'),
(368, 2074, 546, 5, '', 338, 5, '', '2017-09-06'),
(369, 2076, 546, 4.5, '', 338, 5, '', '2017-09-06'),
(370, 2077, 546, 4.5, '', 338, 4.5, '', '2017-09-06'),
(371, 2073, 436, 5, '', 212, 5, '', '2017-09-06'),
(372, 2079, 546, 5, '', 338, 4, '', '2017-09-06'),
(373, 2081, 546, 4, '', 390, 5, '', '2017-09-06'),
(374, 2083, 546, 4.5, '', 338, 5, '', '2017-09-06'),
(375, 2090, 548, 4.5, '', 390, 5, '', '2017-09-06'),
(376, 2103, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(377, 2108, 549, 4, '', 338, 4.5, '', '2017-09-06'),
(378, 2110, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(379, 2112, 549, 5, '', 338, 5, '', '2017-09-06'),
(380, 2116, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(381, 2118, 549, 4, '', 338, 4, '', '2017-09-06'),
(382, 2119, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(383, 2121, 549, 5, '', 338, 5, '', '2017-09-06'),
(384, 2124, 549, 4, '', 338, 5, '', '2017-09-06'),
(385, 2125, 545, 5, '', 390, 5, '', '2017-09-06'),
(386, 2137, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(387, 2138, 549, 5, '', 338, 5, '', '2017-09-06'),
(388, 2143, 549, 5, '', 338, 5, '', '2017-09-06'),
(389, 2145, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(390, 2147, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(391, 2150, 549, 5, '', 338, 5, '', '2017-09-06'),
(392, 2153, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(393, 2155, 549, 5, '', 338, 5, '', '2017-09-06'),
(394, 2157, 549, 4, '', 338, 5, '', '2017-09-06'),
(395, 2160, 551, 5, '', 390, 4.5, '', '2017-09-06'),
(396, 2162, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(397, 2163, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(398, 2161, 551, 4.5, '', 390, 5, '', '2017-09-06'),
(399, 2165, 551, 5, '', 390, 5, '', '2017-09-06'),
(400, 2166, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(401, 2168, 551, 4.5, '', 390, 5, '', '2017-09-06'),
(402, 2169, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(403, 2171, 549, 5, '', 338, 5, '', '2017-09-06'),
(404, 2172, 552, 5, '', 397, 4.5, '', '2017-09-06'),
(405, 2174, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(406, 2175, 551, 5, '', 390, 5, '', '2017-09-06'),
(407, 2177, 551, 5, '', 390, 5, '', '2017-09-06'),
(408, 2176, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(409, 2178, 551, 4.5, '', 390, 5, '', '2017-09-06'),
(410, 2179, 551, 4, '', 390, 5, '', '2017-09-06'),
(411, 2180, 549, 5, '', 338, 5, '', '2017-09-06'),
(412, 2173, 552, 4, '', 397, 0, '', '2017-09-06'),
(413, 2181, 551, 3.5, '', 390, 5, '', '2017-09-06'),
(414, 2182, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(415, 2183, 551, 4.5, '', 390, 4.5, '', '2017-09-06'),
(416, 2184, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(417, 2186, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(418, 2188, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(419, 2190, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(420, 2193, 549, 5, '', 338, 4, '', '2017-09-06'),
(421, 2196, 549, 5, '', 338, 5, '', '2017-09-06'),
(422, 2197, 552, 5, '', 397, 5, '', '2017-09-06'),
(423, 2199, 552, 4.5, '', 397, 3, '', '2017-09-06'),
(424, 2200, 552, 2, '', 397, 2, '', '2017-09-06'),
(425, 2201, 552, 5, '', 397, 5, '', '2017-09-06'),
(426, 2202, 552, 0, '', 397, 5, '', '2017-09-06'),
(427, 2203, 552, 4.5, '', 397, 5, '', '2017-09-06'),
(428, 2204, 549, 5, '', 338, 5, '', '2017-09-06'),
(429, 2209, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(430, 2211, 549, 0, '', 338, 4.5, '', '2017-09-06'),
(431, 2212, 549, 5, '', 338, 4.5, '', '2017-09-06'),
(432, 2213, 549, 4.5, '', 338, 5, '', '2017-09-06'),
(433, 2214, 549, 4, '', 338, 5, '', '2017-09-06'),
(434, 2215, 549, 4.5, '', 338, 4.5, '', '2017-09-06'),
(435, 2216, 549, 4, '', 338, 5, '', '2017-09-06'),
(436, 2217, 554, 5, '', 399, 5, '', '2017-09-06'),
(437, 2218, 554, 4, '', 399, 4.5, '', '2017-09-06'),
(438, 2219, 549, 5, '', 338, 5, '', '2017-09-06'),
(439, 2220, 549, 4, '', 338, 5, '', '2017-09-06'),
(440, 2221, 549, 4, '', 338, 5, '', '2017-09-06'),
(441, 2222, 536, 5, '', 384, 5, 'hhh', '2017-09-06'),
(442, 2223, 556, 5, '', 403, 4.5, '', '2017-09-06'),
(443, 2224, 556, 5, '', 401, 5, '', '2017-09-06'),
(444, 2225, 555, 5, '', 401, 5, '', '2017-09-06'),
(445, 2226, 555, 5, '', 401, 1.5, '', '2017-09-06'),
(446, 2227, 555, 5, '', 400, 5, '', '2017-09-06'),
(447, 2229, 556, 3, '', 400, 5, '', '2017-09-06'),
(448, 2232, 558, 5, '', 400, 0, '', '2017-09-06'),
(449, 2235, 555, 5, '', 401, 5, '', '2017-09-07'),
(450, 2238, 556, 5, '', 401, 5, '', '2017-09-07'),
(451, 2104, 436, 0, '', 391, 4.5, '', '2017-09-07'),
(452, 2254, 514, 4, '', 368, 4, '', '2017-09-07'),
(453, 2258, 549, 5, '', 338, 4, '', '2017-09-07'),
(454, 2261, 549, 5, '', 338, 5, '', '2017-09-07'),
(455, 2263, 549, 5, '', 338, 5, '', '2017-09-07'),
(456, 2266, 549, 5, '', 338, 5, '', '2017-09-07'),
(457, 2279, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(458, 2283, 549, 5, '', 338, 5, '', '2017-09-07'),
(459, 2285, 436, 5, '', 391, 5, '', '2017-09-07'),
(460, 2292, 566, 0, '', 408, 5, 'very bad', '2017-09-07'),
(461, 2297, 436, 5, '', 391, 5, '', '2017-09-07'),
(462, 2316, 360, 4, '', 140, 4.5, '', '2017-09-07'),
(463, 2318, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(464, 2325, 549, 0, '', 338, 5, '', '2017-09-07'),
(465, 2326, 549, 4, '', 338, 4.5, '', '2017-09-07'),
(466, 2327, 549, 4, '', 338, 5, '', '2017-09-07'),
(467, 2333, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(468, 2338, 436, 5, '', 409, 5, 'shxuffjcj', '2017-09-07'),
(469, 2343, 545, 5, '', 409, 5, '', '2017-09-07'),
(470, 2346, 549, 5, '', 338, 5, '', '2017-09-07'),
(471, 2349, 549, 4, '', 338, 4, '', '2017-09-07'),
(472, 2350, 549, 5, '', 338, 4, '', '2017-09-07'),
(473, 2351, 549, 5, '', 338, 5, '', '2017-09-07'),
(474, 2355, 549, 5, '', 338, 4.5, '', '2017-09-07'),
(475, 2356, 558, 5, '', 95, 5, '', '2017-09-07'),
(476, 2357, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(477, 2360, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(478, 2366, 549, 4, '', 338, 4.5, '', '2017-09-07'),
(479, 2368, 549, 4, '', 338, 4, '', '2017-09-07'),
(480, 2369, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(481, 2373, 549, 5, '', 338, 4.5, '', '2017-09-07'),
(482, 2367, 545, 5, '', 390, 5, '', '2017-09-07'),
(483, 2380, 549, 5, '', 338, 5, '', '2017-09-07'),
(484, 2381, 549, 4, '', 338, 5, '', '2017-09-07'),
(485, 2383, 549, 4, '', 338, 4, '', '2017-09-07'),
(486, 2384, 549, 5, '', 338, 5, '', '2017-09-07'),
(487, 2385, 549, 5, '', 338, 5, '', '2017-09-07'),
(488, 2386, 549, 4, '', 338, 5, '', '2017-09-07'),
(489, 2387, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(490, 2388, 549, 4.5, '', 338, 4.5, '', '2017-09-07'),
(491, 2389, 549, 4, '', 338, 5, '', '2017-09-07'),
(492, 2390, 549, 4, '', 338, 5, '', '2017-09-07'),
(493, 2393, 549, 4, '', 338, 4.5, '', '2017-09-07'),
(494, 2396, 545, 5, 'hghhuh', 390, 5, '', '2017-09-07'),
(495, 2403, 528, 5, '', 376, 5, '', '2017-09-07'),
(496, 2406, 528, 3, '', 376, 3, '', '2017-09-07'),
(497, 2410, 569, 4.5, '', 415, 5, '', '2017-09-07'),
(498, 2411, 528, 3, '', 376, 2, '', '2017-09-07'),
(499, 2413, 569, 5, '', 415, 5, '', '2017-09-07'),
(500, 2414, 528, 3, '', 376, 4, '', '2017-09-07'),
(501, 2415, 528, 0, '', 376, 3.5, '', '2017-09-07'),
(502, 2417, 569, 4.5, '', 390, 5, '', '2017-09-07'),
(503, 2422, 545, 4.5, '', 390, 4.5, '', '2017-09-07'),
(504, 2423, 549, 4.5, '', 338, 4, '', '2017-09-07'),
(505, 2424, 549, 4.5, '', 338, 5, '', '2017-09-07'),
(506, 2425, 549, 5, '', 338, 5, '', '2017-09-07'),
(507, 2426, 545, 5, '', 390, 5, '', '2017-09-07'),
(508, 2430, 545, 5, '', 390, 5, '', '2017-09-07'),
(509, 2431, 582, 5, '', 254, 4.5, '', '2017-09-07'),
(510, 2432, 582, 5, '', 265, 5, '', '2017-09-07'),
(511, 2433, 582, 5, '', 265, 4.5, '', '2017-09-07'),
(512, 2434, 582, 5, '', 265, 5, '', '2017-09-07'),
(513, 2435, 582, 5, '', 420, 5, '', '2017-09-07'),
(514, 2436, 582, 5, '', 420, 4.5, '', '2017-09-07'),
(515, 2437, 582, 1.5, '', 420, 5, '', '2017-09-07'),
(516, 2441, 583, 5, '', 420, 5, '', '2017-09-07'),
(517, 2449, 586, 4.5, '', 422, 2, '', '2017-09-07'),
(518, 2453, 545, 5, '', 390, 5, '', '2017-09-08'),
(519, 2454, 583, 5, '', 420, 4.5, '', '2017-09-08'),
(520, 2456, 545, 3.5, '', 390, 4, '', '2017-09-08'),
(521, 2458, 545, 5, '', 390, 4, '', '2017-09-08'),
(522, 2461, 569, 5, '', 409, 5, '', '2017-09-08'),
(523, 2462, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(524, 2463, 586, 4, '', 422, 5, '', '2017-09-08'),
(525, 2464, 586, 5, '', 422, 5, '', '2017-09-08'),
(526, 2466, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(527, 2471, 586, 3, '', 422, 4.5, '', '2017-09-08'),
(528, 2472, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(529, 2473, 528, 1.5, '', 376, 5, '', '2017-09-08'),
(530, 2475, 545, 5, '', 390, 5, '', '2017-09-08'),
(531, 2474, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(532, 2480, 590, 4, '', 410, 5, '', '2017-09-08'),
(533, 2481, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(534, 2482, 586, 4, '', 422, 4, '', '2017-09-08'),
(535, 2483, 586, 5, '', 422, 5, '', '2017-09-08'),
(536, 2484, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(537, 2485, 586, 5, '', 422, 5, '', '2017-09-08'),
(538, 2486, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(539, 2487, 586, 5, '', 422, 4.5, '', '2017-09-08'),
(540, 2488, 586, 5, '', 422, 5, '', '2017-09-08'),
(541, 2489, 586, 5, '', 422, 4.5, '', '2017-09-08'),
(542, 2470, 569, 0, '', 409, 5, '', '2017-09-08'),
(543, 2492, 569, 0, '', 409, 5, '', '2017-09-08'),
(544, 2491, 586, 4, '', 422, 5, '', '2017-09-08'),
(545, 2494, 569, 0, '', 409, 5, '', '2017-09-08'),
(546, 2499, 586, 4, '', 422, 5, '', '2017-09-08'),
(547, 2498, 569, 0, '', 409, 5, '', '2017-09-08'),
(548, 2504, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(549, 2505, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(550, 2506, 586, 5, '', 422, 5, '', '2017-09-08'),
(551, 2508, 586, 4, '', 422, 5, '', '2017-09-08'),
(552, 2511, 586, 5, '', 422, 5, '', '2017-09-08'),
(553, 2512, 586, 4, '', 422, 4.5, '', '2017-09-08'),
(554, 2510, 569, 0, '', 414, 5, '', '2017-09-08'),
(555, 2513, 586, 4, '', 422, 4, '', '2017-09-08'),
(556, 2514, 586, 4, '', 422, 4.5, '', '2017-09-08'),
(557, 2515, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(558, 2516, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(559, 2518, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(560, 2519, 551, 5, '', 390, 4.5, '', '2017-09-08'),
(561, 2520, 569, 0, '', 414, 5, '', '2017-09-08'),
(562, 2521, 569, 0, '', 414, 5, '', '2017-09-08'),
(563, 2525, 586, 4, '', 422, 5, '', '2017-09-08'),
(564, 2526, 586, 4.5, '', 422, 5, '', '2017-09-08'),
(565, 2527, 586, 5, '', 422, 5, '', '2017-09-08'),
(566, 2528, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(567, 2529, 586, 5, '', 422, 5, '', '2017-09-08'),
(568, 2530, 586, 4, '', 422, 5, '', '2017-09-08'),
(569, 2531, 569, 0, '', 397, 5, '', '2017-09-08'),
(570, 2534, 586, 5, '', 422, 5, '', '2017-09-08'),
(571, 2538, 579, 0, '', 428, 5, ' ggf', '2017-09-08'),
(572, 2542, 551, 5, '', 390, 5, '', '2017-09-08'),
(573, 2543, 551, 4.5, '', 390, 4.5, '', '2017-09-08'),
(574, 2544, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(575, 2546, 586, 4.5, '', 422, 4.5, '', '2017-09-08'),
(576, 2547, 441, 0, '', 286, 4, '', '2017-09-08'),
(577, 2548, 594, 5, '', 433, 5, '', '2017-09-08'),
(578, 2549, 464, 3, 'bdbdncnxnxnxnfjfhbccbxbxnnsnnxnxndndndndjdjddkkdksushshdbbnnnnnnÃ±nnnnn', 169, 2.5, '', '2017-09-08'),
(579, 2554, 596, 4, '', 286, 4, '', '2017-09-09'),
(580, 2555, 464, 5, '', 169, 5, '', '2017-09-09'),
(581, 2559, 209, 0, '', 250, 5, '', '2017-09-09'),
(582, 2561, 61, 5, '', 441, 3, '', '2017-09-09'),
(583, 2562, 61, 5, '', 441, 2.5, '', '2017-09-09'),
(584, 2564, 61, 5, '', 441, 5, '', '2017-09-09'),
(585, 2565, 598, 4, '', 441, 4.5, '', '2017-09-09'),
(586, 2556, 464, 5, '', 169, 4.5, '', '2017-09-09'),
(587, 2568, 388, 5, '', 444, 5, '', '2017-09-09'),
(588, 2567, 464, 5, '', 169, 0, '', '2017-09-09'),
(589, 2569, 598, 5, '', 441, 0, '', '2017-09-09'),
(590, 2570, 551, 5, '', 444, 5, '', '2017-09-09'),
(591, 2574, 551, 5, '', 390, 5, '', '2017-09-10'),
(592, 2575, 551, 5, '', 390, 5, '', '2017-09-10'),
(593, 2576, 514, 5, '', 368, 5, '', '2017-09-10'),
(594, 2577, 551, 0, '', 390, 4.5, '', '2017-09-10'),
(595, 2578, 576, 5, '', 268, 5, '', '2017-09-10'),
(596, 2581, 576, 0.5, '', 268, 3, '', '2017-09-10'),
(597, 2420, 360, 5, '', 140, 5, '', '2017-09-10'),
(598, 2582, 360, 5, '', 140, 4, '', '2017-09-10'),
(599, 2584, 393, 5, '', 277, 4, '', '2017-09-10'),
(600, 2585, 583, 4.5, '', 453, 5, '', '2017-09-10'),
(601, 2588, 588, 2.5, '', 423, 3, '', '2017-09-10'),
(602, 2589, 322, 5, '', 456, 5, '', '2017-09-11'),
(603, 2591, 598, 5, '', 453, 4.5, '', '2017-09-11'),
(604, 2593, 545, 5, '', 391, 0, '', '2017-09-11'),
(605, 2594, 545, 5, '', 391, 5, '', '2017-09-11'),
(606, 2595, 545, 4.5, '', 391, 5, '', '2017-09-11'),
(607, 2597, 621, 5, '', 391, 5, '', '2017-09-11'),
(608, 2598, 436, 0, '', 391, 5, '', '2017-09-11'),
(609, 2602, 621, 0, '', 391, 4.5, '', '2017-09-11'),
(610, 2603, 621, 5, '', 391, 5, '', '2017-09-11'),
(611, 2617, 624, 0, '', 466, 5, '', '2017-09-11'),
(612, 2618, 545, 4.5, '', 390, 5, '', '2017-09-11'),
(613, 2619, 545, 0, '', 390, 5, '', '2017-09-11'),
(614, 0, 624, 0, '', 466, 5, '', '2017-09-11'),
(615, 2627, 545, 0, '', 390, 5, '', '2017-09-11'),
(616, 2628, 545, 0, '', 390, 5, '', '2017-09-11'),
(617, 2625, 586, 4.5, '', 422, 0, '', '2017-09-11'),
(618, 2630, 545, 0, '', 390, 4, '', '2017-09-11'),
(619, 2631, 545, 0, '', 390, 4, '', '2017-09-11'),
(620, 2632, 586, 4, '', 422, 4, '', '2017-09-11'),
(621, 2633, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(622, 1352, 545, 4, '', 390, 0, '', '2017-09-11'),
(623, 2634, 586, 4, '', 422, 4.5, '', '2017-09-11'),
(624, 2635, 545, 0, '', 390, 5, '', '2017-09-11'),
(625, 2636, 545, 0, '', 390, 5, '', '2017-09-11'),
(626, 1355, 545, 5, '', 390, 0, '', '2017-09-11'),
(627, 2637, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(628, 2638, 586, 4.5, '', 422, 4.5, '', '2017-09-11'),
(629, 2639, 586, 4.5, '', 422, 5, '', '2017-09-11'),
(630, 2640, 586, 0, '', 422, 4.5, '', '2017-09-11'),
(631, 2641, 586, 4, '', 422, 4.5, '', '2017-09-11'),
(632, 2642, 545, 0, '', 390, 5, '', '2017-09-11'),
(633, 2643, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(634, 2644, 545, 0, '', 390, 5, '', '2017-09-11'),
(635, 2645, 545, 0, '', 390, 5, '', '2017-09-11'),
(636, 1364, 545, 5, '', 390, 0, '', '2017-09-11'),
(637, 2648, 586, 0, '', 422, 4.5, '', '2017-09-11'),
(638, 2649, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(639, 2650, 586, 4.5, '', 422, 5, '', '2017-09-11'),
(640, 2651, 545, 0, '', 390, 5, '', '2017-09-11'),
(641, 2653, 545, 0, '', 390, 5, '', '2017-09-11'),
(642, 1369, 545, 5, '', 390, 0, '', '2017-09-11'),
(643, 1370, 545, 4.5, '', 390, 0, '', '2017-09-11'),
(644, 2655, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(645, 1371, 545, 4.5, '', 390, 0, '', '2017-09-11'),
(646, 2657, 545, 0, '', 390, 5, '', '2017-09-11'),
(647, 1373, 545, 5, '', 390, 0, '', '2017-09-11'),
(648, 2658, 545, 0, '', 390, 4, '', '2017-09-11'),
(649, 1374, 545, 5, '', 390, 0, '', '2017-09-11'),
(650, 2659, 545, 0, '', 390, 5, '', '2017-09-11'),
(651, 1375, 545, 5, '', 390, 0, '', '2017-09-11'),
(652, 2656, 586, 4.5, '', 422, 4.5, '', '2017-09-11'),
(653, 2660, 545, 0, '', 390, 5, '', '2017-09-11'),
(654, 1376, 545, 5, '', 390, 0, '', '2017-09-11'),
(655, 2661, 545, 0, '', 390, 5, '', '2017-09-11'),
(656, 2662, 545, 0, '', 390, 5, '', '2017-09-11'),
(657, 2663, 586, 3.5, '', 422, 5, '', '2017-09-11'),
(658, 2664, 545, 0, '', 390, 4.5, '', '2017-09-11'),
(659, 2665, 586, 5, 'Dgdghdfh', 422, 4.5, '', '2017-09-11'),
(660, 2666, 586, 4.5, '', 422, 4.5, '', '2017-09-11'),
(661, 2667, 586, 4.5, '', 422, 5, '', '2017-09-11'),
(662, 1384, 629, 5, '', 472, 0, '', '2017-09-11'),
(663, 2668, 629, 0, '', 472, 5, '', '2017-09-11'),
(664, 2669, 629, 0, '', 472, 5, '', '2017-09-11'),
(665, 1385, 629, 5, '', 472, 0, '', '2017-09-11'),
(666, 2670, 629, 0, '', 472, 5, '', '2017-09-11'),
(667, 2671, 629, 0, '', 472, 5, '', '2017-09-11'),
(668, 2672, 629, 0, '', 472, 5, '', '2017-09-11'),
(669, 1388, 629, 2.5, '', 472, 0, '', '2017-09-11'),
(670, 2673, 629, 0, '', 472, 5, '', '2017-09-11'),
(671, 1389, 629, 4.5, '', 472, 0, '', '2017-09-11'),
(672, 2674, 629, 0, '', 473, 5, '', '2017-09-11'),
(673, 2675, 629, 0, '', 473, 5, '', '2017-09-11'),
(674, 2677, 586, 4.5, '', 422, 5, '', '2017-09-11'),
(675, 2678, 586, 0, 'Hhh', 422, 5, '', '2017-09-11'),
(676, 2676, 545, 0, '', 390, 4, '', '2017-09-11'),
(677, 2679, 586, 5, '', 422, 4.5, '', '2017-09-11'),
(678, 2685, 632, 0, '', 476, 3.5, '', '2017-09-12'),
(679, 2726, 640, 0, '', 477, 5, '', '2017-09-12'),
(680, 2727, 641, 0, '', 479, 5, '', '2017-09-12'),
(681, 2744, 642, 0, '', 477, 5, '', '2017-09-12'),
(682, 2745, 642, 0, '', 477, 5, '', '2017-09-12'),
(683, 2748, 630, 0, '', 477, 5, '', '2017-09-12'),
(684, 2747, 638, 5, '', 478, 0, '', '2017-09-12'),
(685, 2758, 538, 0, '', 480, 3, '', '2017-09-12'),
(686, 2759, 644, 0, '', 480, 2, '', '2017-09-12'),
(687, 2760, 645, 0, '', 481, 2, '', '2017-09-12'),
(688, 2757, 586, 0, '', 422, 5, '', '2017-09-12'),
(689, 2761, 647, 5, '', 482, 5, '', '2017-09-12'),
(690, 2762, 647, 4.5, '', 482, 4.5, '', '2017-09-12'),
(691, 2764, 647, 4.5, '', 482, 5, '', '2017-09-12'),
(692, 2765, 647, 2.5, '', 482, 5, '', '2017-09-12'),
(693, 2766, 647, 4.5, '', 482, 5, '', '2017-09-12'),
(694, 2767, 647, 4, '', 482, 4.5, '', '2017-09-12'),
(695, 2773, 545, 0, '', 444, 4, '', '2017-09-12'),
(696, 2783, 647, 4, '', 482, 4, '', '2017-09-12'),
(697, 1434, 436, 5, '', 483, 0, '', '2017-09-12'),
(698, 2791, 436, 0, '', 483, 5, '', '2017-09-12'),
(699, 1435, 436, 4.5, 'very nice', 483, 0, '', '2017-09-12'),
(700, 2792, 436, 0, '', 483, 5, 'good', '2017-09-12'),
(701, 2793, 436, 0, '', 483, 5, '', '2017-09-12'),
(702, 2787, 545, 0, '', 444, 5, '', '2017-09-12'),
(703, 2795, 647, 4, '', 482, 4.5, '', '2017-09-12'),
(704, 2796, 647, 3.5, '', 482, 5, '', '2017-09-12'),
(705, 2797, 647, 3, '', 482, 4.5, '', '2017-09-12'),
(706, 2799, 647, 3, '', 482, 4.5, '', '2017-09-12'),
(707, 2800, 647, 0, '', 444, 4.5, '', '2017-09-12'),
(708, 2801, 647, 4, '', 444, 4.5, '', '2017-09-12'),
(709, 2802, 647, 0, '', 482, 5, '', '2017-09-12'),
(710, 2803, 647, 4.5, '', 482, 4.5, '', '2017-09-12'),
(711, 1446, 545, 4.5, '', 444, 0, '', '2017-09-12'),
(712, 2804, 647, 0, '', 482, 4.5, '', '2017-09-12'),
(713, 2805, 545, 0, '', 444, 4.5, '', '2017-09-12'),
(714, 2806, 647, 4.5, '', 482, 4, '', '2017-09-12'),
(715, 2807, 545, 0, '', 444, 5, '', '2017-09-12'),
(716, 1448, 545, 5, '', 444, 0, '', '2017-09-12'),
(717, 1450, 545, 5, '', 444, 0, '', '2017-09-12'),
(718, 2809, 545, 0, '', 444, 4.5, '', '2017-09-12'),
(719, 2808, 647, 0, '', 482, 5, '', '2017-09-12'),
(720, 2810, 545, 0, '', 444, 5, '', '2017-09-12'),
(721, 651, 415, 5, '', 258, 0, '', '2017-09-12'),
(722, 2813, 647, 0, '', 482, 4, '', '2017-09-12'),
(723, 2812, 415, 0, '', 390, 5, '', '2017-09-12'),
(724, 2811, 436, 0, '', 486, 5, '', '2017-09-12'),
(725, 2817, 545, 0, '', 444, 4.5, '', '2017-09-12'),
(726, 1458, 545, 4.5, '', 444, 0, '', '2017-09-12'),
(727, 1455, 415, 4.5, '', 110, 0, '', '2017-09-12'),
(728, 2815, 647, 0, '', 482, 4, '', '2017-09-12'),
(729, 2816, 436, 0, '', 486, 5, '', '2017-09-12'),
(730, 2814, 415, 0, '', 110, 5, '', '2017-09-12'),
(731, 1457, 436, 0, '', 486, 0, '', '2017-09-12'),
(732, 2818, 545, 0, '', 390, 5, '', '2017-09-12'),
(733, 1459, 545, 3, '', 390, 0, '', '2017-09-12'),
(734, 2819, 545, 0, '', 390, 5, '', '2017-09-12'),
(735, 2820, 545, 0, '', 390, 5, '', '2017-09-12'),
(736, 1462, 415, 5, '', 110, 0, '', '2017-09-12'),
(737, 2821, 415, 0, '', 110, 5, '', '2017-09-12'),
(738, 2823, 647, 4.5, '', 482, 4, '', '2017-09-12'),
(739, 2824, 545, 0, '', 486, 5, '', '2017-09-12'),
(740, 2825, 436, 0, '', 110, 5, '', '2017-09-12'),
(741, 2828, 545, 0, '', 390, 5, '', '2017-09-12'),
(742, 2834, 647, 0, '', 482, 4.5, '', '2017-09-12'),
(743, 2836, 647, 4.5, '', 482, 4.5, '', '2017-09-12'),
(744, 1467, 545, 3.5, '', 390, 0, '', '2017-09-12'),
(745, 2838, 647, 0, '', 482, 4, '', '2017-09-12'),
(746, 2840, 647, 0, '', 482, 5, '', '2017-09-12'),
(747, 2841, 647, 4, '', 482, 4, '', '2017-09-12'),
(748, 2842, 545, 0, '', 390, 5, '', '2017-09-12'),
(749, 2843, 647, 0, '', 482, 4.5, '', '2017-09-12'),
(750, 2844, 545, 0, '', 390, 5, '', '2017-09-12'),
(751, 1476, 545, 5, '', 390, 0, '', '2017-09-12'),
(752, 2846, 647, 4.5, '', 482, 5, '', '2017-09-12'),
(753, 2847, 647, 0, '', 482, 5, '', '2017-09-12'),
(754, 2848, 647, 0, '', 482, 4.5, 'H', '2017-09-12'),
(755, 2849, 647, 0, '', 482, 5, '', '2017-09-12'),
(756, 2850, 647, 0, '', 482, 5, '', '2017-09-12'),
(757, 2852, 647, 0, '', 482, 4.5, '', '2017-09-12'),
(758, 2853, 664, 0, '', 494, 5, '', '2017-09-12'),
(759, 1486, 664, 5, '', 494, 0, '', '2017-09-12'),
(760, 2854, 664, 0, '', 494, 5, '', '2017-09-12'),
(761, 2855, 664, 0, '', 494, 5, '', '2017-09-12'),
(762, 2856, 664, 0, '', 494, 5, '', '2017-09-12'),
(763, 2859, 664, 0, '', 494, 5, '', '2017-09-12'),
(764, 2860, 647, 0, '', 482, 5, '', '2017-09-12'),
(765, 2851, 545, 0, '', 390, 5, '', '2017-09-12'),
(766, 2861, 664, 0, '', 494, 5, '', '2017-09-12'),
(767, 2862, 664, 0, '', 494, 5, '', '2017-09-12'),
(768, 2863, 664, 0, '', 496, 3.5, '', '2017-09-12'),
(769, 2865, 545, 0, '', 390, 5, '', '2017-09-12'),
(770, 2875, 603, 0, '', 501, 5, '', '2017-09-13'),
(771, 2878, 603, 0, '', 498, 1.5, '', '2017-09-13'),
(772, 2880, 673, 0, '', 505, 5, '', '2017-09-13'),
(773, 2881, 413, 0, '', 235, 5, '', '2017-09-13'),
(774, 2882, 603, 0, '', 498, 2, '', '2017-09-13'),
(775, 2884, 603, 0, '', 498, 3, '', '2017-09-13'),
(776, 2888, 436, 0, '', 441, 5, '', '2017-09-13'),
(777, 2889, 647, 0, '', 482, 4.5, '', '2017-09-13'),
(778, 2794, 648, 0, '', 485, 5, '', '2017-09-13'),
(779, 2893, 678, 0, '', 397, 5, '', '2017-09-13'),
(780, 1511, 545, 4.5, '', 494, 0, '', '2017-09-13'),
(781, 2894, 545, 0, '', 494, 5, '', '2017-09-13'),
(782, 2897, 436, 0, '', 409, 5, '', '2017-09-13'),
(783, 2899, 436, 0, '', 409, 5, '', '2017-09-13'),
(784, 2902, 680, 0, '', 509, 3, 'mmmm', '2017-09-13'),
(785, 2903, 655, 0, '', 391, 5, '', '2017-09-13'),
(786, 2907, 655, 0, '', 391, 5, '', '2017-09-13'),
(787, 506, 8, 5, 'ggghh', 255, 0, '', '2017-09-13'),
(788, 2917, 436, 0, '', 483, 5, '', '2017-09-13'),
(789, 2921, 687, 0, '', 514, 5, '', '2017-09-13'),
(790, 2923, 687, 0, '', 514, 5, '', '2017-09-13'),
(791, 2924, 687, 0, '', 514, 5, '', '2017-09-13'),
(792, 2925, 680, 0, '', 509, 5, 'mantap', '2017-09-13'),
(793, 2931, 545, 0, '', 390, 5, '', '2017-09-13'),
(794, 2934, 545, 0, '', 390, 5, '', '2017-09-13'),
(795, 2936, 689, 0, '', 235, 4.5, '', '2017-09-13'),
(796, 2942, 603, 0, '', 498, 2, '', '2017-09-13'),
(797, 2944, 689, 0, '', 235, 4.5, '', '2017-09-13'),
(798, 2948, 694, 0, '', 517, 5, '', '2017-09-13'),
(799, 2949, 694, 0, '', 517, 5, '', '2017-09-13'),
(800, 2955, 603, 0, '', 498, 1, '', '2017-09-14'),
(801, 2959, 646, 0, '', 522, 5, '', '2017-09-14'),
(802, 2965, 436, 0, '', 483, 5, '', '2017-09-14'),
(803, 1542, 436, 5, '', 483, 0, '', '2017-09-14'),
(804, 2968, 545, 0, '', 494, 4, '', '2017-09-14'),
(805, 1544, 545, 4.5, '', 494, 0, '', '2017-09-14'),
(806, 2966, 436, 0, '', 483, 5, '', '2017-09-14'),
(807, 1543, 436, 5, '', 483, 0, '', '2017-09-14'),
(808, 1546, 569, 5, '', 486, 0, '', '2017-09-14'),
(809, 2972, 569, 0, '', 486, 5, '', '2017-09-14'),
(810, 2975, 569, 0, '', 486, 5, '', '2017-09-14'),
(811, 2976, 569, 0, '', 486, 4.5, '', '2017-09-14'),
(812, 2969, 545, 0, '', 494, 5, '', '2017-09-14'),
(813, 1545, 545, 4, '', 494, 0, '', '2017-09-14'),
(814, 2979, 545, 0, '', 494, 4, '', '2017-09-14'),
(815, 2981, 569, 0, '', 486, 5, '', '2017-09-14'),
(816, 1550, 569, 5, '', 486, 0, '', '2017-09-14'),
(817, 2982, 569, 0, '', 486, 5, '', '2017-09-14'),
(818, 2985, 545, 0, '', 494, 4.5, '', '2017-09-14'),
(819, 1552, 545, 4.5, '', 494, 0, '', '2017-09-14'),
(820, 2986, 569, 0, '', 486, 5, '', '2017-09-14'),
(821, 2987, 545, 0, '', 494, 5, '', '2017-09-14'),
(822, 2988, 569, 0, '', 486, 5, '', '2017-09-14'),
(823, 2989, 545, 0, '', 494, 5, '', '2017-09-14'),
(824, 1556, 545, 5, '', 494, 0, '', '2017-09-14'),
(825, 2992, 439, 0, '', 322, 4.5, '', '2017-09-14'),
(826, 1564, 689, 5, '', 235, 0, '', '2017-09-14'),
(827, 3001, 689, 0, '', 235, 4.5, '', '2017-09-14'),
(828, 3000, 689, 0, '', 235, 5, '', '2017-09-14'),
(829, 3007, 603, 0, '', 498, 2, '', '2017-09-15'),
(830, 3010, 689, 0, '', 516, 5, '', '2017-09-15'),
(831, 1568, 689, 5, '', 516, 0, '', '2017-09-15'),
(832, 3012, 689, 0, '', 516, 5, '', '2017-09-15'),
(833, 3011, 689, 0, '', 516, 5, '', '2017-09-15'),
(834, 3018, 689, 0, '', 516, 5, '', '2017-09-15'),
(835, 1570, 689, 4, 'fare', 516, 0, '', '2017-09-15'),
(836, 363, 229, 5, '', 156, 0, '', '2017-09-15'),
(837, 3019, 229, 0, '', 534, 5, '', '2017-09-15'),
(838, 2937, 464, 0, '', 169, 1, '', '2017-09-15'),
(839, 3024, 436, 0, '', 95, 5, '', '2017-09-15'),
(840, 1573, 436, 4.5, '', 95, 0, '', '2017-09-15'),
(841, 3025, 707, 0, '', 538, 1.5, 'good customer', '2017-09-15'),
(842, 3027, 710, 0, '', 539, 5, '', '2017-09-15'),
(843, 3028, 689, 0, '', 516, 5, '', '2017-09-15'),
(844, 1576, 689, 5, 'nice ride', 516, 0, '', '2017-09-15'),
(845, 974, 464, 0, '', 169, 0, '', '2017-09-15'),
(846, 3032, 711, 0, '', 540, 4.5, 'Excelent', '2017-09-15'),
(847, 1578, 436, 3.5, '', 95, 0, '', '2017-09-15'),
(848, 3038, 464, 0, '', 169, 5, '', '2017-09-15'),
(849, 1582, 464, 5, 'ygg', 169, 0, '', '2017-09-16'),
(850, 3037, 689, 0, '', 516, 5, '', '2017-09-16'),
(851, 1585, 436, 5, '', 95, 0, '', '2017-09-16'),
(852, 3053, 436, 0, '', 95, 5, '', '2017-09-16'),
(853, 3033, 436, 0, '', 95, 5, '', '2017-09-16'),
(854, 1587, 718, 5, '', 486, 0, '', '2017-09-16'),
(855, 3055, 718, 0, '', 486, 5, '', '2017-09-16'),
(856, 1589, 718, 5, '', 486, 0, '', '2017-09-16'),
(857, 3057, 718, 0, '', 486, 4.5, '', '2017-09-16'),
(858, 1590, 718, 5, '', 486, 0, '', '2017-09-16'),
(859, 3058, 718, 0, '', 486, 5, '', '2017-09-16'),
(860, 3056, 689, 0, '', 516, 5, '', '2017-09-16'),
(861, 1592, 689, 5, '', 235, 0, '', '2017-09-16'),
(862, 3062, 689, 0, '', 235, 5, '', '2017-09-16'),
(863, 3063, 569, 0, '', 486, 5, '', '2017-09-16'),
(864, 3066, 545, 0, '', 390, 5, '', '2017-09-16'),
(865, 1586, 545, 0, '', 110, 0, '', '2017-09-16'),
(866, 3067, 436, 0, '', 549, 5, '', '2017-09-16'),
(867, 3078, 721, 0, '', 552, 5, '', '2017-09-16'),
(868, 3082, 721, 0, '', 552, 5, '', '2017-09-16'),
(869, 3101, 725, 0, '', 556, 4.5, '', '2017-09-16'),
(870, 1604, 725, 5, '', 556, 0, '', '2017-09-16'),
(871, 3105, 721, 0, '', 552, 5, '', '2017-09-17'),
(872, 1606, 721, 5, '', 552, 0, '', '2017-09-17'),
(873, 3106, 721, 0, '', 552, 5, '', '2017-09-17'),
(874, 1607, 721, 5, 'Excelente ', 552, 0, '', '2017-09-17'),
(875, 3107, 514, 0, '', 368, 5, '', '2017-09-17'),
(876, 3109, 721, 0, '', 552, 5, '', '2017-09-17'),
(877, 1609, 721, 5, '', 552, 0, '', '2017-09-17'),
(878, 1611, 731, 4.5, '', 95, 0, '', '2017-09-17'),
(879, 3115, 721, 0, '', 552, 5, '', '2017-09-18'),
(880, 1612, 721, 5, '', 552, 0, '', '2017-09-18'),
(881, 3111, 679, 0, '', 525, 4, '', '2017-09-18'),
(882, 3123, 7, 0, '', 8, 4.5, '', '2017-09-18'),
(883, 3125, 735, 0, '', 560, 5, '', '2017-09-18'),
(884, 3157, 735, 0, '', 562, 5, '', '2017-09-18'),
(885, 3158, 735, 0, '', 560, 5, '', '2017-09-18'),
(886, 3159, 735, 0, '', 560, 5, '', '2017-09-18'),
(887, 1618, 735, 4.5, '', 560, 0, '', '2017-09-18'),
(888, 3181, 739, 0, '', 466, 1, 'gente boa', '2017-09-18'),
(889, 1620, 739, 4.5, '', 466, 0, '', '2017-09-18'),
(890, 3185, 740, 0, '', 564, 4, 'muy bien', '2017-09-19'),
(891, 1623, 740, 4.5, '', 564, 0, '', '2017-09-19'),
(892, 1624, 689, 5, '', 516, 0, '', '2017-09-19'),
(893, 3187, 739, 0, '', 466, 5, 'xghggfcgh', '2017-09-19'),
(894, 1625, 739, 5, '', 466, 0, '', '2017-09-19'),
(895, 3126, 735, 0, '', 368, 5, '', '2017-09-19'),
(896, 3196, 540, 0, '', 9, 5, '', '2017-09-19'),
(897, 1630, 745, 5, '', 565, 0, '', '2017-09-19'),
(898, 3204, 745, 0, '', 565, 5, '', '2017-09-19'),
(899, 1631, 745, 5, '', 565, 0, '', '2017-09-19'),
(900, 3208, 745, 0, '', 565, 5, '', '2017-09-19'),
(901, 1632, 745, 5, '', 565, 0, '', '2017-09-19'),
(902, 3207, 745, 0, '', 565, 4.5, '', '2017-09-19'),
(903, 1633, 745, 5, '', 565, 0, '', '2017-09-19'),
(904, 3210, 745, 0, '', 565, 5, '', '2017-09-19'),
(905, 1634, 745, 5, '', 565, 0, '', '2017-09-19'),
(906, 3212, 745, 0, '', 565, 5, '', '2017-09-19'),
(907, 3215, 745, 0, '', 565, 5, '', '2017-09-19'),
(908, 3217, 749, 0, '', 565, 5, '', '2017-09-19'),
(909, 1636, 749, 5, '', 565, 0, '', '2017-09-19'),
(910, 3219, 748, 0, '', 565, 5, '', '2017-09-19'),
(911, 3218, 749, 0, '', 565, 5, '', '2017-09-19'),
(912, 3222, 200, 0, '', 111, 5, '', '2017-09-19'),
(913, 3223, 749, 0, '', 565, 5, '', '2017-09-19'),
(914, 1642, 749, 5, '', 565, 0, '', '2017-09-19'),
(915, 3220, 750, 0, '', 566, 1, '', '2017-09-19'),
(916, 2784, 602, 0, '', 441, 5, '', '2017-09-19'),
(917, 2940, 648, 0, '', 514, 5, '', '2017-09-19'),
(918, 3248, 11, 0, '', 569, 5, '', '2017-09-19'),
(919, 3249, 741, 5, '', 134, 2, '', '2017-09-20'),
(920, 3251, 689, 0, '', 516, 5, '', '2017-09-20'),
(921, 1650, 689, 5, 'tyyiuthhk', 516, 0, '', '2017-09-20'),
(922, 3254, 514, 0, '', 368, 5, 'good drive ', '2017-09-20'),
(923, 3253, 689, 0, '', 516, 5, '', '2017-09-20'),
(924, 3261, 11, 0, '', 569, 5, '', '2017-09-20'),
(925, 1653, 11, 5, '', 569, 0, '', '2017-09-20'),
(926, 1654, 739, 4.5, '', 478, 0, '', '2017-09-20'),
(927, 3262, 739, 0, '', 478, 3.5, 'gfjhh8j', '2017-09-20'),
(928, 3250, 741, 0, '', 134, 2, '', '2017-09-20'),
(929, 3274, 755, 0, '', 566, 5, '', '2017-09-20'),
(930, 1657, 755, 5, '', 566, 0, '', '2017-09-20'),
(931, 3299, 755, 0, '', 566, 5, '', '2017-09-20'),
(932, 3330, 7, 4.5, '', 8, 4.5, '', '2017-09-20'),
(933, 3331, 7, 0, '', 8, 4.5, '', '2017-09-20'),
(934, 1, 1, 5, '', 1, 3.5, '', '2017-09-26'),
(935, 2, 1, 0, '', 1, 5, '', '2017-09-26'),
(936, 3, 1, 5, 'hhhh', 1, 5, '', '2017-09-26'),
(937, 4, 2, 5, '', 2, 5, '', '2017-10-09'),
(938, 7, 2, 5, '', 2, 5, 'perfect ', '2017-10-10'),
(939, 5, 2, 4.5, 'perfect driver ', 2, 0, '', '2017-10-10'),
(940, 6, 2, 5, 'Nice driver ', 2, 0, '', '2017-10-10'),
(941, 8, 2, 5, 'Test', 2, 5, 'nice ', '2017-10-10'),
(942, 10, 2, 5, '', 2, 5, '', '2017-10-10'),
(943, 11, 2, 5, '', 2, 5, '', '2017-10-10'),
(944, 12, 2, 5, '', 2, 5, 'test', '2017-10-10'),
(945, 9, 2, 5, 'best driver ever', 2, 0, '', '2017-10-10'),
(946, 15, 2, 5, '', 2, 5, '', '2017-10-10'),
(947, 16, 2, 0, '', 2, 5, '', '2017-10-10'),
(948, 18, 2, 0, '', 2, 5, '', '2017-10-10'),
(949, 19, 2, 5, '', 2, 5, '', '2017-10-10'),
(950, 13, 2, 5, '', 2, 0, '', '2017-10-10'),
(951, 20, 2, 5, '', 2, 5, '', '2017-10-10'),
(952, 21, 2, 5, '', 2, 5, '', '2017-10-10'),
(953, 22, 2, 5, '', 2, 5, '', '2017-10-10'),
(954, 17, 2, 5, '', 2, 0, '', '2017-10-10'),
(955, 23, 2, 0, '', 2, 5, '', '2017-10-11'),
(956, 26, 2, 5, '', 2, 5, '', '2017-10-11'),
(957, 27, 2, 5, '', 2, 5, '', '2017-10-11'),
(958, 28, 2, 0, '', 2, 5, '', '2017-10-11'),
(959, 30, 2, 0, '', 2, 5, '', '2017-10-11'),
(960, 32, 2, 0, '', 2, 5, '', '2017-10-11'),
(961, 33, 2, 0, '', 2, 5, '', '2017-10-11'),
(962, 37, 2, 5, '', 2, 5, '', '2017-10-12'),
(963, 42, 4, 5, '', 3, 5, '', '2017-10-14'),
(964, 43, 4, 0, '', 3, 5, '', '2017-10-14'),
(965, 44, 4, 5, '', 3, 5, '', '2017-10-14'),
(966, 45, 2, 5, '', 2, 5, '', '2017-10-14'),
(967, 31, 2, 5, '', 2, 0, '', '2017-10-14'),
(968, 48, 5, 0, '', 2, 5, '', '2017-10-14'),
(969, 49, 2, 5, '', 2, 5, '', '2017-10-14'),
(970, 50, 2, 5, '', 2, 5, '', '2017-10-15'),
(971, 34, 2, 5, '', 2, 0, '', '2017-10-15'),
(972, 52, 2, 0, '', 2, 5, '', '2017-10-15'),
(973, 35, 2, 5, '', 2, 0, '', '2017-10-15'),
(974, 53, 2, 5, '', 2, 5, '', '2017-10-15'),
(975, 36, 2, 5, '', 2, 0, '', '2017-10-15'),
(976, 54, 2, 0, '', 2, 5, '', '2017-10-15'),
(977, 55, 6, 0, '', 2, 5, '', '2017-10-15'),
(978, 56, 7, 0, '', 2, 5, '', '2017-10-15'),
(979, 57, 2, 0, '', 2, 5, '', '2017-10-15'),
(980, 41, 2, 5, '', 2, 0, '', '2017-10-15'),
(981, 58, 2, 0, '', 2, 5, '', '2017-10-15'),
(982, 59, 2, 0, '', 2, 5, '', '2017-10-15'),
(983, 60, 1, 0, '', 1, 5, '', '2017-10-16'),
(984, 63, 1, 0, '', 5, 5, '', '2017-10-16'),
(985, 64, 8, 5, '', 4, 5, '', '2017-10-16'),
(986, 46, 8, 5, '', 4, 0, '', '2017-10-16'),
(987, 66, 8, 0, '', 4, 5, '', '2017-10-16'),
(988, 47, 8, 5, '', 4, 0, '', '2017-10-16'),
(989, 67, 8, 0, '', 5, 5, '', '2017-10-16'),
(990, 69, 8, 0, '', 5, 5, '', '2017-10-16'),
(991, 70, 2, 0, '', 2, 5, '', '2017-10-16'),
(992, 74, 2, 0, '', 7, 5, '', '2017-10-17');
INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(993, 51, 2, 5, '', 7, 0, '', '2017-10-17'),
(994, 76, 10, 5, '', 2, 5, '', '2017-10-17'),
(995, 77, 2, 5, '', 2, 5, '', '2017-10-17'),
(996, 78, 10, 5, '', 2, 5, '', '2017-10-17'),
(997, 79, 10, 5, '', 7, 5, '', '2017-10-17'),
(998, 80, 10, 5, '', 7, 5, '', '2017-10-17'),
(999, 72, 10, 0, '', 7, 5, '', '2017-10-17'),
(1000, 82, 10, 5, '', 7, 0, '', '2017-10-17'),
(1001, 83, 10, 5, '', 7, 5, '', '2017-10-17'),
(1002, 87, 2, 5, '', 2, 5, '', '2017-10-18'),
(1003, 61, 2, 5, '', 2, 0, '', '2017-10-18'),
(1004, 88, 10, 0, '', 7, 5, '', '2017-10-19'),
(1005, 91, 2, 5, '', 2, 5, '', '2017-10-20'),
(1006, 90, 10, 5, '', 7, 5, '', '2017-10-20'),
(1007, 93, 11, 0, '', 8, 5, 'Bababsvsbsb', '2017-10-23'),
(1008, 94, 11, 4.5, 'Abcdehdhshbs hebsbajsjsvbs hsbsbsjgshshsvdbd hahsbbsjsjsbshsjsbsbshdvdbskwobwbd vavabavsbsbsbsbsbbzbsbzbdbdbdbdbbdhdjshs gghhhhhhhahhsbsbsbsbsbsbsb jansjshsbsjhshsjsjsjsjsishsbshdhsjsks vshshahabshshsbdbshshajsbbswubsbshsjsbsbshsjsbshshshshsbsb', 8, 0, '', '2017-10-23'),
(1009, 95, 11, 5, 'Babsshsbbsbsja hahshsshsushs hahabshsjsbshsydhjsoqnwsh iwownsjsksjsnsishwkqpsnbzsk nsnsnsjsjsjsnsjsjsjbshs jajsjshsjsjshu jajsjshsjsjwbsbbfsjownsnxbxjsjsnsjsksjsbsjsjjs najsjsjsjjssjusjahshshahshsb hahahshshsbshshsjsjhsbsbsbs hahabsbshsb', 8, 5, '', '2017-10-23'),
(1010, 97, 11, 5, '', 8, 5, '', '2017-10-23'),
(1011, 98, 12, 5, '', 9, 5, 'HFC', '2017-10-24'),
(1012, 99, 12, 5, '', 9, 5, '', '2017-10-24'),
(1013, 71, 13, 5, '', 1, 0, '', '2017-10-24'),
(1014, 100, 13, 0, '', 1, 4.5, '', '2017-10-24'),
(1015, 116, 8, 5, '', 11, 5, '', '2017-10-26'),
(1016, 73, 8, 5, '', 11, 0, '', '2017-10-26'),
(1017, 120, 10, 5, '', 7, 5, '', '2017-10-26'),
(1018, 122, 10, 5, '', 7, 5, '', '2017-10-26'),
(1019, 123, 2, 5, '', 2, 5, '', '2017-10-26'),
(1020, 124, 2, 5, '', 7, 5, '', '2017-10-26'),
(1021, 126, 8, 5, '', 11, 5, '', '2017-10-27'),
(1022, 127, 8, 5, '', 11, 5, '', '2017-10-27'),
(1023, 81, 8, 3.5, '', 11, 0, '', '2017-10-27'),
(1024, 128, 8, 0, '', 11, 5, '', '2017-10-27'),
(1025, 131, 8, 5, '', 11, 5, '', '2017-10-27'),
(1026, 132, 8, 5, '', 11, 5, '', '2017-10-27'),
(1027, 133, 10, 5, '', 7, 5, '', '2017-10-27'),
(1028, 134, 11, 5, '', 13, 5, '', '2017-10-27'),
(1029, 135, 11, 5, '', 13, 5, '', '2017-10-27'),
(1030, 136, 8, 4.5, '', 14, 5, '', '2017-10-27'),
(1031, 137, 10, 5, '', 7, 5, '', '2017-10-27'),
(1032, 138, 10, 4.5, '', 7, 5, '', '2017-10-27'),
(1033, 139, 2, 0, '', 2, 5, '', '2017-10-28'),
(1034, 151, 5, 0, '', 2, 5, '', '2017-10-28'),
(1035, 152, 10, 0, '', 2, 5, '', '2017-10-28'),
(1036, 153, 10, 0, '', 12, 5, '', '2017-10-28'),
(1037, 154, 10, 5, '', 12, 5, '', '2017-10-28'),
(1038, 156, 10, 0, '', 12, 5, '', '2017-10-29'),
(1039, 157, 10, 5, 'Goed service en goedkoopðŸ˜ƒðŸ‘Œ', 12, 5, '', '2017-10-30'),
(1040, 158, 10, 0, '', 7, 5, '', '2017-10-30'),
(1041, 159, 2, 0, '', 2, 5, '', '2017-11-01'),
(1042, 101, 2, 5, 'testing', 2, 0, '', '2017-11-02'),
(1043, 162, 2, 0, '', 2, 5, 'testing', '2017-11-02'),
(1044, 165, 2, 0, '', 7, 5, '', '2017-11-04'),
(1045, 102, 2, 5, '', 7, 0, '', '2017-11-04'),
(1046, 103, 2, 5, '', 12, 0, '', '2017-11-04'),
(1047, 168, 2, 0, '', 12, 5, '', '2017-11-04'),
(1048, 104, 2, 5, '', 12, 0, '', '2017-11-04'),
(1049, 169, 2, 0, '', 12, 5, '', '2017-11-04'),
(1050, 170, 10, 5, '', 7, 5, '', '2017-11-04'),
(1051, 173, 2, 0, '', 12, 5, '', '2017-11-05'),
(1052, 106, 2, 5, '', 12, 0, '', '2017-11-05'),
(1053, 174, 2, 0, '', 7, 5, '', '2017-11-05'),
(1054, 178, 10, 5, '', 7, 5, '', '2017-11-05'),
(1055, 179, 2, 0, '', 7, 5, '', '2017-11-05'),
(1056, 109, 2, 5, '', 7, 0, '', '2017-11-05'),
(1057, 172, 10, 0, '', 7, 5, '', '2017-11-05'),
(1058, 112, 2, 5, '', 7, 0, '', '2017-11-05'),
(1059, 181, 2, 0, '', 12, 5, '', '2017-11-05'),
(1060, 113, 2, 5, '', 12, 0, '', '2017-11-05'),
(1061, 182, 2, 0, '', 7, 5, '', '2017-11-05'),
(1062, 114, 2, 5, '', 7, 0, '', '2017-11-05'),
(1063, 185, 2, 0, '', 12, 5, '', '2017-11-07'),
(1064, 115, 2, 5, '', 12, 0, '', '2017-11-07'),
(1065, 190, 2, 0, '', 12, 5, '', '2017-11-07'),
(1066, 192, 2, 0, '', 12, 5, '', '2017-11-07'),
(1067, 117, 2, 5, '', 12, 0, '', '2017-11-07'),
(1068, 119, 2, 5, '', 12, 0, '', '2017-11-08'),
(1069, 197, 2, 0, '', 12, 5, '', '2017-11-08'),
(1070, 204, 2, 0, '', 7, 5, '', '2017-11-08'),
(1071, 121, 2, 5, '', 7, 0, '', '2017-11-08'),
(1072, 205, 2, 0, '', 7, 5, '', '2017-11-08'),
(1073, 208, 2, 0, '', 7, 5, '', '2017-11-08'),
(1074, 209, 2, 0, '', 7, 5, '', '2017-11-08'),
(1075, 125, 2, 5, '', 7, 0, '', '2017-11-08'),
(1076, 210, 2, 0, '', 7, 5, '', '2017-11-08'),
(1077, 212, 2, 0, '', 7, 5, '', '2017-11-09'),
(1078, 214, 2, 0, '', 7, 5, '', '2017-11-09'),
(1079, 215, 2, 0, '', 7, 5, '', '2017-11-09'),
(1080, 218, 2, 0, '', 7, 5, '', '2017-11-09'),
(1081, 129, 2, 5, '', 7, 0, '', '2017-11-09'),
(1082, 220, 2, 0, '', 7, 5, '', '2017-11-10'),
(1083, 130, 2, 5, '', 7, 0, '', '2017-11-10'),
(1084, 221, 2, 0, '', 7, 5, '', '2017-11-10'),
(1085, 225, 2, 0, '', 16, 5, '', '2017-11-10'),
(1086, 226, 2, 0, '', 16, 5, '', '2017-11-10'),
(1087, 227, 2, 0, '', 7, 5, '', '2017-11-10'),
(1088, 228, 2, 0, '', 7, 5, '', '2017-11-10'),
(1089, 231, 2, 0, '', 7, 5, '', '2017-11-11'),
(1090, 234, 2, 0, '', 7, 5, '', '2017-11-11'),
(1091, 140, 2, 5, '', 7, 0, '', '2017-11-12'),
(1092, 246, 2, 0, '', 7, 5, '', '2017-11-12'),
(1093, 247, 2, 0, '', 12, 5, '', '2017-11-12'),
(1094, 251, 2, 0, '', 16, 5, '', '2017-11-13'),
(1095, 142, 2, 5, '', 16, 0, '', '2017-11-13');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 1, 1, 1),
(2, 1, 1, 1, 2),
(3, 1, 1, 1, 3),
(4, 1, 2, 2, 4),
(5, 1, 2, 2, 5),
(6, 1, 2, 0, 6),
(7, 1, 2, 2, 7),
(8, 1, 2, 2, 8),
(9, 1, 2, 0, 9),
(10, 1, 2, 2, 10),
(11, 1, 2, 2, 11),
(12, 1, 2, 2, 12),
(13, 1, 2, 0, 13),
(14, 1, 2, 0, 14),
(15, 1, 2, 2, 15),
(16, 1, 2, 2, 16),
(17, 1, 2, 0, 17),
(18, 1, 2, 2, 18),
(19, 1, 2, 2, 19),
(20, 1, 2, 2, 20),
(21, 1, 2, 2, 21),
(22, 1, 2, 2, 22),
(23, 1, 2, 2, 23),
(24, 1, 2, 0, 24),
(25, 1, 2, 0, 25),
(26, 1, 2, 2, 26),
(27, 1, 2, 2, 27),
(28, 1, 2, 2, 28),
(29, 1, 2, 2, 29),
(30, 1, 2, 2, 30),
(31, 1, 2, 2, 31),
(32, 1, 2, 2, 32),
(33, 1, 2, 2, 33),
(34, 1, 2, 2, 34),
(35, 1, 2, 2, 35),
(36, 1, 2, 2, 36),
(37, 1, 2, 2, 37),
(38, 1, 2, 0, 38),
(39, 1, 2, 2, 39),
(40, 1, 2, 2, 40),
(41, 1, 4, 3, 42),
(42, 1, 4, 3, 43),
(43, 1, 4, 3, 44),
(44, 1, 2, 2, 45),
(45, 1, 2, 0, 46),
(46, 1, 2, 0, 47),
(47, 1, 5, 2, 48),
(48, 1, 2, 2, 49),
(49, 1, 2, 2, 50),
(50, 1, 2, 0, 51),
(51, 1, 2, 2, 52),
(52, 1, 2, 2, 53),
(53, 1, 2, 2, 54),
(54, 1, 6, 2, 55),
(55, 1, 7, 2, 56),
(56, 1, 2, 2, 57),
(57, 1, 2, 2, 58),
(58, 1, 2, 2, 59),
(59, 1, 1, 1, 60),
(60, 1, 4, 4, 61),
(61, 1, 4, 0, 62),
(62, 1, 1, 5, 63),
(63, 1, 8, 4, 64),
(64, 1, 8, 5, 65),
(65, 1, 8, 4, 66),
(66, 1, 8, 5, 67),
(67, 1, 8, 0, 68),
(68, 1, 8, 5, 69),
(69, 1, 2, 2, 70),
(70, 1, 10, 0, 71),
(71, 1, 10, 7, 72),
(72, 1, 10, 0, 73),
(73, 1, 2, 7, 74),
(74, 1, 10, 2, 76),
(75, 1, 2, 0, 75),
(76, 1, 2, 2, 77),
(77, 1, 10, 2, 78),
(78, 1, 10, 7, 79),
(79, 1, 10, 7, 80),
(80, 1, 10, 0, 81),
(81, 1, 10, 7, 82),
(82, 1, 10, 7, 83),
(83, 1, 10, 7, 84),
(84, 1, 10, 0, 85),
(85, 1, 10, 0, 86),
(86, 1, 2, 2, 87),
(87, 1, 10, 7, 88),
(88, 1, 10, 0, 89),
(89, 1, 10, 7, 90),
(90, 1, 2, 2, 91),
(91, 1, 11, 0, 92),
(92, 1, 11, 8, 93),
(93, 1, 11, 8, 94),
(94, 1, 11, 8, 95),
(95, 1, 11, 0, 96),
(96, 1, 11, 8, 97),
(97, 1, 12, 9, 98),
(98, 1, 12, 9, 99),
(99, 1, 13, 1, 100),
(100, 1, 1, 0, 104),
(101, 1, 1, 0, 105),
(102, 1, 1, 0, 108),
(103, 1, 1, 8, 109),
(104, 1, 1, 8, 110),
(105, 1, 1, 0, 111),
(106, 1, 8, 0, 112),
(107, 1, 10, 0, 113),
(108, 1, 8, 0, 114),
(109, 1, 8, 0, 115),
(110, 1, 8, 11, 116),
(111, 1, 8, 11, 117),
(112, 1, 8, 11, 118),
(113, 1, 10, 0, 119),
(114, 1, 10, 7, 120),
(115, 1, 5, 7, 121),
(116, 1, 10, 7, 122),
(117, 1, 2, 2, 123),
(118, 1, 2, 7, 124),
(119, 1, 2, 0, 125),
(120, 1, 8, 11, 126),
(121, 1, 8, 11, 127),
(122, 1, 8, 11, 128),
(123, 1, 8, 0, 129),
(124, 1, 8, 0, 130),
(125, 1, 8, 11, 131),
(126, 1, 8, 11, 132),
(127, 1, 10, 7, 133),
(128, 1, 11, 13, 134),
(129, 1, 11, 13, 135),
(130, 1, 8, 14, 136),
(131, 1, 10, 7, 137),
(132, 1, 10, 7, 138),
(133, 1, 2, 2, 139),
(134, 1, 2, 0, 140),
(135, 1, 2, 2, 141),
(136, 1, 2, 0, 142),
(137, 1, 2, 0, 143),
(138, 1, 2, 0, 144),
(139, 1, 2, 0, 147),
(140, 1, 2, 0, 145),
(141, 1, 2, 0, 146),
(142, 1, 2, 0, 148),
(143, 1, 2, 0, 149),
(144, 1, 2, 0, 150),
(145, 1, 5, 2, 151),
(146, 1, 10, 2, 152),
(147, 1, 10, 12, 153),
(148, 1, 10, 12, 154),
(149, 1, 10, 0, 155),
(150, 1, 10, 12, 156),
(151, 1, 10, 12, 157),
(152, 1, 10, 7, 158),
(153, 1, 2, 2, 159),
(154, 1, 10, 0, 160),
(155, 1, 10, 7, 161),
(156, 1, 2, 2, 162),
(157, 1, 10, 0, 163),
(158, 1, 10, 0, 164),
(159, 2, 2, 0, 1),
(160, 2, 2, 0, 2),
(161, 2, 2, 0, 3),
(162, 2, 2, 0, 4),
(163, 1, 2, 7, 165),
(164, 1, 2, 0, 166),
(165, 1, 2, 7, 167),
(166, 1, 2, 12, 168),
(167, 1, 2, 12, 169),
(168, 1, 10, 7, 170),
(169, 1, 10, 0, 171),
(170, 1, 10, 12, 172),
(171, 1, 2, 12, 173),
(172, 1, 2, 7, 174),
(173, 1, 2, 0, 175),
(174, 1, 10, 0, 176),
(175, 1, 10, 0, 177),
(176, 1, 10, 7, 178),
(177, 1, 2, 7, 179),
(178, 1, 2, 7, 180),
(179, 1, 2, 12, 181),
(180, 1, 2, 7, 182),
(181, 1, 2, 12, 183),
(182, 1, 2, 0, 184),
(183, 1, 2, 12, 185),
(184, 1, 2, 0, 186),
(185, 1, 2, 0, 187),
(186, 1, 2, 0, 188),
(187, 1, 2, 0, 189),
(188, 1, 2, 12, 190),
(189, 1, 2, 12, 191),
(190, 1, 2, 12, 192),
(191, 1, 2, 0, 193),
(192, 1, 2, 12, 194),
(193, 1, 2, 0, 195),
(194, 1, 2, 0, 196),
(195, 1, 2, 12, 197),
(196, 1, 2, 0, 198),
(197, 1, 2, 0, 199),
(198, 1, 2, 0, 200),
(199, 1, 2, 0, 201),
(200, 1, 2, 0, 202),
(201, 1, 2, 0, 203),
(202, 1, 2, 7, 204),
(203, 1, 2, 7, 205),
(204, 1, 2, 7, 206),
(205, 1, 2, 7, 207),
(206, 1, 2, 7, 208),
(207, 1, 2, 7, 209),
(208, 1, 2, 7, 210),
(209, 1, 2, 7, 211),
(210, 1, 2, 7, 212),
(211, 1, 2, 7, 213),
(212, 1, 2, 7, 214),
(213, 1, 2, 7, 215),
(214, 1, 2, 7, 216),
(215, 1, 2, 7, 217),
(216, 1, 2, 7, 218),
(217, 1, 2, 0, 219),
(218, 1, 2, 7, 220),
(219, 1, 2, 7, 221),
(220, 1, 2, 0, 222),
(221, 1, 2, 0, 223),
(222, 1, 2, 0, 224),
(223, 1, 2, 16, 225),
(224, 1, 2, 16, 226),
(225, 1, 2, 7, 227),
(226, 1, 2, 7, 228),
(227, 1, 2, 7, 229),
(228, 1, 2, 0, 230),
(229, 1, 2, 7, 231),
(230, 1, 2, 0, 232),
(231, 1, 2, 0, 233),
(232, 1, 2, 7, 234),
(233, 1, 2, 7, 235),
(234, 1, 2, 0, 236),
(235, 1, 2, 0, 237),
(236, 1, 2, 0, 238),
(237, 1, 2, 0, 239),
(238, 1, 2, 0, 240),
(239, 1, 2, 0, 241),
(240, 1, 2, 7, 242),
(241, 1, 2, 7, 243),
(242, 1, 2, 0, 244),
(243, 1, 2, 7, 245),
(244, 1, 2, 7, 246),
(245, 1, 2, 12, 247),
(246, 1, 2, 0, 248),
(247, 1, 2, 0, 249),
(248, 1, 2, 0, 250),
(249, 1, 2, 16, 251),
(250, 1, 2, 16, 252),
(251, 1, 2, 0, 253),
(252, 1, 2, 16, 254),
(253, 1, 2, 16, 255),
(254, 1, 2, 0, 256),
(255, 1, 2, 0, 257),
(256, 1, 2, 0, 258),
(257, 1, 2, 12, 259);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'ankit .', 'ankir@apporio.com', '+919540956147', '123456', '', '', 0, '0', 'Tuesday, Sep 26', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.7', 0, '', 1, '2017-09-26', 2),
(2, 1, 'Rachid Kebdi .', 'rkebdi@gmail.com', '+31631237426', 'Sabrine@05', '', '', 0, '0', 'Tuesday, Oct 10', '', 0, 0, 0, 0, 0, '', '', '', '', '', '110985281083699414855', 'Rachid Kebdi', 'rkebdi@gmail.com', 'https://lh4.googleusercontent.com/-F3U2QWdwpUo/AAAAAAAAAAI/AAAAAAAAAFk/T6fFq2g31GE/photo.jpg', '', '', 0, 0, '3.56435643564', 0, '', 1, '2017-10-10', 1),
(3, 1, 'vikalp .', 'vikalp@apporio.com', '+919898989898', 'qwerty', 'http://apporio.org/TaxiUApp/taxiuapp/uploads/swift_file65.jpeg', '', 0, '0', 'Saturday, Oct 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-14', 2),
(4, 1, 'vikalp .', 'vikalp@apporio.com', '+91979797979797', 'qwerty', '', '', 0, '0', 'Saturday, Oct 14', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '5', 0, '', 1, '2017-10-14', 1),
(5, 1, 'rich', 'rkebdi@gmail.com', '+310624372264', '', '', '', 0, '0', '', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '0000-00-00', 1),
(6, 1, 'rich', 'rkebdi@gmail.com', '+2120624372264', '', '', '', 0, '0', '', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 1, '0000-00-00', 2),
(7, 1, 'rkebdi', 'rkebdi@kpnmail.nl', '+212062437264', '', '', '', 0, '0', '', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '4.4765625', 0, '', 1, '0000-00-00', 1),
(8, 1, 'Apporio devices .', '', '+918874531856', '', '', '', 0, '0', 'Monday, Oct 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '111976537605501585996', 'Apporio devices', 'apporiodevices@gmail.com', 'null', '', '', 0, 0, '3.4375', 0, '', 3, '2017-10-16', 1),
(9, 1, 'Abhaya .', 'bababa@6.com', '+911234567812', 'qwerty', '', '', 0, '0', 'Monday, Oct 16', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-16', 1),
(10, 1, 'Boutaina Kebdi ', 'boutaina@kpnmail.nl', '+31624702937', '', '', '', 0, '0', 'Tuesday, Oct 17', '', 0, 0, 0, 0, 0, '1261731907266584', '1261731907266584@facebook.com', 'https://graph.facebook.com/1261731907266584/picture?width=640&height=640', 'Boutaina Kebdi', '', '', '', '', '', '', '', 0, 0, '4.78260869565', 0, '', 2, '2017-10-17', 1),
(11, 1, 'user .', 'user@8.com', '+911234567821', 'qwerty', '', '', 0, '0', 'Monday, Oct 23', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '3.88888888889', 0, '', 1, '2017-10-23', 1),
(12, 1, 'Apporio Singh ', '', '+91884567866788', '', '', '', 0, '0', 'Tuesday, Oct 24', '', 0, 0, 0, 0, 0, '498368620526444', '498368620526444@facebook.com', 'https://graph.facebook.com/498368620526444/picture?width=640&height=640', 'Apporio Singh', '', '', '', '', '', '', '', 0, 0, '5', 0, '', 2, '2017-10-24', 1),
(13, 1, 'shilpa garg .', '', '+918130039030', '', '', '', 0, '0', 'Tuesday, Oct 24', '', 0, 0, 0, 0, 0, '', '', '', '', '', '110962357007210962758', 'shilpa garg', 'garg26.shilpa@gmail.com', 'https://plus.google.com/_/focus/photos/private/AIbEiAIAAABECMa-y57fs4eRmAEiC3ZjYXJkX3Bob3RvKig1ZDI1NzhjMWZjMGE4N2E2NjBhNTFjNDhjNjNmMTU3OWVmZGU5NGI4MAEortAaGezrYk9clKR9SrV0Xa_cqg', '', '', 0, 0, '2.25', 0, '', 3, '2017-10-24', 1),
(14, 1, 'ANURAG UPADHYAY', 'anurag@apporio.com', '++919205414742', 'anurag', '', '', 0, '0', ' Wednesday, Nov 1', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '0000-00-00', 1),
(15, 1, 'sell .', '9@90.com', '+911234567123', 'qwerty', '', '', 0, '0', 'Thursday, Nov 9', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-09', 1),
(16, 1, 'banana .', '8@0.com', '+911234554321', 'qwerty', '', '', 0, '0', 'Thursday, Nov 9', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 8, 'exkD1ELlfh4:APA91bEUqfMMoySKi_fFD6zHBPxal9_Y7KtN0Hqq-Yn818TpLxZ49duyvU9JZnO2jAo_i1Svqwm3UIh5K1vvh_SOt-Ss1ZuuSQaQD1Z19DkhyXFb-UTZYA2fkHdRN71yzK-Q0KCF5n8j', 0, 'e659dbb5fa6025fb', 1),
(2, 2, 'cOMS1hCIbhw:APA91bHyOsZ5PyhrIF0HfDHzxeqWYRxSYiiRbfCz8kxZoflOcW-8iFO620WoYDuMFs0yV4ArmF5AvQvTUs5tXRsPb9xffTLOEVus-Uouq9M8erMb0i8VBNGwI3-P6qP-rqdjXIcxLZHt', 0, '826021fcd250ae50', 1),
(3, 4, '34D898E779EEA9AAA1731D7041C98D08ACE6AEF1B5B2BEC42BEFBC9B64D48A5B', 1, '8E0D3373-0DC8-43DA-9257-7363DF48C020', 0),
(4, 16, '08A18ACD87FF9A0DF2C8DA756478921DAD9CF62EBFBC9E335FB2976C2DFA7CC9', 1, 'FEBA9BFE-D6A6-433F-81A1-6F538ECCC1E7', 1),
(5, 10, 'BD2C01A1E7D5CC9EC9A6F241BB0173A3715BEF0388A15EA5E1C0A38E14813E22', 1, '441243A3-DBA5-43D8-A24F-6A14BB6DC82D', 1),
(6, 11, '343E3A45288978343E40B88CE4DE952691F03CEE83D1A0BF1FD534A0F2AA634A', 1, 'D90406DE-7F70-4A96-8CB1-1FE9E794C6D0', 0),
(7, 12, 'D692FB76BD5A33B4DCF0EF199941DDD1384800876CD264F220FC61789A39F78D', 1, 'D2B2EF30-084E-4D2C-B71F-B0645D6D37B6', 1),
(8, 1, 'czDGgYFHcI8:APA91bFYdJzqtVh47DyA6bEsyq2U5w8gDNxhleHJvExn6_L2LRclTgYYVyHppMOhgU8xDRYAR53SC7bzVaCNgDXipfYDz4EyjNh2FQuvOdu_miZqvBuERnDHi88EvSITdQW1oWkqxCIg', 0, '32c87e564d926edc', 1),
(9, 13, 'evqqMM9Fqa4:APA91bGberyPuDa-1CNPj4EuyZ3iAhXN2qEtsF3PuhacLoFQBPbp4fD2sGyTWDqmlVCK6CsvGin2p1JOKpRqqJcIoypgovMShoN-tgiR0_wTqcim2kgz-rorGeq1SDFwTONb-5Lbg44d', 0, '9f1891df4b8e4e77', 0),
(10, 10, '1BB3003FD8318A20138F97E9213E6C0D2B6C0067350075D95086118EB7673558', 1, '0C7DD664-FCB1-445D-8948-B9B90C86EFCF', 1),
(11, 10, '1784B96182B59F4A55D9273DEECDC180BBC1594B4CC29D74438E6EC6AED31525', 1, 'CF47D71E-DAA5-4733-B048-BF3E1578E2B9', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'Meet TaxiUapp', '\"TaxiUapp is the best alternative for taxi services in Amsterdam. The TaxiUapp and limousine service offers excellent quality, standard or luxury cars and Minivans at reasonable fares and transparancy for consumers and drivers. Everytime you order a TaxiUapp ride, a standard taxi, luxury taxi or Minivan by our TaxiUapp, you will get a fully licensed and insured professional driver. No exceptions at all service.\"\r\n                                                                                                                                                 \r\n'),
(2, 'معلومات عنا', 'أبوريو إنفولابس الجندي. المحدودة هي شهادة إسو\r\nتطبيقات الهاتف المتحرك وشبكة تطوير التطبيقات على شبكة الإنترنت في الهند. نحن نقدم نهاية إلى نهاية الحل من تصميم لتطوير البرمجيات. نحن فريق من 30+ الناس الذي يتضمن المطورين ذوي الخبرة والمصممين المبدعين.\r\nومن المعروف أبوريو إنفولابس لتقديم برامج ذات جودة ممتازة لعملائها. وتنتشر قاعدة عملائنا في أكثر من 20 دولة بما في ذلك الهند والولايات المتحدة والمملكة المتحدة وأستراليا وإسبانيا والنرويج والسويد والإمارات العربية المتحدة وقطر وسنغافورة وماليزيا ونيجيريا وجنوب أفريقيا وإيطاليا وبرمودا وهونغ كونغ.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@taxiuapp.com', '+31624372264', 'TaxiUapp', 'TaxiUapp, Katernstraat, Almere'),
(2, 'اتصل بنا', 'معلومات الاتصال', 'hello@apporio.com', '+91 8800633884', 'apporio', 'غوروغرام، هريانا، الهند أبوريو إنفولابس الجندي. Ltd.،');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `web_headings`
--

CREATE TABLE `web_headings` (
  `heading_id` int(11) NOT NULL,
  `heading_1` text NOT NULL,
  `heading1` text NOT NULL,
  `heading2` text NOT NULL,
  `heading3` text NOT NULL,
  `heading4` text NOT NULL,
  `heading5` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_headings`
--

INSERT INTO `web_headings` (`heading_id`, `heading_1`, `heading1`, `heading2`, `heading3`, `heading4`, `heading5`) VALUES
(1, 'Book a TaxiUapp ride to your destination in town', 'A cab for every occasion and pocket ', 'Meet our awesome Fleet', 'The widest variety of cars to choose from', 'Why take ride with TaxiUApp?', 'A taxi for every occasion and pocket'),
(2, 'Ø­Ø¬Ø² ØªØ§ÙƒØ³ÙŠÙˆØ§Ø¨ Ø±ÙƒÙˆØ¨ Ø¥Ù„Ù‰ ÙˆØ¬Ù‡ØªÙƒ ÙÙŠ Ø§Ù„Ù…Ø¯ÙŠÙ†Ø©', 'Ø³ÙŠØ§Ø±Ø© Ø£Ø¬Ø±Ø© ÙÙŠ ÙƒÙ„ Ù…Ù†Ø§Ø³Ø¨Ø© ÙˆØ§Ù„Ø¬ÙŠØ¨', 'ØªÙ„Ø¨ÙŠØ© Ø£Ø³Ø·ÙˆÙ„Ù†Ø§ Ø±Ù‡ÙŠØ¨Ø©', 'Ø£ÙˆØ³Ø¹ Ù…Ø¬Ù…ÙˆØ¹Ø© Ù…Ù† Ø§Ù„Ø³ÙŠØ§Ø±Ø§Øª Ù„Ù„Ø§Ø®ØªÙŠØ§Ø± Ù…Ù† Ø¨ÙŠÙ†Ù‡Ø§', 'Ù„Ù…Ø§Ø°Ø§ ØªØ£Ø®Ø° Ø±ÙƒÙˆØ¨ Ù…Ø¹ ØªØ§ÙƒØ³ÙŠÙˆØ§Ø¨ØŸ', 'Ø³ÙŠØ§Ø±Ø© Ø£Ø¬Ø±Ø© ÙÙŠ ÙƒÙ„ Ù…Ù†Ø§Ø³Ø¨Ø© ÙˆØ§Ù„Ø¬ÙŠØ¨');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `app_name` varchar(255) NOT NULL DEFAULT '',
  `web_footer` varchar(765) DEFAULT '',
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `app_name`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'TaxiUapp || Website', 'TaxiUapp', '2017 TaxiUapp', 'uploads/website/banner_1501227855.jpg', '', '', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', '', '', 'uploads/website/google_play_btn1501228522.png', '', 'uploads/website/itunes_btn1501228522.png', '', '', '', 'uploads/website/heading1_img1501228907.png', '', 'uploads/website/heading2_img1501228907.png', '', '', '', 'uploads/website/heading3_img1501228907.png', '', '', '', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', '', '', 'uploads/website/features1_bg1501241213.png', '', '', 'uploads/website/features2_bg1501241213.png'),
(2, 'Ø­Ø¬Ø² Ø±ÙƒÙˆØ¨ Ø³ÙŠØ§Ø±Ø© Ø£Ø¬Ø±Ø© Ø¥Ù„Ù‰ ÙˆØ¬Ù‡ØªÙƒ ÙÙŠ Ø§Ù„Ù…Ø¯ÙŠÙ†Ø©', 'Ø³ÙŠØ§Ø±Ø© Ø£Ø¬Ø±Ø© U Ø§Ù„ØªØ·Ø¨ÙŠÙ‚', '2017 Ø³ÙŠØ§Ø±Ø© Ø£Ø¬Ø±Ø© U Ø§Ù„ØªØ·Ø¨ÙŠÙ‚', 'uploads/website/banner_1501227855.jpg', '', '', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', '', '', 'uploads/website/google_play_btn1501228522.png', '', 'uploads/website/itunes_btn1501228522.png', '', '', '', 'uploads/website/heading1_img1501228907.png', '', 'uploads/website/heading2_img1501228907.png', '', '', '', 'uploads/website/heading3_img1501228907.png', '', '', '', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', NULL, '', '', 'uploads/website/features1_bg1501241213.png', '', '', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_home_pages`
--

CREATE TABLE `web_home_pages` (
  `page_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL DEFAULT '',
  `heading_arabic` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `content_arabic` text NOT NULL,
  `long_content` text NOT NULL,
  `long_content_arabic` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `big_image` varchar(255) NOT NULL DEFAULT '',
  `blog_date` varchar(255) NOT NULL DEFAULT '',
  `blog_time` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home_pages`
--

INSERT INTO `web_home_pages` (`page_id`, `heading`, `heading_arabic`, `content`, `content_arabic`, `long_content`, `long_content_arabic`, `image`, `big_image`, `blog_date`, `blog_time`) VALUES
(1, 'Taxis for Every Pocket', 'سيارات الأجرة لكل جيب', 'From Sedans and SUVs to Luxury cars for special occasions, we have cabs to suit every pocket ', 'من سيارات السيدان وسيارات الدفع الرباعي إلى السيارات الفاخرة للمناسبات الخاصة، لدينا سيارات الأجرة لتناسب كل جيب', '<br>', '', 'webstatic/img/ola-article/why-ola-1.jpg', '', 'Thursday, Oct 26', '18:14:34'),
(2, 'Secure and Safer Rides', 'ركوب آمنة وأكثر أمنا', 'Verified drivers, an emergency alert button, and live ride tracking are some of the features that we have in place to ensure you a safe travel experience.', 'السائقين التحقق، زر تنبيه في حالات الطوارئ، وتتبع ركوب الحية هي بعض من الميزات التي لدينا في مكان لضمان تجربة السفر آمنة.', '', '', 'webstatic/img/ola-article/why-ola-3.jpg', '', '', ''),
(3, 'TaxiU Select', 'أبوريو حدد', 'A membership program with TaxiU that lets you ride a Prime Sedan at Mini fares, book cabs without peak pricing and has zero wait time ', 'برنامج العضوية مع أبوريو التي تمكنك من ركوب رئيس سيدان في فارس مصغرة، سيارات الأجرة كتاب دون التسعير الذروة، ولها وقت الانتظار صفر', '<br>', '', 'webstatic/img/ola-article/why-ola-2.jpg', '', 'Thursday, Oct 26', '18:15:02'),
(4, 'In Cab Entertainment', 'في الكابيه الترفيه', 'Play music, watch videos and a lot more with TaxiUapp Play! Also stay connected even if you are travelling through poor network areas with our free wifi facility. ', 'شغيل الموسيقى، ومشاهدة أشرطة الفيديو وأكثر من ذلك بكثير مع أبوريو اللعب! أيضا البقاء على اتصال حتى لو كنت مسافرا من خلال مناطق الشبكة الفقيرة مع شركائنا في مرفق واي فاي مجانا.', '<br>', '', 'webstatic/img/ola-article/why-ola-9.jpg', '', 'Thursday, Oct 26', '18:15:41'),
(5, 'Cash and cashless Rides', 'ركوب غير النقدي', 'Now go cashless and travel easy. Simply recharge your TaxiU Taxi money or add your credit/debit card to enjoy hassle free payments. ', 'شغيل الموسيقى، ومشاهدة أشرطة الفيديو وأكثر من ذلك بكثير مع أبوريو اللعب! من السهل جدا أن تكون قادرا على البقاء.', '<br>', '', 'webstatic/img/ola-article/why-ola-5.jpg', '', 'Thursday, Oct 26', '18:21:59'),
(6, 'No More Surge Pricing', 'حصة والتعبير', '<p>TaxiUapp says No to Surge pricing. No more ridiculous surge pricing with TaxiUApp, Period.</p>  ', 'للسفر مع أدنى الأسعار اختيار أبوريو حصة. للحصول على تجربة سفر أسرع لدينا حصة اكسبرس على بعض الطرق الثابتة مع الانحرافات صفر. اختيار رحلتك والتكبير بعيدا!', '<br>', '', 'uploads/websitebloppages/blog_118:27:46.jpg', '', 'Thursday, Oct 26', '18:27:46');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `application_currency`
--
ALTER TABLE `application_currency`
  ADD PRIMARY KEY (`application_currency_id`);

--
-- Indexes for table `application_version`
--
ALTER TABLE `application_version`
  ADD PRIMARY KEY (`application_version_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_headings`
--
ALTER TABLE `web_headings`
  ADD PRIMARY KEY (`heading_id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_currency`
--
ALTER TABLE `application_currency`
  MODIFY `application_currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `application_version`
--
ALTER TABLE `application_version`
  MODIFY `application_version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=293;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1096;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_headings`
--
ALTER TABLE `web_headings`
  MODIFY `heading_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
