<?php
function getAddress($latitude,$longitude){
    if(!empty($latitude) && !empty($longitude)){
        $geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&key=AIzaSyAP7-oE7YEjtqIhiAbKVP1MO4Xv_mZAd-k');
        $output = json_decode($geocodeFromLatLong);
        $status = $output->status;
        $address = ($status=="OK")?$output->results[0]->formatted_address:'';
        if(!empty($address)){
            return $address;
        }else{
            return false;
        }
    }else{
        return false;   
    }
}
?>