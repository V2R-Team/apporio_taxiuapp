<?php
function ride_clear($driver_id)
{
    $url = 'https://taxiuapp-4edc1.firebaseio.com/Activeride/'.$driver_id.'/.json';
    $fields = array(
        'ride_id' => "No Ride",
        'ride_status'=>"No Ride Status",
    );
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
    $response = curl_exec($ch);
    if(!$response) {
        return false;
    }else{
        return true;
    }
}

?>