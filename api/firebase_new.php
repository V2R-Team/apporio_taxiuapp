<?php
function new_ride($nodes)
{
    $url = 'https://taxiuapp-4edc1.firebaseio.com/Activeride/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
    curl_setopt($ch,CURLOPT_POST, count($nodes));
    curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($nodes));
    $response = curl_exec($ch);
    if(!$response) {
        return false;
    }else{
        return true;
    }
}

?>