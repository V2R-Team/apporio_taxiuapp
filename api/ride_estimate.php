<?php
//error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

$distance = $_REQUEST['distance'];
$city_id = $_REQUEST['city_id'];
$car_type_id = $_REQUEST['car_type_id'];
//$language_id = $_REQUEST['language_id'];
$language_id=1;
$pickup_lat = $_REQUEST['pickup_lat'];
$pickup_long = $_REQUEST['pickup_long'];
if($distance !="" && $city_id !="" && $car_type_id !="" && $pickup_long != "" && $pickup_lat != "")
{


	$query="select * from price_card where city_id='$city_id' and car_type_id='$car_type_id'";
	$result = $db->query($query);
    $ex_rows=$result->num_rows;
    $list3 = $result->row;
	if($ex_rows==0)
	{
		$re = array('result'=> 0,'msg'	=> "No Rate Card Available",);
	}
	else
	{
		$list3 = $result->row;
		$distance_unit = $list3['distance_unit'];
		if($distance_unit == "Km"){
		   $distance1 = ($distance/1000);
		}else{
		  $distance1 = $distance * 0.00062137;
		}
		$base_distance = $list3['base_distance'];
        $base_distance_price = $list3['base_distance_price'];
		$base_price_per_unit = $list3['base_price_per_unit'];

		if($distance1 <= $base_distance)
		{
			$final_amount = $base_distance_price;
            $final_amount_string =(String)$final_amount;
            $final_amount_string = number_format($final_amount_string , 2, '.', '');
		}
		else
		{
			$diff_distance = $distance1-$base_distance;
			$amount1=($diff_distance * $base_price_per_unit);
			$final_amount = $base_distance_price+$amount1;
            $final_amount_string =(String)$final_amount;
            $final_amount_string =(String)$final_amount;
            $final_amount_string = number_format($final_amount_string , 2, '.', '');
		}
        $query3="select * from driver where car_type_id='$car_type_id'  and online_offline = 1 and driver_admin_status=1 and busy=0 and login_logout=1";
        $result3 = $db->query($query3);
        $ex_rows=$result3->num_rows;
        if($ex_rows == 0){
            $time = "NO Nearest Driver";
        }else{
            $list3=$result3->rows;
            $c = array();
            foreach($list3 as $login3)
            {
                $driver_lat = $login3['current_lat'];
                $driver_long =$login3['current_long'];

                $theta = $pickup_long - $driver_long;
                $dist = sin(deg2rad($pickup_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($pickup_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $km=$miles* 1.609344;
                if($km<= 10)
                {
                    $c[] = array("driver_id"=> $login3['driver_id'],"distance" => $km,"driver_lat"	=> $login3['current_lat'],"driver_long"	=> $login3['current_long']);
                }
            }
            if(!empty($c)){

                foreach ($c as $value)
                {
                    $distance = $value['distance'];
                    $v[] = $distance;
                }
                array_multisort($v,SORT_ASC,$c);
                $current_lat = $c[0]['driver_lat'];
                $current_long = $c[0]['driver_long'];

                $from 		= $pickup_lat.",".$pickup_long;
                $to 		= $current_lat.",".$current_long;
                $data 	= file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=en-EN&key=AIzaSyCTAg2TXt8AiswL_Ro6k76c_p1bisIWxEU");
                $data 		= json_decode($data, true);
                $status=$data['rows'][0]['elements'][0]['status'];

                if($status=="OK") {
                    $dist = $data['rows'][0]['elements'][0]['distance']['text'];
                    $dist_inval = $data['rows'][0]['elements'][0]['distance']['value'];
                    $time = $data['rows'][0]['elements'][0]['duration']['text'];
                }else{
                    $time = "No Time Found";
                }
            }else{
                $time = "NO Nearest Driver";
            }
        }
        $re = array('result'=> 1,'msg'	=> $final_amount_string,'estimatetime'=>$time);
	}
}
else
{
	$re = array('result'=> 0,'msg'=> "Require fields Missing!!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>