<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';
include 'firebase_clar.php';
function sortByOrder($a, $b)
{
    return $a['distance'] - $b['distance'];
}

$ride_id=$_REQUEST['ride_id'];
$driver_id=$_REQUEST['driver_id'];
$ride_status=$_REQUEST['ride_status'];
$driver_token=$_REQUEST['driver_token'];
//$language_id=$_REQUEST['language_id'];
$language_id=1;

if($ride_id != "" && $driver_id != "" && $ride_status != "" && $driver_token!= ""  )
{

    $query="select * from ride_table where ride_id='$ride_id'";
    $result = $db->query($query);
    $list = $result->row;

    $ride_status = $list['ride_status'];
    $ride_type = $list['ride_type'];
    $pem_file = $list['pem_file'];
    if($ride_type == 1){
        $ride_time = strtotime($list['ride_time']);
        $ride_expire_time =   strtotime(date('H:i:s',time() - 1 * 60));
    }else{
        $ride_time = 1;
        $ride_expire_time = 0;
    }

    if($ride_status == 1 && $ride_time >= $ride_expire_time){

        $query="select * from driver where driver_token='$driver_token' AND driver_id='$driver_id'";
        $result = $db->query($query);
        $ex_rows=$result->num_rows;
        if($ex_rows==1)
        {

            $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
            $date=$dt->format('M j, Y');
            $day=date("l");
            $date=$day.", ".$date;
            $new_time=date("H:i");
            ride_clear($driver_id);
            $query2="INSERT INTO ride_reject (reject_ride_id, reject_driver_id) VALUES('$ride_id','$driver_id')" ;
            $db->query($query2);

            $query3="select * from driver WHERE driver_id='$driver_id'" ;
            $result3 = $db->query($query3);
            $list3=$result3->row;
            $reject_rides=$list3['reject_rides']+"1";
            $query4="UPDATE driver SET reject_rides='$reject_rides',last_update='$new_time',last_update_date='$date' WHERE driver_id='$driver_id'" ;
            $db->query($query4);

            $query35="select * from ride_reject WHERE reject_ride_id='$ride_id'";
            $result35 = $db->query($query35);
            $ex_rows1=$result35->num_rows;


            if($ex_rows1==1)
            {
                $query5="select * from ride_table where ride_id='$ride_id'";
                $result5 = $db->query($query5);
                $list5=$result5->row;

                $pickup_lat=$list5['pickup_lat'];
                $pickup_long=$list5['pickup_long'];
                $car_type_id=$list5['car_type_id'];

                $query3="select * from driver where car_type_id='$car_type_id' and driver_id !='$driver_id' and online_offline = 1 and driver_admin_status=1 and busy=0 and login_logout=1";
                $result3 = $db->query($query3);
                $list3=$result3->rows;


                $c = array();
                foreach($list3 as $login3)
                {
                    $driver_lat = $login3['current_lat'];
                    $driver_long =$login3['current_long'];

                    $theta = $pickup_long - $driver_long;
                    $dist = sin(deg2rad($pickup_lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($pickup_lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
                    $dist = acos($dist);
                    $dist = rad2deg($dist);
                    $miles = $dist * 60 * 1.1515;
                    $unit = strtoupper("K");
                    if ($unit == "K")
                    {
                        $km=$miles* 1.609344;
                    }
                    else if ($unit == "N")
                    {
                        $miles * 0.8684;
                    }
                    else
                    {
                        $miles;
                    }
                    if($km<= 10)
                    {
                        $c[] = array("driver_id"=> $login3['driver_id'],"distance" => $km,);
                    }
                }
                usort($c, 'sortByOrder');
                $size=sizeof($c);

                if($size > 0)
                {
                    $query4="select * from ride_allocated where allocated_ride_id='$ride_id' and allocated_driver_id='$driver_id'";
                    $result4 = $db->query($query4);
                    $ex_rows=$result4->num_rows;

                    if($ex_rows==1)
                    {

                        $driver_id = $c[0]['driver_id'];

                        $query5="INSERT INTO ride_allocated (allocated_ride_id,allocated_driver_id) VALUES ('$ride_id','$driver_id')";
                        $db->query($query5);

                        $query4="select * from driver where driver_id='$driver_id'";
                        $result4 = $db->query($query4);
                        $list4=$result4->row;

                        $device_id=$list4['device_id'];

                        $message="New Ride Allocated";
                        $ride_id= (String) $ride_id;
                        $ride_status= "1";

                        if($device_id!="")
                        {
                            if($list4['flag'] == 1)
                            {
                                IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);
                            }
                            else
                            {
                                AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                            }
                        }

                    }
                    else
                    {
                        $query5="INSERT INTO ride_allocated (allocated_ride_id,allocated_driver_id) VALUES ('$ride_id','$driver_id')";
                        $db->query($query5);

                        $query4="select * from driver where driver_id='$driver_id'";
                        $result4 = $db->query($query4);
                        $list4=$result4->row;

                        $device_id=$list4['device_id'];

                        $message="New Ride Allocated";
                        $ride_id= (String) $ride_id;
                        $ride_status= "1";


                        if($device_id!="")
                        {
                            if($list4['flag'] == 1)
                            {
                                IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status,$pem_file);
                            }
                            else
                            {
                                AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                    }
                }
                else
                {

                    $query11="UPDATE ride_table SET driver_id='$driver_id' , ride_status='$ride_status' WHERE ride_id='$ride_id'" ;
                    $db->query($query11);

                    $query5="select * from ride_table where ride_id='$ride_id'";
                    $result5 = $db->query($query5);
                    $list5=$result5->row;
                    $user_id=$list5['user_id'];

                    $query6="select * from user_device where user_id='$user_id' AND login_logout=1";
                    $result6= $db->query($query6);
                    $list6=$result6->row;

                    $message="Booking Rejected";
                    $ride_id= (String) $ride_id;
                    $ride_status= (String) $ride_status;


                    if (!empty($list6))
                    {
                        foreach ($list6 as $user)
                        {
                            $device_id = $user['device_id'];
                            $flag = $user['flag'];
                            if($flag == 1)
                            {
                                IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                            }
                            else
                            {
                                AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                    }
                }
            }
            else if($ex_rows==2)
            {

                $query11="UPDATE ride_table SET driver_id='$driver_id' , ride_status='$ride_status' WHERE ride_id='$ride_id'" ;
                $db->query($query11);

                $query5="select * from ride_table where ride_id='$ride_id'";
                $result5 = $db->query($query5);
                $list5=$result5->row;
                $user_id=$list5['user_id'];

                $query6="select * from user_device where user_id='$user_id' AND login_logout=1";
                $result6= $db->query($query6);
                $list6=$result6->row;

                $message="Booking Rejected";
                $ride_id= (String) $ride_id;
                $ride_status= (String) $ride_status;


                if (!empty($list6))
                {
                    foreach ($list6 as $user)
                    {
                        $device_id = $user['device_id'];
                        $flag = $user['flag'];
                        if($flag == 1)
                        {
                            IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status,$pem_file);
                        }
                        else
                        {
                            AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
                        }
                    }
                }
            }
            $language="select * from messages where language_id='$language_id' and message_id=30";
            $lang_result = $db->query($language);
            $lang_list=$lang_result->row;
            $message_name=$lang_list['message_name'];
            $re = array('result'=> 1,'msg'=> $message_name);
        }
        else
        {
            $re = array('result'=> 419,'msg'=> "No Record Found",);
        }
    }else{
        $re = array('result'=> 0,'msg'=>"Ride Expire");
    }
}
else
{
    $re = array('result' => 0,'msg'	=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>
