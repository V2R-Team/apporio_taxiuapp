<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';
include 'location_fromlatlog.php';
include 'ride-sms.php';
include 'snaptoroad.php';
$ride_id = $_REQUEST['ride_id'];
$driver_id=$_REQUEST['driver_id'];
$end_lat=$_REQUEST['end_lat'];
$end_long=$_REQUEST['end_long'];
$meter_distance = $_REQUEST['distance'];
$driver_token=$_REQUEST['driver_token'];
//$language_id=$_REQUEST['language_id'];
$lat_long = $_REQUEST['lat_long'];
$language_id=1;
$log  = "ride end Api - : ".date("F j, Y, g:i a").PHP_EOL.
        "ride_id: ".$ride_id.PHP_EOL.
        "driver_id: ".$driver_id.PHP_EOL.
        "end_lat: ".$end_lat.PHP_EOL.
        "end_long: ".$end_long.PHP_EOL.
        "meter_distance: ".$meter_distance.PHP_EOL.
        "lat_long: ".$lat_long.PHP_EOL.
        "-------------------------".PHP_EOL;
file_put_contents('../logfile/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
if($ride_id!="" && $driver_id!="" && $end_lat!="" && $end_long!="" && $driver_token!= "" )
{
    $query="select * from driver where driver_token='$driver_token' AND driver_id='$driver_id'";
    $result = $db->query($query);
    $ex_rows=$result->num_rows;
    $list = $result->row;
    $total_payment_eraned = $list['total_payment_eraned'];
    $company_payment = $list['company_payment'];
    $driver_payment = $list['driver_payment'];
    $commision = $list['commission'];
    if($ex_rows==1)
    {
        $last_time_stamp = date("h:i:s A");
        $end_time = date("h:i:s A");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date=$day.", ".$data ;
        $new_time=date("h:i");
        $completed_rides = $list['completed_rides']+"1";
        $city_id = $list['city_id'];
        $car_type_id = $list['car_type_id'];
       
        $query5="UPDATE driver SET last_update='$new_time',completed_rides='$completed_rides',busy=0 WHERE driver_id='$driver_id'" ;
        $db->query($query5);

        $query1="UPDATE ride_table SET driver_id='$driver_id' ,last_time_stamp='$last_time_stamp', ride_status='7' WHERE ride_id='$ride_id'" ;
        $db->query($query1);



        $query2 = "select * from price_card where city_id='$city_id' and car_type_id='$car_type_id'";
        $result2 = $db->query($query2);
        $list3 = $result2->row;
        $distance_unit = $list3['distance_unit'];
        $base_distance = $list3['base_distance'];
        $base_distance_price = $list3['base_distance_price'];
        $base_price_per_unit = $list3['base_price_per_unit'];


        $free_ride_minutes = $list3['free_ride_minutes'];
        $price_per_ride_minute = $list3['price_per_ride_minute'];

        $free_waiting_time = $list3['base_wating_time'];
        $wating_price_minute = $list3['wating_price_minute'];
        $query3="select * from done_ride where ride_id='$ride_id'";
        $result3 = $db->query($query3);
        $list=$result3->row;
        $begin_lat = $list['begin_lat'];
        $begin_long = $list['begin_long'];
        $start = $begin_lat.",".$begin_long;
        $finish = $end_lat.",".$end_long;
        if ($lat_long != "")
        {
            $distance = Get_distance($ride_id,$start,$finish,$lat_long);
        }else{
            $distance = $meter_distance;
            
        }

		if($distance_unit == "Km"){
		    $dist1 = ($distance/1000);
		}else{
		  $dist1 = $distance*0.00062137;
		}
        $dist1 = sprintf("%.2f",$dist1);
        if($dist1 <= $base_distance)
        {
            $final_amount = $base_distance_price;
            $final_amount = sprintf("%.2f",$final_amount);
        }
        else
        {
            $diff_distance = $dist1-$base_distance;
            $amount1= ($diff_distance * $base_price_per_unit);
            $final_amount = $base_distance_price+$amount1;
            $final_amount = sprintf("%.2f",$final_amount);
        }

        $done_ride=$list['done_ride_id'];
        $begin_time = $list['begin_time'];
        $waiting_time = $list['waiting_time'];

        $datetime1 = strtotime($begin_time);
        $datetime2 = strtotime($end_time);
        $interval  = abs($datetime2 - $datetime1);
        $ride_time   = round($interval / 60);



        $dist = $dist1." ".$distance_unit;
		$end_location = getAddress($end_lat,$end_long);
        $end_location = $end_location?$end_location:'Address Not found';
        $query2="UPDATE done_ride SET meter_distance='$meter_distance',end_lat='$end_lat',end_long='$end_long', end_location='$end_location', end_time='$end_time', amount='$final_amount', distance='$dist',tot_time='$ride_time' WHERE ride_id='$ride_id'";
        $db->query($query2);


        if($ride_time > $free_ride_minutes)
        {
            $diff_distance = $ride_time-$free_ride_minutes;
            $amount1= ($diff_distance * $price_per_ride_minute);
            $amount1 = sprintf("%.2f", $amount1);
            $query2="UPDATE done_ride SET  ride_time_price='$amount1' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }else{
            $query2="UPDATE done_ride SET  ride_time_price='00.00' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }


        if($waiting_time > $free_waiting_time)
        {
            $diff_distance = $waiting_time-$free_waiting_time;
            $final_amount1= ($diff_distance * $wating_price_minute);
            $final_amount1 = sprintf("%.2f", $final_amount1);
            $query2="UPDATE done_ride SET waiting_price='$final_amount1' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }else{
            $query2="UPDATE done_ride SET waiting_price='00.00' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }
        $query4="select * from ride_table where ride_id='$ride_id'";
        $result4 = $db->query($query4);
        $list4=$result4->row;
        $user_id=$list4['user_id'];
        $pem_file = $list4['pem_file'];
        $coupan = $list4['coupon_code'];
        $ride_time = $list4['ride_time'];
        $card_id=$list4['card_id'];
        $ride_status = $list4['ride_status'];
        $payment_option_id=$list4['payment_option_id'];
        $extra_charges_day = date("l");

         
         $query="select * from done_ride where ride_id='$ride_id'";
	      $result = $db->query($query);
	      $list12345 = $result->row;
         $waiting_price = $list12345['waiting_price'];
         $amount =  $list12345['amount'];
         $ride_time_price =  $list12345['ride_time_price'];
         $night_time_charge = $list12345['night_time_charge'];
         $peak_time_charge = $list12345['peak_time_charge'];
         $total_amount =  $waiting_price+$amount+$ride_time_price+$night_time_charge+$peak_time_charge;



        $query2 = "select * from extra_charges where city_id='$city_id' and extra_charges_day='$extra_charges_day'";
        $result2 = $db->query($query2);
        $extra_charges = $result2->row;
        if(!empty($extra_charges)){
            $slot_one_starttime = $extra_charges["slot_one_starttime"];
            $slot_one_endtime = $extra_charges["slot_one_endtime"];
            $slot_two_starttime = $extra_charges["slot_two_starttime"];
            $slot_two_endtime = $extra_charges["slot_two_endtime"];
            $payment_type = $extra_charges["payment_type"];
            $slot_price = $extra_charges["slot_price"];
            $time_exploade = explode(":",$ride_time);
            $time = $time_exploade[0].":".$time_exploade[1];
            $ride_strtotime = strtotime($time);
            $slot_one_starttime = strtotime($slot_one_starttime);
            $slot_one_endtime = strtotime($slot_one_endtime);
            $slot_two_starttime = strtotime($slot_two_starttime);
            $slot_two_endtime = strtotime($slot_two_endtime);
            if($ride_strtotime >= $slot_one_starttime && $slot_one_endtime >= $ride_strtotime || $ride_strtotime >= $slot_two_starttime && $slot_one_endtime >= $slot_two_endtime)
            {
                if($payment_type == 1){
                    $peak_time_charge = $slot_price;
                    $total_amount = $total_amount+$peak_time_charge;
                }else{
                    $aa = $total_amount;
                    $total_amount = $total_amount*$slot_price;
                    $peak_time_charge = $total_amount-$aa;
                }
            }
            else
            {
                $peak_time_charge = "0.00";
            }
            $query2="UPDATE done_ride SET  peak_time_charge='$peak_time_charge' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }
        $query2 = "select * from extra_charges where city_id='$city_id' and extra_charges_type=2";
        $result2 = $db->query($query2);
        $extra_charges = $result2->row;
        if(!empty($extra_charges)){
            $slot_one_starttime = $extra_charges["slot_one_starttime"];
            $slot_one_endtime = $extra_charges["slot_one_endtime"];
            $payment_type = $extra_charges["payment_type"];
            $slot_price = $extra_charges["slot_price"];
            $time_exploade = explode(":",$ride_time);
            $time = $time_exploade[0].":".$time_exploade[1];
            $ride_strtotime = strtotime($time);
            $slot_one_starttime = strtotime($slot_one_starttime);
            $slot_one_endtime = strtotime($slot_one_endtime);
            if($ride_strtotime >= $slot_one_starttime && $slot_one_endtime >= $ride_strtotime)
            {
                if($payment_type == 1){
                    $night_time_charge = $slot_price;
                    $total_amount = $total_amount+$night_time_charge;
                }else{
                    $aa = $total_amount;
                    $total_amount = $total_amount*$slot_price;
                    $night_time_charge = $total_amount-$aa;
                }
            }
            else
            {
                $night_time_charge = "0.00";
            }
            $query2="UPDATE done_ride SET  night_time_charge='$night_time_charge' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }






         if($coupan != "")
         {
	    	$query1 ="select * from coupons where coupons_code='$coupan'";
	        $result1 = $db->query($query1);
	        $list1 = $result1->row; 
                $coupon_type = $list1['coupon_type'];
                $coupon_code = $list1['coupons_code'];
               $coupan_price = $list1['coupons_price'];
               if($coupon_type == "Nominal"){
				   if($coupan_price > $total_amount){
					   $total_amount = "0.00";
				   }else{
					   $total_amount = $total_amount-$coupan_price;
				   }
                  
             }else{
                   $coupan_price = ($total_amount*$coupan_price)/100;
                   $total_amount =  $total_amount-$coupan_price;
             }
         }else{
            $coupan_price  = "0.00";
         }
        switch ($payment_option_id) {
            case "1" :
                $total_payed_amount = $total_amount;
                $query="select * from payment_option where payment_option_id='$payment_option_id'";
                $result = $db->query($query);
                $payment =$result->row;
                $payment_method = $payment['payment_option_name'];
                $payment_platform = "Admin";
                $query2="INSERT INTO payment_confirm (order_id,user_id, payment_id, payment_method,payment_platform,payment_amount,payment_date_time,payment_status) 
VALUES('$done_ride',$user_id,'$payment_option_id','$payment_method','$payment_platform','$total_amount','$date','1')";
                $db->query($query2);
                $query3="UPDATE ride_table SET payment_status=1  WHERE ride_id='$ride_id'";
                $db->query($query3);
                $query3="UPDATE done_ride SET payment_status=1,total_payable_amount='$total_amount'  WHERE ride_id='$ride_id'";
                $db->query($query3);
                break;
            case "3":
                require_once('Stripe/lib/Stripe.php');
                Stripe::setApiKey("sk_test_tp8pHbIHhvLlTaJ1180WtUbd");
                Stripe::$apiBase = "https://api.stripe.com";
                $total_payed_amount = $total_amount;
                $query="select * from card where card_id='$card_id'";
                $result = $db->query($query);
                $list=$result->row;
                $customer_id=$list['customer_id'];
                try
                {
                    $charge = Stripe_Charge::create(array( "amount" => $total_amount*100,"currency" => "usd","customer" => $customer_id,));
                    $payment_id = $charge['id'];
                    $status= $charge['outcome']['seller_message'];
                    $query="select * from payment_option where payment_option_id='$payment_option_id'";
                    $result = $db->query($query);
                    $list=$result->row;
                    $payment_method = $list['payment_option_name'];
                    $payment_platform = "Stripe";
                    $query2="INSERT INTO payment_confirm (order_id,user_id,payment_id,payment_method,payment_platform,payment_amount,payment_date_time,payment_status) 
VALUES('$done_ride',$user_id,'$payment_option_id','$payment_method','$payment_platform','$total_amount','$date','$status')";
                    $db->query($query2);
                    $query3="UPDATE ride_table SET payment_status=1  WHERE ride_id='$ride_id'";
                    $db->query($query3);
                    $query3="UPDATE done_ride SET payment_status=1,payment_falied_message='$status',total_payable_amount='$total_amount'  WHERE ride_id='$ride_id'";
                    $db->query($query3);
                }catch(Stripe_CardError $e)
                {
                    $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                    $db->query($query3);
                }
                catch (Stripe_InvalidRequestError $e) {
                    $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                    $db->query($query3);
                } catch (Stripe_AuthenticationError $e) {
                    $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                    $db->query($query3);
                } catch (Stripe_ApiConnectionError $e) {
                    $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                    $db->query($query3);
                } catch (Stripe_Error $e) {
                    $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                    $db->query($query3);
                } catch (Exception $e) {
                    $query3="UPDATE done_ride SET payment_falied_message='$e' WHERE ride_id='$ride_id'";
                    $db->query($query3);
                }
                break;
            case "4":
                $query="select * from user where user_id =$user_id";
                $result = $db->query($query);
                $list2344=$result->row;
                $wallet_money = $list2344['wallet_money'];
                if($wallet_money < $total_amount){
                    $total_payed_amount = $wallet_money;
                    $remain = "0.00";
                    $remain1 = $total_amount-$wallet_money;
                    $payment_falied_message = "Insufficent Balance";
                    $query3="UPDATE done_ride SET wallet_deducted_amount='$wallet_money',total_payable_amount='$remain1', payment_falied_message='$payment_falied_message' WHERE ride_id='$ride_id'";
                    $db->query($query3);
                }else{
                    $total_payed_amount = $total_amount;
                    $remain = $wallet_money-$total_amount;
                    $query="select * from payment_option where payment_option_id='$payment_option_id'";
                    $result = $db->query($query);
                    $list=$result->row;
                    $payment_method = $list['payment_option_name'];
                    $payment_platform = "Admin";
                    $query2="INSERT INTO payment_confirm (order_id,user_id, payment_id, payment_method,payment_platform,payment_amount,payment_date_time,payment_status) 
VALUES('$done_ride',$user_id,'$payment_option_id','$payment_method','$payment_platform','$total_amount','$date','1')";
                    $db->query($query2);
                    $query3="UPDATE ride_table SET payment_status=1  WHERE ride_id='$ride_id'";
                    $db->query($query3);
                    $query3="UPDATE done_ride SET payment_status=1,wallet_deducted_amount='$total_amount'  WHERE ride_id='$ride_id'";
                    $db->query($query3);
                }
                $sql2="UPDATE user SET wallet_money='$remain' WHERE user_id='$user_id'";
                $db->query($sql2);
                break;
            case "2":
                $total_payed_amount = "Paypal";
                $payment_falied_message = "You Have Select Paypal";
                $query3="UPDATE done_ride SET total_payable_amount='$total_amount', payment_falied_message='$payment_falied_message' WHERE ride_id='$ride_id'";
                $db->query($query3);
               break;
        }
     
        if($commision != 0){
	         $commision_price = ($total_payed_amount*$commision)/100;
             $company_commision = number_format((float)$commision_price, 2, '.', '');
             $driver_amount = $total_payed_amount-$company_commision;
             $driver_amount=number_format((float)$driver_amount, 2, '.', '');
	     }else{
            $company_commision = "0.00";
	        $driver_amount = $total_amount;
	    }

        if($payment_option_id == 1){
	      $company_payment1 = $company_payment+$company_commision;
	      $driver_payment1 = $driver_payment;
	      $company_amount = $company_commision;
	      }else{
            if ($total_payed_amount == "Paypal")
            {
                $total_payed_amount = $total_amount;
                $company_payment1 = $company_payment;
                $driver_payment1 = $driver_payment;
                $company_amount = "0.00";
            }else{
                $company_payment1 = ($company_payment+$company_commision)-$total_payed_amount;
                $driver_payment1 = $driver_payment+$driver_amount;
                $company_amount = $company_commision-$total_amount;
            }
	    }
             $total_payment_eraned = $total_payment_eraned+$driver_amount;
	     $query5="UPDATE driver SET total_payment_eraned='$total_payment_eraned',company_payment='$company_payment1',driver_payment='$driver_payment1' WHERE driver_id='$driver_id'" ;
         $db->query($query5);
        $query2="UPDATE done_ride SET  total_amount='$total_amount',company_commision='$company_commision',driver_amount='$driver_amount',coupan_price='$coupan_price' WHERE ride_id='$ride_id'";
         $db->query($query2);
        $query1="select * from driver_earnings where driver_id='$driver_id' AND date=CURDATE()";
	$result1 = $db->query($query1);
	$driver_earn=$result1->row;
	if(empty($driver_earn))
	{
	$date = date("Y-m-d");
	$query2="INSERT INTO driver_earnings(driver_id,rides,total_amount,amount,date,outstanding_amount) 
    VALUES('$driver_id','1','$total_payed_amount','$driver_amount','$date','$company_amount')";
	$db->query($query2);
	}else{
	         $driver_earning_id = $driver_earn['driver_earning_id'];
	         $rides = $driver_earn['rides']+1;
	         $amount = $driver_earn['amount']+$driver_amount;
             $total_amount = $driver_earn['total_amount']+$total_payed_amount;
             $company_amount = $driver_earn['outstanding_amount']+$company_amount;
		$query3="UPDATE driver_earnings SET outstanding_amount='$company_amount',rides='$rides',amount='$amount',total_amount='$total_amount' WHERE driver_earning_id='$driver_earning_id'";
		$db->query($query3);
	}

        $query5="select * from user_device where user_id='$user_id' AND login_logout=1";
        $result5 = $db->query($query5);
        $list5=$result5->rows;
        $language="select * from messages where language_id='$language_id' and message_id=28";
        $lang_result = $db->query($language);
        $lang_list=$lang_result->row;
        $message=$lang_list['message_name'];
        $done_ride= (String) $done_ride;
        $ride_status= (String) $ride_status;

        if (!empty($list5))
        {
            foreach ($list5 as $user)
            {
                $device_id = $user['device_id'];
                $flag = $user['flag'];
                if($flag == 1)
                {
                    IphonePushNotificationCustomer($device_id,$message,$done_ride,$ride_status,$pem_file);
                }
                else
                {
                    AndroidPushNotificationCustomer($device_id,$message,$done_ride,$ride_status);
                }
            }
        }else{
                $query5="select * from user where user_id='$user_id'";
                $result5 = $db->query($query5);
                $list5=$result5->row;
                $device_id=$list5['device_id'];
                if($device_id!="")
                {
                    if($list5['flag'] == 1)
                    {
                        IphonePushNotificationCustomer($device_id, $message,$done_ride,$ride_status,$pem_file);
                    }
                    else
                    {
                        AndroidPushNotificationCustomer($device_id, $message,$done_ride,$ride_status);
                    }
                }
        }



        $query4="select * from user where user_id='$user_id'";
        $result4 = $db->query($query4);
        $list41=$result4->rows;
        $user_phone = $list41['user_phone'];
        //ride_end_sms($user_phone);
        $query3="select * from done_ride where ride_id='$ride_id'";
        $result3 = $db->query($query3);
        $list=$result3->row;
        $list['payment_option_id']=$payment_option_id;
        $re = array('result'=> 1,'msg'=> $message,'details'	=> $list);

    }
    else {
        $re = array('result'=> 419,'msg'=> "No Record Found",);
    }
}
else
{
    $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>